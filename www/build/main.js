webpackJsonp([0],{

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return global; });
var global = Object.freeze({
    BASE_API_URL: "http://www.condominioapp.com/mobile/api/",
    BASE_API_UPLOAD: "http://www.condominioapp.com/uploads/",
    BASE_SIGNALR: "http://www.condominioapp.com/mobile/signalr/js",
    BASE_API_BOLETO: "http://techdog-001-site16.dtempurl.com/api/",
    EMAIL_REGEX: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    PHONE_REGEX: /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/,
    ERROR_MSG: "Serviço indisponível no momento.",
    FOTO_ANEXADA: "Foto anexada com sucesso!"
});
//# sourceMappingURL=globalVariables.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceHome; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__ = __webpack_require__(579);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_timeout__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the ServiceHome provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceHome = (function () {
    function ServiceHome(http) {
        this.http = http;
        this._baseWebUrl = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        // console.log('Hello ServiceHome Provider');
    }
    ServiceHome.prototype.getConfiguracaoCondominio = function (idCondominio) {
        var params = 'condominioId=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "Condominio/getCondominioConfig?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceHome.prototype.getMeteorologiaCloud = function () {
        var params = 'cid=BRXX0201';
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "externo/GetJsonTempo?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceHome.prototype.getPrioridadeHome = function (idUnidade) {
        var params = 'aptID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "apartamento/getPrioridadeAppIonic?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceHome.prototype.getNovaNoticia = function (idUnidade, condominioID, usuarioID) {
        var params = 'aptId=' + idUnidade + '&idCondominio=' + condominioID + "&idUsuario=" + usuarioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "Condominio/getNovaNoticias?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceHome.prototype.getVerificaVeiculo = function (idUnidade) {
        var params = 'aptID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "carro/verificarCarro?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceHome.prototype.isConnection = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseWebUrl + "values", options)
            .timeout(2500)
            .map(function (res) { return res.json(); });
    };
    return ServiceHome;
}());
ServiceHome = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceHome);

//# sourceMappingURL=ServiceHome.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return comunicadoOcorrencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabComunicados__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabOcorrencias__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabArquivosCategoria__ = __webpack_require__(388);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var comunicadoOcorrencia = (function (_super) {
    __extends(comunicadoOcorrencia, _super);
    function comunicadoOcorrencia(navCtrl, navParams, _storage) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._storage = _storage;
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
        // this.listarRecentes();
        _this.comunicadosTab = __WEBPACK_IMPORTED_MODULE_4__tabComunicados__["a" /* tabComunicados */];
        _this.ocorrenciasTab = __WEBPACK_IMPORTED_MODULE_5__tabOcorrencias__["a" /* tabOcorrencias */];
        _this.arquivosTab = __WEBPACK_IMPORTED_MODULE_6__tabArquivosCategoria__["a" /* tabArquivosCategoria */];
        return _this;
    }
    comunicadoOcorrencia.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    return comunicadoOcorrencia;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
comunicadoOcorrencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/comunicadoOcorrencia.html"*/'﻿<ion-header>\n  <ion-navbar color="topComunicado">\n    <ion-title>Comunicados | Ocorrências</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-tabs>\n  <ion-tab  tabTitle="Comunicados" tabIcon="text" [root]="comunicadosTab"></ion-tab>\n  <ion-tab  tabTitle="Ocorrências" tabIcon="book" [root]="ocorrenciasTab"></ion-tab>\n  <ion-tab  tabTitle="Arquivos" tabIcon="document" [root]="arquivosTab"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/comunicadoOcorrencia.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], comunicadoOcorrencia);

//# sourceMappingURL=comunicadoOcorrencia.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalComunicadoContent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceComunicado__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var modalComunicadoContent = (function (_super) {
    __extends(modalComunicadoContent, _super);
    function modalComunicadoContent(platform, params, navCtrl, viewCtrl, storage, comunicadoService, sp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.comunicadoService = comunicadoService;
        _this.sp = sp;
        _this.arrArquivos = [];
        _this.selectComunicadoID = _this.params.get('idDoComunicado');
        _this.comunicadoService.getSelectComunicado(_this.selectComunicadoID)
            .subscribe(function (comunicado) { return _this.comunicadoSelectObj = comunicado; }, function (error) { return console.log('Erro no get Comunicado: '); }, function () {
            _this.titulo = _this.comunicadoSelectObj.titulo;
            _this.tp = _this.comunicadoSelectObj.tp;
            _this.descricao = _this.comunicadoSelectObj.descricao;
            _this.strDataDeRealizacao = _this.comunicadoSelectObj.strDataDeRealizacao;
            _this.nomeUsuario = _this.comunicadoSelectObj.nomeUsuario;
            _this.publico = _this.comunicadoSelectObj.publico;
            for (var i = 0; i < _this.comunicadoSelectObj.listaDeArquivo.length; i++) {
                _this.arrArquivos.push(_this.urlArquivosUpload + 'comunicado/' + _this.comunicadoSelectObj.listaDeArquivo[i].condominioId + "/" + _this.comunicadoSelectObj.tp + "/" + _this.comunicadoSelectObj.listaDeArquivo[i].nome);
            }
            //console.log(JSON.stringify(this.arrArquivos));
        });
        return _this;
    }
    modalComunicadoContent.prototype.viewArquivo = function (strPath) {
        //alert(strPath);
        this.sp.openFileDocs(strPath);
    };
    modalComunicadoContent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalComunicadoContent;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
modalComunicadoContent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalComunicadoContent.html"*/'﻿<ion-header>\n  <ion-toolbar color="topComunicado">\n    <ion-title>\n      {{titulo}}\n    </ion-title>\n    <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="light" showWhen="ios">Voltar</span>\n          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content class="bgApp">\n\n  <ion-card>\n    <ion-card-header>\n      Categoria: {{tp}}\n    </ion-card-header>\n    <ion-card-content>\n     {{descricao}}\n      <br />\n      <p *ngFor="let arq of arrArquivos" (click)="viewArquivo(arq)" text-wrap class="linkDownload">\n        <ion-icon name="attach" item-start></ion-icon>\n        Visualizar arquivo\n      </p>\n      <br />\n      <p *ngIf="strDataDeRealizacao">Data de Realização: {{strDataDeRealizacao}}</p>\n      <p>\n        <span class="pub" *ngIf="publico"><ion-icon name="home"></ion-icon> Público</span>\n        <span class="priv" *ngIf="!publico"><ion-icon name="person"></ion-icon> Privado</span>\n      </p>\n      <p *ngIf="nomeUsuario"><ion-icon name="person"></ion-icon> Att: {{nomeUsuario}}</p>\n\n    </ion-card-content>\n  </ion-card>\n \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalComunicadoContent.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceComunicado__["a" /* ServiceComunicado */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */]])
], modalComunicadoContent);

//# sourceMappingURL=modalComunicadoContent.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return agendaReserva; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceAgenda__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__atividade__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__menuSuspenso__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__minhasReservas__ = __webpack_require__(393);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var agendaReserva = (function (_super) {
    __extends(agendaReserva, _super);
    function agendaReserva(navCtrl, navParams, _serviceAgenda, sp, _storage, popoverCtrl, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._serviceAgenda = _serviceAgenda;
        _this.sp = sp;
        _this._storage = _storage;
        _this.popoverCtrl = popoverCtrl;
        _this.modalCtrl = modalCtrl;
        _this.calendar = {
            mode: 'month',
            currentDate: new Date(),
            dateFormatter: {
                formatMonthViewDay: function (date) {
                    return date.getDate().toString();
                }
            }
        };
        _this.markDisabled = function (date) {
            var current = new Date();
            current.setHours(0, 0, 0);
            return date < current;
        };
        _this.arrEventosDoDia = [];
        _this.arrServMethod = [];
        _this.arrAreasComuns = [];
        return _this;
    }
    agendaReserva.prototype.ngOnInit = function () {
        this.arrServMethod = this.loadAgendaCondominio();
        this.getAreaComum();
    };
    agendaReserva.prototype.getAreaComum = function () {
        var _this = this;
        this._storage.get("UsuarioLogado").then(function (data) {
            _this._serviceAgenda.getAreaComumList(data.condominioId)
                .subscribe(function (areas) { return _this.arrAreasComuns = areas; }, function (error) { return console.log("error no get áreas comuns"); }, function () { });
        });
    };
    agendaReserva.prototype.openModalMinhasReservas = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__minhasReservas__["a" /* minhasReservas */]);
        modal.present();
    };
    agendaReserva.prototype.openModalAtividade = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__atividade__["a" /* atividade */]);
        modal.present();
    };
    agendaReserva.prototype.openMenuSuspenso = function (ev) {
        var popover = this.popoverCtrl.create(__WEBPACK_IMPORTED_MODULE_7__menuSuspenso__["a" /* menuSuspenso */], this.arrAreasComuns);
        popover.present({
            ev: ev
        });
    };
    agendaReserva.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.sp.showLoading("Carregando eventos...", 3000);
        setTimeout(function () {
            _this.eventSource = _this.arrServMethod;
        }, 2500);
    };
    agendaReserva.prototype.loadEvents = function () {
        //  console.log("Carregar...");
        //  this.eventSource = this.createRandomEvents();
        // this.eventSource = this.loadAgendaCondominio();
        this.eventSource = this.arrServMethod;
    };
    agendaReserva.prototype.onViewTitleChanged = function (title) {
        this.viewTitle = title;
    };
    // onEventSelected(event) {
    //     //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    //    // this.sp.showAlert("Agenda",event.title);
    // }
    agendaReserva.prototype.changeMode = function (mode) {
        this.calendar.mode = mode;
    };
    // today() {
    //     this.calendar.currentDate = new Date();
    // }
    agendaReserva.prototype.onTimeSelected = function (ev) {
        // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
        //     (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
        if (ev.events !== undefined && ev.events.length !== 0) {
            this.arrEventosDoDia = ev.events;
        }
        else {
            this.arrEventosDoDia = [];
        }
    };
    agendaReserva.prototype.onCurrentDateChanged = function (event) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    };
    // createRandomEvents() {
    //     var events = [];
    //     for (var i = 0; i < 50; i += 1) {
    //         var date = new Date();
    //         var eventType = Math.floor(Math.random() * 2);
    //         var startDay = Math.floor(Math.random() * 90) - 45;
    //         var endDay = Math.floor(Math.random() * 2) + startDay;
    //         var startTime;
    //         var endTime;
    //         if (eventType === 0) {
    //             startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
    //             if (endDay === startDay) {
    //                 endDay += 1;
    //             }
    //             endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
    //             events.push({
    //                 title: 'All Day - ' + i,
    //                 startTime: startTime,
    //                 endTime: endTime,
    //                 allDay: true
    //             });
    //         } else {
    //             var startMinute = Math.floor(Math.random() * 24 * 60);
    //             var endMinute = Math.floor(Math.random() * 180) + startMinute;
    //             startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
    //             endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
    //             events.push({
    //                 title: 'Event - ' + i,
    //                 startTime: startTime,
    //                 endTime: endTime,
    //                 allDay: false
    //             });
    //         }
    //     }
    //     return events;
    // }
    agendaReserva.prototype.loadAgendaCondominio = function () {
        var _this = this;
        var eventsAgenda = [];
        var events = [];
        this._storage.get("UsuarioLogado").then(function (data) {
            _this._serviceAgenda.getAgendaCondominio(data.aptId)
                .subscribe(function (eventos) { return eventsAgenda = eventos; }, function (error) { return console.log("Falha ao carregar a agenda"); }, function () {
                for (var i = 0; i < eventsAgenda.length; i++) {
                    var arrDt = eventsAgenda[i].dataDeRealizacao.split("T");
                    var arrDt2 = arrDt[0].split("-");
                    var t = new Date(arrDt2[0] + "/" + arrDt2[1] + "/" + arrDt2[2]);
                    events.push({
                        title: arrDt2[2] + "/" + arrDt2[1] + "/" + arrDt2[0] + "  -  " + eventsAgenda[i].descricao,
                        startTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                        endTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                        allDay: false
                    });
                }
                //console.log(JSON.stringify(events));
            });
        });
        return events;
    };
    agendaReserva.prototype.onRangeChanged = function (ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    };
    return agendaReserva;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], agendaReserva.prototype, "content", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], agendaReserva.prototype, "text", void 0);
agendaReserva = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-agendaReserva",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/agendaReserva.html"*/'<ion-header>\n  <ion-navbar color="topAgendaReserva">\n    <ion-title>Agenda | Reserva</ion-title>\n    <ion-buttons end>\n      <button ion-button icon-only color="royal" (click)="openModalMinhasReservas()">\n        <ion-icon name="more"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="bgApp">\n  <ion-toolbar color="subAgendaReserva">\n      <ion-row>\n          <ion-col>\n              <button ion-button color="light" clear block small  class="text-on-bottom">\n                <ion-icon name="calendar" class="sizeIcon"></ion-icon>\n                <label class="sizeFont">Agenda</label></button>       \n          </ion-col>\n          <ion-col>\n          <button ion-button color="light" clear block small class="text-on-bottom" (click)="openModalAtividade()">\n             <ion-icon name="bicycle" class="sizeIcon"></ion-icon>\n             <label class="sizeFont">Atividades</label>\n          </button>            \n          </ion-col>\n          <ion-col center text-center>\n              <button ion-button color="light" clear block small class="text-on-bottom" (click)="openMenuSuspenso($event)">\n                <ion-icon name="timer" class="sizeIcon"></ion-icon>\n                <label class="sizeFont">Reservas</label>\n              </button> \n          </ion-col>\n        </ion-row>\n  </ion-toolbar>\n  \n  <ion-toolbar color="subAgendaReserva">\n      <ion-row>\n          <ion-col width-100 style="text-align:center;color:#fff">Agenda do condomínio - {{viewTitle}}</ion-col>\n      </ion-row>  \n  </ion-toolbar>\n\n  <ion-toolbar>\n    <ion-row>\n        <ion-col width-100 style="text-align:center">\n            <ion-icon name="calendar" color="topAgendaReserva"></ion-icon> Toque nas datas marcadas e descubra as atividades agendadas para o dia.\n        </ion-col>\n    </ion-row>  \n  </ion-toolbar>\n       \n   <ng-template #template let-showEventDetail="showEventDetail" let-selectedDate="selectedDate" let-noEventsLabel="noEventsLabel">\n      <ion-list>\n        <ion-item text-wrap *ngFor="let evento of arrEventosDoDia" color="bg">\n          <span class="sizeFont">{{evento.title}}</span>\n          <ion-icon name="calendar" item-end item-left large color="topAgendaReserva"></ion-icon>\n        </ion-item>\n    </ion-list> \n  </ng-template>\n\n    <calendar [eventSource]="eventSource"\n              [calendarMode]="calendar.mode"\n              [currentDate]="calendar.currentDate"\n              [monthviewEventDetailTemplate]="template"\n              [dateFormatter]="calendar.dateFormatter"\n              (onCurrentDateChanged)="onCurrentDateChanged($event)"\n              (onEventSelected)="onEventSelected($event)"\n              (onTitleChanged)="onViewTitleChanged($event)"\n              (onTimeSelected)="onTimeSelected($event)"\n              step="30"\n              noEventsLabel="Sem eventos para este dia">\n    </calendar>\n  \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/agendaReserva.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceAgenda__["a" /* ServiceAgenda */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], agendaReserva);

//# sourceMappingURL=agendaReserva.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return correspondencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceCorrespondencia__ = __webpack_require__(394);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var correspondencia = (function (_super) {
    __extends(correspondencia, _super);
    function correspondencia(navCtrl, navParams, _storage, serviceCorreio, sp) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._storage = _storage;
        _this.serviceCorreio = serviceCorreio;
        _this.sp = sp;
        _this.correspondenciaList = [];
        return _this;
    }
    correspondencia.prototype.ionViewDidLoad = function () {
    };
    correspondencia.prototype.ngOnInit = function () {
        var _this = this;
        this.sp.showLoader("Carregando correspondências...");
        this._storage.get("UsuarioLogado").then(function (dataUser) {
            _this.serviceCorreio.ListarHistorico(dataUser.aptId)
                .subscribe(function (correspondencias) { return _this.correspondenciaList = correspondencias; }, function (error) { return console.log("falha em obter correspondencias"); }, function () {
                _this.sp.dismissLoader();
                setTimeout(function () { return _this.setVistoDeCorrespondencia(); }, 3500);
            });
        });
    };
    correspondencia.prototype.openModel = function () {
        this.sp.showAlert("Correspondência", "Existe uma nova correspondência para sua unidade, não se esqueça de retirar.");
    };
    correspondencia.prototype.setVistoDeCorrespondencia = function () {
        var _this = this;
        var ret;
        this._storage.get("UsuarioLogado").then(function (dataUser) {
            _this.serviceCorreio.setVisto(dataUser.aptId)
                .subscribe(function (r) { return ret = r; }, function (error) { return console.log("falha em set visto"); }, function () { });
        });
    };
    correspondencia.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.setVistoDeCorrespondencia();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return correspondencia;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
correspondencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-correspondencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/correspondencia/correspondencia.html"*/'<!--\n  Generated template for the NewPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="topCorrespondencia">\n    <ion-title>Correspondências</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="bgApp">\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n  <!-- <ion-list>\n     <ion-item text-wrap *ngFor="let correio of correspondenciaList">\n      <span class="dataDisplay"> {{correio.strDataDeCadastro}}</span> - Correspondência recebida\n      <ion-icon name="mail-open" item-end item-right large *ngIf="correio.visto"></ion-icon>\n      <ion-icon name="mail" item-end item-right large *ngIf="!correio.visto"></ion-icon>\n    </ion-item> \n  </ion-list> -->\n\n  <ion-card *ngFor="let correio of correspondenciaList">\n      <ion-card-content>\n        <ion-item *ngIf="!correio.visto" (click)="openModel()"> \n            <span class="colorTopCorrespondencia">{{correio.strDataDeCadastro}}</span><br />\n            <ion-icon iten-end name="mail" item-end item-right large *ngIf="!correio.visto" color="topCorrespondencia"></ion-icon>\n            <span class="fontSize">Aviso de correspondência.</span>\n        </ion-item>\n\n        <ion-item *ngIf="correio.visto"> \n            <span>{{correio.strDataDeCadastro}}</span><br />\n            <ion-icon iten-end name="mail-open" item-end item-right large></ion-icon>\n            <span class="fontSize">Aviso de correspondência.</span>\n        </ion-item>\n        \n      </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/correspondencia/correspondencia.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceCorrespondencia__["a" /* ServiceCorrespondencia */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */]])
], correspondencia);

//# sourceMappingURL=correspondencia.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return portaria; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabVisitas__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabTemporada__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabRecados__ = __webpack_require__(404);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var portaria = (function (_super) {
    __extends(portaria, _super);
    function portaria(storage, navCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.visitasTab = __WEBPACK_IMPORTED_MODULE_4__tabVisitas__["a" /* tabVisitas */];
        _this.temporadasTab = __WEBPACK_IMPORTED_MODULE_5__tabTemporada__["a" /* tabTemporada */];
        _this.recadosTab = __WEBPACK_IMPORTED_MODULE_6__tabRecados__["a" /* tabRecados */];
        return _this;
    }
    return portaria;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
portaria = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'portaria-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/portaria.html"*/'<ion-header>\n\n  <ion-navbar color="topPortaria">\n    <ion-title>Portaria</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-tabs>\n  <ion-tab tabTitle="Visitas" tabIcon="people" [root]="visitasTab"></ion-tab>\n  <ion-tab tabTitle="Temporada" tabIcon="globe" [root]="temporadasTab"></ion-tab>\n  <ion-tab tabTitle="Recados" tabIcon="mail" [root]="recadosTab"></ion-tab> \n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/portaria.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */]])
], portaria);

//# sourceMappingURL=portaria.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalAutorizacaoEntrada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalAutorizacaoEntrada = (function (_super) {
    __extends(modalAutorizacaoEntrada, _super);
    function modalAutorizacaoEntrada(storage, navCtrl, param, servVisita, sp, signalr, viewCtrl, modalCtrl, fb) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.param = param;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.signalr = signalr;
        _this.viewCtrl = viewCtrl;
        _this.modalCtrl = modalCtrl;
        _this.fb = fb;
        _this.ctVisitaTemp = false;
        _this.currentDateCadastro = (new Date()).toISOString();
        _this.currentDateEntrada = (new Date()).toISOString();
        _this.currentDateSaida = (new Date()).toISOString();
        _this.visitante = _this.param.data;
        //console.log(JSON.stringify(this.visitante)); 
        //console.log(this.visitante);
        _this.currentDate = (new Date()).toISOString();
        var dataFutura = new Date();
        dataFutura = _this.addDays(dataFutura, 30);
        _this.dataLimite = dataFutura.toISOString();
        var arrDataMin = _this.currentDate.split('T');
        _this.initmoth = arrDataMin[0];
        _this.form = fb.group({
            visitanteId: [_this.visitante.visitanteID],
            aprovado: [true],
            ctVisitaTemporaria: [false],
            dataDeCadastro: [""],
            dataInicioPermanente: [""],
            dataFimPermanente: [""],
            visitaPermanente: [false],
            visitaCarro: [false],
            descricao: [""],
        });
        return _this;
    }
    modalAutorizacaoEntrada.prototype.addDays = function (date, days) {
        date.setDate(date.getDate() + days);
        return date;
    };
    modalAutorizacaoEntrada.prototype.ngOnInit = function () {
    };
    modalAutorizacaoEntrada.prototype.adicionarVisita = function () {
        var _this = this;
        this.sp.showLoader("Criando nova visita");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.form.addControl("apartamentoID", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.aptId));
            _this.form.addControl("usuarioLogadoId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.userID));
            var novaVisita;
            novaVisita = _this.form.value;
            // console.log(novaVisita);
            var novaVisitaModal = {
                visitanteId: novaVisita.visitanteId,
                aprovado: novaVisita.aprovado,
                dataDeCadastro: novaVisita.dataDeCadastro,
                dataInicioPermanente: "",
                dataFimPermanente: "",
                visitaPermanente: novaVisita.visitaPermanente,
                descricao: novaVisita.descricao,
                visitaCarro: novaVisita.visitaCarro,
                apartamentoID: novaVisita.apartamentoID,
                usuarioLogadoId: novaVisita.usuarioLogadoId
            };
            if (novaVisita.ctVisitaTemporaria) {
                novaVisitaModal.dataDeCadastro = novaVisita.dataInicioPermanente;
                novaVisitaModal.dataInicioPermanente = novaVisita.dataInicioPermanente;
                novaVisitaModal.dataFimPermanente = novaVisita.dataFimPermanente;
            }
            //console.log(novaVisitaModal);
            var result;
            _this.servVisita.novaVisita(novaVisitaModal)
                .subscribe(function (s) { return result = s; }, function (error) { return console.log("Error ao postar nova visita"); }, function () {
                if (result.status == 0) {
                    _this.form.reset();
                    _this.sp.showAviso(result.mensagem);
                    _this.signalr.connect().then(function (cnx) {
                        cnx.invoke("SendAutorizacaoVisita", novaVisitaModal.visitanteId, dataUser.condominioId).then(function (res) { return console.log("Pre autorização de visita enviada!"); });
                    });
                    _this.dismiss();
                }
                if (result.status == 1) {
                    _this.sp.showAviso(result.mensagem);
                }
                if (result.status == -1) {
                    _this.sp.showAviso(_this.errorMensage);
                }
                _this.sp.dismissLoader();
            });
        });
    };
    modalAutorizacaoEntrada.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalAutorizacaoEntrada;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalAutorizacaoEntrada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalAutorizacaoEntrada-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizacaoEntrada.html"*/'<ion-header>\n        <ion-toolbar color="topPortaria">\n          <ion-title>\n              <ion-icon name="person"></ion-icon> Autorizar visita\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n\n<ion-content>\n<ion-card>  \n    \n    <img src=\'{{urlImageServ}}usuario/{{visitante.foto}}\' *ngIf="visitante.foto">\n\n    <ion-card-content>\n        <p>{{visitante.nome}}</p>\n    </ion-card-content>\n\n    <form [formGroup]="form" (submit)="adicionarVisita()"> \n            \n             <ion-list no-lines>    \n                <ion-item *ngIf="!ctVisitaTemp" color="topPortaria">\n                   <ion-label class="txtLabel">Visita para: </ion-label>\n                   <ion-datetime displayFormat="DD/MMM/YYYY" min={{initmoth}} [(ngModel)]="currentDateCadastro"  formControlName="dataDeCadastro" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <ion-item>\n                    <ion-label>Por período</ion-label>\n                    <ion-checkbox [(ngModel)]="ctVisitaTemp" formControlName="ctVisitaTemporaria"></ion-checkbox>\n                </ion-item>\n\n                <ion-item-divider color="light" *ngIf="ctVisitaTemporaria">Autorização temporária</ion-item-divider>\n                <ion-item *ngIf="ctVisitaTemp">\n                     <ion-label>De: </ion-label>\n                     <ion-datetime displayFormat="DD/MMM/YYYY" max={{dataLimite}} min={{initmoth}} [(ngModel)]="currentDateEntrada"  formControlName="dataInicioPermanente" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <ion-item *ngIf="ctVisitaTemp">\n                    <ion-label>Até: </ion-label>\n                    <ion-datetime displayFormat="DD/MMM/YYYY" max={{dataLimite}} min={{initmoth}} [(ngModel)]="currentDateSaida"  formControlName="dataFimPermanente" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <ion-item *ngIf="!ctVisitaTemp">\n                   <ion-label>Fixo </ion-label>\n                   <ion-checkbox  formControlName="visitaPermanente"></ion-checkbox>\n                </ion-item>\n\n                <ion-item >\n                    <ion-label>Visitante possui veículo </ion-label>\n                    <ion-checkbox  formControlName="visitaCarro"></ion-checkbox>\n                </ion-item>\n              \n                <ion-item>\n                    <ion-label stacked>Descrição</ion-label>\n                    <ion-textarea placeholder="Descreva uma observação sobre esta visita" formControlName="descricao" rows="5"></ion-textarea>\n                </ion-item>\n         \n                 <ion-item>\n                    <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Criar visita</button>\n                 </ion-item>  \n             </ion-list>\n         \n            </form> \n\n</ion-card>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizacaoEntrada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], modalAutorizacaoEntrada);

//# sourceMappingURL=modalAutorizacaoEntrada.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mural; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceMural__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalNovoPost__ = __webpack_require__(406);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var mural = (function (_super) {
    __extends(mural, _super);
    function mural(storage, navCtrl, servMural, suporte, modal, alertCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servMural = servMural;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.listaDePosts = [];
        _this.storage.get("objCondominio").then(function (dataCondominio) {
            _this.permitePost = dataCondominio.mural;
        });
        _this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.userID = dataUser.userID;
        });
        return _this;
    }
    mural.prototype.permitirDeletar = function (userIDpost) {
        if (this.userID == userIDpost)
            return true;
        return false;
    };
    mural.prototype.excluirPost = function (idPost) {
        this.confirmRemoverPost("Excluir postagem", "Tem certeza que deseja excluir este post?", idPost);
    };
    mural.prototype.confirmRemoverPost = function (titulo, msg, postID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servMural.removerPost(postID)
                            .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao remover Post!"); }, function () {
                            console.log(retorno);
                            if (retorno.status == 0) {
                                _this.suporte.showAviso(retorno.mensagem);
                                _this.listarPosts();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    mural.prototype.ionViewDidLoad = function () {
        // this.content.scrollToBottom(300);
    };
    mural.prototype.ngOnInit = function () {
        this.suporte.showLoader("Carregando mural...");
        this.listarPosts();
    };
    mural.prototype.listarPosts = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servMural.listarMural(dataUser.condominioId)
                .subscribe(function (posts) { return _this.listaDePosts = posts; }, function (error) { return console.log("Error ao obter a lista de recados"); }, function () {
                _this.suporte.dismissLoader();
                _this.ultimoID = _this.listaDePosts[_this.listaDePosts.length - 1].id;
            });
        });
    };
    mural.prototype.openModalNovoPost = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_6__modalNovoPost__["a" /* modalNovoPost */]);
        m.present();
    };
    mural.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.listarPosts();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    mural.prototype.doInfinite = function (infiniteScroll) {
        // console.log('Begin async operation');
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servMural.getMaisPosts(dataUser.condominioId, _this.ultimoID)
                .subscribe(function (maisPosts) { return _this.listMorePost = maisPosts; }, function (error) { return console.log("Error ao puxar mais Posts"); }, function () {
                for (var i = 0; i < _this.listMorePost.length - 1; i++) {
                    //console.log(this.listMorePost[i]);
                    _this.listaDePosts.push(_this.listMorePost[i]);
                    _this.ultimoID = _this.listMorePost[i].id;
                }
            });
        });
        setTimeout(function () {
            infiniteScroll.complete();
        }, 2500);
    };
    return mural;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
mural = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'mural-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/mural/mural.html"*/'<ion-header>     \n    <ion-navbar color="topMural">\n    <ion-title>Mural</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bgApp">\n        \n        <ion-refresher (ionRefresh)="doRefresh($event)">\n           <ion-refresher-content></ion-refresher-content>\n        </ion-refresher>\n\n\n        <ion-card *ngFor="let post of listaDePosts">\n                \n            <ion-item>\n                <ion-avatar item-start *ngIf="post.userFoto">\n                    <img src=\'{{urlImageServ}}usuario/{{post.userFoto}}\'>\n                </ion-avatar>\n                <h2>{{post.userNome}}</h2>\n                <p>{{post.strDataDeCadastro}}</p>\n            </ion-item>\n        \n            <img src=\'{{urlImageServ}}post/{{post.img}}\' *ngIf="post.img">\n        \n            <ion-card-content>\n                <p>{{post.titulo}}</p>\n                <p>{{post.conteudo}}</p>\n            </ion-card-content>\n\n            <ion-row>\n                <ion-col>\n                  <button ion-button icon-left clear small *ngIf="permitirDeletar(post.usuarioId)" (click)="excluirPost(post.id)">\n                    <ion-icon name="trash"></ion-icon>\n                    <div>Excluir</div>\n                  </button>\n                </ion-col>\n                <ion-col>\n                  <!-- <button ion-button icon-left clear small>\n                    <ion-icon name="text"></ion-icon>\n                    <div>4 Comments</div>\n                  </button> -->\n                </ion-col>\n                <ion-col center text-center>\n                  <!-- <ion-note>\n                    11h ago\n                  </ion-note> -->\n                </ion-col>\n            </ion-row>\n        \n        </ion-card>\n\n        <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n             <ion-infinite-scroll-content></ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n\n        <ion-fab right bottom *ngIf="permitePost">\n            <button ion-fab color="topMural" (click)="openModalNovoPost()"><ion-icon name="add"></ion-icon></button>\n            <!--<ion-fab-list side="left">\n                <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n            </ion-fab-list>-->\n        </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/mural/mural.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceMural__["a" /* ServiceMural */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], mural);

//# sourceMappingURL=mural.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceMural; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceMural = (function () {
    function ServiceMural(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        // console.log('Hello ServiceMural Provider');
    }
    ServiceMural.prototype.listarMural = function (idCondominio) {
        var params = 'condominioID=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Mural/getPostMural?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceMural.prototype.getMaisPosts = function (idCondominio, uIDPost) {
        var params = 'condominioID=' + idCondominio + '&postLastId=' + uIDPost;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Mural/getPostMuralMais?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceMural.prototype.AddNovoPost = function (oPost) {
        // console.log(oPost);
        var params = JSON.stringify(oPost);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Mural/NovoPost', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceMural.prototype.removerPost = function (idPost) {
        var params = 'postId=' + idPost;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Mural/DeletePost?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceMural;
}());
ServiceMural = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceMural);

//# sourceMappingURL=ServiceMural.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return classificado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabClassificado__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabMeusAnuncios__ = __webpack_require__(408);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var classificado = (function (_super) {
    __extends(classificado, _super);
    function classificado(navCtrl, _storage) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
        // this.listarRecentes();
        _this.classificadoTab = __WEBPACK_IMPORTED_MODULE_4__tabClassificado__["a" /* tabClassificado */];
        _this.meusAnunciosTab = __WEBPACK_IMPORTED_MODULE_5__tabMeusAnuncios__["a" /* tabMeusAnuncios */];
        return _this;
    }
    classificado.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    return classificado;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
classificado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'classificado-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/classificado.html"*/'<ion-header>\n    <ion-navbar color="topClassificado">\n        <ion-title>Classificados</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-tabs>\n    <ion-tab tabTitle="Classificados" tabIcon="megaphone" [root]="classificadoTab"></ion-tab>\n    <ion-tab tabTitle="Meu Anúncios" tabIcon="menu" [root]="meusAnunciosTab"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/classificado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], classificado);

//# sourceMappingURL=classificado.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovoClassificado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceClassificado__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var modalNovoClassificado = (function (_super) {
    __extends(modalNovoClassificado, _super);
    function modalNovoClassificado(platform, params, navCtrl, viewCtrl, storage, servClassificado, sp, fb, camera, fileUp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.servClassificado = servClassificado;
        _this.sp = sp;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.form = fb.group({
            str2Preco: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].compose([__WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern("^(([0-9]*)|(([0-9]*)\,([0-9]*)))$")])],
            chamada: [''],
            descricao: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    modalNovoClassificado.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            _this.form.addControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'Classificado');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    modalNovoClassificado.prototype.novoAnuncio = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("usuarioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.userID));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.condominioId));
            if (!_this.form.contains("foto"))
                _this.form.addControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]());
            var oAnuncio = _this.form.value;
            console.log(oAnuncio);
            _this.servClassificado.addNovoClassificado(oAnuncio)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao cadastrar"); }, function () {
                if (result.status == 0) {
                    _this.sp.showAviso(result.mensagem);
                    _this.dismiss();
                }
            });
        });
    };
    modalNovoClassificado.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovoClassificado;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalNovoClassificado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalNovoClassificado-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/modalNovoClassificado.html"*/'<ion-header>\n        <ion-toolbar color="topClassificado">\n          <ion-title>\n            Novo anúncio - Classificado\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n            \n      <ion-content>\n        <form [formGroup]="form" (submit)="novoAnuncio()">\n         \n          <ion-list no-lines>\n            <ion-item>\n                <ion-label stacked>Título:</ion-label>\n                <ion-input formControlName="chamada"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <ion-label stacked>Descrição:</ion-label>\n              <ion-textarea placeholder="Conteúdo do anúncio" formControlName="descricao" rows="5"></ion-textarea>\n            </ion-item>\n               \n            <ion-item>\n                <ion-label stacked>Preço:</ion-label>\n                <ion-input formControlName="str2Preco"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <button ion-button round padding type="button" color="topClassificado" (click)="takePic(\'camera\')">Câmera</button>\n              <button ion-button round padding type="button" color="topClassificado"(click)="takePic(\'galeria\')">Galeria</button>\n            </ion-item>\n      \n            <ion-item *ngIf="pathPreviewImage">\n                <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n            </ion-item>\n      \n            <ion-item>\n                 <button ion-button block padding type="submit" color="topClassificado" [disabled]="!form.valid">Criar anúncio</button>\n            </ion-item>\n      \n          </ion-list>\n      \n        </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/modalNovoClassificado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceClassificado__["a" /* ServiceClassificado */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__["a" /* Filetransferencia */]])
], modalNovoClassificado);

//# sourceMappingURL=modalNovoClassificado.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return enquete; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__enqueteDetails__ = __webpack_require__(410);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var enquete = (function (_super) {
    __extends(enquete, _super);
    function enquete(storage, navCtrl, servEnquete, suporte, modal) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servEnquete = servEnquete;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.listaDeEnquetes = [];
        return _this;
    }
    enquete.prototype.ionViewDidLoad = function () {
    };
    enquete.prototype.ngOnInit = function () {
        this.listarEnquetes();
    };
    enquete.prototype.listarEnquetes = function () {
        var _this = this;
        this.suporte.showLoader("Carregando enquetes...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servEnquete.listarEnquete(dataUser.condominioId)
                .subscribe(function (enquetes) { return _this.listaDeEnquetes = enquetes; }, function (error) { return console.log("Error ao obter a lista de Enquetes"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    enquete.prototype.detalhesEnquetes = function (oEnquete) {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_6__enqueteDetails__["a" /* enqueteDetails */], oEnquete);
        m.present();
    };
    enquete.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.listarEnquetes();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return enquete;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
enquete = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'enquete-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enquete.html"*/'<ion-header>     \n   <ion-navbar color="topEnquete">\n     <ion-title>Enquetes</ion-title>\n   </ion-navbar>\n</ion-header>\n  \n<ion-content class="bgApp">          \n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n    <ion-card *ngFor="let enquete of listaDeEnquetes" (click)="detalhesEnquetes(enquete)">\n       <ion-card-content>\n            <span> {{enquete.descricao}}</span>\n            <p> <ion-icon name="calendar"></ion-icon> <strong>Data início:</strong> {{enquete.dataInicial}} <br />\n                <ion-icon name="calendar"></ion-icon> <strong>Data de Término:</strong> {{enquete.dataFinal}} <br /> \n                <strong>Nº Votos:</strong> {{enquete.qtdVotos}}</p>\n       </ion-card-content>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enquete.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__["a" /* ServiceEnquete */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], enquete);

//# sourceMappingURL=enquete.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return vaga; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vaga_tabClassificadoVagas__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__vaga_tabVeiculos__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__vaga_tabVagas__ = __webpack_require__(414);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var vaga = (function (_super) {
    __extends(vaga, _super);
    function vaga(navCtrl, navParams, _storage) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._storage = _storage;
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
        // this.listarRecentes();
        _this.classificadoTab = __WEBPACK_IMPORTED_MODULE_4__vaga_tabClassificadoVagas__["a" /* tabClassificadoVagas */];
        _this.veiculosTab = __WEBPACK_IMPORTED_MODULE_5__vaga_tabVeiculos__["a" /* tabVeiculos */];
        _this.ctrlVagasTab = __WEBPACK_IMPORTED_MODULE_6__vaga_tabVagas__["a" /* tabVagas */];
        return _this;
    }
    vaga.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    return vaga;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
vaga = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-vaga',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/vaga.html"*/'<ion-header>\n    <ion-navbar color="dark">\n      <ion-title>Vagas</ion-title>\n    </ion-navbar>\n  </ion-header>\n  <ion-tabs>\n    <ion-tab tabTitle="Classificado" tabIcon="text" [root]="classificadoTab"></ion-tab>\n    <ion-tab tabTitle="Veículos" tabIcon="car" [root]="veiculosTab"></ion-tab>\n    <ion-tab tabTitle="Controle de Vagas" tabIcon="swap" [root]="ctrlVagasTab"></ion-tab>\n  </ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/vaga.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], vaga);

//# sourceMappingURL=vaga.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceVaga; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceVaga = (function () {
    function ServiceVaga(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceVaga');
    }
    ServiceVaga.prototype.ListarVagasDisponibilizadas = function (idCondominio) {
        var params = 'condominio_ID=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "condominio/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVaga.prototype.ListarMinhasVagas = function (apartamentoID) {
        var params = 'Id_apt=' + apartamentoID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVaga.prototype.enviarSolicitacao = function (aptIdFonte, aptIdDest, idUsuarioSolicitante) {
        var params = "aptFonte=" + aptIdFonte + "&aptDest=" + aptIdDest + "&solicitanteID=" + idUsuarioSolicitante;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVaga.prototype.disponibilizarVaga = function (unidadeID) {
        var params = 'Id_apartamento=' + unidadeID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVaga.prototype.retomarVaga = function (idDaVaga) {
        var params = 'vagaID=' + idDaVaga;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/retomarVagaDisponibilizada?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceVaga;
}());
ServiceVaga = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceVaga);

//# sourceMappingURL=ServiceVaga.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalChatVaga; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__chat_mensagens__ = __webpack_require__(56);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var modalChatVaga = (function (_super) {
    __extends(modalChatVaga, _super);
    function modalChatVaga(storage, navCtrl, params, servChat, suporte, modal, alertCtrl, viewCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.params = params;
        _this.servChat = servChat;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.viewCtrl = viewCtrl;
        _this.listaDeUsuariosChat = [];
        _this.unidadeId = _this.params.get("idAptId");
        _this.userId = _this.params.get("usuarioID");
        return _this;
    }
    modalChatVaga.prototype.ionViewDidLoad = function () {
    };
    modalChatVaga.prototype.ngOnInit = function () {
        this.getUsuariosClientesDaUnidade();
    };
    modalChatVaga.prototype.getUsuariosClientesDaUnidade = function () {
        var _this = this;
        this.suporte.showLoader("Carregando usuários...");
        this.servChat.listarClientesDaUnidade(this.unidadeId, this.userId)
            .subscribe(function (arr) { return _this.listaDeUsuariosChat = arr; }, function (error) { return console.log("Error ao obter a lista de Usuario Chat"); }, function () {
            console.log(_this.listaDeUsuariosChat);
            _this.suporte.dismissLoader();
        });
    };
    modalChatVaga.prototype.iniciarConversa = function (convidadoId) {
        var _this = this;
        var retorno;
        this.suporte.showLoader("Iniciando conversa...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.servChat.iniciarConversa(dataUser.userID, convidadoId)
                    .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao iniciar a conversa"); }, function () {
                    _this.suporte.dismissLoader();
                    var conversa;
                    _this.servChat.findConversa(retorno.status, dataUser.userID)
                        .subscribe(function (c) { return conversa = c; }, function (error) { return console.log("Error ao obter conversa"); }, function () {
                        // console.log(conversa);
                        var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_6__chat_mensagens__["a" /* mensagens */], conversa);
                        m.present();
                    });
                });
            }
        });
    };
    modalChatVaga.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalChatVaga;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
modalChatVaga = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalChatVaga.html"*/'<ion-header>\n        <ion-toolbar color="primary">\n          <ion-title>\n              <ion-icon name="chatbubbles"></ion-icon> Chat\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n      \n      <ion-content>     \n          <ion-list>\n              <ion-item text-wrap *ngFor="let usuario of listaDeUsuariosChat" (click)="iniciarConversa(usuario.id)">\n                  <ion-label> \n                      <ion-icon name="person"></ion-icon> {{usuario.nome}} {{usuario.sobrenome}}\n                      <!-- <p *ngFor="let  of usuario.diasDaSemana">\n                           {{dia}} - {{ativ.horaInicio}}\n                      </p>  -->\n                  </ion-label>\n                  <ion-icon name="ios-arrow-forward" item-end item-right small></ion-icon>\n              </ion-item>  \n          </ion-list>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalChatVaga.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceChat__["a" /* ServiceChat */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */]])
], modalChatVaga);

//# sourceMappingURL=modalChatVaga.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return conversas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__mensagens__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__contatos__ = __webpack_require__(420);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var conversas = (function (_super) {
    __extends(conversas, _super);
    function conversas(storage, navCtrl, suporte, modal, alertCtrl, servChat) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servChat = servChat;
        _this.listaDeConversas = [];
        return _this;
    }
    conversas.prototype.ngOnInit = function () {
        this.getConversas();
    };
    conversas.prototype.addNovaConversa = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_7__contatos__["a" /* contatos */]);
        m.present();
    };
    conversas.prototype.getConversas = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.suporte.showLoader("Carregando conversas...");
                _this.servChat.listarConversas(dataUser.userID)
                    .subscribe(function (conv) { return _this.listaDeConversas = conv; }, function (error) { return console.log("Error ao obter as conversas"); }, function () { return _this.suporte.dismissLoader(); });
            }
        });
    };
    conversas.prototype.openConversa = function (objConversa) {
        //console.log(JSON.stringify(objConversa));
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (objConversa.block && objConversa.removeUsuarioId != dataUser.userID) {
                _this.suporte.showAviso("Não é possível iniciar essa conversa.");
            }
            else {
                var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_6__mensagens__["a" /* mensagens */], objConversa);
                m.present();
            }
        });
    };
    conversas.prototype.bloquearConversa = function (conversaID) {
        this.confirmBloquearConversa("Bloquear usuário", "Tem certeza que deseja bloquear este usuário?", conversaID);
    };
    conversas.prototype.desbloquearConversa = function (conversaID) {
        this.confirmBloquearConversa("Desbloquear usuário", "Tem certeza que deseja desbloquear este usuário?", conversaID);
    };
    conversas.prototype.confirmBloquearConversa = function (titulo, msg, idConversa) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.storage.get("UsuarioLogado").then(function (dataUser) {
                            _this.servChat.bloquearConversa(idConversa, dataUser.userID)
                                .subscribe(function (r) { return retorno = r; }, function (error) { return console.log("Erro ao deletar veiculo"); }, function () {
                                if (retorno.status != -1) {
                                    _this.suporte.showAviso(retorno.mensagem);
                                    _this.getConversas();
                                }
                                else {
                                    _this.suporte.showAviso(_this.errorMensage);
                                }
                            });
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    conversas.prototype.removerConversa = function (id) {
        this.confirmRemoverConversa("Remover conversa", "Tem certeza que deseja remover a conversa?", id);
    };
    conversas.prototype.confirmRemoverConversa = function (titulo, msg, idConversa) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servChat.removerConversa(idConversa)
                            .subscribe(function (r) { return retorno = r; }, function (error) { return console.log("Erro ao deletar veiculo"); }, function () {
                            if (retorno.status == 0) {
                                _this.suporte.showAviso(retorno.mensagem);
                                _this.getConversas();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    conversas.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getConversas();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 1500);
    };
    return conversas;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
conversas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'conversas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/conversas.html"*/'<ion-header>\n    \n      <ion-navbar color="primary">\n        <ion-title> Minhas conversas</ion-title>\n      </ion-navbar>\n    \n    </ion-header>\n    \n    \n    <ion-content>\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n            <ion-refresher-content></ion-refresher-content>\n         </ion-refresher>\n\n         <ion-toolbar color="subChat">\n            <ion-row>\n                <ion-col col-12 style="text-align:center;color:#fff">Deslize para a esquerda para mais opções.</ion-col>\n            </ion-row>  \n        </ion-toolbar>\n\n        <ion-list>\n           \n           <ion-item-sliding *ngFor="let conversa of listaDeConversas">\n\n              <ion-item text-wrap>\n\n                  <ion-avatar item-start>\n                      <img src=\'{{urlImageServ}}usuario/{{conversa.foto}}\' *ngIf="conversa.foto">\n                      <img src="assets/img/no_image.png" *ngIf="!conversa.foto">\n                  </ion-avatar>\n\n                  <p (click)="openConversa(conversa)">\n                      <span *ngIf="conversa.block"><ion-icon name="lock"></ion-icon></span>\n                      {{conversa.strNome}} <br /> \n                      <span *ngIf="conversa.msgNova" class="nova">{{conversa.descricao}}</span> \n                      <span *ngIf="!conversa.msgNova">{{conversa.descricao}}</span>\n                  </p>\n\n                  <p class="txtMenor">{{conversa.strDataDaUltimaMsg}}</p>\n                  <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n              </ion-item>\n\n              <ion-item-options side="right">\n                  <button ion-button color="danger" (click)="bloquearConversa(conversa.id)" *ngIf="!conversa.block">\n                      <ion-icon name="lock"></ion-icon>\n                      Bloquear\n                  </button>\n                  <button ion-button color="secondary" (click)="desbloquearConversa(conversa.id)" *ngIf="conversa.block">\n                    <ion-icon name="unlock"></ion-icon>\n                     Desbloquear\n                </button>\n                  <button ion-button color="remover" (click)="removerConversa(conversa.id)">\n                       <ion-icon name="trash"></ion-icon>\n                       Remover\n                  </button>\n              </ion-item-options>\n            </ion-item-sliding> \n       </ion-list> \n       <ion-fab right bottom>\n            <button ion-fab color="primary" (click)="addNovaConversa()"><ion-icon name="chatbubbles"></ion-icon></button>\n            <!--<ion-fab-list side="left">\n                <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n            </ion-fab-list>-->\n        </ion-fab>\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/conversas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceChat__["a" /* ServiceChat */]])
], conversas);

//# sourceMappingURL=conversas.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return produtoServicos; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabHome__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabCategorias__ = __webpack_require__(424);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var produtoServicos = (function (_super) {
    __extends(produtoServicos, _super);
    function produtoServicos(navCtrl, navParams, _storage) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._storage = _storage;
        _this.homeTab = __WEBPACK_IMPORTED_MODULE_4__tabHome__["a" /* tabHome */];
        _this.categoriaTab = __WEBPACK_IMPORTED_MODULE_5__tabCategorias__["a" /* tabCategorias */];
        return _this;
    }
    produtoServicos.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    return produtoServicos;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
produtoServicos = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-produtoServicos',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/produtoServicos.html"*/'<ion-header>\n    <ion-navbar color="topProdutoServ">\n        <ion-title>Produtos | Serviços</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-tabs>\n    <ion-tab tabTitle="Home" tabIcon="home" [root]="homeTab"></ion-tab>\n    <ion-tab tabTitle="Categorias" tabIcon="menu" [root]="categoriaTab"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/produtoServicos.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
], produtoServicos);

//# sourceMappingURL=produtoServicos.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return loja; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__anuncioDetalhes__ = __webpack_require__(422);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var loja = (function (_super) {
    __extends(loja, _super);
    // mapElement: HTMLElement;
    function loja(navCtrl, navParams, sp, _storage, modalCtrl, servProdServ, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.servProdServ = servProdServ;
        _this.viewCtrl = viewCtrl;
        _this.listDeAnuncios = [];
        _this.anunciante = _this.navParams.data;
        _this.ftBannerAnunciantes = _this.anunciante.ftBanner;
        _this.ftLogoAnunciante = _this.anunciante.ftLogo;
        return _this;
    }
    loja.prototype.ngOnInit = function () {
        this.getAnuncios(this.anunciante.id);
    };
    loja.prototype.ionViewDidLoad = function () {
        this.loadMap();
    };
    loja.prototype.loadMap = function () {
        var _this = this;
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': this.anunciante.endereco.logradouro + "," + this.anunciante.endereco.complemento + ', Brasil', 'region': 'BR' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
                    var latLng = new google.maps.LatLng(latitude, longitude);
                    var mapOptions = {
                        center: latLng,
                        zoom: 15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
                    setTimeout(function () { return _this.addMarker(); }, 1000);
                }
            }
        });
    };
    loja.prototype.addMarker = function () {
        var marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: this.map.getCenter()
        });
        var content = "";
        this.addInfoWindow(marker, content);
    };
    loja.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.open(_this.map, marker);
        });
    };
    loja.prototype.getAnuncios = function (anuncianteID) {
        var _this = this;
        this.sp.showLoader("Carregando anúncios...");
        this.servProdServ.listarAnuncios(anuncianteID)
            .subscribe(function (r) { return _this.listDeAnuncios = r; }, function (error) { return console.log("Error ao Listar Anuncios"); }, function () {
            for (var i = 0; i < _this.listDeAnuncios.length; i++) {
                var itemArr = _this.listDeAnuncios[i].ftProduto.split('|');
                _this.listDeAnuncios[i].ftProduto = itemArr[0];
            }
            //console.log(this.listDeAnuncios);
            _this.sp.dismissLoader();
        });
    };
    loja.prototype.detalhesAnuncio = function (anuncioObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__anuncioDetalhes__["a" /* anuncioDetalhes */], { 'anuncioObj': anuncioObj, 'anuncianteObj': this.anunciante });
        m.present();
    };
    loja.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return loja;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])("map"),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], loja.prototype, "mapElement", void 0);
loja = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'loja-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/loja.html"*/'<ion-header>\n        <ion-toolbar color="topProdutoServ">\n            <ion-title>\n                <ion-icon name="home"></ion-icon> {{anunciante.nomeFantasia}}\n            </ion-title>\n            <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n            </ion-buttons>\n        </ion-toolbar>\n</ion-header>\n       \n<ion-content class="bgApp">\n\n        <img src="{{urlArquivosUpload}}anunciantes/{{ftBannerAnunciantes}}" *ngIf="ftBannerAnunciantes" />\n        <img src="assets/img/Banners/produtosServicos.jpg" *ngIf="!ftBannerAnunciantes" />\n\n        <ion-card>\n            <ion-card-header>\n                   <span class="fontColor">{{anunciante.nomeFantasia}}</span>\n            </ion-card-header>\n            <ion-card-content>\n                   <span class="fontSize">{{anunciante.descricao}}</span>\n            </ion-card-content>\n        </ion-card>\n        <p class="subTitulo" *ngIf="listDeAnuncios.length > 0">Anúncios</p>\n        <ion-card *ngFor="let anuncio of listDeAnuncios" (click)="detalhesAnuncio(anuncio)">\n            <ion-item text-wrap> \n                <ion-avatar item-start *ngIf="anuncio.ftProduto">\n                    <img src="{{urlImageServ}}anuncio/{{anuncio.ftProduto}}" />\n                </ion-avatar>\n                <p> {{anuncio.chamada}}</p>\n                <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n            </ion-item>\n        </ion-card>\n\n        <!-- <ion-card>\n            <ion-card-content>\n                <p class="fontSize">\n                    <span *ngIf="anunciante.cnpj">CNPJ: {{anunciante.cnpj}}<br /></span> \n                    End: {{anunciante.endereco.logradouro}} {{anunciante.endereco.numero}} <span *ngIf="anunciante.endereco.cep">/ Cep: {{anunciante.endereco.cep}}</span> <br />\n                    Bairro : {{ anunciante.endereco.bairro }}<br />\n                    Cidade: {{anunciante.endereco.cidade}}<br />\n                    Estado: {{anunciante.endereco.estado}}<br />\n                    <ion-icon name="call"></ion-icon> {{anunciante.tel}}\n                </p>\n            </ion-card-content>\n        </ion-card> -->\n\n        <p class="subTitulo">Localização</p>\n        <ion-card>\n            <ion-card-header>\n                <div #map id="map" style="width: 100%; height:250px"></div>\n            </ion-card-header>\n            <ion-card-content>\n                    <p class="fontSize">\n                            <span *ngIf="anunciante.cnpj">CNPJ: {{anunciante.cnpj}}<br /></span> \n                            End: {{anunciante.endereco.logradouro}} {{anunciante.endereco.numero}} <span *ngIf="anunciante.endereco.cep">/ Cep: {{anunciante.endereco.cep}}</span> <br />\n                            Bairro : {{ anunciante.endereco.bairro }}<br />\n                            Cidade: {{anunciante.endereco.cidade}}<br />\n                            Estado: {{anunciante.endereco.estado}}<br />\n                            <ion-icon name="call"></ion-icon> {{anunciante.tel}}\n                        </p>\n            </ion-card-content>\n        </ion-card>\n\n        <!-- <ion-toolbar color="light" no-border class="fontSize">\n            <p>{{anunciante.nomeFantasia}}</p>\n            <p>{{anunciante.descricao}}</p><br />\n            <p><span *ngIf="anunciante.cnpj">CNPJ: {{anunciante.cnpj}}<br /></span> \n            End: {{anunciante.endereco.logradouro}} {{anunciante.endereco.numero}} <span *ngIf="anunciante.endereco.cep">/ Cep: {{anunciante.endereco.cep}}</span> <br />\n            Cidade: {{anunciante.endereco.cidade}}<br />\n            Estado: {{anunciante.endereco.estado}}<br />\n            <ion-icon name="call"></ion-icon> {{anunciante.tel}}<br />\n        </ion-toolbar> -->\n\n        \n   \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/loja.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__["a" /* ServiceProdServ */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], loja);

//# sourceMappingURL=loja.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return perfil; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalVariables__ = __webpack_require__(13);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var perfil = (function (_super) {
    __extends(perfil, _super);
    function perfil(navCtrl, navParams, sp, _storage, modalCtrl, fb, camera, fileUp, servUsuario) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.servUsuario = servUsuario;
        _this.pathPreviewImage = _this.urlImageServ + "usuario/";
        _this.usuarioLogado = _this.navParams.data;
        //console.log(this.usuarioLogado);
        _this.form = _this.fb.group({
            id: [_this.usuarioLogado.id],
            foto: [_this.usuarioLogado.foto],
            nome: [_this.usuarioLogado.nome, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            sobrenome: [_this.usuarioLogado.sobrenome, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            rg: [_this.usuarioLogado.rg],
            cpf: [_this.usuarioLogado.cpf],
            cel: [_this.usuarioLogado.cel],
            email: [_this.usuarioLogado.email, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            senha: [_this.usuarioLogado.senha, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    perfil.prototype.ngOnInit = function () {
    };
    perfil.prototype.salvarPerfil = function () {
        var _this = this;
        var userSave = this.form.value;
        //console.log(userSave);
        this.sp.showLoader("Atualizando perfil...");
        var result;
        this.servUsuario.editUsuario(userSave)
            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao salvar perfil"); }, function () {
            if (result.status == 0) {
                _this.sp.showAviso(result.mensagem);
                _this.form.reset();
                _this.navCtrl.pop();
            }
            if (result.status == 1) {
                _this.sp.showAviso(result.mensagem);
            }
            if (result.status == -1) {
                _this.sp.showAviso(__WEBPACK_IMPORTED_MODULE_9__app_globalVariables__["a" /* global */].ERROR_MSG);
            }
            _this.sp.dismissLoader();
        });
    };
    perfil.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            // this.form.addControl("foto",new FormControl(nomeImage));
            _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    return perfil;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
perfil = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "perfil-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/perfil/perfil.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n      <ion-title><ion-icon name="person"></ion-icon> Meu perfil</ion-title>\n      <!-- <ion-buttons end>\n        <button ion-button icon-only color="royal" (click)="openModalMinhasReservas()">\n          <ion-icon name="more"></ion-icon>\n        </button>\n      </ion-buttons> -->\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n    <form [formGroup]="form" (submit)="salvarPerfil()">\n        \n         <ion-list no-lines>\n     \n           <!-- <ion-item *ngIf="usuarioLogado.foto">\n                <img src="{{pathPreviewImage}}{{usuarioLogado.foto}}" style="max-width:280px;height:auto;margin:4px auto" />\n           </ion-item> -->\n          \n           <ion-item>\n            <button ion-button round padding type="button" color="secondary" (click)="takePic(\'camera\')">Câmera</button>\n            <button ion-button round padding type="button" color="secondary"(click)="takePic(\'galeria\')">Galeria</button>\n          </ion-item>\n\n           <ion-item>\n             <ion-label stacked>Nome</ion-label>\n             <ion-input placeholder="Nome de usuário" formControlName="nome" type="text"></ion-input>\n           </ion-item>\n           \n           <ion-item>\n            <ion-label stacked>Sobrenome</ion-label>\n            <ion-input placeholder="Sobrenome" formControlName="sobrenome" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>Rg</ion-label>\n            <ion-input placeholder="RG" formControlName="rg" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>CPF</ion-label>\n            <ion-input placeholder="CPF" formControlName="cpf" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>Cel</ion-label>\n            <ion-input placeholder="Celular" formControlName="cel" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>E-mail</ion-label>\n            <ion-input placeholder="E-mail" formControlName="email" type="text"></ion-input>\n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>Senha</ion-label>\n            <ion-input placeholder="Senha" formControlName="senha" type="password"></ion-input>\n          </ion-item>\n     \n           <ion-item>\n                <button ion-button block padding type="submit" color="secondary" [disabled]="!form.valid">Salvar alterações</button>\n           </ion-item>\n     \n         </ion-list>\n     \n       </form>\n  </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/perfil/perfil.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__["a" /* Filetransferencia */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__["a" /* ServiceUsuario */]])
], perfil);

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return unidade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceUnidade__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { global } from "../../app/globalVariables";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var unidade = (function (_super) {
    __extends(unidade, _super);
    function unidade(navCtrl, navParams, sp, _storage, modalCtrl, servUnidade) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.servUnidade = servUnidade;
        _this.unidadesLst = [];
        return _this;
        //console.log(this.usuarioLogado);
    }
    unidade.prototype.ngOnInit = function () {
        this.getMinhasUnidades();
    };
    unidade.prototype.getMinhasUnidades = function () {
        var _this = this;
        this._storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                // console.log(dataUser);
                _this.idUnidadeAtiva = dataUser.aptId;
                _this.sp.showLoader("Carregando unidades...");
                _this.servUnidade.listarMinhaUnidades(dataUser.userID)
                    .subscribe(function (unidades) { return _this.unidadesLst = unidades; }, function (error) { return console.log("Error ao obter novas unidades"); }, function () { return _this.sp.dismissLoader(); });
            }
        });
    };
    unidade.prototype.setUnidadeAtiva = function (idUnidade) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            //console.log(dataUser);
            _this.sp.showLoader("Trocando unidade...");
            var result;
            _this.servUnidade.getUnidadeID(idUnidade)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao obter unidade"); }, function () {
                dataUser.aptId = result.id;
                dataUser.condominioId = result.condominioId;
                dataUser.proprietarioId = result.proprietarioID;
                dataUser.strApartamento = result.codigo + '|' + result.numero + '|' + result.grupoDescricao;
                dataUser.strNomeCondominio = result.strNomeCondominio;
                dataUser.strCodUnidade = result.codigo;
                //console.log(dataUser);
                _this.storage.set("UsuarioLogado", dataUser);
                var retorno;
                _this.servUnidade.updateNotificacoes(idUnidade, dataUser.userID)
                    .subscribe(function (rtn) { return retorno = rtn; }, function (error) { return console.log("Error ao set notificações"); }, function () { return _this.getMinhasUnidades(); });
                _this.sp.dismissLoader();
            });
        });
    };
    unidade.prototype.setNotification = function (id, ct) {
        var _this = this;
        console.log("id da not: " + id + " controle: " + ct);
        var result;
        this.servUnidade.checkNotification(id, ct)
            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao alterar notificações"); }, function () {
            _this.getMinhasUnidades();
            _this.sp.showAviso(result.mensagem);
        });
    };
    unidade.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getMinhasUnidades();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return unidade;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
unidade = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "unidade-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/unidade/unidade.html"*/'<ion-header>\n    <ion-navbar color="danger">\n      <ion-title><ion-icon name="home"></ion-icon> Unidades</ion-title>\n      <!-- <ion-buttons end>\n        <button ion-button icon-only color="royal" (click)="openModalMinhasReservas()">\n          <ion-icon name="more"></ion-icon>\n        </button>\n      </ion-buttons> -->\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n    <ion-list>\n        <ion-item-sliding *ngFor="let unidade of unidadesLst">\n           <ion-item text-wrap>\n               <p>\n                  <ion-icon name="home"></ion-icon>  Unidade: {{unidade.complemento}} <br /> {{unidade.grupoDescricao}} \n               </p>\n               <p class="txtMenor">{{unidade.strNomeCondominio}}</p>\n               <ion-icon name="md-checkmark" item-end item-right small *ngIf="idUnidadeAtiva == unidade.id"></ion-icon>\n               <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n           </ion-item>\n\n           <ion-item-options side="right">\n             <button ion-button color="secondary" *ngIf="idUnidadeAtiva != unidade.id" (click)="setUnidadeAtiva(unidade.id)">\n                 <ion-icon name="md-checkmark"></ion-icon>\n                  Ativar\n             </button>\n             <button ion-button color="topReservas" *ngIf="idUnidadeAtiva == unidade.id" (click)="setNotification(unidade.notificacaoID,!unidade.notificacao)">\n                <ion-icon name="notifications" *ngIf="unidade.notificacao"></ion-icon>\n                <ion-icon name="notifications-off" *ngIf="!unidade.notificacao"></ion-icon>\n                 Notificações\n            </button>\n           </ion-item-options>\n         </ion-item-sliding> \n    </ion-list> \n\n  </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/unidade/unidade.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceUnidade__["a" /* ServiceUnidade */]])
], unidade);

//# sourceMappingURL=unidade.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceUnidade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceUnidade = (function () {
    function ServiceUnidade(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
    }
    ServiceUnidade.prototype.listarMinhaUnidades = function (clienteId) {
        var params = 'clienteID=' + clienteId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Cliente/getApartamentosDoCliente?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUnidade.prototype.getUnidadeID = function (unidadeId) {
        var params = 'idApartamento=' + unidadeId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Apartamento/getApartamento?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUnidade.prototype.updateNotificacoes = function (unidadeID, idCliente) {
        var params = 'aptId=' + unidadeID + '&clienteId=' + idCliente;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Notificacao/updateNotificacoes?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUnidade.prototype.checkNotification = function (notificationId, controle) {
        var params = 'uID=' + notificationId + '&controleAtivo=' + controle;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Notificacao/setConfigNotificacao?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUnidade.prototype.validarCodigoUnidade = function (strCodigo) {
        var params = 'cod_apt=' + strCodigo;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/validaCodApartamento?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceUnidade;
}());
ServiceUnidade = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceUnidade);

//# sourceMappingURL=ServiceUnidade.js.map

/***/ }),

/***/ 164:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return carteiraVirtual; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabAcesso__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabAreasDeAcesso__ = __webpack_require__(430);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var carteiraVirtual = (function (_super) {
    __extends(carteiraVirtual, _super);
    function carteiraVirtual(navCtrl, _storage) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
        // this.listarRecentes();
        _this.acessoTab = __WEBPACK_IMPORTED_MODULE_4__tabAcesso__["a" /* tabAcesso */];
        _this.carteirasTab = __WEBPACK_IMPORTED_MODULE_5__tabAreasDeAcesso__["a" /* tabAreasDeAcesso */];
        return _this;
    }
    carteiraVirtual.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    return carteiraVirtual;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
carteiraVirtual = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/carteiraVirtual.html"*/'<ion-header>\n    <ion-navbar color="topCarteiraVirtual">\n        <ion-title>Carteira Virtual</ion-title>\n    </ion-navbar>\n</ion-header>\n<ion-tabs color="topCarteiraVirtual">\n    <ion-tab tabTitle="Acesso" tabIcon="barcode" [root]="acessoTab"></ion-tab>\n    <ion-tab tabTitle="Carteiras" tabIcon="card" [root]="carteirasTab"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/carteiraVirtual.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */]])
], carteiraVirtual);

//# sourceMappingURL=carteiraVirtual.js.map

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 215:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 215;

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceVisita; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceVisita = (function () {
    function ServiceVisita(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceUsuario Provider/ Host' + this._baseUrlServer);
    }
    ServiceVisita.prototype.listarVisitasAtivas = function (idUnidade) {
        var params = 'apartamentoID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visita/GetVisitasAtivas?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.listarMeusVisitantes = function (idUnidade) {
        var params = 'unidId=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visitante/getVisitantesDoApartamento?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.novoVisitante = function (oVisitante) {
        var params = JSON.stringify(oVisitante);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "apartamento/addVisitante", params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.novosMultiplosVisitantes = function (strVisitantes, unidadeID, visitasTP) {
        var params = 'listVisitantesNomes=' + strVisitantes + "&apartamentoID=" + unidadeID + "&tpVisitante=" + visitasTP;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visitante/AdicionarMutiplosVisitantes?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.criarVisitaNVisitantes = function (novasVisitas) {
        var params = JSON.stringify(novasVisitas);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "visita/CriarVisitaForNVisitantes", params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.findVisitante = function (idVisitante) {
        var params = 'visitanteID=' + idVisitante;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visitante/getVisitante?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.editarVisitante = function (oVisitante) {
        var params = JSON.stringify(oVisitante);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "visitante/Edit", params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.novaVisita = function (oVisita) {
        var params = JSON.stringify(oVisita);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "apartamento/addNovaVisitaParaVisitante", params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.aprovarVisita = function (visitaID) {
        var params = 'visitaId=' + visitaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visita/GetAprovarVisita?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.reaprovarVisita = function (visitaID) {
        var params = 'visitaId=' + visitaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visita/GetReprovarVisita?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.isVisitaPendente = function (unidadeID) {
        var params = 'aptIdApartamento=' + unidadeID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'apartamento/getIdVisitantePendente?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVisita.prototype.excluirVisita = function (idVisita) {
        var params = 'idVisita=' + idVisita;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'visita/GetExcluirVisita?' + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceVisita;
}());
ServiceVisita = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceVisita);

//# sourceMappingURL=ServiceVisita.js.map

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceUsuario; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceUsuario = (function () {
    function ServiceUsuario(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
    }
    ServiceUsuario.prototype.autenticarUsuario = function (strEmail, strPassWord) {
        var params = 'email=' + strEmail + '&senha=' + strPassWord;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'usuario/login', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.getCliente = function (clienteID) {
        var params = 'uID=' + clienteID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Usuario/getUserID?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.editUsuario = function (objUser) {
        var params = JSON.stringify(objUser);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Usuario/EditPerfilUser', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.novoUsuario = function (objUser) {
        var params = JSON.stringify(objUser);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Apartamento/AddCliente', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.editConvidadoCarteira = function (objUser) {
        var params = JSON.stringify(objUser);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'ConvidadoCarteira/EditConvidado', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.novoConvidadoCarteira = function (objUser) {
        var params = JSON.stringify(objUser);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'ConvidadoCarteira/AddConvidado', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.ativaDesativaConvidadoCarteira = function (carteiraConvidadoId, areaId, ct) {
        var params = "convidadoCarteirinhaId=" + carteiraConvidadoId + "&idAreaDeAcesso=" + areaId + "&controle=" + ct;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "ConvidadoCarteira/ativaDesativaConvidado?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.enviarEmailGetSenha = function (emailStr) {
        var params = 'strEmail=' + emailStr;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "usuario/getNovaSenha?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.gravaDispositivo = function (objDevice) {
        var params = JSON.stringify(objDevice);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Usuario/dispositivo', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.destroiDevice = function (deviceId) {
        var params = 'deviceID=' + deviceId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "device/DestruirDeviceId?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceUsuario.prototype.setCpfUser = function (userID, cpfStr) {
        var params = 'idUser=' + userID + "&cpfStr=" + cpfStr;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "usuario/atualizaCpf?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceUsuario;
}());
ServiceUsuario = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceUsuario);

//# sourceMappingURL=ServiceUsuario.js.map

/***/ }),

/***/ 37:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Filetransferencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__globalVariables__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Filetransferencia = (function () {
    function Filetransferencia(transfer) {
        this.transfer = transfer;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__globalVariables__["a" /* global */].BASE_API_URL;
        this.fileTransfer = this.transfer.create();
        this.options = {
            fileKey: "file",
            chunkedMode: true
        };
    }
    Filetransferencia.prototype.upload = function (strNomeArquivo, localPath, metodoUp) {
        this.options.fileName = strNomeArquivo;
        this.fileTransfer.upload(localPath, encodeURI(this._baseUrlServer + 'upload/' + metodoUp), this.options).then(function (data) {
            // success
            console.log('Upload com Sucesso');
        }, function (err) {
            console.log(err);
        });
    };
    return Filetransferencia;
}());
Filetransferencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_file_transfer__["a" /* FileTransfer */]])
], Filetransferencia);

//# sourceMappingURL=app.filetransferencia.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabComunicados; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__comunicadoOcorrencia_modalComunicadoContent__ = __webpack_require__(145);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabComunicados = (function (_super) {
    __extends(tabComunicados, _super);
    function tabComunicados(navCtrl, navParams, _serviceComunicado, sp, _storage, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._serviceComunicado = _serviceComunicado;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        return _this;
    }
    tabComunicados.prototype.ngOnInit = function () {
        this.listarRecentes();
    };
    tabComunicados.prototype.openModal = function (comunicadoId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__comunicadoOcorrencia_modalComunicadoContent__["a" /* modalComunicadoContent */], { idDoComunicado: comunicadoId });
        modal.present();
    };
    tabComunicados.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad tabComunicados');
    };
    tabComunicados.prototype.listarRecentes = function () {
        var _this = this;
        this.arrComunicados = [];
        this.sp.showLoader("Carregando últimos comunicados...");
        this._storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                _this._serviceComunicado.ListarUltimos(data.aptId, data.condominioId)
                    .subscribe(function (comunicados) { return _this.arrComunicados = comunicados; }, function (error) { return console.log("erro interno"); }, function () {
                    _this.initializeItems();
                    _this.sp.dismissLoader();
                });
            }
        });
    };
    tabComunicados.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // console.log(val);
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.arrComunicados.filter(function (item) {
                return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    tabComunicados.prototype.initializeItems = function () {
        var _this = this;
        this._storage.get('UsuarioLogado').then(function (data) {
            _this.UsuarioLogado = data;
            _this.items = [];
            // this.items = this.arrComunicados;
            for (var i = 0; i < _this.arrComunicados.length; i++) {
                if (_this.arrComunicados[i].isProprietario) {
                    if (_this.UsuarioLogado.userID == _this.UsuarioLogado.proprietarioId) {
                        _this.items.push(_this.arrComunicados[i]);
                    }
                }
                else {
                    _this.items.push(_this.arrComunicados[i]);
                }
            }
        });
        //console.log(this.items);
    };
    return tabComunicados;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabComunicados = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabComunicados.html"*/'﻿<!--\n  Generated template for the NewPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="topComunicado">\n    <ion-title>Comunicados</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content class="bgApp">\n\n    <ion-toolbar color="subComunicado">\n        <ion-searchbar (ionInput)="getItems($event)" placeholder="Buscar comunicado" showCancelButton="true" cancelButtonText="Cancelar"></ion-searchbar>\n    </ion-toolbar>\n\n    <ion-card *ngFor="let comunicado of items" (click)="openModal(comunicado.id)">\n        <ion-card-content>\n            <ion-item text-wrap>\n                <ion-avatar item-start text-center>\n                    <span *ngIf="comunicado.publico"> \n                      <ion-icon name="home" color="topComunicado" class="custom-icon"></ion-icon><br />\n                         <span class="txtLabel">Público</span>\n                    </span>\n                    <span *ngIf="!comunicado.publico">\n                      <ion-icon name="person" color="topComunicado" class="custom-icon"></ion-icon> <br />\n                       <span class="txtLabel">Privado</span>\n                      </span>\n                </ion-avatar>\n                <label>\n                    {{ comunicado.titulo }}\n                    <p class="dataDisplay">{{comunicado.strDataDeCadastro}}</p>\n                    <p>{{ comunicado.descricao | limitTo : 140 }}</p>\n                </label>\n            </ion-item>\n        </ion-card-content>\n        <ion-item>\n            <span class="txtLabel" *ngIf="comunicado.nomeUsuario">Att: {{ comunicado.nomeUsuario }}</span>\n           <ion-icon *ngFor="let arquivo of comunicado.listaDeArquivo" item-end item-right name="ios-link" class="iconAnexo"></ion-icon>\n        </ion-item>\n      </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabComunicados.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__["a" /* ServiceComunicado */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], tabComunicados);

//# sourceMappingURL=tabComunicados.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabOcorrencias; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalNovaOcorrencia__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalOcorrenciaContent__ = __webpack_require__(387);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabOcorrencias = (function (_super) {
    __extends(tabOcorrencias, _super);
    function tabOcorrencias(navCtrl, navParams, _serviceOcorrencia, _storage, sp, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._serviceOcorrencia = _serviceOcorrencia;
        _this._storage = _storage;
        _this.sp = sp;
        _this.modalCtrl = modalCtrl;
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
        _this.arrOcorrencias = [];
        return _this;
    }
    tabOcorrencias.prototype.ngOnInit = function () {
        var _this = this;
        this.listarOcorrencias();
        this.storage.get("objCondominio").then(function (condominio) {
            _this.ocorrenciaControle = condominio.flOcorrenciaPost;
            // console.log(condominio);
        });
    };
    tabOcorrencias.prototype.initializeItems = function () {
        this.items = [];
        //for (var i = 0; i < this.listaDeContatos.length -1; i++){
        //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
        //}
        this.items = this.arrOcorrencias;
        //console.log(this.items);
    };
    tabOcorrencias.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad tabOcorrencias');
    };
    tabOcorrencias.prototype.listarOcorrencias = function () {
        var _this = this;
        this.sp.showLoader("Carregando últimas ocorrências...");
        this._storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                _this._serviceOcorrencia.getOcorrencias(data.userID, data.condominioId)
                    .subscribe(function (ocorrencias) { return _this.arrOcorrencias = ocorrencias; }, function (error) { return console.log("erro interno"); }, function () {
                    _this.initializeItems();
                    _this.sp.dismissLoader();
                });
            }
        });
    };
    tabOcorrencias.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        //  console.log(val);
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.arrOcorrencias.filter(function (item) {
                return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    tabOcorrencias.prototype.openModal = function (objOcorrencia) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modalOcorrenciaContent__["a" /* modalOcorrenciaContent */], objOcorrencia);
        modal.present();
    };
    tabOcorrencias.prototype.openModalNovaOcorrencia = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modalNovaOcorrencia__["a" /* modalNovaOcorrencia */]);
        modal.present();
    };
    tabOcorrencias.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.listarOcorrencias();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabOcorrencias;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabOcorrencias = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabOcorrencias.html"*/'﻿<ion-header>\n\n  <ion-navbar color="topComunicado">\n    <ion-title>Ocorrências</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="bgApp">\n\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n          <ion-refresher-content></ion-refresher-content>\n       </ion-refresher>\n\n          <ion-toolbar color="subComunicado">\n              <ion-searchbar (ionInput)="getItems($event)" placeholder="Buscar ocorrência" showCancelButton="true" cancelButtonText="Cancelar"></ion-searchbar>\n          </ion-toolbar>\n      \n          <ion-card *ngFor="let ocorrencia of items" (click)="openModal(ocorrencia)">\n              <ion-card-content>\n                  <ion-item text-wrap>\n                      <ion-avatar item-start text-center>\n                          <span *ngIf="ocorrencia.publica"> \n                            <ion-icon name="book" color="topComunicado" class="custom-icon"></ion-icon><br />\n                               <span class="txtLabel">Público</span>\n                            </span>\n                          <span *ngIf="!ocorrencia.publica">\n                            <ion-icon name="book" color="topComunicado" class="custom-icon"></ion-icon> <br />\n                             <span class="txtLabel">Privado</span>\n                            </span>\n                      </ion-avatar>\n                      <label>\n                          <p class="dataDisplay">{{ocorrencia.strDataDeCadastro}} - {{ocorrencia.nomeResp}}</p>\n                          <p>{{ ocorrencia.descricao | limitTo : 140 }}</p>\n                          <p *ngIf="ocorrencia.resolvida" class="pub"> <ion-icon name="md-checkmark"></ion-icon> Resolvida </p>\n                          <p *ngIf="!ocorrencia.resolvida" class="priv"><ion-icon name="md-checkmark"></ion-icon> Pendente </p>\n                      </label>\n                  </ion-item>\n              </ion-card-content>\n            </ion-card>\n\n  <ion-fab right bottom *ngIf="ocorrenciaControle">\n    <button ion-fab color="topComunicado" (click)="openModalNovaOcorrencia()"><ion-icon name="add"></ion-icon></button>\n    <!--<ion-fab-list side="left">\n      <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n      <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n      <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n      <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n    </ion-fab-list>-->\n  </ion-fab>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabOcorrencias.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], tabOcorrencias);

//# sourceMappingURL=tabOcorrencias.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovaOcorrencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var modalNovaOcorrencia = (function (_super) {
    __extends(modalNovaOcorrencia, _super);
    function modalNovaOcorrencia(platform, params, navCtrl, viewCtrl, storage, ocorrenciaService, signalr, sp, fb, camera, fileUp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.ocorrenciaService = ocorrenciaService;
        _this.signalr = signalr;
        _this.sp = sp;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.form = fb.group({
            descricao: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            publica: [false]
        });
        _this.storage.get("objCondominio").then(function (config) {
            _this.ocorrenciaPublica = config.flOcorrenciaAtivar;
        });
        return _this;
    }
    modalNovaOcorrencia.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            _this.form.addControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'ocorrencia');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    modalNovaOcorrencia.prototype.novaOcorrencia = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("clienteId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.userID));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.condominioId));
            _this.form.addControl("idApartamentoOcorrencia", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.aptId));
            var ocorrencia = _this.form.value;
            if (ocorrencia.publica)
                ocorrencia.publica = false;
            else
                ocorrencia.publica = true;
            if (!_this.form.contains("foto"))
                _this.form.addControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]());
            //console.log(ocorrencia);
            _this.ocorrenciaService.addnovaOcorrencia(ocorrencia).subscribe(function (ocorr) { return result = ocorr; }, function (error) { return console.log('Error reportado: ' + error); }, function () {
                //console.log(result);  
                if (result.status == 0) {
                    _this.sp.showAviso(result.mensagem);
                    _this.ocorrenciaService.getOcorrencias(data.userID, data.condominioId);
                    _this.form.reset();
                    _this.signalr.connection.invoke("SendNovaOcorrencia", data.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    _this.dismiss();
                }
                else {
                    _this.sp.showAviso(result.mensagem);
                }
            });
        });
    };
    modalNovaOcorrencia.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovaOcorrencia;
}(__WEBPACK_IMPORTED_MODULE_9__base_pageBase__["a" /* PageBase */]));
modalNovaOcorrencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalNovaOcorrencia.html"*/'﻿<ion-header>\n  <ion-toolbar color="topComunicado">\n    <ion-title>\n      Nova Ocorrência\n    </ion-title>\n    <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="light" showWhen="ios">Voltar</span>\n          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n      </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n  <form [formGroup]="form" (submit)="novaOcorrencia()">\n   \n    <ion-list no-lines>\n\n      <ion-item>\n        <ion-label stacked>Descrição</ion-label>\n        <ion-textarea placeholder="Entre com sua ocorrência." formControlName="descricao" rows="5"></ion-textarea>\n      </ion-item>\n\n      <ion-item text-wrap *ngIf="ocorrenciaPublica">\n        <ion-label class="txtPermiteAdministracao">Esta ocorrência é restrita a Administração.</ion-label>\n        <ion-checkbox color="topComunicado" formControlName="publica"></ion-checkbox> \n      </ion-item>\n\n      <ion-item>\n        <button ion-button round padding type="button" color="topComunicado" (click)="takePic(\'camera\')">Câmera</button>\n        <button ion-button round padding type="button" color="topComunicado"(click)="takePic(\'galeria\')">Galeria</button>\n      </ion-item>\n\n      <ion-item *ngIf="pathPreviewImage">\n          <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n      </ion-item>\n\n      <ion-item>\n           <button ion-button block padding type="submit" color="topComunicado" [disabled]="!form.valid">Criar ocorrência</button>\n      </ion-item>\n\n    </ion-list>\n\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalNovaOcorrencia.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceSignalr__["a" /* ServiceSignalr */],
        __WEBPACK_IMPORTED_MODULE_7__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_8__app_app_filetransferencia__["a" /* Filetransferencia */]])
], modalNovaOcorrencia);

//# sourceMappingURL=modalNovaOcorrencia.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalOcorrenciaContent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var modalOcorrenciaContent = (function (_super) {
    __extends(modalOcorrenciaContent, _super);
    function modalOcorrenciaContent(platform, params, navCtrl, viewCtrl, storage, ocorrenciaService, sp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.ocorrenciaService = ocorrenciaService;
        _this.sp = sp;
        //this.selectOcorrenciaID = this.params.get('idDaOcorrencia');
        _this.ocorrenciaSelectObj = _this.params.data;
        //console.log(this.ocorrenciaSelectObj);
        _this.titulo = _this.ocorrenciaSelectObj.titulo;
        _this.descricao = _this.ocorrenciaSelectObj.descricao;
        _this.foto = _this.urlArquivosUpload + "ocorrencias/" + _this.ocorrenciaSelectObj.foto;
        _this.nomeResponsavel = _this.ocorrenciaSelectObj.nomeResp;
        _this.publica = _this.ocorrenciaSelectObj.publica;
        _this.resolvida = _this.ocorrenciaSelectObj.resolvida;
        _this.parecer = _this.ocorrenciaSelectObj.parecer;
        _this.strDataDeCadastro = _this.ocorrenciaSelectObj.strDataDeCadastro;
        return _this;
    }
    modalOcorrenciaContent.prototype.ngOnInit = function () {
        //this.getOcorrenciaID();
    };
    // getOcorrenciaID(){
    //     this.ocorrenciaService.getOcorrenciaID(this.selectOcorrenciaID)
    //     .subscribe(
    //         ocorrencia => this.ocorrenciaSelectObj = ocorrencia,
    //         error => console.log('Erro no get Ocorrencia: '),
    //         () => {
    //             this.titulo = this.ocorrenciaSelectObj.titulo;
    //             this.descricao = this.ocorrenciaSelectObj.descricao;
    //             this.foto = this.urlArquivosUpload + "ocorrencias/" + this.ocorrenciaSelectObj.foto;
    //             this.nomeResponsavel = this.ocorrenciaSelectObj.nomeResponsavel;
    //             this.publica = this.ocorrenciaSelectObj.publica;
    //             this.resolvida = this.ocorrenciaSelectObj.resolvida;
    //             this.parecer = this.ocorrenciaSelectObj.parecer;
    //             this.strDataDeCadastro = this.ocorrenciaSelectObj.strDataDeCadastro;
    //             //console.log(JSON.stringify(this.arrArquivos));
    //         }
    //     );
    // }
    modalOcorrenciaContent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalOcorrenciaContent;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
modalOcorrenciaContent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalOcorrenciaContent.html"*/'<ion-header>\n  <ion-toolbar color="topComunicado">\n    <ion-title>\n      <ion-icon name="book"></ion-icon> Ocorrência\n    </ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="dismiss()">\n        <span ion-text color="light" showWhen="ios">Voltar</span>\n        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class="bgApp">\n\n  <ion-card>\n     <img src="{{foto}}" *ngIf="foto" />\n\n     <ion-card-content>\n        \n        <p> \n            {{descricao}}<br /><br />\n            <span *ngIf="nomeResponsavel">\n                Por: {{nomeResponsavel}}\n            </span><br />\n            Em: {{strDataDeCadastro}}\n            <br />\n            <span *ngIf="resolvida">\n                 <ion-icon name="checkmark"></ion-icon> Resolvida\n            </span>\n            <span class="pub" *ngIf="publica">\n                <ion-icon name="people"></ion-icon> Pública\n             </span>\n             <span class="priv" *ngIf="!publica">\n                <ion-icon name="person" ></ion-icon> Privada\n             </span>\n         </p>\n         <p *ngIf="parecer">\n            <ion-icon name="book"></ion-icon> Parecer <br /> {{parecer}}\n         </p> \n    </ion-card-content>\n   \n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/modalOcorrenciaContent.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */]])
], modalOcorrenciaContent);

//# sourceMappingURL=modalOcorrenciaContent.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabArquivosCategoria; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabArquivosComunicados__ = __webpack_require__(389);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabArquivosCategoria = (function (_super) {
    __extends(tabArquivosCategoria, _super);
    function tabArquivosCategoria(navCtrl, navParams, _serviceComunicado, sp, _storage, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._serviceComunicado = _serviceComunicado;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.arrFolders = [];
        return _this;
    }
    // openModal(comunicadoId: number) {
    //     let modal = this.modalCtrl.create(modalComunicadoContent, { idDoComunicado: comunicadoId });
    //     modal.present();
    // }
    tabArquivosCategoria.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad tabComunicados');
    };
    tabArquivosCategoria.prototype.ngOnInit = function () {
        this.listarDocuments();
    };
    tabArquivosCategoria.prototype.listarDocuments = function () {
        var _this = this;
        this.sp.showLoader("Carregando pastas...");
        this._storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                _this._serviceComunicado.getListCatComunicado(data.condominioId)
                    .subscribe(function (comunicados) { return _this.arrFolders = comunicados; }, function (error) { return console.log("erro interno"); }, function () { return _this.sp.dismissLoader(); });
            }
        });
    };
    tabArquivosCategoria.prototype.getArquivos = function (cat) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__tabArquivosComunicados__["a" /* tabArquivosComunicados */], { strCategoria: cat });
        m.present();
    };
    return tabArquivosCategoria;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabArquivosCategoria = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabArquivosCategoria.html"*/'<!--\n  Generated template for the NewPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n     <ion-navbar color="topComunicado">\n         <ion-title>Arquivos</ion-title>\n     </ion-navbar>\n </ion-header>\n                \n <ion-content class="bgApp">\n    <!-- <ion-row class="marginTop">\n       <ion-col *ngFor="let pasta of arrFolders">\n           <button ion-button color="topComunicado" clear block class="text-on-bottom" (click)="getArquivos(pasta)">\n              <ion-icon name="folder" class="custom-icon-folder"></ion-icon><label class="rotuloFolder">{{pasta}}</label>\n            </button>       \n       </ion-col>  \n       <ion-col>\n          <button ion-button color="topComunicado" clear block class="text-on-bottom" (click)="getArquivos(\'todas\')">\n             <ion-icon name="folder" class="custom-icon-folder"></ion-icon><label class="rotuloFolder">Todas</label>\n           </button>       \n      </ion-col>           \n    </ion-row>          -->\n\n    <ion-list>\n        <ion-item text-wrap (click)="getArquivos(\'todas\')" class="bgLista">\n            <ion-label>\n                Todas\n            </ion-label>\n            <ion-icon item-end color="topComunicado" name="folder" class="custom-icon-folder"></ion-icon>\n        </ion-item>   \n        <ion-item text-wrap *ngFor="let pasta of arrFolders" class="fontSize" (click)="getArquivos(pasta)" class="bgLista">\n            <ion-label>\n                {{pasta}}\n            </ion-label>\n            <ion-icon item-end color="topComunicado" name="folder" class="custom-icon-folder"></ion-icon>\n        </ion-item> \n       \n   </ion-list> \n  </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabArquivosCategoria.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__["a" /* ServiceComunicado */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], tabArquivosCategoria);

//# sourceMappingURL=tabArquivosCategoria.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabArquivosComunicados; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__comunicadoOcorrencia_modalComunicadoContent__ = __webpack_require__(145);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabArquivosComunicados = (function (_super) {
    __extends(tabArquivosComunicados, _super);
    function tabArquivosComunicados(navCtrl, navParams, viewCtrl, _serviceComunicado, sp, _storage, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.viewCtrl = viewCtrl;
        _this._serviceComunicado = _serviceComunicado;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.items = [];
        _this.strCategoria = _this.navParams.get("strCategoria");
        return _this;
        //console.log(this.strCategoria);
    }
    tabArquivosComunicados.prototype.openModal = function (comunicadoId) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__comunicadoOcorrencia_modalComunicadoContent__["a" /* modalComunicadoContent */], { idDoComunicado: comunicadoId });
        modal.present();
    };
    tabArquivosComunicados.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad tabComunicados');
    };
    tabArquivosComunicados.prototype.ngOnInit = function () {
        this.arrComunicados = [];
        this.listarDocuments();
    };
    tabArquivosComunicados.prototype.listarDocuments = function () {
        var _this = this;
        this.sp.showLoader("Carregando últimos documentos...");
        this._storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                var arrAux_1;
                _this._serviceComunicado.getListComunicadoDocs(data.aptId)
                    .subscribe(function (comunicados) { return arrAux_1 = comunicados; }, function (error) { return console.log("erro interno"); }, function () {
                    if (_this.strCategoria != "todas") {
                        _this.arrComunicados = arrAux_1.filter(function (com) { return com.tp === _this.strCategoria; });
                    }
                    else {
                        _this.arrComunicados = arrAux_1;
                    }
                    _this.initializeItems();
                    _this.sp.dismissLoader();
                });
            }
        });
    };
    tabArquivosComunicados.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        //console.log(val);
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.arrComunicados.filter(function (item) {
                return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    tabArquivosComunicados.prototype.initializeItems = function () {
        this.items = [];
        //for (var i = 0; i < this.listaDeContatos.length -1; i++){
        //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
        //}
        this.items = this.arrComunicados;
        //console.log(this.items);
    };
    tabArquivosComunicados.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return tabArquivosComunicados;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabArquivosComunicados = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comunicadoOcorrencia',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabArquivosComunicados.html"*/'﻿<!--\n  Generated template for the NewPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-toolbar color="topComunicado">\n      <ion-title>\n          <ion-icon name="folder"></ion-icon> Arquivos\n      </ion-title>\n      <ion-buttons start>\n          <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n          </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n\n<ion-content>\n   <ion-toolbar color="subComunicado">\n       <ion-searchbar (ionInput)="getItems($event)" placeholder="Buscar arquivo" showCancelButton="true" cancelButtonText="Cancelar"></ion-searchbar>\n   </ion-toolbar>\n   <ion-list>  \n     <ion-item text-wrap *ngFor="let comunicado of items" (click)="openModal(comunicado.id)">\n        <span class="dataDisplay"> {{comunicado.strDataDeCadastro}}</span><br />\n        <span class="sizeFont"> {{ comunicado.titulo }}</span>\n     </ion-item>\n  </ion-list> \n \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/comunicadoOcorrencia/tabArquivosComunicados.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceComunicado__["a" /* ServiceComunicado */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], tabArquivosComunicados);

//# sourceMappingURL=tabArquivosComunicados.js.map

/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return menuSuspenso; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceAgenda__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__calendarioReservas__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var menuSuspenso = (function () {
    function menuSuspenso(navCtrl, navParams, _serviceAgenda, _serviceReserva, sp, _storage, viewCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._serviceAgenda = _serviceAgenda;
        this._serviceReserva = _serviceReserva;
        this.sp = sp;
        this._storage = _storage;
        this.viewCtrl = viewCtrl;
        this.arrAreaComum = this.navParams.data;
    }
    menuSuspenso.prototype.ngOnInit = function () {
        //this.getAreaComum();
    };
    menuSuspenso.prototype.fechar = function () {
        this.viewCtrl.dismiss();
    };
    menuSuspenso.prototype.changePageReserva = function (objArea) {
        //console.log(JSON.stringify(objArea));
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__calendarioReservas__["a" /* calendarioReservas */], objArea);
        this.fechar();
    };
    return menuSuspenso;
}());
menuSuspenso = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "popover-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/menuSuspenso.html"*/'<ion-list text-wrap>\n    <button ion-item *ngFor="let area of arrAreaComum" (click)="changePageReserva(area)">\n          {{area.nome}}\n    </button>\n</ion-list>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/menuSuspenso.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceAgenda__["a" /* ServiceAgenda */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__["a" /* ServiceReserva */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], menuSuspenso);

//# sourceMappingURL=menuSuspenso.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return calendarioReservas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalNovaReserva__ = __webpack_require__(392);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var calendarioReservas = (function (_super) {
    __extends(calendarioReservas, _super);
    function calendarioReservas(navCtrl, navParams, _serviceReserva, sp, _storage, popoverCtrl, modalCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._serviceReserva = _serviceReserva;
        _this.sp = sp;
        _this._storage = _storage;
        _this.popoverCtrl = popoverCtrl;
        _this.modalCtrl = modalCtrl;
        _this.calendar = {
            mode: 'month',
            currentDate: new Date(),
            dateFormatter: {
                formatMonthViewDay: function (date) {
                    return date.getDate().toString();
                }
            }
        };
        _this.markDisabled = function (date) {
            var current = new Date();
            current.setHours(0, 0, 0);
            return date < current;
        };
        _this.arrEventosDoDia = [];
        _this.arrServMethod = [];
        _this.selectAreaComum = _this.navParams.data;
        _this.urlArquivosUpload = _this.urlArquivosUpload + 'AreaComum/' + _this.selectAreaComum.condominioId + "/" + _this.selectAreaComum.nome + "/";
        return _this;
        //console.log(this.selectAreaComum);
    }
    calendarioReservas.prototype.ngOnInit = function () {
        var _this = this;
        this.arrServMethod = this.loadReservasDaArea();
        this.storage.get("objCondominio").then(function (condominio) {
            _this.reservaControler = condominio.flReserva;
        });
    };
    calendarioReservas.prototype.abrirArquivo = function (docStr) {
        this.sp.openFileDocs(this.urlArquivosUpload + docStr);
    };
    calendarioReservas.prototype.openModalReserva = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modalNovaReserva__["a" /* modalNovaReserva */], { oAreaComum: this.selectAreaComum, dataPRE: this.dataPreDefinida });
        modal.present();
    };
    calendarioReservas.prototype.ionViewDidLoad = function () {
        var _this = this;
        //console.log('ionViewDidLoad tabAgenda'); 
        this.sp.showLoading("Carregando reservas...", 3000);
        setTimeout(function () {
            _this.eventSource = _this.arrServMethod;
        }, 2500);
    };
    calendarioReservas.prototype.loadEvents = function () {
        //  console.log("Carregar...");
        //  this.eventSource = this.createRandomEvents();
        // this.eventSource = this.loadAgendaCondominio();
        this.eventSource = this.arrServMethod;
    };
    calendarioReservas.prototype.onViewTitleChanged = function (title) {
        this.viewTitle = title;
    };
    // onEventSelected(event) {
    //     //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    //    // this.sp.showAlert("Agenda",event.title);
    // }
    calendarioReservas.prototype.changeMode = function (mode) {
        this.calendar.mode = mode;
    };
    // today() {
    //     this.calendar.currentDate = new Date();
    // }
    calendarioReservas.prototype.onTimeSelected = function (ev) {
        this.dataPreDefinida = new Date(ev.selectedTime);
        //console.log(this.dataPreDefinida);
        // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
        //     (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
        if (ev.events !== undefined && ev.events.length !== 0) {
            this.arrEventosDoDia = ev.events;
        }
        else {
            this.arrEventosDoDia = [];
        }
    };
    calendarioReservas.prototype.onCurrentDateChanged = function (event) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    };
    calendarioReservas.prototype.loadReservasDaArea = function () {
        var eventsReservas = [];
        var events = [];
        this._serviceReserva.getListReservasDaArea(this.selectAreaComum.idAreaComum)
            .subscribe(function (eventos) { return eventsReservas = eventos; }, function (error) { return console.log("Falha ao carregar a agenda"); }, function () {
            //console.log(eventsReservas);
            for (var i = 0; i < eventsReservas.length; i++) {
                var arrDt = eventsReservas[i].dataDeRealizacao.split("T");
                var arrDt2 = arrDt[0].split("-");
                var strdata = arrDt2[0] + "/" + arrDt2[1] + "/" + arrDt2[2];
                var t = new Date(strdata);
                events.push({
                    title: "Reservado para o horário:" + eventsReservas[i].horaInicio + " as " + eventsReservas[i].horaFim,
                    startTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                    endTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                    allDay: false
                });
            }
            // console.log(JSON.stringify(events));
        });
        return events;
    };
    calendarioReservas.prototype.onRangeChanged = function (ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    };
    return calendarioReservas;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverContent', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], calendarioReservas.prototype, "content", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('popoverText', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"] }),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"])
], calendarioReservas.prototype, "text", void 0);
calendarioReservas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-modalCalendarioReserva",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/calendarioReservas.html"*/'<ion-header>\n    <ion-navbar color="topReservas">\n      <ion-title>Calendário de Reservas</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bgApp">\n\n<ion-toolbar color="subReservas">\n    <ion-row>\n      <ion-col width-100 style="text-align:center;color:#fff">{{selectAreaComum.nome}} - {{viewTitle}}</ion-col>\n    </ion-row>  \n</ion-toolbar>\n\n<ion-toolbar color="light">\n  <ion-row>\n    <ion-col width-100 style="text-align:center">{{selectAreaComum.descricao}}\n      <div *ngIf="selectAreaComum.listaDeArquivos.length > 0"><br /><strong>Documentos:</strong></div>\n       <span *ngFor="let doc of selectAreaComum.listaDeArquivos" (click)="abrirArquivo(doc.nome)" style="margin:0 4px">\n          <span style="color:#0980F0"><ion-icon name="attach"></ion-icon> Arquivo</span>\n       </span>\n    </ion-col>\n  </ion-row>  \n</ion-toolbar>\n       \n   <ng-template #template let-showEventDetail="showEventDetail" let-selectedDate="selectedDate" let-noEventsLabel="noEventsLabel">\n      <ion-list>\n        <ion-item text-wrap *ngFor="let evento of arrEventosDoDia">\n         <span class="sizeFont">{{evento.title}}</span>\n          <ion-icon name="clock" item-end item-left large></ion-icon>\n        </ion-item>\n    </ion-list> \n  </ng-template>\n\n    <calendar [eventSource]="eventSource"\n              [calendarMode]="calendar.mode"\n              [currentDate]="calendar.currentDate"\n              [monthviewEventDetailTemplate]="template"\n              [dateFormatter]="calendar.dateFormatter"\n              (onCurrentDateChanged)="onCurrentDateChanged($event)"\n              (onEventSelected)="onEventSelected($event)"\n              (onTitleChanged)="onViewTitleChanged($event)"\n              (onTimeSelected)="onTimeSelected($event)"\n              step="30"\n              noEventsLabel="Sem eventos para este dia">\n    </calendar>\n\n <ion-fab right bottom *ngIf="reservaControler">\n      <button ion-fab color="topReservas" (click)="openModalReserva()"><ion-icon name="add"></ion-icon></button>\n </ion-fab>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/calendarioReservas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__["a" /* ServiceReserva */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* PopoverController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], calendarioReservas);

//# sourceMappingURL=calendarioReservas.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovaReserva; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceReserva__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceAgenda__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var modalNovaReserva = (function (_super) {
    __extends(modalNovaReserva, _super);
    function modalNovaReserva(platform, params, navCtrl, viewCtrl, storage, reservaService, agendaService, sp, fb, signalr) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.reservaService = reservaService;
        _this.agendaService = agendaService;
        _this.sp = sp;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.areaComumSelecionada = _this.params.get('oAreaComum');
        var dataSelected = _this.params.get('dataPRE');
        if (_this.validaDataDeReserva(dataSelected))
            _this.currentDate = dataSelected.toISOString();
        else
            _this.currentDate = new Date().toISOString();
        //var now = new Date();
        //var mesAtual = now.getMonth() + 1;
        // this.initmoth = now.getFullYear() + '-' + mesAtual + '-' + now.getDate();
        var dataFutura = new Date();
        dataFutura = _this.addYears(dataFutura, 1);
        _this.dataLimite = dataFutura.toISOString();
        _this.areaComumList = [];
        //  this.listaAreaComum();
        //this.currentDate = (new Date()).toISOString();
        var arrDataMin = _this.currentDate.split('T');
        _this.initmoth = arrDataMin[0];
        _this.arrItemPeriodos = [];
        _this.form = fb.group({
            areaComumId: [_this.areaComumSelecionada.idAreaComum, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            dataDeRealizacao: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            observacao: [""],
            horaInicio: [""],
            horaFim: [""]
        });
        _this.areaSelect();
        return _this;
    }
    modalNovaReserva.prototype.validaDataDeReserva = function (data) {
        var d = new Date();
        if (data < d)
            return false;
        else
            return true;
    };
    modalNovaReserva.prototype.addYears = function (date, year) {
        // date.setDate(date.getDate() + days);
        date.setFullYear(date.getFullYear() + year);
        return date;
    };
    // listaAreaComum(){
    //     this.storage.get("UsuarioLogado").then((data) => {
    //         this.agendaService.getAreaComumList(data.condominioId)
    //             .subscribe(
    //                 areaList => this.areaComumList = areaList,
    //                 error => console.log('error ao carregar as listas de area comum'),
    //                 () => {}
    //             );
    //     });
    // }
    modalNovaReserva.prototype.areaSelect = function () {
        var ev = this.areaComumSelecionada;
        this.horariosEspecificos = ev.flHorariosEspecifico;
        if (ev.flHorariosEspecifico) {
            this.labelPeriodos = "Período";
            this.arrItemPeriodos = ev.lstperiodos;
            this.form.setControl("horaInicio", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](""));
            this.form.setControl("horaFim", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](""));
            this.selectPeriodoID(this.arrItemPeriodos[0].id, this.arrItemPeriodos[0].valor, this.arrItemPeriodos[0].horaInicio, this.arrItemPeriodos[0].horaFim);
        }
        else {
            this.labelPeriodos = "Horário de reservas: de " + ev.lstperiodos[0].horaInicio + " até " + ev.lstperiodos[0].horaFim + " / Valor: R$ " + ev.lstperiodos[0].valor;
            this.arrItemPeriodos = [];
            this.selectPeriodoID(ev.lstperiodos[0].id, ev.lstperiodos[0].valor, ev.lstperiodos[0].horaInicio, ev.lstperiodos[0].horaFim);
            this.form.setControl("horaInicio", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required));
            this.form.setControl("horaFim", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]("", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required));
        }
    };
    modalNovaReserva.prototype.novaReserva = function () {
        var _this = this;
        var retorno;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("apartamentoId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.aptId));
            _this.form.addControl("solicitanteId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.userID));
            _this.form.addControl("periodoID", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](_this.periodoSelecionadoID));
            _this.form.addControl("precoDaReserva", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](_this.precoPeriodo));
            // console.log(this.form.value);
            var reservaNova = _this.form.value;
            console.log(reservaNova);
            _this.reservaService.addNovaReserva(reservaNova)
                .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error reportado:"); }, function () {
                if (retorno.status == 0) {
                    _this.sp.showAviso(retorno.mensagem);
                    _this.form.reset();
                    _this.signalr.connection.invoke("SendNovaReserva", data.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    _this.dismiss();
                }
                if (retorno.status == 1)
                    _this.sp.showAviso(retorno.mensagem);
                if (retorno.status == -1)
                    _this.sp.showAviso("Serviço indisponível no momento, por favor tente mais tarde!");
            });
        });
    };
    modalNovaReserva.prototype.selectPeriodoID = function (id, preco, strHoraInicio, strHoraFim) {
        //console.log("Periodo Selecionado: " + id + " preço: " + preco + "Hora inicio " + strHoraInicio + "Hora Fim: " + strHoraFim);
        this.periodoSelecionadoID = id;
        this.precoPeriodo = preco;
        this._horaInicio = strHoraInicio;
        this._horaFim = strHoraFim;
        this.form.setControl("horaInicio", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](strHoraInicio));
        this.form.setControl("horaFim", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](strHoraFim));
    };
    modalNovaReserva.prototype.checkFirst = function (indice) {
        if (indice > 0)
            return false;
        else
            return true;
    };
    modalNovaReserva.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovaReserva;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalNovaReserva = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modal-novaReserva",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/modalNovaReserva.html"*/'<ion-header>\n  <ion-toolbar color="topReservas">\n    <ion-title>\n     <ion-icon name="clock"></ion-icon> Nova Reserva\n    </ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="dismiss()">\n        <span ion-text color="light" showWhen="ios">Voltar</span>\n        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n\n<ion-content class="imgFundo">\n\n   <ion-toolbar color="secondary">\n      <ion-row>\n          <ion-col width-100 style="text-align:center;color:#fff">{{areaComumSelecionada.nome}}</ion-col>\n      </ion-row>  \n  </ion-toolbar>\n\n   <form [formGroup]="form" (submit)="novaReserva()"> \n   \n    <ion-list no-lines>\n        <ion-item>\n          <ion-label>Data de Realização:</ion-label>\n           <ion-datetime class="txtColorItem" displayFormat="DD/MMM/YYYY" min={{initmoth}} max={{dataLimite}} [(ngModel)]="currentDate" formControlName="dataDeRealizacao" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n        </ion-item>\n\n        <ion-item>\n          <ion-label stacked>Observação</ion-label>\n          <ion-textarea placeholder="Entre com sua observação." formControlName="observacao" rows="3" maxlength="50"></ion-textarea>\n        </ion-item>\n\n        <ion-item>\n            <ion-label stacked>{{labelPeriodos}}</ion-label>\n        </ion-item>\n\n        <ion-list radio-group>\n          <ion-item *ngFor="let item of arrItemPeriodos; let i = index">\n            <ion-label>De: {{item.horaInicio}} Até: {{item.horaFim}} - R${{item.valor}}</ion-label>\n            <ion-radio [value]="item.id" (click)="selectPeriodoID(item.id, item.valor, item.horaInicio,item.horaFim)" [checked]="checkFirst(i)"></ion-radio>\n          </ion-item>\n        </ion-list>\n\n        <ion-list *ngIf="!horariosEspecificos">\n          <ion-item>\n            <ion-label stacked>Hora início: </ion-label>\n            <ion-datetime displayFormat="h:mm A"  formControlName="horaInicio" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n          </ion-item>\n\n          <ion-item>\n            <ion-label stacked>Hora fim: </ion-label>\n            <ion-datetime displayFormat="h:mm A" formControlName="horaFim" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n          </ion-item>\n        </ion-list>\n\n        <ion-item>\n           <button ion-button block padding type="submit" color="topReservas" [disabled]="!form.valid">Criar reserva</button>\n        </ion-item>  \n\n    </ion-list>\n   </form> \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/modalNovaReserva.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceReserva__["a" /* ServiceReserva */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceAgenda__["a" /* ServiceAgenda */],
        __WEBPACK_IMPORTED_MODULE_7__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceSignalr__["a" /* ServiceSignalr */]])
], modalNovaReserva);

//# sourceMappingURL=modalNovaReserva.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return minhasReservas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var minhasReservas = (function (_super) {
    __extends(minhasReservas, _super);
    function minhasReservas(storage, sp, reservaService, navCtrl, navParams, viewCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.sp = sp;
        _this.reservaService = reservaService;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.viewCtrl = viewCtrl;
        _this.listMinhaReservas = [];
        _this.viewListMinhasReservas = [];
        _this.carregaMinhasReservas();
        return _this;
    }
    minhasReservas.prototype.carregaMinhasReservas = function () {
        var _this = this;
        this.sp.showLoader("Carregando reservas...");
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.reservaService.getListMinhasReservas(data.aptId)
                .subscribe(function (list) { return _this.listMinhaReservas = list; }, function (Error) { return console.log("Error ao obter as minha Reservas"); }, function () {
                _this.viewListMinhasReservas = [];
                for (var i = 0; i < _this.listMinhaReservas.length; i++) {
                    var oReserva = _this.listMinhaReservas[i];
                    var arrDetailsArea = oReserva.strAreaComun.split('|');
                    var objViewReserva = {
                        id: oReserva.id,
                        areaComumId: oReserva.areaComumId,
                        apartamentoId: oReserva.apartamentoId,
                        horaInicio: oReserva.horaInicio,
                        horaFim: oReserva.horaFim,
                        editavel: oReserva.editavel,
                        solicitanteId: oReserva.solicitanteId,
                        preco: oReserva.precoDaReserva,
                        isFila: oReserva.isFila,
                        dtCadastro: oReserva.strDataDeCadastro,
                        dtRealizacao: oReserva.strdataDeRealizacao,
                        strAreaComum: arrDetailsArea[0],
                        descricao: arrDetailsArea[1],
                        classeStatus: "",
                        reservaStatus: "",
                        justificativaReserva: oReserva.strJustificativa
                    };
                    if (oReserva.isAprovado) {
                        objViewReserva.reservaStatus = "Aprovada";
                        objViewReserva.classeStatus = "aprovado";
                    }
                    if (oReserva.isPendente) {
                        objViewReserva.reservaStatus = "Aguardando aprovação";
                        objViewReserva.classeStatus = "pendente";
                    }
                    if (oReserva.isRejeitado) {
                        objViewReserva.reservaStatus = "Não aprovado";
                        objViewReserva.classeStatus = "rejeitado";
                    }
                    if (oReserva.isExpirado) {
                        objViewReserva.reservaStatus = "Expirada";
                        objViewReserva.classeStatus = "rejeitado";
                    }
                    //console.log(objViewReserva);
                    _this.viewListMinhasReservas.push(objViewReserva);
                }
                _this.sp.dismissLoader();
            });
        });
    };
    minhasReservas.prototype.excluirReserva = function (id) {
        var _this = this;
        var retorno;
        this.reservaService.rejeitarReserva(id)
            .subscribe(function (result) { return retorno = result; }, function (Error) { return console.log("Error ao remover Reserva"); }, function () {
            _this.carregaMinhasReservas();
            _this.sp.showAviso(retorno.mensagem);
        });
    };
    minhasReservas.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return minhasReservas;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
minhasReservas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-minhasReservas",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/minhasReservas.html"*/'<ion-header>\n  <ion-toolbar color="topReservas">\n    <ion-title>\n        <ion-icon name="clock"></ion-icon> Minhas Reservas\n    </ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="dismiss()">\n        <span ion-text color="light" showWhen="ios">Voltar</span>\n        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>  \n        <ion-toolbar color="subReservas">\n                <ion-row>\n                    <ion-col col-12 style="text-align:center;color:#fff">Deslize para a esquerda para remover a reserva se possível.</ion-col>\n                </ion-row>  \n            </ion-toolbar>   \n     <ion-list>\n         \n          <ion-item-sliding *ngFor="let reserva of viewListMinhasReservas">\n                <ion-item text-wrap>\n                    <ion-label class="fontSize"> \n                        <strong>{{reserva.strAreaComum}}</strong><br />\n                        {{reserva.descricao}}<br />\n                        <span *ngIf="reserva.isFila" class="fila"><strong>Fila de espera</strong><br /></span> \n                        <span *ngIf="!reserva.isFila"><strong>Status: </strong><span class={{reserva.classeStatus}}> {{reserva.reservaStatus}}</span><br /></span>\n                        Cadastrado em {{reserva.dtCadastro}}, para o dia {{reserva.dtRealizacao}}<br /> \n                        <strong>Horário: </strong> {{reserva.horaInicio}} - {{reserva.horaFim}}\n                        <p *ngIf="reserva.justificativaReserva">\n                            {{reserva.justificativaReserva}}\n                        </p>\n                    </ion-label>\n                    <ion-icon name="ios-arrow-back-outline" item-end item-right small *ngIf="reserva.editavel && reserva.reservaStatus !=\'Não aprovado\'"></ion-icon>\n                </ion-item> \n                <ion-item-options side="right" *ngIf="reserva.editavel && reserva.reservaStatus !=\'Não aprovado\'">\n                    <button ion-button color="danger" (click)="excluirReserva(reserva.id)">\n                        <ion-icon name="trash"></ion-icon>\n                        Remover\n                    </button>\n                </ion-item-options>\n        </ion-item-sliding> \n    </ion-list> \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/minhasReservas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceReserva__["a" /* ServiceReserva */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], minhasReservas);

//# sourceMappingURL=minhasReservas.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceCorrespondencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceCorrespondencia = (function () {
    function ServiceCorrespondencia(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceCorrespondencia Provider');
    }
    ServiceCorrespondencia.prototype.ListarHistorico = function (apartamentoID) {
        var params = 'aptID=' + apartamentoID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "correspondencia/getHistorico?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceCorrespondencia.prototype.setVisto = function (apartamentoID) {
        var params = 'aptID=' + apartamentoID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "correspondencia/setVisto?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceCorrespondencia;
}());
ServiceCorrespondencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceCorrespondencia);

//# sourceMappingURL=ServiceCorrespondencia.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabVisitas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__meusVisitantes__ = __webpack_require__(396);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabVisitas = (function (_super) {
    __extends(tabVisitas, _super);
    function tabVisitas(storage, navCtrl, servVisita, sp, signalr, alertCtrl, modal) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.signalr = signalr;
        _this.alertCtrl = alertCtrl;
        _this.modal = modal;
        _this.visitasAtivasList = [];
        return _this;
    }
    tabVisitas.prototype.ngOnInit = function () {
        this.getVisitasAtivas();
    };
    tabVisitas.prototype.getVisitasAtivas = function () {
        var _this = this;
        this.sp.showLoader("Carregando visitas...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servVisita.listarVisitasAtivas(dataUser.aptId).subscribe(function (itens) { return _this.visitasAtivasList = itens; }, function (error) { return console.log('falha ao obter as visitas'); }, function () { return _this.sp.dismissLoader(); });
        });
    };
    tabVisitas.prototype.provarVisita = function (idDaVisita) {
        var _this = this;
        var result;
        this.servVisita.aprovarVisita(idDaVisita).subscribe(function (retorno) { return result = retorno; }, function (error) { return console.log(result.mensagem); }, function () {
            if (result.status == 0) {
                _this.sp.showAviso(result.mensagem);
                _this.storage.get("UsuarioLogado").then(function (dataUser) {
                    _this.signalr.connect().then(function (c) {
                        c.invoke("SendAutorizacaoVisitaApp", idDaVisita, dataUser.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    });
                });
            }
        });
    };
    tabVisitas.prototype.confirmRemoverPost = function (titulo, msg, visitaID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servVisita.excluirVisita(visitaID)
                            .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao remover Visita!"); }, function () {
                            // console.log(retorno);
                            if (retorno.status == 0) {
                                _this.sp.showAviso(retorno.mensagem);
                                _this.getVisitasAtivas();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabVisitas.prototype.removerVisita = function (visitaID) {
        this.confirmRemoverPost("Visita permanente", "Tem certeza que deseja excluir esta visita?", visitaID);
    };
    tabVisitas.prototype.openModalMeusVisitantes = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_7__meusVisitantes__["a" /* meusVisitantes */]);
        m.present();
    };
    tabVisitas.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getVisitasAtivas();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabVisitas;
}(__WEBPACK_IMPORTED_MODULE_6__base_pageBase__["a" /* PageBase */]));
tabVisitas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabVisitas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabVisitas.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n    <ion-title>Visitas</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="bgIlustracao">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n     <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-toolbar color="subPortaria">\n      <ion-row>\n          <ion-col col-12 style="text-align:center;color:#fff">Acompanhe suas visitas.</ion-col>\n      </ion-row>  \n  </ion-toolbar>\n  <ion-list>\n      <ion-item-sliding *ngFor="let visita of visitasAtivasList">\n            <ion-item text-wrap>\n              <ion-avatar item-start>\n                <img src=\'{{urlImageServ}}usuario/{{visita.foto}}\'  *ngIf="visita.foto">\n                <img src="assets/img/no_image.png" *ngIf="!visita.foto">\n              </ion-avatar>\n              <ion-label class="fontSize"> \n                <strong>{{visita.nome}}</strong><br />\n                <span *ngIf="visita.nomeEmpresa">Empresa: {{visita.nomeEmpresa}}</span><br *ngIf="visita.nomeEmpresa" />\n                  {{visita.descricao}}<br *ngIf="visita.descricao"/>\n                  <strong>Tipo de visita: </strong>{{visita.tipoDeVisita}}<br />\n                  <span *ngIf="visita.aprovado" class="aprovada"><strong>Visita aprovada</strong><br /></span> \n                  <span *ngIf="visita.isPreAprovado" class="">A portaria estará aguardando a chegada de seu visitante.<br /></span> \n                  <span *ngIf="visita.visitaPermanente" class="permanente"><strong>Visitante fixo</strong><br /></span>\n                  agendado para {{visita.strdataDeCadastro}}\n              </ion-label>\n              <ion-icon name="ios-arrow-back-outline" item-end item-right small *ngIf="visita.visitaPermanente||visita.isPreAprovado"></ion-icon>\n            </ion-item> \n            <ion-item-options side="right" >\n                <button ion-button color="primary" (click)="provarVisita(visita.id)" *ngIf="!visita.aprovado">\n                    <ion-icon name="checkmark"></ion-icon>\n                    Aprovar\n                </button>\n                <button ion-button color="danger" *ngIf="visita.visitaPermanente||visita.isPreAprovado||!visita.aprovado" (click)="removerVisita(visita.id)" >\n                  <ion-icon name="trash"></ion-icon>\n                  Remover\n                </button>\n            </ion-item-options>\n        </ion-item-sliding> \n    </ion-list> \n    \n  <ion-fab right bottom>\n    <button ion-fab color="topPortaria" (click)="openModalMeusVisitantes()"><ion-icon name="people"></ion-icon></button>\n     <!-- <ion-fab-list side="left">\n      <button ion-fab><ion-icon name="add"></ion-icon></button>\n      <button ion-fab><ion-icon name="people"></ion-icon></button>\n      <button ion-fab><ion-icon name="person"></ion-icon></button>\n    </ion-fab-list> -->\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabVisitas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], tabVisitas);

//# sourceMappingURL=tabVisitas.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return meusVisitantes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__listaDeConvidados__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalNovoVisitante__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__modalEditVisitante__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__modalAutorizacaoEntrada__ = __webpack_require__(149);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var meusVisitantes = (function (_super) {
    __extends(meusVisitantes, _super);
    function meusVisitantes(storage, navCtrl, servVisita, sp, signalr, viewCtrl, modalCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.signalr = signalr;
        _this.viewCtrl = viewCtrl;
        _this.modalCtrl = modalCtrl;
        _this.meusVisitantesList = [];
        return _this;
    }
    meusVisitantes.prototype.ngOnInit = function () {
        this.getMeusVisitantes();
    };
    meusVisitantes.prototype.getMeusVisitantes = function () {
        var _this = this;
        this.sp.showLoader("Carregando meus visitantes...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servVisita.listarMeusVisitantes(dataUser.aptId).subscribe(function (itens) { return _this.meusVisitantesList = itens; }, function (error) { return console.log('falha ao obter meus visitantes'); }, function () {
                _this.initializeItems();
                _this.sp.dismissLoader();
            });
        });
    };
    meusVisitantes.prototype.provarVisita = function (idDaVisita) {
        var _this = this;
        var result;
        this.servVisita.aprovarVisita(idDaVisita).subscribe(function (retorno) { return result = retorno; }, function (error) { return console.log(result.mensagem); }, function () {
            if (result.status == 0) {
                _this.sp.showAviso(result.mensagem);
                _this.storage.get("UsuarioLogado").then(function (dataUser) {
                    _this.signalr.connect().then(function (c) {
                        c.invoke("SendAutorizacaoVisitaApp", idDaVisita, dataUser.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    });
                });
            }
        });
    };
    meusVisitantes.prototype.openNovoVisitante = function () {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__modalNovoVisitante__["a" /* modalNovoVisitante */]);
        m.present();
    };
    meusVisitantes.prototype.openListaDeConvidados = function () {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__listaDeConvidados__["a" /* listaDeConvidados */]);
        m.present();
    };
    meusVisitantes.prototype.openEdit = function (visitanteObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_9__modalEditVisitante__["a" /* modalEditVisitante */], visitanteObj);
        m.present();
    };
    meusVisitantes.prototype.openModalAutorizacao = function (visitanteObj) {
        var s = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_10__modalAutorizacaoEntrada__["a" /* modalAutorizacaoEntrada */], visitanteObj);
        s.present();
    };
    meusVisitantes.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        console.log(val);
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.meusVisitantesList.filter(function (item) {
                return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    meusVisitantes.prototype.initializeItems = function () {
        this.items = [];
        //for (var i = 0; i < this.listaDeContatos.length -1; i++){
        //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
        //}
        this.items = this.meusVisitantesList;
        //console.log(this.items);
    };
    meusVisitantes.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getMeusVisitantes();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    meusVisitantes.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return meusVisitantes;
}(__WEBPACK_IMPORTED_MODULE_6__base_pageBase__["a" /* PageBase */]));
meusVisitantes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'meusVisitantes-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/meusVisitantes.html"*/'<ion-header>\n    <ion-toolbar color="topPortaria">\n        <ion-title>\n            <ion-icon name="people"></ion-icon> Meus visitantes\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n     \n        \n<ion-content>\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n       <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-toolbar color="topPortaria">\n        <ion-row>\n            <ion-col col-12 style="text-align:center;color:#fff"> Deslize para a esquerda e obtenha mais opções.</ion-col>\n        </ion-row>  \n    </ion-toolbar>\n    <ion-toolbar color="topPortaria">\n          <ion-searchbar (ionInput)="getItems($event)" placeholder="Buscar visitante" showCancelButton="true" cancelButtonText="Cancelar"></ion-searchbar>\n    </ion-toolbar>\n    <ion-list>\n            <ion-item-sliding *ngFor="let visitante of items">\n                <ion-item text-wrap>\n                    <ion-avatar item-start>\n                        <img src=\'{{urlImageServ}}usuario/{{visitante.foto}}\' *ngIf="visitante.foto">\n                        <img src="assets/img/no_image.png" *ngIf="!visitante.foto">\n                    </ion-avatar>\n                    <ion-label> \n                    {{visitante.nome}}<br />\n                    <span *ngIf="visitante.email"><ion-icon name="mail" small></ion-icon> {{visitante.email}}<br /> </span>\n                    <span *ngIf="visitante.nomeEmpresa">Empresa: {{visitante.nomeEmpresa}}<br /></span>\n                        {{visitante.descricao}}<br *ngIf="visitante.descricao"/>\n                        <span *ngIf="visitante.permanente" class="permanente"><strong>Visita permanente</strong><br /></span> \n                        <span *ngIf="visitante.placaCarro">Placa: {{visitante.placaCarro}}</span><br />\n                        <span *ngIf="visitante.modeloCarro">Modelo do veículo: {{visitante.modeloCarro}}</span>\n\n                    </ion-label>\n                    <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n                </ion-item> \n                <ion-item-options side="right">\n                    <button ion-button color="secondary" (click)="openEdit(visitante)">\n                     <ion-icon name="create"></ion-icon>\n                        Editar\n                     </button>\n                    <button ion-button color="primary" (click)="openModalAutorizacao(visitante)">\n                        <ion-icon name="add"></ion-icon>\n                         Visita\n                    </button>\n                </ion-item-options>\n        </ion-item-sliding> \n    </ion-list> \n    \n    <ion-fab right bottom>\n        <button ion-fab color="topPortaria"><ion-icon name="add"></ion-icon></button>\n            <ion-fab-list side="left">\n            <button ion-fab (click)="openNovoVisitante()"><ion-icon name="person"></ion-icon></button>\n            <button ion-fab (click)="openListaDeConvidados()"><ion-icon name="people"></ion-icon></button>\n        </ion-fab-list>\n    </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/meusVisitantes.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], meusVisitantes);

//# sourceMappingURL=meusVisitantes.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return listaDeConvidados; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalAutorizacaoMultEntrada__ = __webpack_require__(398);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var listaDeConvidados = (function (_super) {
    __extends(listaDeConvidados, _super);
    function listaDeConvidados(storage, navCtrl, servVisita, sp, fb, signalr, modal, viewCtrl, alertCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.modal = modal;
        _this.viewCtrl = viewCtrl;
        _this.alertCtrl = alertCtrl;
        _this.listaDeVisitantes = [];
        _this.novosVisitantes = [];
        _this.form = fb.group({
            tpVisita: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            nomeVisitante: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    listaDeConvidados.prototype.ngOnInit = function () {
        this.tiposDeVisitas = [{ id: 1, descricao: 'Serviço' },
            { id: 2, descricao: 'Pessoal' }];
    };
    listaDeConvidados.prototype.addVisitante = function () {
        var novo = this.form.value;
        this.listaDeVisitantes.push(novo.nomeVisitante);
        this.form.setControl("nomeVisitante", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]());
        //console.log(novo.nomeVisitante);
    };
    listaDeConvidados.prototype.remove = function (strObj) {
        var indice = this.listaDeVisitantes.indexOf(strObj);
        this.listaDeVisitantes.splice(indice, 1);
    };
    listaDeConvidados.prototype.criarMultiplosVisitantes = function () {
        var _this = this;
        if (this.listaDeVisitantes.length > 0) {
            var strNomesVisitantes_1 = "";
            var f_1 = this.form.value;
            for (var i = 0; i < this.listaDeVisitantes.length; i++) {
                strNomesVisitantes_1 += this.listaDeVisitantes[i] + "|";
            }
            this.storage.get("UsuarioLogado").then(function (dataUser) {
                _this.servVisita.novosMultiplosVisitantes(strNomesVisitantes_1, dataUser.aptId, f_1.tpVisita)
                    .subscribe(function (r) { return _this.novosVisitantes = r; }, function (error) { return console.log("Error ao criar multiplos visitantes."); }, function () {
                    var confirm = _this.alertCtrl.create({
                        title: "Novas visitas",
                        message: "Deseja agendar uma visita para seus novos visitantes ?",
                        buttons: [
                            {
                                text: 'Não',
                                handler: function () {
                                    //console.log("Negativo");
                                }
                            },
                            {
                                text: 'Sim',
                                handler: function () {
                                    var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_8__modalAutorizacaoMultEntrada__["a" /* modalAutorizacaoMultEntrada */], _this.novosVisitantes);
                                    m.present();
                                }
                            }
                        ]
                    });
                    confirm.present();
                });
            });
        }
    };
    listaDeConvidados.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return listaDeConvidados;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
listaDeConvidados = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'listaDeConvidados-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/listaDeConvidados.html"*/'<ion-header>\n        <ion-toolbar color="topPortaria">\n            <ion-title>\n                <ion-icon name="people"></ion-icon> Lista de convidados\n            </ion-title>\n            <ion-buttons start>\n                <button ion-button (click)="dismiss()">\n                    <span ion-text color="light" showWhen="ios">Voltar</span>\n                    <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n                </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </ion-header>\n        \n        \n<ion-content>\n\n    <form [formGroup]="form" (submit)="addVisitante()">\n        <ion-list>\n            <ion-item>\n                <ion-label>Tipo de visitas: </ion-label>\n                <ion-select formControlName="tpVisita" interface="popover" cancelText="Cancelar">\n                    <ion-option  *ngFor="let tp of tiposDeVisitas" [value]="tp.id">{{tp.descricao}}</ion-option>\n                </ion-select>\n            </ion-item>\n           \n            <ion-item>\n                <ion-label text-wrap >Nome do convidado</ion-label>\n                <ion-input formControlName="nomeVisitante"></ion-input>\n            </ion-item>\n            <ion-item>\n                <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Add a Lista</button>\n            </ion-item>  \n            <ion-item-divider color="light">Lista de convidados:</ion-item-divider>\n           \n            <ion-item text-wrap *ngFor="let item of listaDeVisitantes" (click)="remove(item)">\n              <ion-icon name="person"></ion-icon>   {{item}}  \n              <ion-icon name="trash" item-end item-right small></ion-icon>\n            </ion-item>\n        </ion-list>\n    </form>\n\n</ion-content>\n<ion-footer>\n    <button ion-button block padding (click)="criarMultiplosVisitantes()" color="topPortaria" [disabled]="!listaDeVisitantes.length > 0">Salvar multiplas visitas</button>\n</ion-footer>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/listaDeConvidados.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], listaDeConvidados);

//# sourceMappingURL=listaDeConvidados.js.map

/***/ }),

/***/ 398:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalAutorizacaoMultEntrada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalAutorizacaoMultEntrada = (function (_super) {
    __extends(modalAutorizacaoMultEntrada, _super);
    function modalAutorizacaoMultEntrada(storage, navCtrl, param, servVisita, sp, signalr, viewCtrl, modalCtrl, fb) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.param = param;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.signalr = signalr;
        _this.viewCtrl = viewCtrl;
        _this.modalCtrl = modalCtrl;
        _this.fb = fb;
        _this.ctVisitaTemp = false;
        _this.currentDateCadastro = (new Date()).toISOString();
        _this.currentDateEntrada = (new Date()).toISOString();
        _this.currentDateSaida = (new Date()).toISOString();
        _this.listaDeVisitantes = _this.param.data;
        _this.currentDate = (new Date()).toISOString();
        var arrDataMin = _this.currentDate.split('T');
        _this.initmoth = arrDataMin[0];
        // console.log(JSON.stringify(this.visitante));
        //console.log(this.visitante);
        // this.form = fb.group({
        //     aprovado: [true],
        //     ctVisitaTemporaria : [false],
        //     dataDeCadastro : [""],
        //     dataInicioPermanente : [""],
        //     dataFimPermanente : [""],
        //     visitaPermanente : [false],
        //     visitaCarro : [false],
        //     descricao : [""],
        // });
        _this.form = fb.group({
            dataDeCadastro: [""],
            descricao: [""]
        });
        return _this;
    }
    modalAutorizacaoMultEntrada.prototype.ngOnInit = function () {
    };
    modalAutorizacaoMultEntrada.prototype.adicionarMultVisitas = function () {
        var _this = this;
        this.sp.showLoader("Criando visitas !");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var novaVisita;
            novaVisita = _this.form.value;
            var strIdsVisitantes = "";
            // console.log(novaVisita);
            for (var i = 0; i < _this.listaDeVisitantes.length; i++) {
                strIdsVisitantes += _this.listaDeVisitantes[i].visitanteID + "|";
            }
            var novaVisitaModal = {
                strIdVisitantes: strIdsVisitantes,
                aprovado: novaVisita.aprovado,
                dataDeCadastro: novaVisita.dataDeCadastro,
                dataInicioPermanente: "",
                dataFimPermanente: "",
                visitaPermanente: novaVisita.visitaPermanente,
                descricao: novaVisita.descricao,
                visitaCarro: novaVisita.visitaCarro,
                apartamentoID: dataUser.aptId
            };
            if (novaVisita.ctVisitaTemporaria) {
                novaVisitaModal.dataDeCadastro = novaVisita.dataInicioPermanente;
                novaVisitaModal.dataInicioPermanente = novaVisita.dataInicioPermanente;
                novaVisitaModal.dataFimPermanente = novaVisita.dataFimPermanente;
            }
            //console.log(novaVisitaModal);
            var result;
            _this.servVisita.criarVisitaNVisitantes(novaVisitaModal)
                .subscribe(function (s) { return result = s; }, function (error) { return console.log("Error ao postar nova visita"); }, function () {
                if (result.status == 0) {
                    _this.dismiss();
                    _this.form.reset();
                    _this.sp.showAviso(result.mensagem);
                    _this.signalr.connect().then(function (cnx) {
                        cnx.invoke("SendAutorizacaoVisitaNVisitantes", dataUser.condominioId).then(function (res) { return console.log("Pre autorização de visita multi!"); });
                    });
                }
                if (result.status == 1) {
                    _this.sp.showAviso(result.mensagem);
                }
                if (result.status == -1) {
                    _this.sp.showAviso(_this.errorMensage);
                }
                _this.sp.dismissLoader();
            });
        });
    };
    modalAutorizacaoMultEntrada.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalAutorizacaoMultEntrada;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalAutorizacaoMultEntrada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalAutorizacaoMultEntrada-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizacaoMultEntrada.html"*/'<ion-header>\n        <ion-toolbar color="topPortaria">\n          <ion-title>\n              <ion-icon name="person"></ion-icon> Autorizar multiplas visitas\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n\n<ion-content>\n \n    <form [formGroup]="form" (submit)="adicionarMultVisitas()"> \n            \n             <ion-list no-lines>    \n                <ion-item *ngIf="!ctVisitaTemp">\n                   <ion-label>Agendar a visita para: </ion-label>\n                   <ion-datetime displayFormat="DD/MMM/YYYY" min={{initmoth}} [(ngModel)]="currentDateCadastro"  formControlName="dataDeCadastro" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <!-- <ion-item>\n                    <ion-label>Visita temporária</ion-label>\n                    <ion-checkbox [(ngModel)]="ctVisitaTemp" formControlName="ctVisitaTemporaria"></ion-checkbox>\n                </ion-item>\n\n                <ion-item-divider color="light" *ngIf="ctVisitaTemporaria">Autorização temporária</ion-item-divider>\n                <ion-item *ngIf="ctVisitaTemp">\n                     <ion-label>De: </ion-label>\n                     <ion-datetime displayFormat="DD/MMM/YYYY" min={{initmoth}} [(ngModel)]="currentDateEntrada"  formControlName="dataInicioPermanente" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <ion-item *ngIf="ctVisitaTemp">\n                    <ion-label>Até: </ion-label>\n                    <ion-datetime displayFormat="DD/MMM/YYYY" min={{initmoth}} [(ngModel)]="currentDateSaida"  formControlName="dataFimPermanente" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                </ion-item>\n\n                <ion-item *ngIf="!ctVisitaTemp">\n                   <ion-label>Permanente </ion-label>\n                   <ion-checkbox  formControlName="visitaPermanente"></ion-checkbox>\n                </ion-item>\n\n                <ion-item >\n                    <ion-label>Visitante possui veículo </ion-label>\n                    <ion-checkbox  formControlName="visitaCarro"></ion-checkbox>\n                </ion-item>-->\n              \n                <ion-item>\n                    <ion-label stacked>Descrição</ion-label>\n                    <ion-textarea placeholder="Descreva uma observação sobre esta visita" formControlName="descricao" rows="5"></ion-textarea>\n                </ion-item> \n         \n                 <ion-item>\n                    <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Criar visitas</button>\n                 </ion-item>  \n             </ion-list>\n         \n            </form> \n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizacaoMultEntrada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], modalAutorizacaoMultEntrada);

//# sourceMappingURL=modalAutorizacaoMultEntrada.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovoVisitante; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalAutorizacaoEntrada__ = __webpack_require__(149);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var modalNovoVisitante = (function (_super) {
    __extends(modalNovoVisitante, _super);
    function modalNovoVisitante(platform, params, navCtrl, viewCtrl, storage, suporte, fb, signalr, servVisita, alertCtrl, modal) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.suporte = suporte;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.servVisita = servVisita;
        _this.alertCtrl = alertCtrl;
        _this.modal = modal;
        _this.tiposDeVisitas = [{ id: 1, descricao: 'Serviço' },
            { id: 2, descricao: 'Pessoal' }];
        _this.form = fb.group({
            tpVisita: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            nomeEmpresa: [""],
            nome: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            rg: [""],
            placaCarro: [""],
            modeloCarro: [""],
            corCarro: [""]
        });
        return _this;
    }
    modalNovoVisitante.prototype.addNovoVisitante = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.form.addControl("apartamentoID", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.aptId));
            var oVisitante = _this.form.value;
            //console.log(oVisitante);
            var result;
            //console.log(JSON.stringify(oVisitante));
            _this.servVisita.novoVisitante(oVisitante)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao cadastrar novo Visitante"); }, function () {
                if (result.status > 0) {
                    _this.form.reset();
                    _this.dismiss();
                    var visitanteModel = {
                        visitanteID: result.status,
                        apartamentoID: oVisitante.apartamentoID,
                        corCarro: oVisitante.corCarro,
                        modeloCarro: oVisitante.modeloCarro,
                        nome: oVisitante.nome,
                        nomeEmpresa: oVisitante.nomeEmpresa,
                        placaCarro: oVisitante.placaCarro,
                        rg: oVisitante.rg,
                        tpVisita: oVisitante.tpVisita
                    };
                    var confirm_1 = _this.alertCtrl.create({
                        title: "Informar nova visita",
                        message: "Deseja informar uma nova visita para este visitante?",
                        buttons: [
                            {
                                text: 'Não',
                                handler: function () {
                                    //console.log("Negativo");
                                }
                            },
                            {
                                text: 'Sim',
                                handler: function () {
                                    console.log(JSON.stringify(visitanteModel));
                                    var modal = _this.modal.create(__WEBPACK_IMPORTED_MODULE_8__modalAutorizacaoEntrada__["a" /* modalAutorizacaoEntrada */], visitanteModel);
                                    modal.present();
                                }
                            }
                        ]
                    });
                    confirm_1.present();
                }
                if (result.status == 0) {
                    _this.suporte.showAviso(result.mensagem);
                }
                if (result.status == -1) {
                    _this.suporte.showAviso("Serviço indisponível, tente mais tarde.");
                }
            });
        });
    };
    // confirmRemoverTemporada(titulo : string, msg : string, temporadaID : number) {
    //   }
    modalNovoVisitante.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovoVisitante;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalNovoVisitante = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modalNovoVisitante-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalNovoVisitante.html"*/'<ion-header>\n        <ion-toolbar color="topPortaria">\n            <ion-title>\n            <ion-icon name="person"></ion-icon> Novo visitante\n            </ion-title>\n            <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </ion-header>\n    \n    <ion-content>\n            <form [formGroup]="form" (submit)="addNovoVisitante()"> \n                    \n            <ion-list no-lines>\n                <ion-item-divider color="light">Informações do visitante</ion-item-divider>\n\n                <ion-item>\n                    <ion-label>Tipo de visitante: </ion-label>\n                    <ion-select formControlName="tpVisita" interface="popover" cancelText="Cancelar" [(ngModel)]="ctTipoDeVisita">\n                    <ion-option  *ngFor="let tp of tiposDeVisitas" [value]="tp.id">{{tp.descricao}}</ion-option>\n                    </ion-select>\n                </ion-item>\n\n                <ion-item *ngIf="ctTipoDeVisita == 1">\n                    <ion-label>Nome da empresa: </ion-label>\n                    <ion-input type="text" formControlName="nomeEmpresa"></ion-input>\n                </ion-item>\n\n                <ion-item>\n                    <ion-label>Nome: </ion-label>\n                    <ion-input type="text" formControlName="nome"></ion-input>\n                </ion-item>\n\n                <ion-item>\n                    <ion-label>RG: </ion-label>\n                    <ion-input type="text" formControlName="rg"></ion-input>\n                </ion-item>\n\n                <ion-item-divider color="light">Dados do Veículo</ion-item-divider>   \n\n                <ion-item>\n                    <ion-label>Placa:</ion-label>\n                    <ion-input type="text" formControlName="placaCarro"></ion-input>\n                </ion-item>\n\n                <ion-item>\n                    <ion-label>Modelo do veículo: </ion-label>\n                    <ion-input type="text" formControlName="modeloCarro"></ion-input>\n                </ion-item>\n            \n                <ion-item>\n                    <ion-label>Cor do veículo: </ion-label>\n                    <ion-input type="text" formControlName="corCarro"></ion-input>\n                </ion-item>\n        \n                <ion-item>\n                    <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Criar visitante</button>\n                </ion-item>  \n            </ion-list>\n            </form>\n\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalNovoVisitante.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */]])
], modalNovoVisitante);

//# sourceMappingURL=modalNovoVisitante.js.map

/***/ }),

/***/ 4:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PageBase; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_globalVariables__ = __webpack_require__(13);

var PageBase = (function () {
    function PageBase(navCtrl, storage) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.urlImageServ = __WEBPACK_IMPORTED_MODULE_0__app_globalVariables__["a" /* global */].BASE_API_UPLOAD;
        this.urlArquivosUpload = __WEBPACK_IMPORTED_MODULE_0__app_globalVariables__["a" /* global */].BASE_API_UPLOAD;
        this.errorMensage = __WEBPACK_IMPORTED_MODULE_0__app_globalVariables__["a" /* global */].ERROR_MSG;
        this.fotoAnexada = __WEBPACK_IMPORTED_MODULE_0__app_globalVariables__["a" /* global */].FOTO_ANEXADA;
    }
    return PageBase;
}());

//# sourceMappingURL=pageBase.js.map

/***/ }),

/***/ 400:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalEditVisitante; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalEditVisitante = (function (_super) {
    __extends(modalEditVisitante, _super);
    // currentDateEntrada = (new Date()).toISOString();
    // currentDateSaida = (new Date()).toISOString();
    function modalEditVisitante(platform, params, navCtrl, viewCtrl, storage, suporte, fb, signalr, servVisita) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.suporte = suporte;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.servVisita = servVisita;
        _this.visitante = _this.params.data;
        _this.tiposDeVisitas = [{ id: 1, descricao: 'Serviço' },
            { id: 2, descricao: 'Pessoal' }];
        _this.form = fb.group({
            visitanteID: [_this.visitante.visitanteID],
            tpVisita: [_this.visitante.tpVisita, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            nomeEmpresa: [_this.visitante.nomeEmpresa],
            nome: [_this.visitante.nome],
            rg: [_this.visitante.rg],
            placaCarro: [_this.visitante.placaCarro],
            modeloCarro: [_this.visitante.modeloCarro],
            corCarro: [_this.visitante.corCarro]
        });
        return _this;
    }
    modalEditVisitante.prototype.salvarAlteracaoVisitante = function () {
        var _this = this;
        var oVisitante = this.form.value;
        //console.log(oVisitante);
        var result;
        this.servVisita.editarVisitante(oVisitante)
            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao editar Visitante"); }, function () {
            //console.log(result);
            // if (result.status == 0){
            _this.dismiss();
            _this.suporte.showAviso("Alterações salvas!");
            // }
        });
    };
    // novaTemporada(){
    //     this.suporte.showLoading("Criando nova temporada...",2000);
    //     let objTemporada :any;
    //     let retorno : any;
    //     this.storage.get("UsuarioLogado").then((dataUser) => {
    //         this.form.addControl("clienteId",new FormControl(dataUser.userID));
    //         this.form.addControl("apartamentoId",new FormControl(dataUser.aptId));
    //         this.form.addControl("condominioId",new FormControl(dataUser.condominioId));
    //         objTemporada = this.form.value;
    //        objTemporada.dataDeEntrada = this.suporte.formatDate(objTemporada.dataDeEntrada);
    //        objTemporada.dataDeSaida = this.suporte.formatDate(objTemporada.dataDeSaida);
    //        // console.log(JSON.stringify(objTemporada));
    //         this.servTemporada.addNovaTemporada(objTemporada).subscribe(
    //             result => retorno = result,
    //             error => console.log("Error ao adicionar nova hospedagem!"),
    //             () =>{
    //                 this.suporte.showAviso("Temporada criada com sucesso!");
    //                 this.dismiss();
    //                // console.log("Retorno: " + JSON.stringify(retorno));
    //                 // if (retorno.status == 0){
    //                 //     var arr = retorno.mensagem.split("|");
    //                 //     console.log(arr);
    //                 //     this.suporte.showAviso(arr[1]);
    //                 // }
    //                 // if (retorno.status == 1)
    //                 // {
    //                 //     this.suporte.showAviso(retorno.mensagem);
    //                 // }
    //             }
    //         );
    //     })
    // }
    // paisSelect(oPais : any){
    //     console.log("Selecionado " + JSON.stringify(oPais));
    // }
    modalEditVisitante.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalEditVisitante;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalEditVisitante = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modalEditVisitante-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalEditVisitante.html"*/'<ion-header>\n        <ion-toolbar color="topPortaria">\n            <ion-title>\n            <ion-icon name="people"></ion-icon> Editar visitante\n            </ion-title>\n            <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </ion-header>\n    \n    <ion-content>\n    \n        <form [formGroup]="form" (submit)="salvarAlteracaoVisitante()"> \n                    \n            <ion-list no-lines>\n                <ion-item-divider color="light">Informações do visitante</ion-item-divider>\n\n            <ion-item>\n                <ion-label>Tipo de visitante: </ion-label>\n                <ion-select formControlName="tpVisita" interface="popover" cancelText="Cancelar">\n                <ion-option  *ngFor="let tp of tiposDeVisitas" [value]="tp.id">{{tp.descricao}}</ion-option>\n                </ion-select>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>Nome da empresa: </ion-label>\n                <ion-input type="text" formControlName="nomeEmpresa"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>Nome: </ion-label>\n                <ion-input type="text" formControlName="nome"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>RG: </ion-label>\n                <ion-input type="text" formControlName="rg"></ion-input>\n            </ion-item>\n\n            <ion-item-divider color="light">Dados do Veículo</ion-item-divider>   \n\n            <ion-item>\n                <ion-label>Placa:</ion-label>\n                <ion-input type="text" formControlName="placaCarro"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>Modelo do veículo: </ion-label>\n                <ion-input type="text" formControlName="modeloCarro"></ion-input>\n            </ion-item>\n            \n            <ion-item>\n                <ion-label>Cor do veículo: </ion-label>\n                <ion-input type="text" formControlName="corCarro"></ion-input>\n             </ion-item>\n        \n             <ion-item>\n                <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Salvar alterações</button>\n             </ion-item>  \n            </ion-list>     \n        </form> \n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalEditVisitante.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVisita__["a" /* ServiceVisita */]])
], modalEditVisitante);

//# sourceMappingURL=modalEditVisitante.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabTemporada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceTemporada__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalNovaTemporada__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalEditTemporada__ = __webpack_require__(403);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var tabTemporada = (function (_super) {
    __extends(tabTemporada, _super);
    function tabTemporada(storage, navCtrl, servTemporada, suporte, modalCtrl, alertCtrl, signalr) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servTemporada = servTemporada;
        _this.suporte = suporte;
        _this.modalCtrl = modalCtrl;
        _this.alertCtrl = alertCtrl;
        _this.signalr = signalr;
        _this.temporadaList = [];
        _this.getTemporadas();
        return _this;
    }
    tabTemporada.prototype.getTemporadas = function () {
        var _this = this;
        this.suporte.showLoader("Carregando temporadas...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servTemporada.listarTemporadas(dataUser.aptId).subscribe(function (itens) { return _this.temporadaList = itens; }, function (error) { return console.log("Error ao listar Temporadas"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    tabTemporada.prototype.openModalNovaTemporada = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modalNovaTemporada__["a" /* modalNovaTemporada */]);
        modal.present();
    };
    tabTemporada.prototype.excluirTemporada = function (temporadaID) {
        this.confirmRemoverTemporada("Excluir Temporada", "Tem certeza que deseja excluir a temporada", temporadaID);
    };
    tabTemporada.prototype.confirmRemoverTemporada = function (titulo, msg, temporadaID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servTemporada.removerTemporada(temporadaID)
                            .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao remover Temporada!"); }, function () {
                            console.log(retorno);
                            if (retorno.status == 0) {
                                _this.suporte.showAviso(retorno.mensagem);
                                _this.getTemporadas();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabTemporada.prototype.atualizarTemporada = function (oTemporada) {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__modalEditTemporada__["a" /* modalEditTemporada */], oTemporada);
        modal.present();
    };
    tabTemporada.prototype.doRefresh = function (refresher) {
        this.getTemporadas();
        setTimeout(function () {
            refresher.complete();
        }, 2000);
    };
    return tabTemporada;
}(__WEBPACK_IMPORTED_MODULE_6__base_pageBase__["a" /* PageBase */]));
tabTemporada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabTemporada-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabTemporada.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Temporada</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n       <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n    <ion-toolbar color="subPortaria">\n        <ion-row>\n            <ion-col col-12 style="text-align:center;color:#fff"> Deslize para a esquerda e obtenha mais opções.</ion-col>\n        </ion-row>  \n    </ion-toolbar>\n  \n    <ion-list>  \n      <ion-item-sliding *ngFor="let temp of temporadaList">\n        <ion-item text-wrap>\n            <ion-label class="fontSize"> \n                <ion-icon name="contacts"></ion-icon> <span style="font-weight:bold">{{temp.hospNomeResponsavel}}</span><br />\n                Entrada: {{temp.getFormatDataEntrada}}<br />\n                Saída: {{temp.getFormatDataSaida}} <br />\n                Chegada: {{temp.hospHoraChegada}} / Adultos : {{temp.hospNAdults}} / Crianças: {{temp.hospNCriancas}}<br />\n                Total de hospedes:{{temp.nHospedes}} <br />\n                Origem: {{temp.hospOrigemResponsavel}}<br />\n                <span *ngIf="!temp.descricao"> {{temp.descricao}}</span><br *ngIf="!temp.descricao" />\n            </ion-label>\n        </ion-item> \n        <ion-item-options side="right">\n            <button ion-button color="danger" (click)="excluirTemporada(temp.hospedagemId)">\n                <ion-icon name="trash"></ion-icon>\n                Remover\n            </button>\n            <!-- <button ion-button color="primary" (click)="atualizarTemporada(temp)">\n                <ion-icon name="create"></ion-icon>\n                Editar\n            </button> -->\n        </ion-item-options>\n      </ion-item-sliding>\n  </ion-list>\n\n  <ion-fab right bottom>\n    <button ion-fab color="topPortaria" (click)="openModalNovaTemporada()"><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabTemporada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceTemporada__["a" /* ServiceTemporada */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__["b" /* SignalR */]])
], tabTemporada);

//# sourceMappingURL=tabTemporada.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovaTemporada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceTemporada__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalNovaTemporada = (function (_super) {
    __extends(modalNovaTemporada, _super);
    function modalNovaTemporada(platform, params, navCtrl, viewCtrl, storage, suporte, fb, signalr, servTemporada) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.suporte = suporte;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.servTemporada = servTemporada;
        _this.currentDateEntrada = (new Date()).toISOString();
        _this.currentDateSaida = (new Date()).toISOString();
        var data = new Date();
        _this.dataMinima = data.toISOString();
        _this.dataLimite = _this.addYears(data, 1).toISOString();
        _this.paisList = [{ id: 1, nome: 'Brasil' },
            { id: 2, nome: 'Afeganistão' },
            { id: 3, nome: 'África do Sul' },
            { id: 4, nome: 'Akrotiri' },
            { id: 5, nome: 'Albânia' },
            { id: 6, nome: 'Alemanha' },
            { id: 7, nome: 'Andorra' },
            { id: 8, nome: 'Angola' },
            { id: 9, nome: 'Anguila' },
            { id: 10, nome: 'Antárctida' },
            { id: 11, nome: 'Antígua e Barbuda' },
            { id: 12, nome: 'Antilhas Neerlandesas' },
            { id: 13, nome: 'Arábia Saudita' },
            { id: 14, nome: 'Arctic Ocean' },
            { id: 15, nome: 'Argélia' },
            { id: 16, nome: 'Argentina' },
            { id: 17, nome: 'Arménia' },
            { id: 18, nome: 'Aruba' },
            { id: 19, nome: 'Ashmore and Cartier Islands' },
            { id: 20, nome: 'Atlantic Ocean' },
            { id: 21, nome: 'Austrália' },
            { id: 22, nome: 'Áustria' },
            { id: 23, nome: 'Azerbaijão' },
            { id: 24, nome: 'Baamas' },
            { id: 25, nome: 'Bangladeche' },
            { id: 26, nome: 'Barbados' },
            { id: 27, nome: 'Barém' },
            { id: 28, nome: 'Bélgica' },
            { id: 29, nome: 'Belize' },
            { id: 30, nome: 'Benim' },
            { id: 31, nome: 'Bermudas' },
            { id: 32, nome: 'Bielorrússia' },
            { id: 33, nome: 'Birmânia' },
            { id: 34, nome: 'Bolívia' },
            { id: 35, nome: 'Bósnia e Herzegovina' },
            { id: 36, nome: 'Botsuana' },
            { id: 37, nome: 'Brunei' },
            { id: 38, nome: 'Bulgária' },
            { id: 39, nome: 'Burquina Faso' },
            { id: 40, nome: 'Burúndi' },
            { id: 41, nome: 'Butão' },
            { id: 42, nome: 'Cabo Verde' },
            { id: 43, nome: 'Camarões' },
            { id: 44, nome: 'Camboja' },
            { id: 45, nome: 'Canadá' },
            { id: 46, nome: 'Catar' },
            { id: 47, nome: 'Cazaquistão' },
            { id: 48, nome: 'Chade' },
            { id: 49, nome: 'Chile' },
            { id: 50, nome: 'China' },
            { id: 51, nome: 'Chipre' },
            { id: 52, nome: 'Clipperton Island' },
            { id: 53, nome: 'Colômbia' },
            { id: 54, nome: 'Comores' },
            { id: 55, nome: 'Congo Brazzaville' },
            { id: 56, nome: 'Congo Kinshasa' },
            { id: 57, nome: 'Brazzaville' },
            { id: 58, nome: 'Kinshasa' },
            { id: 59, nome: 'Coral Sea Islands' },
            { id: 60, nome: 'Coreia do Norte' },
            { id: 61, nome: 'Coreia do Sul' },
            { id: 62, nome: 'Costa do Marfim' },
            { id: 63, nome: 'Costa Rica' },
            { id: 64, nome: 'Croácia' },
            { id: 65, nome: 'Cuba' },
            { id: 66, nome: 'Dhekelia' },
            { id: 67, nome: 'Dinamarca' },
            { id: 68, nome: 'Domínica' },
            { id: 69, nome: 'Egipto' },
            { id: 70, nome: 'Emiratos Árabes Unidos' },
            { id: 71, nome: 'Equador' },
            { id: 72, nome: 'Eritreia' },
            { id: 73, nome: 'Eslováquia' },
            { id: 74, nome: 'Eslovénia' },
            { id: 75, nome: 'Espanha' },
            { id: 76, nome: 'Estados Unidos' },
            { id: 77, nome: 'Estónia' },
            { id: 78, nome: 'Etiópia' },
            { id: 79, nome: 'Faroé' },
            { id: 80, nome: 'Fiji' },
            { id: 81, nome: 'Filipinas' },
            { id: 82, nome: 'Finlândia' },
            { id: 83, nome: 'França' },
            { id: 84, nome: 'Gabão' },
            { id: 85, nome: 'Gâmbia' },
            { id: 86, nome: 'Gana' },
            { id: 87, nome: 'Gaza Strip' },
            { id: 88, nome: 'Geórgia' },
            { id: 89, nome: 'Geórgia do Sul e Sandwich do Sul' },
            { id: 90, nome: 'Gibraltar' },
            { id: 91, nome: 'Granada' },
            { id: 92, nome: 'Grécia' },
            { id: 93, nome: 'Gronelândia' },
            { id: 94, nome: 'Guame' },
            { id: 95, nome: 'Guatemala' },
            { id: 96, nome: 'Guernsey' },
            { id: 97, nome: 'Guiana' },
            { id: 98, nome: 'Guiné' },
            { id: 99, nome: 'Guiné Equatorial' },
            { id: 100, nome: 'Guiné' },
            { id: 101, nome: 'Bissau' },
            { id: 102, nome: 'Haiti' },
            { id: 103, nome: 'Honduras' },
            { id: 104, nome: 'Hong Kong' },
            { id: 105, nome: 'Hungria' },
            { id: 106, nome: 'Iémen' },
            { id: 107, nome: 'Ilha Bouvet' },
            { id: 108, nome: 'Ilha do Natal' },
            { id: 109, nome: 'Ilha Norfolk' },
            { id: 110, nome: 'Ilhas Caimão' },
            { id: 111, nome: 'Ilhas Cook' },
            { id: 112, nome: 'Ilhas dos Cocos' },
            { id: 113, nome: 'Ilhas Falkland' },
            { id: 114, nome: 'Ilhas Heard e McDonald' },
            { id: 115, nome: 'Ilhas Marshall' },
            { id: 116, nome: 'Ilhas Salomão' },
            { id: 117, nome: 'Ilhas Turcas e Caicos' },
            { id: 118, nome: 'Ilhas Virgens Americanas' },
            { id: 119, nome: 'Ilhas Virgens Britânicas' },
            { id: 120, nome: 'Índia' },
            { id: 121, nome: 'Indian Ocean' },
            { id: 122, nome: 'Indonésia' },
            { id: 123, nome: 'Irão' },
            { id: 124, nome: 'Iraque' },
            { id: 125, nome: 'Irlanda' },
            { id: 126, nome: 'Islândia' },
            { id: 127, nome: 'Israel' },
            { id: 128, nome: 'Itália' },
            { id: 129, nome: 'Jamaica' },
            { id: 130, nome: 'Jan Mayen' },
            { id: 131, nome: 'Japão' },
            { id: 132, nome: 'Jersey' },
            { id: 133, nome: 'Jibuti' },
            { id: 134, nome: 'Jordânia' },
            { id: 135, nome: 'Kuwait' },
            { id: 136, nome: 'Laos' },
            { id: 137, nome: 'Lesoto' },
            { id: 138, nome: 'Letónia' },
            { id: 139, nome: 'Líbano' },
            { id: 140, nome: 'Libéria' },
            { id: 141, nome: 'Líbia' },
            { id: 142, nome: 'Listenstaine' },
            { id: 143, nome: 'Lituânia' },
            { id: 144, nome: 'Luxemburgo' },
            { id: 145, nome: 'Macau' },
            { id: 146, nome: 'Macedónia' },
            { id: 147, nome: 'Madagáscar' },
            { id: 148, nome: 'Malásia' },
            { id: 149, nome: 'Malávi' },
            { id: 150, nome: 'Maldivas' },
            { id: 151, nome: 'Mali' },
            { id: 152, nome: 'Malta' },
            { id: 153, nome: 'Man, Isle of' },
            { id: 154, nome: 'Marianas do Norte' },
            { id: 155, nome: 'Marrocos' },
            { id: 156, nome: 'Maurícia' },
            { id: 157, nome: 'Mauritânia' },
            { id: 158, nome: 'Mayotte' },
            { id: 159, nome: 'México' },
            { id: 160, nome: 'Micronésia' },
            { id: 161, nome: 'Moçambique' },
            { id: 162, nome: 'Moldávia' },
            { id: 163, nome: 'Mónaco' },
            { id: 164, nome: 'Mongólia' },
            { id: 165, nome: 'Monserrate' },
            { id: 166, nome: 'Montenegro' },
            { id: 167, nome: 'Mundo' },
            { id: 168, nome: 'Namíbia' },
            { id: 169, nome: 'Nauru' },
            { id: 170, nome: 'Navassa Island' },
            { id: 171, nome: 'Nepal' },
            { id: 172, nome: 'Nicarágua' },
            { id: 173, nome: 'Níger' },
            { id: 174, nome: 'Nigéria' },
            { id: 175, nome: 'Niue' },
            { id: 176, nome: 'Noruega' },
            { id: 177, nome: 'Nova Caledónia' },
            { id: 178, nome: 'Nova Zelândia' },
            { id: 179, nome: 'Omã' },
            { id: 180, nome: 'Pacific Ocean' },
            { id: 181, nome: 'Países Baixos' },
            { id: 182, nome: 'Palau' },
            { id: 183, nome: 'Panamá' },
            { id: 184, nome: 'Papua' },
            { id: 185, nome: 'Nova Guiné' },
            { id: 186, nome: 'Paquistão' },
            { id: 187, nome: 'Paracel Islands' },
            { id: 188, nome: ' Paraguai' },
            { id: 189, nome: 'Peru' },
            { id: 190, nome: 'Pitcairn' },
            { id: 191, nome: 'Polinésia Francesa' },
            { id: 192, nome: 'Polónia' },
            { id: 193, nome: 'Porto Rico' },
            { id: 194, nome: 'Portugal' },
            { id: 195, nome: 'Quénia' },
            { id: 196, nome: 'Quirguizistão' },
            { id: 197, nome: 'Quiribáti' },
            { id: 198, nome: 'Reino Unido' },
            { id: 199, nome: 'República Centro' },
            { id: 200, nome: 'Africana' },
            { id: 201, nome: 'República Checa' },
            { id: 202, nome: 'República Dominicana' },
            { id: 203, nome: 'Roménia' },
            { id: 204, nome: 'Ruanda' },
            { id: 205, nome: 'Rússia' },
            { id: 206, nome: 'Salvador' },
            { id: 207, nome: 'Samoa' },
            { id: 208, nome: 'Samoa Americana' },
            { id: 209, nome: 'Santa Helena' },
            { id: 210, nome: 'Santa Lúcia' },
            { id: 211, nome: 'São Cristóvão e Neves' },
            { id: 212, nome: 'São Marinho' },
            { id: 213, nome: 'São Pedro e Miquelon' },
            { id: 214, nome: 'São Tomé e Príncipe' },
            { id: 215, nome: 'São Vicente e Granadinas' },
            { id: 216, nome: 'Sara Ocidental' },
            { id: 217, nome: 'Seicheles' },
            { id: 218, nome: 'Senegal' },
            { id: 219, nome: 'Serra Leoa' },
            { id: 220, nome: 'Sérvia' },
            { id: 221, nome: 'Singapura' },
            { id: 222, nome: 'Síria' },
            { id: 223, nome: 'Somália' },
            { id: 224, nome: 'Southern Ocean' },
            { id: 225, nome: 'Spratly Islands' },
            { id: 226, nome: 'Sri Lanca' },
            { id: 227, nome: 'Suazilândia' },
            { id: 228, nome: 'Sudão' },
            { id: 229, nome: 'Suécia' },
            { id: 230, nome: 'Suíça' },
            { id: 231, nome: 'Suriname' },
            { id: 232, nome: 'Svalbard e Jan Mayen' },
            { id: 233, nome: 'Tailândia' },
            { id: 234, nome: 'Taiwan' },
            { id: 235, nome: 'Tajiquistão' },
            { id: 236, nome: 'Tanzânia' },
            { id: 237, nome: 'Território Britânico do Oceano Índico' },
            { id: 238, nome: 'Territórios Austrais Franceses' },
            { id: 239, nome: 'Timor Leste' },
            { id: 240, nome: 'Togo' },
            { id: 241, nome: 'Tokelau' },
            { id: 242, nome: 'Tonga' },
            { id: 243, nome: 'Trindade e Tobago' },
            { id: 244, nome: 'Tunísia' },
            { id: 245, nome: 'Turquemenistão' },
            { id: 246, nome: 'Turquia' },
            { id: 247, nome: 'Tuvalu' },
            { id: 248, nome: 'Ucrânia' },
            { id: 249, nome: 'Uganda' },
            { id: 250, nome: 'União Europeia' },
            { id: 251, nome: 'Uruguai' },
            { id: 252, nome: 'Usbequistão' },
            { id: 253, nome: 'Vanuatu' },
            { id: 254, nome: 'Vaticano' },
            { id: 255, nome: 'Venezuela' },
            { id: 256, nome: 'Vietname' },
            { id: 257, nome: 'Wake Island' },
            { id: 258, nome: 'Wallis e Futuna' },
            { id: 259, nome: 'West Bank' },
            { id: 260, nome: 'Zâmbia' },
            { id: 261, nome: 'Zimbabué' }];
        _this.form = fb.group({
            nomeResponsavel: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            rgResponsavel: [""],
            passaporteResponsavel: [""],
            origemResponsavel: [""],
            horaChegada: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern("^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$")],
            nAdults: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern("^([0-1]?[0-9]|2[0-4])?$")],
            nCriancas: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern("^([0-1]?[0-9]|2[0-4])?$")],
            placa: [""],
            cor: [""],
            modelo: [""],
            dataDeEntrada: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            dataDeSaida: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            descricao: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(144)]
        });
        return _this;
    }
    modalNovaTemporada.prototype.novaTemporada = function () {
        var _this = this;
        this.suporte.showLoading("Criando nova temporada...", 2000);
        var objTemporada;
        var retorno;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.form.addControl("clienteId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.userID));
            _this.form.addControl("apartamentoId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.aptId));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.condominioId));
            objTemporada = _this.form.value;
            objTemporada.dataDeEntrada = _this.suporte.formatDate(objTemporada.dataDeEntrada);
            objTemporada.dataDeSaida = _this.suporte.formatDate(objTemporada.dataDeSaida);
            // console.log(JSON.stringify(objTemporada));
            _this.servTemporada.addNovaTemporada(objTemporada).subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao adicionar nova hospedagem!"); }, function () {
                if (retorno.status == 0) {
                    var arr = retorno.mensagem.split("|");
                    // console.log(arr);
                    _this.suporte.showAviso(arr[1]);
                    _this.signalr.connect().then(function (c) {
                        c.invoke("SendNovaHospedagem", arr[0], dataUser.condominioId).then(function (s) { return console.log("Nova hospedagem enviada com sucesso!"); });
                    });
                    _this.dismiss();
                }
                if (retorno.status == 1) {
                    _this.suporte.showAviso(retorno.mensagem);
                }
                if (retorno.status == -1) {
                    _this.suporte.showAviso("Serviço indisponível no momento, por favor tente mais tarde!");
                }
            });
        });
    };
    modalNovaTemporada.prototype.paisSelect = function (oPais) {
        console.log("Selecionado " + JSON.stringify(oPais));
    };
    modalNovaTemporada.prototype.addYears = function (date, year) {
        date.setFullYear(date.getFullYear() + year);
        return date;
    };
    modalNovaTemporada.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovaTemporada;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalNovaTemporada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modal-novaTemporada",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalNovaTemporada.html"*/'<ion-header>\n    <ion-toolbar color="topPortaria">\n        <ion-title>\n        <ion-icon name="globe"></ion-icon> Nova Temporada\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content class="bgApp">\n\n        <form [formGroup]="form" (submit)="novaTemporada()"> \n                \n                 <ion-list>\n                        <ion-item-divider color="light">Responsável pela temporada</ion-item-divider>\n                    <ion-item>\n                        <ion-label>Nome do Responsável</ion-label>\n                        <ion-input type="text" formControlName="nomeResponsavel"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label>RG do Responsável</ion-label>\n                        <ion-input type="text" formControlName="rgResponsavel"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label>Passaporte: </ion-label>\n                        <ion-input type="text" formControlName="passaporteResponsavel"></ion-input>\n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label>Origem:</ion-label>\n                        <ion-select formControlName="origemResponsavel" interface="popover" cancelText="Cancelar">\n                        <ion-option  *ngFor="let pais of paisList" [value]="pais.nome" (ionSelect)="paisSelect(pais)">{{pais.nome}}</ion-option>\n                        </ion-select>\n                     </ion-item>\n             \n                     <ion-item>\n                       <ion-label>Hora de chegada:</ion-label>\n                       <ion-datetime displayFormat="h:mm A"  formControlName="horaChegada" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                     </ion-item>\n\n                     <ion-item>\n                        <ion-label>Número de Adultos: </ion-label>\n                        <ion-input type="text" formControlName="nAdults"></ion-input>\n                     </ion-item>\n                  \n                     <ion-item>\n                        <ion-label>Número de Crianças: </ion-label>\n                        <ion-input type="text" formControlName="nCriancas"></ion-input>\n                     </ion-item>\n                     <ion-item-divider color="light">Dados do Veículo</ion-item-divider>\n                     <ion-item>\n                        <ion-label>Placa:</ion-label>\n                        <ion-input type="text" formControlName="placa"></ion-input>\n                    </ion-item>\n\n                     <ion-item>\n                        <ion-label>Cor:</ion-label>\n                        <ion-input type="text" formControlName="cor"></ion-input>\n                     </ion-item>\n\n                     <ion-item>\n                        <ion-label>Modelo:</ion-label>\n                        <ion-input type="text" formControlName="modelo"></ion-input>\n                     </ion-item>\n                     <ion-item-divider color="light">Período</ion-item-divider>\n\n                     <ion-item>\n                        <ion-label>Data de entrada:</ion-label>\n                        <ion-datetime displayFormat="DD/MMM/YYYY" min="{{dataMinima}}" max="{{dataLimite}}" [(ngModel)]="currentDateEntrada"  formControlName="dataDeEntrada" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                    </ion-item>\n\n                    <ion-item>\n                        <ion-label>Data de saída:</ion-label>\n                        <ion-datetime displayFormat="DD/MMM/YYYY" min="{{dataMinima}}" max="{{dataLimite}}" [(ngModel)]="currentDateSaida"  formControlName="dataDeSaida" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n                    </ion-item>\n\n                    <ion-item>\n                       <ion-label stacked>Descrição</ion-label>\n                       <ion-textarea placeholder="Descreva uma observação sobre esta temporada" formControlName="descricao" rows="5"></ion-textarea>\n                    </ion-item>\n\n                    <ion-item-divider color="light"></ion-item-divider>\n\n                     <ion-item class="bgApp">\n                        <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Criar Temporada</button>\n                     </ion-item>  \n                 </ion-list>            \n            </form> \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalNovaTemporada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceTemporada__["a" /* ServiceTemporada */]])
], modalNovaTemporada);

//# sourceMappingURL=modalNovaTemporada.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalEditTemporada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceTemporada__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalEditTemporada = (function (_super) {
    __extends(modalEditTemporada, _super);
    // currentDateEntrada = (new Date()).toISOString();
    // currentDateSaida = (new Date()).toISOString();
    function modalEditTemporada(platform, params, navCtrl, viewCtrl, storage, suporte, fb, signalr, servTemporada) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.suporte = suporte;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.servTemporada = servTemporada;
        _this.temporadaObj = _this.params.data;
        _this.form = fb.group({
            id: [_this.temporadaObj.hospedagemId],
            // rgResponsavel: [""],
            // passaporteResponsavel : [""],
            // origemResponsavel : [""],
            //horaChegada : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$")],
            //nAdults : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            //nCriancas : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            //placa : [""],
            //cor : [""],
            //modelo : [""],
            dataDeEntrada: [_this.temporadaObj.hospDataDeEntrada, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            dataDeSaida: [_this.temporadaObj.hospDataDeSaida, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            descricao: [_this.temporadaObj.hospDescricao, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].maxLength(144)]
        });
        return _this;
    }
    modalEditTemporada.prototype.editTemporada = function () {
        var _this = this;
        var temporadaPost;
        // let retornoServ : any;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.form.addControl("clienteId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.userID));
            _this.form.addControl("apartamentoId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.aptId));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.condominioId));
            temporadaPost = _this.form.value;
            temporadaPost.dataDeEntrada = _this.suporte.formatDate(temporadaPost.dataDeEntrada);
            temporadaPost.dataDeSaida = _this.suporte.formatDate(temporadaPost.dataDeSaida);
            console.log(temporadaPost);
            // this.servTemporada.editarTemporada(temporadaPost)
            //     .subscribe(result => retornoServ = result,
            //         error => console.log("Error ao atualizada a temporada!"),
            //         () => {
            //             console.log(JSON.stringify(retornoServ));
            //             if(retornoServ.status == 0){
            //                 this.suporte.showAviso(retornoServ.mensagem);
            //                 this.dismiss();
            //             }
            //             if(retornoServ.status == 1){
            //                 this.suporte.showAviso(retornoServ.mensagem);
            //             }
            //         })
        });
    };
    modalEditTemporada.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalEditTemporada;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalEditTemporada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modal-editTemporada",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalEditTemporada.html"*/'<ion-header>\n    <ion-toolbar color="topPortaria">\n        <ion-title>\n        <ion-icon name="globe"></ion-icon> Editar Temporada\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="form" (submit)="editTemporada()">                \n        <ion-list no-lines>                   \n            <ion-item-divider color="light">Período</ion-item-divider>\n\n            <ion-item>\n                <ion-label>Data de entrada:</ion-label>\n                <ion-datetime displayFormat="DD/MMM/YYYY" formControlName="dataDeEntrada" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n            </ion-item>\n\n            <ion-item>\n                <ion-label>Data de saída:</ion-label>\n                <ion-datetime displayFormat="DD/MMM/YYYY"  formControlName="dataDeSaida" cancelText="Cancelar"  doneText="ok"></ion-datetime> \n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>Descrição</ion-label>\n                <ion-textarea placeholder="Descreva uma observação sobre esta temporada" formControlName="descricao" rows="5"></ion-textarea>\n            </ion-item>\n    \n            <ion-item>\n                <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Salvar alterações</button>\n            </ion-item>  \n        </ion-list>         \n    </form> \n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalEditTemporada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceTemporada__["a" /* ServiceTemporada */]])
], modalEditTemporada);

//# sourceMappingURL=modalEditTemporada.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabRecados; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceRecado__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { SignalR } from "ng2-signalr";




var tabRecados = (function (_super) {
    __extends(tabRecados, _super);
    function tabRecados(storage, navCtrl, servRecado, fb, signalr, sp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servRecado = servRecado;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.sp = sp;
        _this.listaDeRecados = [];
        _this.form = fb.group({
            descricao: ["", __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].maxLength(144)]
        });
        return _this;
    }
    tabRecados.prototype.scrollToBottom = function () {
        this.content.scrollToBottom();
    };
    tabRecados.prototype.ionViewDidLoad = function () {
        var _this = this;
        //console.log('ionViewDidLoad mensagens');
        setTimeout(function () { return _this.scrollToBottom(); }, 1000);
    };
    tabRecados.prototype.addRecado = function () {
        var _this = this;
        var oRecado;
        var retorno;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.form.addControl("autor", new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](dataUser.nome));
            _this.form.addControl("fotoAutor", new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](dataUser.foto));
            _this.form.addControl("apartamentoId", new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](dataUser.aptId));
            oRecado = _this.form.value;
            // console.log(JSON.stringify(oRecado));
            _this.servRecado.AddNovoRecado(oRecado)
                .subscribe(function (result) { return retorno = result; }, function (error) { return console.log(retorno.mensagem); }, function () {
                if (retorno.status == 0) {
                    _this.ngOnInit();
                    _this.form.setControl("descricao", new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](""));
                    // this.scrollToBottom();
                    setTimeout(function () { return _this.scrollToBottom(); }, 1000);
                    _this.storage.get("UsuarioLogado").then(function (dataUser) {
                        var nDate = new Date();
                        var mes = nDate.getMonth() + 1;
                        var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();
                        var arr = dataUser.strApartamento.split('|');
                        //this.signalr.connect().then((cx) =>{
                        _this.signalr.connection.invoke("SendRecadoPortaria", {
                            lixeira: false,
                            visto: false,
                            apartamentoId: dataUser.aptId,
                            strApartamento: arr[1],
                            condominioID: dataUser.condominioId,
                            autor: dataUser.nome,
                            fotoAutor: dataUser.foto,
                            strDataDeCadastro: ddt,
                            descricao: oRecado.descricao
                        }).then(function (resultServ) { return console.log("Enviado com sucesso"); });
                        //})
                    });
                }
            });
        });
    };
    tabRecados.prototype.ngOnInit = function () {
        var _this = this;
        this.sp.showLoader("Carregando recados...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servRecado.listarRecados(dataUser.aptId)
                .subscribe(function (recados) { return _this.listaDeRecados = recados; }, function (error) { return console.log("Error ao obter a lista de recados"); }, function () { return _this.sp.dismissLoader(); });
        });
    };
    return tabRecados;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Content */])
], tabRecados.prototype, "content", void 0);
tabRecados = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabRecados-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabRecados.html"*/'<ion-header>\n    \n      <ion-navbar color="primary">\n        <ion-title>Recado</ion-title>\n      </ion-navbar>\n    \n    </ion-header>\n    \n    \n    <ion-content>\n      <ion-list no-lines class="setEspaco">\n        <ion-item text-wrap *ngFor="let recado of listaDeRecados">\n          <span class="bubble">\n            <span class="dataMsg">{{recado.strDataDeCadastro}}</span> <br /> {{recado.descricao}}\n          </span>\n\n          <ion-list no-lines class="setEspaco" *ngIf="recado.listaDeRespostas">\n            <ion-item text-wrap *ngFor="let resposta of recado.listaDeRespostas">\n                  <label class="bubble2" *ngFor="let resposta of recado.listaDeRespostas"> \n                      <span class="dataMsg"> {{recado.strDataDeCadastro}}</span> <br />{{resposta.descricao}}\n                  </label>\n            </ion-item>\n          </ion-list>\n\n\n          <!-- <span *ngIf="idUsuarioLogado == msg.usuarioId" class="bubble2"> \n            <span class="textMenor">{{msg.strDataDeCadastro}}</span><br />{{msg.descricao}}\n        </span>\n        <span *ngIf="idUsuarioLogado != msg.usuarioId" class="bubble"> \n                <span class="textMenor">{{msg.strDataDeCadastro}}</span> <br /> {{msg.descricao}}\n        </span> -->\n\n\n        </ion-item>\n      </ion-list>\n     \n\n    </ion-content>\n    <ion-footer>\n      <form [formGroup]="form" method="post" (submit)="addRecado()">\n      <ion-textarea type="text" placeholder="Deixa um recado para a portaria." class="inputLocation" rows="2" formControlName="descricao"></ion-textarea>\n      <ion-buttons end>\n        <button ion-button icon-only clear>\n          <ion-icon name="send"></ion-icon>\n        </button>\n      </ion-buttons>\n      </form>\n  </ion-footer>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/tabRecados.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceRecado__["a" /* ServiceRecado */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceSignalr__["a" /* ServiceSignalr */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */]])
], tabRecados);

//# sourceMappingURL=tabRecados.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceRecado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceRecado = (function () {
    function ServiceRecado(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceRecado Provider');
    }
    ServiceRecado.prototype.listarRecados = function (idUnidade) {
        var params = 'aptID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "recado/getRecadosApt?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceRecado.prototype.AddNovoRecado = function (oRecado) {
        console.log(oRecado);
        var params = JSON.stringify(oRecado);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'recado/postRecado', params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceRecado;
}());
ServiceRecado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceRecado);

//# sourceMappingURL=ServiceRecado.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalNovoPost; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceMural__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var modalNovoPost = (function (_super) {
    __extends(modalNovoPost, _super);
    function modalNovoPost(platform, params, navCtrl, viewCtrl, storage, servMural, sp, fb, camera, fileUp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.servMural = servMural;
        _this.sp = sp;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.form = fb.group({
            titulo: [''],
            conteudo: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    modalNovoPost.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            _this.form.addControl("img", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'Mural');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    modalNovoPost.prototype.novoPost = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("usuarioID", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.userID));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.condominioId));
            if (!_this.form.contains("img"))
                _this.form.addControl("img", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]());
            var oPost = _this.form.value;
            console.log(oPost);
            _this.servMural.AddNovoPost(oPost)
                .subscribe(function (post) { return result = post; }, function (error) { return console.log("Error ao postar no mural"); }, function () {
                if (result.id > 0) {
                    _this.dismiss();
                }
                //   var dt = new Date();
                //   var mes = dt.getMonth() + 1;
                //   var strDataDeCadastro = dt.getDate() + '/' + mes + '/' + dt.getFullYear();
                //    this.storage.get("UsuarioLogado").then((dataUser) =>{
                //             let novoItem =  {
                //                 "id": result.id,
                //                 "condominioId": dataUser.condominioId,
                //                 "usuarioId": dataUser.userID,
                //                 "userFoto": dataUser.foto,
                //                 "userNome": dataUser.nome,
                //                 "titulo": oPost.titulo,
                //                 "conteudo": oPost.conteudo,
                //                 "img": "{{urlImageServ}}mural/{{oPost.img}}",
                //                 "strDataDeCadastro": strDataDeCadastro
                //             }           
                //   });
            });
        });
    };
    modalNovoPost.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalNovoPost;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalNovoPost = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalNovoPost-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/mural/modalNovoPost.html"*/'<ion-header>\n        <ion-toolbar color="topMural">\n          <ion-title>\n            Novo Post - Mural\n          </ion-title>\n          <ion-buttons start>\n              <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n              </button>\n            </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n      \n      \n      <ion-content>\n        <form [formGroup]="form" (submit)="novoPost()">\n         \n          <ion-list no-lines>\n            <ion-item>\n                <ion-label stacked>titulo:</ion-label>\n                <ion-input formControlName="titulo"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <ion-label stacked>Comentário:</ion-label>\n              <ion-textarea placeholder="Entre com conteudo de seu post." formControlName="conteudo" rows="5"></ion-textarea>\n            </ion-item>\n                     \n            <ion-item>\n              <button ion-button round padding type="button" color="topMural" (click)="takePic(\'camera\')">Câmera</button>\n              <button ion-button round padding type="button" color="topMural"(click)="takePic(\'galeria\')">Galeria</button>\n            </ion-item>\n      \n            <ion-item *ngIf="pathPreviewImage">\n                <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n            </ion-item>\n      \n            <ion-item>\n                 <button ion-button block padding type="submit" color="topMural" [disabled]="!form.valid">Criar post</button>\n            </ion-item>\n      \n          </ion-list>\n      \n        </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/mural/modalNovoPost.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceMural__["a" /* ServiceMural */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__["a" /* Filetransferencia */]])
], modalNovoPost);

//# sourceMappingURL=modalNovoPost.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabClassificado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceClassificado__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__chat_mensagens__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__modalNovoClassificado__ = __webpack_require__(153);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var tabClassificado = (function (_super) {
    __extends(tabClassificado, _super);
    function tabClassificado(storage, navCtrl, servClassificado, servChat, suporte, modal, alertCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servClassificado = servClassificado;
        _this.servChat = servChat;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.listaDeClassificados = [];
        // this.storage.get("objCondominio").then((dataCondominio) => {
        //     this.permitePost = dataCondominio.mural;
        // })
        _this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.idUsuarioLogado = dataUser.userID;
        });
        return _this;
    }
    tabClassificado.prototype.iniciarConversa = function (convidadoId) {
        var _this = this;
        var retorno;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.suporte.showLoader("Carregando chat...");
                _this.servChat.iniciarConversa(dataUser.userID, convidadoId)
                    .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao iniciar a conversa"); }, function () {
                    var conversa;
                    _this.servChat.findConversa(retorno.status, dataUser.userID)
                        .subscribe(function (c) { return conversa = c; }, function (error) { return console.log("Error ao obter conversa"); }, function () {
                        // console.log(conversa);
                        var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_7__chat_mensagens__["a" /* mensagens */], conversa);
                        m.present();
                    });
                    _this.suporte.dismissLoader();
                });
            }
        });
    };
    tabClassificado.prototype.openModalNovoClassificado = function () {
        var modal = this.modal.create(__WEBPACK_IMPORTED_MODULE_8__modalNovoClassificado__["a" /* modalNovoClassificado */]);
        modal.present();
    };
    // permitirDeletar(userIDpost : number) : boolean{
    //     if(this.userID == userIDpost)
    //         return true;
    //     return false;
    // }
    // excluirPost(idPost : number){
    //    this.confirmRemoverPost("Excluir postagem","Tem certeza que deseja excluir este post?",idPost);  
    // }
    // confirmRemoverPost(titulo : string, msg : string,postID : number) {
    //       let confirm = this.alertCtrl.create({
    //         title: titulo,
    //         message: msg,
    //         buttons: [
    //           {
    //             text: 'Não',
    //             handler: () => {
    //               //console.log("Negativo");
    //             }
    //           },
    //           {
    //             text: 'Sim',
    //             handler: () => {
    //               let retorno : any;
    //               this.servMural.removerPost(postID)
    //                   .subscribe(result => retorno = result,
    //                   error => console.log("Error ao remover Post!"),
    //               () =>{
    //                   console.log(retorno);
    //                   if(retorno.status == 0){
    //                       this.suporte.showAviso(retorno.mensagem);
    //                       this.listarPosts();
    //                   }
    //               })
    //             }
    //           }
    //         ]
    //       });
    //       confirm.present();
    // }
    tabClassificado.prototype.ionViewDidLoad = function () {
        // this.content.scrollToBottom(300);
    };
    tabClassificado.prototype.ngOnInit = function () {
        this.getClassificados();
    };
    tabClassificado.prototype.getClassificados = function () {
        var _this = this;
        this.suporte.showLoader("Carregando classificados...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servClassificado.listarClassificados(dataUser.condominioId)
                .subscribe(function (classifi) { return _this.listaDeClassificados = classifi; }, function (error) { return console.log("Error ao obter a lista de recados"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    // openModalNovoPost(){
    //     let m = this.modal.create(modalNovoPost);
    //     m.present();
    // }
    tabClassificado.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getClassificados();
        setTimeout(function () {
            // console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    tabClassificado.prototype.doInfinite = function (infiniteScroll) {
        // console.log('Begin async operation');
        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.servMural.getMaisPosts(dataUser.condominioId,this.ultimoID)
        //             .subscribe(maisPosts => this.listMorePost = maisPosts,
        //             error => console.log("Error ao puxar mais Posts"),
        //             () =>{
        //                 for (let i = 0; i < this.listMorePost.length -1; i++) {
        //                     //console.log(this.listMorePost[i]);
        //                   this.listaDePosts.push(this.listMorePost[i]);
        //                   this.ultimoID = this.listMorePost[i].id;
        //                 }
        //             });
        // })
        setTimeout(function () {
            infiniteScroll.complete();
        }, 2500);
    };
    return tabClassificado;
}(__WEBPACK_IMPORTED_MODULE_6__base_pageBase__["a" /* PageBase */]));
tabClassificado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabClassificado-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/tabClassificado.html"*/'<ion-header>     \n        <ion-navbar color="topClassificado">\n        <ion-title>Classificados</ion-title>\n        </ion-navbar>\n    </ion-header>\n    \n    <ion-content class="bgApp">\n            \n            <ion-refresher (ionRefresh)="doRefresh($event)">\n               <ion-refresher-content></ion-refresher-content>\n            </ion-refresher>\n        \n            <ion-card *ngFor="let classificado of listaDeClassificados">\n                    \n                <ion-item>\n                    <h2>{{classificado.nomeDoUsuario}}</h2>\n                    <p>{{classificado.strDataDeCadastro}}</p>\n                </ion-item>\n\n                <img src=\'{{urlImageServ}}classificado/{{classificado.foto}}\' *ngIf="classificado.foto">\n            \n                <ion-card-content>\n                    <p *ngIf="classificado.chamada">{{classificado.chamada}}</p>\n                    <p>{{classificado.descricao}}</p>\n                </ion-card-content>\n    \n                <ion-row>\n                    <ion-col>\n                        <button ion-button icon-left clear small (click)="iniciarConversa(classificado.usuarioId)" *ngIf="classificado.usuarioId != idUsuarioLogado">\n                            <ion-icon name="chatbubbles"></ion-icon>\n                            <div>Chat</div>\n                        </button>\n                    </ion-col>\n                    <ion-col>\n                      \n                    </ion-col>\n                    <ion-col center text-center>\n                      <ion-note color="topClassificado">\n                       {{classificado.strPreco}}\n                      </ion-note>\n                    </ion-col>\n                </ion-row>\n\n            \n            </ion-card>\n    \n            <!-- <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n                 <ion-infinite-scroll-content></ion-infinite-scroll-content>\n            </ion-infinite-scroll> -->\n            <ion-fab right bottom>\n                    <button ion-fab color="topClassificado" (click)="openModalNovoClassificado()"><ion-icon name="add"></ion-icon></button>\n                    <!--<ion-fab-list side="left">\n                        <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n                        <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n                        <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n                        <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n                    </ion-fab-list>-->\n                </ion-fab>\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/tabClassificado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceClassificado__["a" /* ServiceClassificado */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceChat__["a" /* ServiceChat */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], tabClassificado);

//# sourceMappingURL=tabClassificado.js.map

/***/ }),

/***/ 408:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabMeusAnuncios; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceClassificado__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalNovoClassificado__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalEditClassificado__ = __webpack_require__(409);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabMeusAnuncios = (function (_super) {
    __extends(tabMeusAnuncios, _super);
    function tabMeusAnuncios(storage, navCtrl, servClassificado, suporte, modal, alertCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.servClassificado = servClassificado;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.listaDeAnuncios = [];
        return _this;
        // this.storage.get("objCondominio").then((dataCondominio) => {
        //     this.permitePost = dataCondominio.mural;
        // })
        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.userID = dataUser.userID;
        // })
    }
    tabMeusAnuncios.prototype.openModalNovoClassificado = function () {
        var modal = this.modal.create(__WEBPACK_IMPORTED_MODULE_6__modalNovoClassificado__["a" /* modalNovoClassificado */]);
        modal.present();
    };
    tabMeusAnuncios.prototype.editClassificado = function (obj) {
        var modalEdit = this.modal.create(__WEBPACK_IMPORTED_MODULE_7__modalEditClassificado__["a" /* modalEditClassificado */], obj);
        modalEdit.present();
    };
    tabMeusAnuncios.prototype.excluirAnuncio = function (idAnuncio) {
        this.confirmRemoverAnuncio("Excluir Anúncio", "Tem certeza que deseja excluir este anúncio?", idAnuncio);
    };
    tabMeusAnuncios.prototype.confirmRemoverAnuncio = function (titulo, msg, anuncioID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servClassificado.removerClassificado(anuncioID)
                            .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao remover Classificado!"); }, function () {
                            //console.log(retorno);
                            if (retorno.status == 0) {
                                _this.suporte.showAviso(retorno.mensagem);
                                _this.getMeusAnuncios();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabMeusAnuncios.prototype.ionViewDidLoad = function () {
        // this.content.scrollToBottom(300);
    };
    tabMeusAnuncios.prototype.ngOnInit = function () {
        this.getMeusAnuncios();
    };
    tabMeusAnuncios.prototype.getMeusAnuncios = function () {
        var _this = this;
        this.suporte.showLoader("Carregando Anúncios...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servClassificado.listarMeusAnuncios(dataUser.userID)
                .subscribe(function (classifi) { return _this.listaDeAnuncios = classifi; }, function (error) { return console.log("Error ao obter a lista de recados"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    // openModalNovoPost(){
    //     let m = this.modal.create(modalNovoPost);
    //     m.present();
    // }
    tabMeusAnuncios.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getMeusAnuncios();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    tabMeusAnuncios.prototype.doInfinite = function (infiniteScroll) {
        // console.log('Begin async operation');
        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.servMural.getMaisPosts(dataUser.condominioId,this.ultimoID)
        //             .subscribe(maisPosts => this.listMorePost = maisPosts,
        //             error => console.log("Error ao puxar mais Posts"),
        //             () =>{
        //                 for (let i = 0; i < this.listMorePost.length -1; i++) {
        //                     //console.log(this.listMorePost[i]);
        //                   this.listaDePosts.push(this.listMorePost[i]);
        //                   this.ultimoID = this.listMorePost[i].id;
        //                 }
        //             });
        // })
        setTimeout(function () {
            infiniteScroll.complete();
        }, 2500);
    };
    return tabMeusAnuncios;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabMeusAnuncios = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabMeusAnuncios-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/tabMeusAnuncios.html"*/'<ion-header>     \n        <ion-navbar color="topClassificado">\n        <ion-title>Classificados</ion-title>\n        </ion-navbar>\n    </ion-header>\n    \n    <ion-content class="bgApp">\n            \n            <ion-refresher (ionRefresh)="doRefresh($event)">\n               <ion-refresher-content></ion-refresher-content>\n            </ion-refresher> \n        \n            <ion-card *ngFor="let classificado of listaDeAnuncios">\n                    \n                <ion-item>\n                    <h2>{{classificado.nomeDoUsuario}}</h2>\n                    <p *ngIf="classificado.chamada">{{classificado.chamada}}</p>\n                </ion-item>\n\n                <img src=\'{{urlImageServ}}classificado/{{classificado.foto}}\' *ngIf="classificado.foto">\n            \n                <ion-card-content>\n                <p>{{classificado.descricao}}</p>\n                </ion-card-content>\n    \n                <ion-row>\n                    <ion-col>\n                      <button ion-button icon-left clear small (click)="excluirAnuncio(classificado.id)">\n                        <ion-icon name="trash"></ion-icon>\n                        <div>Excluir</div>\n                      </button>\n                    </ion-col>\n                    <ion-col>\n                       <button ion-button icon-left clear small (click)="editClassificado(classificado)">\n                        <ion-icon name="create"></ion-icon>\n                        <div>Editar</div>\n                      </button> \n                    </ion-col>\n                    <ion-col center text-center>\n                      <ion-note color="topClassificado">\n                       {{classificado.strPreco}}\n                      </ion-note>\n                    </ion-col>\n                </ion-row>\n           \n            </ion-card>\n    \n            <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n                 <ion-infinite-scroll-content></ion-infinite-scroll-content>\n            </ion-infinite-scroll> \n    \n            <ion-fab right bottom>\n                <button ion-fab color="topClassificado" (click)="openModalNovoClassificado()"><ion-icon name="add"></ion-icon></button>\n                <!--<ion-fab-list side="left">\n                    <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n                    <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n                    <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n                    <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n                </ion-fab-list>-->\n            </ion-fab>\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/tabMeusAnuncios.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceClassificado__["a" /* ServiceClassificado */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], tabMeusAnuncios);

//# sourceMappingURL=tabMeusAnuncios.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalEditClassificado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceClassificado__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var modalEditClassificado = (function (_super) {
    __extends(modalEditClassificado, _super);
    function modalEditClassificado(platform, params, navCtrl, viewCtrl, storage, servClassificado, sp, fb, camera, fileUp) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.servClassificado = servClassificado;
        _this.sp = sp;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.oClassificado = _this.params.data;
        _this.form = fb.group({
            id: [_this.oClassificado.id],
            str2Preco: [_this.oClassificado.str2Preco, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].pattern("^(([0-9]*)|(([0-9]*)\,([0-9]*)))$")],
            chamada: [_this.oClassificado.chamada],
            descricao: [_this.oClassificado.descricao, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            foto: [_this.oClassificado.foto]
        });
        return _this;
    }
    modalEditClassificado.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'Classificado');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    modalEditClassificado.prototype.editAnuncio = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("usuarioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.userID));
            _this.form.addControl("condominioId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.condominioId));
            if (!_this.form.contains("foto"))
                _this.form.addControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */]());
            var oAnuncio = _this.form.value;
            //console.log(oAnuncio);
            _this.servClassificado.editarClassificado(oAnuncio)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao Editar"); }, function () {
                if (result.status == 0) {
                    _this.sp.showAviso(result.mensagem);
                    _this.dismiss();
                }
            });
        });
    };
    modalEditClassificado.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalEditClassificado;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalEditClassificado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalEditClassificado-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/modalEditClassificado.html"*/'<ion-header>\n        <ion-toolbar color="topClassificado">\n          <ion-title>\n            Editar anúncio - Classificado\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n            \n      <ion-content>\n\n        <ion-toolbar *ngIf="oClassificado.foto">\n            <img src=\'{{urlImageServ}}classificado/{{oClassificado.foto}}\' class="thumb">\n        </ion-toolbar>\n\n        <form [formGroup]="form" (submit)="editAnuncio()">\n         \n          <ion-list no-lines>\n            <ion-item>\n                <ion-label stacked>Título:</ion-label>\n                <ion-input formControlName="chamada"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <ion-label stacked>Descrição:</ion-label>\n              <ion-textarea placeholder="Conteúdo do anúncio" formControlName="descricao" rows="5"></ion-textarea>\n            </ion-item>\n               \n            <ion-item>\n                <ion-label stacked>Preço:</ion-label>\n                <ion-input formControlName="str2Preco"></ion-input>\n            </ion-item>\n\n            <ion-item>\n              <button ion-button round padding type="button" color="topClassificado" (click)="takePic(\'camera\')">Câmera</button>\n              <button ion-button round padding type="button" color="topClassificado"(click)="takePic(\'galeria\')">Galeria</button>\n            </ion-item>\n      \n            <ion-item *ngIf="pathPreviewImage">\n                <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n            </ion-item>\n            \n      \n            <ion-item>\n                 <button ion-button block padding type="submit" color="topClassificado" [disabled]="!form.valid">Salvar alterações</button>\n            </ion-item>\n      \n          </ion-list>\n      \n        </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/classificado/modalEditClassificado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceClassificado__["a" /* ServiceClassificado */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_7__app_app_filetransferencia__["a" /* Filetransferencia */]])
], modalEditClassificado);

//# sourceMappingURL=modalEditClassificado.js.map

/***/ }),

/***/ 410:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return enqueteDetails; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var enqueteDetails = (function (_super) {
    __extends(enqueteDetails, _super);
    function enqueteDetails(storage, navCtrl, param, servEnquete, suporte, viewCtrl, modal) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.param = param;
        _this.servEnquete = servEnquete;
        _this.suporte = suporte;
        _this.viewCtrl = viewCtrl;
        _this.modal = modal;
        _this.doughnutChartType = 'doughnut';
        _this.doughnutChartLabels = [];
        _this.doughnutChartData = [];
        return _this;
    }
    enqueteDetails.prototype.ionViewDidLoad = function () {
        //this.content.scrollToBottom(300);
    };
    enqueteDetails.prototype.ngOnInit = function () {
        this.oEnquete = this.param.data;
        for (var i = 0; i < this.oEnquete.alternativasModel.length; i++) {
            this.doughnutChartLabels.push(this.oEnquete.alternativasModel[i].descricao);
            this.doughnutChartData.push(this.oEnquete.alternativasModel[i].qtdVotos);
        }
        //console.log(this.oEnquete);
    };
    enqueteDetails.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return enqueteDetails;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
enqueteDetails = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'enqueteDetails-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enqueteDetails.html"*/'<ion-header>\n    <ion-toolbar color="topEnquete">\n        <ion-title>\n          Enquete - detalhes\n        </ion-title>\n        <ion-buttons start>\n          <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n         </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>             \n    \n    <ion-card>      \n        <ion-item>             \n            <p>\n                <ion-icon name="calendar"></ion-icon> <strong>De:</strong> {{oEnquete.dataInicial}} <br />\n                <ion-icon name="calendar"></ion-icon> <strong>Até:</strong> {{oEnquete.dataFinal}}\n            </p>\n        </ion-item>\n\n        <ion-card-content>\n            <p>{{oEnquete.descricao}}</p>\n            <br />\n            <!-- <ion-list>\n                <ion-item text-wrap *ngFor="let alternativa of oEnquete.alternativasModel">\n                    <span> {{alternativa.descricao}}</span> - <span> {{alternativa.qtdVotos}}</span>\n                </ion-item>\n            </ion-list> -->\n\n\n            <canvas baseChart \n                [data]="doughnutChartData"\n                [labels]="doughnutChartLabels"\n                [chartType]="doughnutChartType"\n                width="150"\n                height="150"></canvas>\n\n        </ion-card-content>\n\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enqueteDetails.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__["a" /* ServiceEnquete */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], enqueteDetails);

//# sourceMappingURL=enqueteDetails.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabClassificadoVagas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVaga__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalChatVaga__ = __webpack_require__(157);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabClassificadoVagas = (function (_super) {
    __extends(tabClassificadoVagas, _super);
    function tabClassificadoVagas(storage, navCtrl, suporte, modal, alertCtrl, servVaga, servChat) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servVaga = servVaga;
        _this.servChat = servChat;
        _this.vagaDisponibilizadasList = [];
        return _this;
    }
    tabClassificadoVagas.prototype.ngOnInit = function () {
        this.getVagasDisponibilizadas();
    };
    tabClassificadoVagas.prototype.getVagasDisponibilizadas = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando classificados de vaga...");
            _this.servVaga.ListarVagasDisponibilizadas(dataUser.condominioId)
                .subscribe(function (listVagas) { return _this.vagaDisponibilizadasList = listVagas; }, function (error) { return console.log("Error em obter classificado de vagas"); }, function () {
                //console.log(this.vagaDisponibilizadasList);
                _this.suporte.dismissLoader();
            });
        });
    };
    tabClassificadoVagas.prototype.requisitarVaga = function (unidadeID) {
        this.confirmSolicitacaoDeVaga("Solicitação de Vaga", "Deseja enviar uma solicitação desta vaga para a unidade em questão?", unidadeID);
    };
    tabClassificadoVagas.prototype.confirmSolicitacaoDeVaga = function (titulo, msg, unidadeID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.storage.get("UsuarioLogado").then(function (dataUser) {
                            if (unidadeID != dataUser.aptId) {
                                _this.servVaga.enviarSolicitacao(unidadeID, dataUser.aptId, dataUser.userID)
                                    .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao remover Temporada!"); }, function () {
                                    //console.log(retorno);
                                    if (retorno.status == 0) {
                                        _this.suporte.showAviso(retorno.mensagem);
                                    }
                                    if (retorno.status == 1) {
                                        _this.suporte.showAlert("Aviso", retorno.mensagem);
                                    }
                                });
                            }
                            else {
                                _this.suporte.showAviso("Ops, Você não pode requisitar sua própria vaga.");
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabClassificadoVagas.prototype.presentActionSheetClientes = function (idUnidade) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_7__modalChatVaga__["a" /* modalChatVaga */], { idAptId: idUnidade, usuarioID: dataUser.userID });
                m.present();
            }
        });
    };
    tabClassificadoVagas.prototype.doRefresh = function (refresher) {
        this.getVagasDisponibilizadas();
        setTimeout(function () {
            refresher.complete();
        }, 2000);
    };
    return tabClassificadoVagas;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabClassificadoVagas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabClassificadoVagas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabClassificadoVagas.html"*/'<ion-header>\n    \n      <ion-navbar color="primary">\n        <ion-title>Classificados</ion-title>\n      </ion-navbar>\n    \n    </ion-header>\n    \n    \n    <ion-content>\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n            <ion-refresher-content></ion-refresher-content>\n        </ion-refresher>\n\n        <ion-toolbar color="subVagas">\n            <ion-row>\n                <ion-col width-100 style="text-align:center;color:#fff">Classificado de Vagas <br />Deslize para a esquerda para chamar no chat.</ion-col>\n            </ion-row>  \n        </ion-toolbar>\n        \n        <ion-list>    \n           <ion-item-sliding *ngFor="let vaga of vagaDisponibilizadasList">\n              <ion-item text-wrap>\n                  <ion-avatar item-start class="numUnidade">\n                     Unidade<br /> {{vaga.numeroApt}}\n                  </ion-avatar>\n                  <p>Vaga disponibilizada pela unidade {{vaga.numeroApt}} do {{vaga.strGrupo}}</p>\n                  <p class="txtMenor">Vaga disponível para negociação</p>\n                  <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n              </ion-item> \n              <ion-item-options side="right">\n                    <button ion-button color="secondary" (click)="requisitarVaga(vaga.apartamentoID, vaga.id)">\n                        <ion-icon name="swap"></ion-icon>\n                        Solicitar\n                    </button>\n                  <button ion-button color="primary" (click)="presentActionSheetClientes(vaga.apartamentoID)">\n                      <ion-icon name="chatbubbles"></ion-icon>\n                      Chat\n                  </button>\n              </ion-item-options>\n      </ion-item-sliding> \n       </ion-list> \n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabClassificadoVagas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVaga__["a" /* ServiceVaga */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceChat__["a" /* ServiceChat */]])
], tabClassificadoVagas);

//# sourceMappingURL=tabClassificadoVagas.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalAddVeiculo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVeiculo__ = __webpack_require__(87);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var modalAddVeiculo = (function (_super) {
    __extends(modalAddVeiculo, _super);
    function modalAddVeiculo(platform, params, navCtrl, viewCtrl, storage, sp, fb, servVeiculo) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.sp = sp;
        _this.fb = fb;
        _this.servVeiculo = servVeiculo;
        _this.form = fb.group({
            modelo: [''],
            cor: [''],
            Placa: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    modalAddVeiculo.prototype.novoVeiculo = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.form.addControl("apartamentoId", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](data.aptId));
            var oVeiculo = _this.form.value;
            //console.log(oVeiculo);
            _this.servVeiculo.addVeiculo(oVeiculo)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("error ao postar novo veiculo"); }, function () {
                if (result.status == 0) {
                    _this.form.reset();
                    _this.dismiss();
                    _this.sp.showAviso(result.mensagem);
                }
                if (result.status == 1) {
                    _this.sp.showAviso(result.mensagem);
                }
                if (result.status == -1) {
                    _this.sp.showAviso("Serviço indisponível no momento, favor tente mais tarde!");
                }
            });
        });
    };
    modalAddVeiculo.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalAddVeiculo;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
modalAddVeiculo = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalAddVeiculo-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalAddVeiculo.html"*/'<ion-header>\n        <ion-toolbar color="dark">\n          <ion-title>\n            Novo Veículo\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n      \n      \n      <ion-content>\n        <form [formGroup]="form" (submit)="novoVeiculo()">\n         \n          <ion-list no-lines>\n\n            <ion-item>\n                <ion-label stacked>Modelo:</ion-label>\n                <ion-input formControlName="modelo"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>Cor:</ion-label>\n                <ion-input formControlName="cor"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>Placa:</ion-label>\n                <ion-input formControlName="Placa"></ion-input>\n            </ion-item>            \n      \n            <ion-item>\n                 <button ion-button block padding type="submit" color="dark" [disabled]="!form.valid">Criar veículo</button>\n            </ion-item>\n      \n          </ion-list>\n      \n        </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalAddVeiculo.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVeiculo__["a" /* ServiceVeiculo */]])
], modalAddVeiculo);

//# sourceMappingURL=modalAddVeiculo.js.map

/***/ }),

/***/ 413:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalEditVeiculo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVeiculo__ = __webpack_require__(87);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var modalEditVeiculo = (function (_super) {
    __extends(modalEditVeiculo, _super);
    function modalEditVeiculo(platform, params, navCtrl, viewCtrl, storage, sp, fb, servVeiculo) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.platform = platform;
        _this.params = params;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.storage = storage;
        _this.sp = sp;
        _this.fb = fb;
        _this.servVeiculo = servVeiculo;
        _this.oVeiculo = _this.params.data;
        _this.form = fb.group({
            id: [_this.oVeiculo.id],
            apartamentoId: [_this.oVeiculo.apartamentoId],
            modelo: [_this.oVeiculo.modelo],
            cor: [_this.oVeiculo.cor],
            Placa: [_this.oVeiculo.placa, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    modalEditVeiculo.prototype.editVeiculo = function () {
        var _this = this;
        var result;
        this.storage.get("UsuarioLogado").then(function (data) {
            var oVeiculo = _this.form.value;
            console.log(oVeiculo);
            _this.servVeiculo.editVeiculo(oVeiculo)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("error ao editar veiculo"); }, function () {
                if (result.status == 0) {
                    _this.form.reset();
                    _this.dismiss();
                    _this.sp.showAviso(result.mensagem);
                }
                if (result.status == -1) {
                    _this.sp.showAviso("Serviço indisponível no momento, favor tente mais tarde!");
                }
            });
        });
    };
    modalEditVeiculo.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalEditVeiculo;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
modalEditVeiculo = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalEditVeiculo-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalEditVeiculo.html"*/'<ion-header>\n        <ion-toolbar color="dark">\n          <ion-title>\n            Editar Veículo\n          </ion-title>\n          <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n              <span ion-text color="light" showWhen="ios">Voltar</span>\n              <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n          </ion-buttons>\n        </ion-toolbar>\n      </ion-header>\n      \n      \n      <ion-content>\n        <form [formGroup]="form" (submit)="editVeiculo()">\n         \n          <ion-list no-lines>\n\n            <ion-item>\n                <ion-label stacked>Marca / Modelo:</ion-label>\n                <ion-input formControlName="modelo"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>Cor:</ion-label>\n                <ion-input formControlName="cor"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked>Placa:</ion-label>\n                <ion-input formControlName="Placa"></ion-input>\n            </ion-item>            \n      \n            <ion-item>\n                 <button ion-button block padding type="submit" color="dark" [disabled]="!form.valid">Salvar alterações</button>\n            </ion-item>\n      \n          </ion-list>\n      \n        </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/modalEditVeiculo.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVeiculo__["a" /* ServiceVeiculo */]])
], modalEditVeiculo);

//# sourceMappingURL=modalEditVeiculo.js.map

/***/ }),

/***/ 414:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabVagas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVaga__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__transferencia_transferencia__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__transferencia_recebidas__ = __webpack_require__(419);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabVagas = (function (_super) {
    __extends(tabVagas, _super);
    function tabVagas(storage, navCtrl, suporte, modal, alertCtrl, servVaga) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servVaga = servVaga;
        _this.listaDeVagasDisplay = [];
        return _this;
    }
    tabVagas.prototype.ngOnInit = function () {
        this.getMinhasVagas();
    };
    tabVagas.prototype.openModalTransferencia = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_6__transferencia_transferencia__["a" /* transferencia */]);
        m.present();
    };
    tabVagas.prototype.openModalRecebidas = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_7__transferencia_recebidas__["a" /* recebidas */]);
        m.present();
    };
    tabVagas.prototype.getMinhasVagas = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando minhas vagas...");
            _this.servVaga.ListarMinhasVagas(dataUser.aptId)
                .subscribe(function (list) { return _this.listaDeVagasDisplay = list; }, function (error) { return console.log("error em obter minha vagas"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    tabVagas.prototype.disponibilizarVaga = function (apID) {
        this.confirmDisponibilizarVaga("Vagas", "Deseja disponibilizar esta vaga no classificado?", apID);
    };
    tabVagas.prototype.confirmDisponibilizarVaga = function (titulo, msg, aptID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var result;
                        _this.servVaga.disponibilizarVaga(aptID)
                            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao disponibilizar a Vaga"); }, function () {
                            if (result.status == 0) {
                                _this.getMinhasVagas();
                                _this.suporte.showAviso(result.mensagem);
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabVagas.prototype.retomarMinhaVaga = function (vagaId) {
        this.confirmRetomarVaga("Vagas", "Tem certeza que deseja retomar sua vaga, se a mesma estiver transferida para outra Unidade, esta tranferência será cancelada.", vagaId);
    };
    tabVagas.prototype.confirmRetomarVaga = function (titulo, msg, idVaga) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var result;
                        _this.servVaga.retomarVaga(idVaga)
                            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao retomar a Vaga"); }, function () {
                            if (result.status == 0) {
                                _this.getMinhasVagas();
                                _this.suporte.showAviso(result.mensagem);
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabVagas.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getMinhasVagas();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabVagas;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabVagas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabVagas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabVagas.html"*/'<ion-header>\n    \n      <ion-navbar>\n        <ion-title>Controle de Vagas</ion-title>\n      </ion-navbar>\n    \n    </ion-header>\n    \n    \n    <ion-content>\n        <ion-toolbar color="subVagas">\n            <ion-row>\n                <ion-col width-100 style="text-align:center;color:#fff">Deslize para a esquerda para mais opções.</ion-col>\n            </ion-row>  \n        </ion-toolbar>\n       <ion-toolbar color="light">\n            <ion-row>\n                <ion-col>\n                    <button ion-button color="primary" clear block small  class="text-on-bottom">\n                      <ion-icon name="swap"></ion-icon><label>Minhas</label></button>       \n                </ion-col>\n                <ion-col>\n                 <button ion-button color="secondary" clear block small class="text-on-bottom" (click)="openModalRecebidas()">\n                   <ion-icon name="swap"></ion-icon><label>Recebidas</label></button>            \n                </ion-col>\n                <ion-col center text-center>\n                    <button ion-button color="vagaCedida" clear block small class="text-on-bottom" (click)="openModalTransferencia()">\n                      <ion-icon name="swap"></ion-icon><label>Requisições</label></button> \n                </ion-col>\n              </ion-row>\n        </ion-toolbar>\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n                <ion-refresher-content></ion-refresher-content>\n             </ion-refresher>\n        <ion-list>\n           <ion-item-sliding *ngFor="let vaga of listaDeVagasDisplay">\n              <ion-item text-wrap>\n                  <ion-avatar item-start class="numUnidade">\n                      <ion-icon name="car"></ion-icon>\n                  </ion-avatar>\n                  <p>Vaga pertencente a unidade</p>\n                  <p *ngIf="vaga.ocupado" class="vagaOcupada"> <ion-icon name="car"></ion-icon> Ocupada</p>\n                  <p *ngIf="!vaga.ocupado&&!vaga.transferido" class="vagaLivre"> <ion-icon name="car"></ion-icon> Livre</p>\n                  <p *ngIf="vaga.transferido" class="vagaTransferidaParaOutraUnidade"> <ion-icon name="car"></ion-icon> Transferida para outra unidade.</p>\n                  <p class="txtMenor" *ngIf="vaga.disponibilizada" class="disponivelParaNegociar">Disponível para negociação</p>\n                  <ion-icon name="ios-arrow-back-outline" item-end item-right small *ngIf="(!vaga.ocupado&&!vaga.disponibilizada&&!vaga.transferido)||(vaga.disponibilizada||vaga.transferido)"  ></ion-icon>\n              </ion-item> \n              <ion-item-options side="right">\n                  <button ion-button color="primary" *ngIf="!vaga.ocupado&&!vaga.disponibilizada&&!vaga.transferido"  (click)="disponibilizarVaga(vaga.apartamentoID)">\n                      <ion-icon name="redo"></ion-icon>\n                      Anunciar\n                  </button>\n                  <button ion-button color="secondary" *ngIf="vaga.disponibilizada||vaga.transferido" (click)="retomarMinhaVaga(vaga.id)">\n                      <ion-icon name="swap"></ion-icon>\n                      Retomar\n                  </button>\n              </ion-item-options>\n      </ion-item-sliding> \n       </ion-list>\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabVagas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVaga__["a" /* ServiceVaga */]])
], tabVagas);

//# sourceMappingURL=tabVagas.js.map

/***/ }),

/***/ 415:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return transferencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabSolicitacoes__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabTransferidas__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__tabRequisicoes__ = __webpack_require__(418);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var transferencia = (function (_super) {
    __extends(transferencia, _super);
    function transferencia(navCtrl, navParams, _storage, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this._storage = _storage;
        _this.viewCtrl = viewCtrl;
        _this.solicitacoesTab = __WEBPACK_IMPORTED_MODULE_4__tabSolicitacoes__["a" /* tabSolicitacoes */];
        _this.transferidasTab = __WEBPACK_IMPORTED_MODULE_5__tabTransferidas__["a" /* tabTransferidas */];
        _this.requisicoesTab = __WEBPACK_IMPORTED_MODULE_6__tabRequisicoes__["a" /* tabRequisicoes */];
        return _this;
    }
    transferencia.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    transferencia.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return transferencia;
}(__WEBPACK_IMPORTED_MODULE_3__base_pageBase__["a" /* PageBase */]));
transferencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'transferencia-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/transferencia.html"*/'<ion-header>\n<ion-toolbar color="vagaCedida">\n    <ion-title>\n        <ion-icon name="swap"></ion-icon> Transferências\n    </ion-title>\n    <ion-buttons start>\n    <button ion-button (click)="dismiss()">\n        <span ion-text color="light" showWhen="ios">Voltar</span>\n        <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n    </button>\n    </ion-buttons>\n</ion-toolbar>\n</ion-header>\n\n      \n<ion-tabs>\n    <ion-tab tabTitle="Solicitações" tabIcon="swap" [root]="solicitacoesTab"></ion-tab>\n    <ion-tab tabTitle="Transferidas" tabIcon="swap" [root]="transferidasTab"></ion-tab>\n    <ion-tab tabTitle="Requisições" tabIcon="swap" [root]="requisicoesTab"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/transferencia.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */]])
], transferencia);

//# sourceMappingURL=transferencia.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabSolicitacoes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceTransferencia__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__vaga_modalChatVaga__ = __webpack_require__(157);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabSolicitacoes = (function (_super) {
    __extends(tabSolicitacoes, _super);
    function tabSolicitacoes(storage, navCtrl, suporte, modal, alertCtrl, viewCtrl, servTransferencia, signalr) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.viewCtrl = viewCtrl;
        _this.servTransferencia = servTransferencia;
        _this.signalr = signalr;
        _this.solicitacoesList = [];
        return _this;
    }
    tabSolicitacoes.prototype.ngOnInit = function () {
        this.getSolicitacoes();
    };
    tabSolicitacoes.prototype.getSolicitacoes = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando solicitações de vaga...");
            _this.servTransferencia.listarSolicitacoesPendentes(dataUser.aptId)
                .subscribe(function (list) { return _this.solicitacoesList = list; }, function (error) { return console.log("Error em obter solicitacoes"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    tabSolicitacoes.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    tabSolicitacoes.prototype.transferirVaga = function (transferenciaID) {
        this.confirmTransferenciaDeVaga("Transferência de Vaga", "Tem certeza que deseja transferir esta vaga para a unidade em questão?", transferenciaID);
    };
    tabSolicitacoes.prototype.confirmTransferenciaDeVaga = function (titulo, msg, transferenciaID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.storage.get("UsuarioLogado").then(function (dataUser) {
                            _this.servTransferencia.transferirVaga(transferenciaID, dataUser.userID)
                                .subscribe(function (r) { return retorno = r; }, function (error) { return console.log("Falha ao Transferir sua Vaga"); }, function () {
                                if (retorno.status == 0) {
                                    _this.getSolicitacoes();
                                    _this.suporte.showAviso(retorno.mensagem);
                                    _this.signalr.connect().then(function (c) {
                                        c.invoke("SendVagaTransferida", dataUser.condominioId, transferenciaID).then(function (result) { return console.log("Mensagem de Transferencia de Vaga enviada."); });
                                    });
                                }
                                if (retorno.status == 1) {
                                    _this.suporte.showAviso(retorno.mensagem);
                                }
                            });
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabSolicitacoes.prototype.presentActionSheetClientes = function (idUnidade) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_7__vaga_modalChatVaga__["a" /* modalChatVaga */], { idAptId: idUnidade, usuarioID: dataUser.userID });
                m.present();
            }
        });
    };
    tabSolicitacoes.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getSolicitacoes();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabSolicitacoes;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabSolicitacoes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabSolicitacoes-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabSolicitacoes.html"*/'<ion-header>\n        \n    <ion-navbar color="primary">\n    <ion-title>Solicitações</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-toolbar color="subTransferencia">\n        <ion-row>\n            <ion-col width-100 style="text-align:center;color:#fff">Deslize para a esquerda para mais opções.</ion-col>\n        </ion-row>  \n    </ion-toolbar>\n    <ion-list>\n        <ion-item-sliding *ngFor="let requisicao of solicitacoesList">\n            <ion-item text-wrap>\n                <ion-avatar item-start class="numUnidade">\n                   <ion-icon name="swap"></ion-icon> Unidade<br /> {{requisicao.numeroApt}}\n                </ion-avatar>\n                <p>A Unidade {{requisicao.numeroApt}} - {{requisicao.strGrupo}} solicitou a vaga que você disponibilizou.</p>\n                <p class="txtMenor">Requisição de vaga</p>\n                <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n            </ion-item> \n            <ion-item-options side="right">\n                <button ion-button color="secondary" (click)="transferirVaga(requisicao.id)">\n                    <ion-icon name="swap"></ion-icon>\n                    Ceder vaga\n                </button>\n                <button ion-button color="primary" (click)="presentActionSheetClientes(requisicao.apartamentoId)">\n                    <ion-icon name="chatbubbles"></ion-icon>\n                    Chat\n                </button>\n            </ion-item-options>\n    </ion-item-sliding> \n   \n    </ion-list> \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabSolicitacoes.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceTransferencia__["a" /* ServiceTransferencia */],
        __WEBPACK_IMPORTED_MODULE_3_ng2_signalr__["b" /* SignalR */]])
], tabSolicitacoes);

//# sourceMappingURL=tabSolicitacoes.js.map

/***/ }),

/***/ 417:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabTransferidas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__ = __webpack_require__(71);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var tabTransferidas = (function (_super) {
    __extends(tabTransferidas, _super);
    function tabTransferidas(storage, navCtrl, suporte, modal, alertCtrl, servTransferencia) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servTransferencia = servTransferencia;
        _this.transferenciasConfirmadasList = [];
        return _this;
    }
    tabTransferidas.prototype.ngOnInit = function () {
        this.getTransferenciasCedidas();
    };
    tabTransferidas.prototype.getTransferenciasCedidas = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando transferidas...");
            _this.servTransferencia.listarTransferenciasCedidasConfirmadas(dataUser.aptId)
                .subscribe(function (r) { return _this.transferenciasConfirmadasList = r; }, function (error) { return console.log("Error ao Listar Transferidas"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    // requisitarVaga(unidadeID : number){
    //     this.confirmSolicitacaoDeVaga("Solicitação de Vaga","Deseja enviar uma solicitação desta vaga para a unidade em questão?",unidadeID);  
    //   }
    // confirmSolicitacaoDeVaga(titulo : string, msg : string, unidadeID : number) {
    //     let confirm = this.alertCtrl.create({
    //     title: titulo,
    //     message: msg,
    //     buttons: [
    //             {
    //                 text: 'Não',
    //                 handler: () => {
    //                 //console.log("Negativo");
    //                 }
    //             },
    //             {
    //                 text: 'Sim',
    //                 handler: () => {
    //                   let retorno : any;
    //                   this.storage.get("UsuarioLogado").then((dataUser) =>{
    //                     if (unidadeID != dataUser.aptId)
    //                     {
    //                         this.servVaga.enviarSolicitacao(unidadeID,dataUser.aptId)
    //                             .subscribe(result => retorno = result,
    //                             error => console.log("Error ao remover Temporada!"),
    //                         () =>{
    //                             //console.log(retorno);
    //                             if(retorno.status == 0){
    //                                 this.suporte.showAviso(retorno.mensagem);
    //                             }
    //                             if(retorno.status == 1){
    //                                 this.suporte.showAlert("Aviso",retorno.mensagem);
    //                             }
    //                         })
    //                     }
    //                     else
    //                     {
    //                         this.suporte.showAviso("Ops, Você não pode requisitar sua própria vaga.");
    //                     }
    //                   })
    //                 }
    //             }
    //         ]
    //     });
    //     confirm.present();
    // }
    tabTransferidas.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getTransferenciasCedidas();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabTransferidas;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabTransferidas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabTransferidas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabTransferidas.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title>Tranferidas</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n\n    <ion-list>\n        <ion-item text-wrap *ngFor="let t of transferenciasConfirmadasList">\n            <ion-avatar item-start class="numUnidade">\n                Unidade<br /> {{t.numeroApt}}\n            </ion-avatar>\n            <p>Vaga em uso pela unidade {{t.numeroApt}} - {{t.strGrupo}}</p>\n            <p class="txtMenor">Transferência ativa</p>\n        </ion-item> \n    </ion-list> \n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabTransferidas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__["a" /* ServiceTransferencia */]])
], tabTransferidas);

//# sourceMappingURL=tabTransferidas.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabRequisicoes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__ = __webpack_require__(71);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var tabRequisicoes = (function (_super) {
    __extends(tabRequisicoes, _super);
    function tabRequisicoes(storage, navCtrl, suporte, modal, alertCtrl, servTransferencia) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servTransferencia = servTransferencia;
        _this.requisicoesDeVagas = [];
        return _this;
    }
    tabRequisicoes.prototype.ngOnInit = function () {
        this.getRequisicoes();
    };
    tabRequisicoes.prototype.getRequisicoes = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando minhas requisições...");
            _this.servTransferencia.listarRequisicoesDeTransferencias(dataUser.aptId)
                .subscribe(function (r) { return _this.requisicoesDeVagas = r; }, function (error) { return console.log("Error ao Listar requisições"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    tabRequisicoes.prototype.excluirRequisicao = function (trasnID) {
        this.confirmSolicitacaoDeVaga("Requisição de Vaga", "Deseja excluir esta requisição de vaga?", trasnID);
    };
    tabRequisicoes.prototype.confirmSolicitacaoDeVaga = function (titulo, msg, id) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.suporte.showLoader('Cancelando solicitações...');
                        _this.servTransferencia.cancelarRequisicoesDeTransferencias(id)
                            .subscribe(function (r) { return retorno = r; }, function (error) { return console.log('error ao carregar rquisições'); }, function () {
                            if (retorno.status == 0) {
                                _this.suporte.showAviso("Solicitação removida com sucesso.");
                                _this.getRequisicoes();
                            }
                            if (retorno.status == 1) {
                            }
                            _this.suporte.dismissLoader();
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabRequisicoes.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getRequisicoes();
        setTimeout(function () {
            refresher.complete();
        }, 2000);
    };
    return tabRequisicoes;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabRequisicoes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabRequisicoes-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabRequisicoes.html"*/'<ion-header>\n        \n    <ion-navbar color="primary">\n    <ion-title>Requisições</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-toolbar color="subTransferencia">\n        <ion-row>\n            <ion-col width-100 style="text-align:center;color:#fff">Deslize para a esquerda para mais opções.</ion-col>\n        </ion-row>  \n    </ion-toolbar>\n    <ion-list>\n        <ion-item-sliding *ngFor="let requisicao of requisicoesDeVagas">\n            <ion-item text-wrap>\n                <ion-avatar item-start class="numUnidade">\n                   <ion-icon name="swap"></ion-icon> Unidade<br /> {{requisicao.numeroApt}}\n                </ion-avatar>\n                <p>Você requisitou a vaga da unidade {{requisicao.numeroApt}} - {{requisicao.strGrupo}}</p>\n                <p class="txtMenor">Requisição de vaga</p>\n                <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n            </ion-item> \n            <ion-item-options side="right">\n                <button ion-button color="danger" (click)="excluirRequisicao(requisicao.id)">\n                    <ion-icon name="trash"></ion-icon>\n                    Remover\n                </button>\n            </ion-item-options>\n    </ion-item-sliding> \n   \n    </ion-list> \n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/tabRequisicoes.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__["a" /* ServiceTransferencia */]])
], tabRequisicoes);

//# sourceMappingURL=tabRequisicoes.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return recebidas; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__ = __webpack_require__(71);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var recebidas = (function (_super) {
    __extends(recebidas, _super);
    function recebidas(storage, navCtrl, suporte, viewCtrl, servTransferencia) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.viewCtrl = viewCtrl;
        _this.servTransferencia = servTransferencia;
        _this.transferenciasConfirmadasList = [];
        return _this;
    }
    recebidas.prototype.ngOnInit = function () {
        this.getTranferencias();
    };
    recebidas.prototype.getTranferencias = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.servTransferencia.listarTransferenciasConfirmadas(dataUser.aptId)
                .subscribe(function (list) { return _this.transferenciasConfirmadasList = list; }, function (error) { return console.log("Error em obter confirmações"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    recebidas.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    recebidas.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getTranferencias();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return recebidas;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
recebidas = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'recebidas-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/recebidas.html"*/'<ion-header>\n    <ion-toolbar color="secondary">\n        <ion-title>\n            <ion-icon name="swap"></ion-icon> Recebidas\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n</ion-toolbar>\n</ion-header>\n\n<ion-content>\n        \n            <ion-refresher (ionRefresh)="doRefresh($event)">\n                <ion-refresher-content></ion-refresher-content>\n            </ion-refresher>\n        \n            <ion-list>\n                <ion-item text-wrap *ngFor="let t of transferenciasConfirmadasList">\n                    <ion-avatar item-start class="numUnidade">\n                        Unidade<br /> {{t.numeroApt}}\n                    </ion-avatar>\n                    <p>Vaga fornecida pela unidade {{t.numeroApt}} <br /> {{t.strGrupo}}</p>\n                    <p class="txtMenor">Transferência ativa</p>\n                </ion-item> \n            </ion-list> \n        \n        </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/transferencia/recebidas.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceTransferencia__["a" /* ServiceTransferencia */]])
], recebidas);

//# sourceMappingURL=recebidas.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return contatos; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__mensagens__ = __webpack_require__(56);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var contatos = (function (_super) {
    __extends(contatos, _super);
    function contatos(storage, navCtrl, suporte, modal, alertCtrl, servChat, viewCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servChat = servChat;
        _this.viewCtrl = viewCtrl;
        _this.listaDeContatos = [];
        _this.items = [];
        return _this;
    }
    contatos.prototype.ngOnInit = function () {
        this.getContatos();
    };
    contatos.prototype.getContatos = function () {
        var _this = this;
        var uClienteId = 0;
        var arrContatos = [];
        this.suporte.showLoader("Carregando contatos...");
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.storage.get("lstAutocompletar").then(function (contatos) {
                    if (contatos != null && contatos != undefined) {
                        if (contatos.length > 0) {
                            uClienteId = contatos[0].id;
                        }
                    }
                    else {
                        contatos = [];
                    }
                    _this.servChat.listarClientesDoCondominio(dataUser.condominioId, uClienteId)
                        .subscribe(function (conts) { return arrContatos = conts; }, function (error) { return console.log("Error ao obter contatos do condominio"); }, function () {
                        if (contatos.length > 0) {
                            for (var i = 0; i < arrContatos.length - 1; i++) {
                                contatos.unshift(arrContatos[i]);
                            }
                        }
                        else {
                            for (var j = 0; j < arrContatos.length - 1; j++) {
                                contatos.push(arrContatos[j]);
                            }
                        }
                        _this.storage.set("lstAutocompletar", contatos);
                        _this.listaDeContatos = [];
                        _this.listaDeContatos = contatos;
                        _this.initializeItems();
                        _this.suporte.dismissLoader();
                    });
                });
            }
        });
    };
    contatos.prototype.initializeItems = function () {
        this.items = [];
        this.items = this.listaDeContatos;
    };
    contatos.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        this.initializeItems();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.listaDeContatos.filter(function (item) {
                return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    contatos.prototype.iniciarConversa = function (convidadoId) {
        var _this = this;
        var retorno;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.suporte.showAviso("Carregando conversa..");
                _this.servChat.iniciarConversa(dataUser.userID, convidadoId)
                    .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("Error ao iniciar a conversa"); }, function () {
                    var conversa;
                    _this.servChat.findConversa(retorno.status, dataUser.userID)
                        .subscribe(function (c) { return conversa = c; }, function (error) { return console.log("Error ao obter conversa"); }, function () {
                        _this.suporte.dismissLoader();
                        // console.log(conversa);
                        var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_6__mensagens__["a" /* mensagens */], conversa);
                        m.present();
                    });
                });
            }
        });
    };
    contatos.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return contatos;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
contatos = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'contatos-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/contatos.html"*/'<ion-header>\n    <ion-toolbar color="primary">\n      <ion-title>\n          <ion-icon name="chatbubbles"></ion-icon> Nova Conversa\n      </ion-title>\n      <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="light" showWhen="ios">Voltar</span>\n          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n  </ion-header>\n        \n\n    <ion-content>\n        <ion-toolbar color="primary">\n            <ion-searchbar (ionInput)="getItems($event)" placeholder="Buscar vizinho" showCancelButton="true" cancelButtonText="Cancelar"></ion-searchbar>\n        </ion-toolbar>\n        \n        <ion-list>\n            <ion-item text-wrap (click)="iniciarConversa(item.id)" *ngFor="let item of items">\n                <p>\n                      {{item.nome}} {{item.sobrenome}} \n                </p>\n                <p class="txtMenor" *ngFor="let unid of item.lstApartamentos">Número: {{unid.numero}} / {{unid.grupoDescricao}}</p>\n            </ion-item>\n       </ion-list> \n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/contatos.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceChat__["a" /* ServiceChat */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */]])
], contatos);

//# sourceMappingURL=contatos.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabHome; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__loja__ = __webpack_require__(160);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabHome = (function (_super) {
    __extends(tabHome, _super);
    function tabHome(navCtrl, navParams, sp, _storage, modalCtrl, servProdServ) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.servProdServ = servProdServ;
        return _this;
    }
    tabHome.prototype.ngOnInit = function () {
        this.getCategorias();
        //this.listarAnunciantes(8);
    };
    tabHome.prototype.getCategorias = function () {
        var _this = this;
        // this.sp.showLoader("Carregando categorias...");
        this.servProdServ.listarCategorias()
            .subscribe(function (r) { return _this.listDeCategorias = r; }, function (error) { return console.log("Error ao Listar categorias"); }, function () {
            _this.listarAnunciantes(8);
            //this.sp.dismissLoader()
        });
    };
    tabHome.prototype.listarAnunciantes = function (idCategoria) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.sp.showLoader("Carregando anunciantes...");
            _this.servProdServ.listarAnunciantesPorCategoria(idCategoria, dataUser.condominioId)
                .subscribe(function (r) { return _this.listDeAnunciantes = r; }, function (error) { return console.log("Error ao Listar Anunciantes"); }, function () { return _this.sp.dismissLoader(); });
        });
    };
    tabHome.prototype.listaLoja = function (anuncianteObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__loja__["a" /* loja */], anuncianteObj);
        m.present();
    };
    return tabHome;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabHome = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabHome-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/tabHome.html"*/'<ion-header>\n        \n    <ion-navbar color="topProdutoServ">\n    <ion-title>Home Produtos</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n<ion-content class="bgPage">\n    \n    <div class="divSlide">\n        <ion-slides pages [loop]="true" [autoplay]="3500">\n            <ion-slide style="background-image:url(assets/img/Banners/chave-banner.jpg);background-repeat:no-repeat;background-size:cover">\n                <h2>Perdeu a Chave?</h2>\n                <p>De chaveiro a pedreiro, diversos serviços ao seu alcance.</p> \n            </ion-slide>\n            <ion-slide style="background-image:url(assets/img/Banners/chave-beleza.jpg);background-repeat:no-repeat;background-size:cover">\n                <h2>Fique mais bonita</h2>\n                <p>Os melhores produtos e serviços de Beleza você encontra aqui.</p>\n            </ion-slide>\n            <ion-slide style="background-image:url(assets/img/Banners/bannerOutEstasaSerguros.jpg);background-repeat:no-repeat;background-size:cover">\n                <!-- <h2>O Melhor Hamburger</h2>\n                <p>Não fique com fome aproveite</p> -->\n            </ion-slide>\n        </ion-slides>\n    </div>\n\n    <ion-toolbar color="toolBarProdServ" no-border>\n        <ion-slides [slidesPerView]="3" [loop]="true" [spaceBetween]="3">      \n            <ion-slide *ngFor="let categoria of listDeCategorias">\n                <button ion-button clear (click)="listarAnunciantes(categoria.id)" class="sizeFont">\n                    {{categoria.descricao}}\n                </button>\n            </ion-slide>\n        </ion-slides> \n     </ion-toolbar>\n        \n    <ion-card *ngFor="let anunciante of listDeAnunciantes" (click)="listaLoja(anunciante)">\n        <ion-item text-wrap>\n            <ion-avatar item-start *ngIf="anunciante.ftLogo">\n                <img src="{{urlImageServ}}anunciantes/{{anunciante.ftLogo}}" />\n            </ion-avatar>\n            <label> {{anunciante.nomeFantasia}}</label>\n            <p><ion-icon name="call"></ion-icon> {{anunciante.tel}}</p>\n            <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n        </ion-item>\n    </ion-card>\n\n    \n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/tabHome.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__["a" /* ServiceProdServ */]])
], tabHome);

//# sourceMappingURL=tabHome.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return anuncioDetalhes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__EnviarContato__ = __webpack_require__(423);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var anuncioDetalhes = (function (_super) {
    __extends(anuncioDetalhes, _super);
    function anuncioDetalhes(navCtrl, navParams, sp, _storage, modalCtrl, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.viewCtrl = viewCtrl;
        _this.isCotacao = false;
        _this.arrFotosproduto = [];
        _this.objAnuncio = _this.navParams.get("anuncioObj");
        _this.objAnunciante = _this.navParams.get("anuncianteObj");
        if (_this.objAnuncio.descricao.indexOf('{{residencial}}') > 0) {
            _this.isCotacao = true;
            _this.descricao = _this.objAnuncio.descricao.replace('{{residencial}}', '');
            _this.cotacaoUrl = "https://wwws.portoseguro.com.br/vendaonline/residencia/home.ns?cod=fa5ed517499248709a85e99e978a68d4&amp;utm_source=X1029J&amp;utm_medium=geradorLinks&amp;utm_campaign=GeradordeLinks_IC30YJ&amp;utm_content=ESTASA_CORRETORA_DE_SEGUROS";
        }
        else if (_this.objAnuncio.descricao.indexOf('{{SeguroDeVida}}') > 0) {
            _this.isCotacao = true;
            _this.descricao = _this.objAnuncio.descricao.replace('{{SeguroDeVida}}', '');
            _this.cotacaoUrl = "https://wwws.portoseguro.com.br/vendaonline/vidamaissimples/home.ns?cod=0acac145832542b28f96a0840cb4ca34&utm_source=X1029J&utm_medium=geradorLinks&utm_campaign=GeradordeLinks_IC30YJ&utm_content=ESTASA_CORRETORA_DE_SEGUROS";
        }
        else {
            _this.descricao = _this.objAnuncio.descricao;
        }
        _this.strNomeFantasia = _this.objAnunciante.nomeFantasia;
        _this.preco = _this.objAnuncio.precoView;
        _this.chamada = _this.objAnuncio.chamada;
        _this.siteUrl = _this.objAnunciante.siteUrl;
        //console.log(this.objAnuncio);
        //console.log(this.objAnunciante);
        if (_this.objAnuncio.ftProduto)
            _this.arrFotosproduto = _this.objAnuncio.ftProduto.split("|");
        return _this;
    }
    anuncioDetalhes.prototype.ngOnInit = function () {
    };
    anuncioDetalhes.prototype.openCotacaoResidencial = function () {
        this.sp.openUrlSite(this.cotacaoUrl);
    };
    anuncioDetalhes.prototype.openSaibaMais = function () {
        if (this.siteUrl != null && this.siteUrl != "") {
            this.sp.openUrlSite(this.siteUrl);
        }
        else {
            this.sp.showAviso("Não há um endereço externo configurado.");
        }
    };
    anuncioDetalhes.prototype.openEnviarContato = function () {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__EnviarContato__["a" /* EnviarContato */], { "anuncianteObj": this.objAnunciante, "anuncioObj": this.objAnuncio });
        m.present();
    };
    anuncioDetalhes.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return anuncioDetalhes;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
anuncioDetalhes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'anuncioDetalhes-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/anuncioDetalhes.html"*/'<ion-header>\n        <ion-toolbar color="topProdutoServ">\n            <ion-title>\n                <ion-icon name="cube"></ion-icon>  {{strNomeFantasia}}\n            </ion-title>\n            <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </ion-header>\n                    \n    <ion-content class="bgPage">\n        \n        <div class="divSlide" *ngIf="arrFotosproduto.length > 0">\n            <ion-slides pages [loop]="true" [autoplay]="3500">\n                <ion-slide *ngFor="let sld of arrFotosproduto">\n                    <img src="{{urlImageServ}}Anuncio/{{sld}}" /> \n                </ion-slide>\n            </ion-slides>\n        </div>\n\n        <ion-grid>\n            <ion-row>\n                <ion-col col-6>\n                    <button *ngIf="isCotacao" (click)="openCotacaoResidencial()" ion-button color="topProdutoServ" block round>Fazer cotação</button>\n                    <!-- <button *ngIf="!isResidencial" (click)="openSaibaMais()" ion-button color="topProdutoServ" block round>Saiba mais</button> -->\n                </ion-col>\n                <ion-col col-6>\n                    <button (click)="openEnviarContato()" ion-button color="secondary" block round>Contratar</button>\n                </ion-col>\n            </ion-row>\n        </ion-grid>\n\n        <ion-card>\n            <ion-item text-wrap>   \n                <span item-end item-right small class="preco" *ngIf="precoView != 0">{{precoView}}</span>\n                <h2 class="chamada"> {{chamada}}</h2>\n                <p>{{descricao}}</p>\n               \n            </ion-item>\n        </ion-card>\n    \n    \n        <!-- <ion-list>\n            <ion-item text-wrap *ngFor="let anunciante of listDeAnunciantes"> \n                <ion-avatar item-start *ngIf="anunciante.ftLogo">\n                    <img src="{{urlImageServ}}usuario/{{anunciante.ftLogo}}" />\n                     <img src="assets/img/Icon_CondominioApp.png" *ngIf="!anunciante.ftLogo" /> \n                </ion-avatar>\n                <h2> {{anunciante.nomeFantasia}}</h2>\n                <p><ion-icon name="call"></ion-icon> {{anunciante.tel}}</p>\n                <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n            </ion-item>\n        </ion-list> -->\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/anuncioDetalhes.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], anuncioDetalhes);

//# sourceMappingURL=anuncioDetalhes.js.map

/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnviarContato; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceProdServ__ = __webpack_require__(57);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var EnviarContato = (function (_super) {
    __extends(EnviarContato, _super);
    function EnviarContato(navCtrl, navParams, sp, _storage, fb, modalCtrl, servProdServ, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.fb = fb;
        _this.modalCtrl = modalCtrl;
        _this.servProdServ = servProdServ;
        _this.viewCtrl = viewCtrl;
        _this.anunciante = _this.navParams.get("anuncianteObj");
        _this.anuncio = _this.navParams.get("anuncioObj");
        _this.form = fb.group({
            nome: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            telefone: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            mensagem: ['']
        });
        return _this;
    }
    EnviarContato.prototype.ngOnInit = function () {
    };
    EnviarContato.prototype.enviarContato = function () {
        var _this = this;
        this.form.addControl("idAnunciante", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](this.anunciante.id));
        this.form.addControl("itemClicado", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](this.anuncio.chamada));
        //console.log(this.form.value);
        var novoContratar = this.form.value;
        var ret;
        this.servProdServ.enviaRequisicaoContratar(novoContratar)
            .subscribe(function (retorno) { return ret = retorno; }, function (error) { return console.log(_this.errorMensage); }, function () {
            if (ret.status == 0) {
                _this.sp.showAviso(ret.mensagem);
                _this.form.reset();
                _this.dismiss();
            }
            if (ret.status == -1) {
                _this.sp.showAviso(ret.mensagem);
            }
        });
    };
    EnviarContato.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return EnviarContato;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
EnviarContato = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'EnviarContato-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/EnviarContato.html"*/'<ion-header>\n    <ion-toolbar color="topProdutoServ">\n        <ion-title>\n            <ion-icon name="mail"></ion-icon>  Fale com o anunciante\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n   \n<ion-content class="bgApp">\n    <form [formGroup]="form" (submit)="enviarContato()">\n        <ion-list no-lines>\n            \n            <ion-item>\n                <ion-label>Nome</ion-label>\n                <ion-input type="text" formControlName="nome"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>E-mail</ion-label>\n                <ion-input type="text" formControlName="email"></ion-input>\n            </ion-item>\n    \n            <ion-item>\n                <ion-label>Telefone</ion-label>\n                <ion-input type="text" formControlName="telefone"></ion-input>\n            </ion-item>\n          \n            <ion-item>\n                <ion-label>Mensagem</ion-label>\n                <ion-textarea placeholder="Digite sua mensagem." formControlName="mensagem" rows="5"></ion-textarea>\n            </ion-item>\n\n            <ion-item text-center>\n                <button ion-button round padding type="submit" color="topProdutoServ" [disabled]="!form.valid" style="width: 220px">Enviar mensagem</button>\n            </ion-item>\n\n        </ion-list>\n    </form>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/EnviarContato.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceProdServ__["a" /* ServiceProdServ */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], EnviarContato);

//# sourceMappingURL=EnviarContato.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabCategorias; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__anunciantes__ = __webpack_require__(425);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var tabCategorias = (function (_super) {
    __extends(tabCategorias, _super);
    function tabCategorias(navCtrl, navParams, sp, _storage, modalCtrl, servProdServ) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.servProdServ = servProdServ;
        return _this;
    }
    tabCategorias.prototype.ngOnInit = function () {
        this.getCategorias();
    };
    tabCategorias.prototype.getCategorias = function () {
        var _this = this;
        this.sp.showLoader("Carregando categorias...");
        this.servProdServ.listarCategorias()
            .subscribe(function (r) { return _this.listDeCategorias = r; }, function (error) { return console.log("Error ao Listar categorias"); }, function () { return _this.sp.dismissLoader(); });
    };
    tabCategorias.prototype.listarAnunciantes = function (categoriaObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__anunciantes__["a" /* anunciantes */], categoriaObj);
        m.present();
    };
    return tabCategorias;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabCategorias = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabCategorias-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/tabCategorias.html"*/'<ion-header>\n        \n    <ion-navbar color="topProdutoServ">\n       <ion-title>Categorias</ion-title>\n    </ion-navbar>\n        \n</ion-header>\n       \n<ion-content>\n    <!-- <ion-list> -->\n        <ion-card *ngFor="let cat of listDeCategorias" (click)="listarAnunciantes(cat)">\n            <ion-item text-wrap> \n                <ion-icon name="home" item-start item-left small ></ion-icon>\n                 <span class="dataDisplay"> {{cat.descricao}}</span>\n                 <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n            </ion-item>\n        </ion-card>\n    <!-- </ion-list> -->\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/tabCategorias.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__["a" /* ServiceProdServ */]])
], tabCategorias);

//# sourceMappingURL=tabCategorias.js.map

/***/ }),

/***/ 425:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return anunciantes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__loja__ = __webpack_require__(160);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var anunciantes = (function (_super) {
    __extends(anunciantes, _super);
    function anunciantes(navCtrl, navParams, sp, _storage, modalCtrl, servProdServ, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.servProdServ = servProdServ;
        _this.viewCtrl = viewCtrl;
        _this.categoria = _this.navParams.data;
        return _this;
    }
    anunciantes.prototype.ngOnInit = function () {
        this.getAnunciantes();
    };
    anunciantes.prototype.getAnunciantes = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.sp.showLoader("Carregando anunciantes...");
            _this.servProdServ.listarAnunciantesPorCategoria(_this.categoria.id, dataUser.condominioId)
                .subscribe(function (r) { return _this.listDeAnunciantes = r; }, function (error) { return console.log("Error ao Listar Anunciantes"); }, function () { return _this.sp.dismissLoader(); });
        });
    };
    anunciantes.prototype.listaLoja = function (anuncianteObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__loja__["a" /* loja */], anuncianteObj);
        m.present();
    };
    anunciantes.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return anunciantes;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
anunciantes = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'anunciantes-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/anunciantes.html"*/'<ion-header>\n    <ion-toolbar color="topProdutoServ">\n        <ion-title>\n            <ion-icon name="home"></ion-icon>  {{categoria.descricao}}\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n                \n<ion-content class="bgPage">\n    \n    <ion-card *ngFor="let anunciante of listDeAnunciantes" (click)="listaLoja(anunciante)">\n        <ion-item text-wrap>\n            <ion-avatar item-start *ngIf="anunciante.ftLogo">\n                <img src="{{urlImageServ}}anunciantes/{{anunciante.ftLogo}}" />\n                <!-- <img src="assets/img/Icon_CondominioApp.png" *ngIf="!anunciante.ftLogo" /> -->\n            </ion-avatar>\n            <h2> {{anunciante.nomeFantasia}}</h2>\n            <p><ion-icon name="call"></ion-icon> {{anunciante.tel}}</p>\n            <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n        </ion-item>\n    </ion-card>\n\n\n    <!-- <ion-list>\n        <ion-item text-wrap *ngFor="let anunciante of listDeAnunciantes"> \n            <ion-avatar item-start *ngIf="anunciante.ftLogo">\n                <img src="{{urlImageServ}}usuario/{{anunciante.ftLogo}}" />\n                 <img src="assets/img/Icon_CondominioApp.png" *ngIf="!anunciante.ftLogo" /> \n            </ion-avatar>\n            <h2> {{anunciante.nomeFantasia}}</h2>\n            <p><ion-icon name="call"></ion-icon> {{anunciante.tel}}</p>\n            <ion-icon name="ios-arrow-forward" item-end item-right small ></ion-icon>\n        </ion-item>\n    </ion-list> -->\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/produtoServicos/anunciantes.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceProdServ__["a" /* ServiceProdServ */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], anunciantes);

//# sourceMappingURL=anunciantes.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalAutorizarEntrada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var modalAutorizarEntrada = (function (_super) {
    __extends(modalAutorizarEntrada, _super);
    function modalAutorizarEntrada(storage, navCtrl, param, servVisita, sp, signalr, viewCtrl, modalCtrl, fb) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.param = param;
        _this.servVisita = servVisita;
        _this.sp = sp;
        _this.signalr = signalr;
        _this.viewCtrl = viewCtrl;
        _this.modalCtrl = modalCtrl;
        _this.fb = fb;
        _this.visitante = _this.param.data;
        return _this;
        //console.log(JSON.stringify(this.visitante));
        //console.log(this.visitante);
    }
    modalAutorizarEntrada.prototype.ngOnInit = function () {
    };
    modalAutorizarEntrada.prototype.provarVisita = function (idDaVisita) {
        var _this = this;
        var result;
        this.servVisita.aprovarVisita(idDaVisita).subscribe(function (retorno) { return result = retorno; }, function (error) { return console.log(result.mensagem); }, function () {
            if (result.status == 0) {
                _this.sp.showAviso(result.mensagem);
                _this.dismiss();
                _this.storage.get("UsuarioLogado").then(function (dataUser) {
                    //this.signalr.connect().then((c) => {
                    _this.signalr.connection.invoke("SendAutorizacaoVisitaApp", idDaVisita, dataUser.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    //});
                });
            }
        });
    };
    modalAutorizarEntrada.prototype.rejeitarVisita = function (idDaVisita) {
        var _this = this;
        var result;
        this.servVisita.reaprovarVisita(idDaVisita).subscribe(function (retorno) { return result = retorno; }, function (error) { return console.log(result.mensagem); }, function () {
            if (result.status == 0) {
                _this.sp.showAviso(result.mensagem);
                _this.dismiss();
                _this.storage.get("UsuarioLogado").then(function (dataUser) {
                    //this.signalr.connect().then((c) => {
                    _this.signalr.connection.invoke("SendAutorizacaoVisitaApp", idDaVisita, dataUser.condominioId).then(function (resutServer) { return console.log("Enviado com sucesso!"); });
                    //});
                });
            }
        });
    };
    modalAutorizarEntrada.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalAutorizarEntrada;
}(__WEBPACK_IMPORTED_MODULE_7__base_pageBase__["a" /* PageBase */]));
modalAutorizarEntrada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'modalAutorizarEntrada-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizarEntrada.html"*/'<ion-card class="marginTOP">  \n    <ion-card-title>\n        <ion-title>\n            <ion-icon name="person"></ion-icon> Autorizar entrada\n        </ion-title>\n    </ion-card-title>\n\n    <img src=\'{{urlImageServ}}usuario/{{visitante.foto}}\' *ngIf="visitante.foto">\n\n    <ion-card-content>\n        <p>{{visitante.nome}}</p>\n    </ion-card-content>\n\n    <ion-row>\n        <ion-col center text-center>\n            <button ion-button icon-left clear small (click)="provarVisita(visitante.id)">\n              <ion-icon name="md-checkmark"></ion-icon>\n              <div>Autorizar</div>\n            </button>\n        </ion-col>\n        <ion-col>\n           \n        </ion-col>\n        <ion-col center text-center>\n            <button ion-button icon-left clear small (click)="rejeitarVisita(visitante.id)">\n                <ion-icon name="md-exit"></ion-icon>\n                <div>Não autorizar</div>\n            </button>\n        </ion-col>\n    </ion-row>\n\n</ion-card> '/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/portaria/modalAutorizarEntrada.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceSignalr__["a" /* ServiceSignalr */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
], modalAutorizarEntrada);

//# sourceMappingURL=modalAutorizarEntrada.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return enqueteVotar; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var enqueteVotar = (function (_super) {
    __extends(enqueteVotar, _super);
    function enqueteVotar(storage, navCtrl, alertCtrl, param, servEnquete, suporte, viewCtrl, modal) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.alertCtrl = alertCtrl;
        _this.param = param;
        _this.servEnquete = servEnquete;
        _this.suporte = suporte;
        _this.viewCtrl = viewCtrl;
        _this.modal = modal;
        _this.arrEnquetesNaoRespondidas = _this.param.data;
        _this.oEnquete = _this.arrEnquetesNaoRespondidas[0];
        console.log(_this.arrEnquetesNaoRespondidas);
        console.log(_this.oEnquete);
        return _this;
    }
    enqueteVotar.prototype.ionViewDidLoad = function () {
        //this.content.scrollToBottom(300);
    };
    enqueteVotar.prototype.ngOnInit = function () {
    };
    enqueteVotar.prototype.votarEnquete = function (idAlternativa, strDescricao) {
        //console.log("alternativa Id: " + idAlternativa);
        this.confirmVotarEnquete("Enquetes", "Tem certeza que deseja votar em " + strDescricao + "?", idAlternativa);
    };
    enqueteVotar.prototype.confirmVotarEnquete = function (titulo, msg, alternativaID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.suporte.showLoader("Enviando seu voto...");
                        _this.storage.get("UsuarioLogado").then(function (dataUser) {
                            if (dataUser != null) {
                                //console.log(this.arrEnquetesNaoRespondidas);
                                var retorno_1;
                                _this.servEnquete.votarEnquete(alternativaID, dataUser.userID, dataUser.aptId)
                                    .subscribe(function (result) { return retorno_1 = result; }, function (error) { return console.log("Error ao votar na alternativa"); }, function () {
                                    //  console.log(retorno); 
                                    if (retorno_1.status == 0) {
                                        _this.suporte.showAviso(retorno_1.mensagem);
                                        _this.arrEnquetesNaoRespondidas.shift();
                                        if (_this.arrEnquetesNaoRespondidas.length > 0) {
                                            _this.oEnquete = _this.arrEnquetesNaoRespondidas[0];
                                            _this.suporte.dismissLoader();
                                        }
                                        else {
                                            _this.suporte.dismissLoader();
                                            _this.dismiss();
                                        }
                                    }
                                    if (retorno_1.status == 1) {
                                        _this.suporte.showAviso(retorno_1.mensagem);
                                        _this.suporte.dismissLoader();
                                    }
                                    if (retorno_1.status == -1) {
                                        _this.suporte.showAviso(_this.errorMensage);
                                        _this.suporte.dismissLoader();
                                    }
                                });
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    enqueteVotar.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return enqueteVotar;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
enqueteVotar = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'enqueteVotar-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enqueteVotar.html"*/'\n<!-- <ion-header>\n    <ion-toolbar color="topEnquete">\n        <ion-title>\n          Enquete - Votar\n        </ion-title>\n        <ion-buttons start>\n          <button ion-button (click)="dismiss()">\n            <ion-icon name="close" color="light" showWhen="ios"></ion-icon>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n         </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header> -->\n\n<!-- <ion-content> -->\n    \n    <ion-card class="marginTOP">  \n        <ion-card-title>  \n            <ion-item>\n                <ion-icon name="list"></ion-icon> Vote na enquete\n                <span item-end (click)="dismiss()"><ion-icon name="close"></ion-icon></span>\n            </ion-item>\n        </ion-card-title>    \n      \n        <ion-card-content>\n            \n            <ion-list>\n                <ion-list-header text-wrap>\n                     {{oEnquete.descricao}}\n                </ion-list-header>\n                <ion-item text-wrap *ngFor="let alternativa of oEnquete.alternativasModel" (click)="votarEnquete(alternativa.id,alternativa.descricao)"> \n                      {{alternativa.descricao}}\n                     <!--<span item-end> {{alternativa.qtdVotos}}</span> -->\n                </ion-item>\n            </ion-list>\n               \n        </ion-card-content>\n    </ion-card>\n<!-- </ion-content> -->'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/enquete/enqueteVotar.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceEnquete__["a" /* ServiceEnquete */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], enqueteVotar);

//# sourceMappingURL=enqueteVotar.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabAcesso; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__carteiraVirtual_qr_carteiraVirtual__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceCarteiraDigital__ = __webpack_require__(54);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabAcesso = (function (_super) {
    __extends(tabAcesso, _super);
    function tabAcesso(navCtrl, _storage, params, modal, barcodeScanner, servCkecIn, suporte) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        _this.params = params;
        _this.modal = modal;
        _this.barcodeScanner = barcodeScanner;
        _this.servCkecIn = servCkecIn;
        _this.suporte = suporte;
        _this.listDeCarteiras = [];
        return _this;
    }
    tabAcesso.prototype.ngOnInit = function () {
    };
    tabAcesso.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    tabAcesso.prototype.lerCodigoDeBarra = function () {
        //  let opt = {
        //     nomeAreaAcesso : 'Ônibus',
        //     nomeCondominio : 'Palmeiras',
        //     idAreaDeAcesso : 2,
        //     idCondominio : 1
        // }
        var _this = this;
        //this.navCtrl.push(qr_carteiraVirtual,opt);
        // let m = this.modal.create(qr_carteiraVirtual, opt);
        // m.present();
        var options = {
            resultDisplayDuration: 0
        };
        this.barcodeScanner.scan(options).then(function (barcodeData) {
            // Success! Barcode data is here
            // alert(JSON.stringify(barcodeData));
            if (!barcodeData.cancelled && barcodeData.format == "QR_CODE") {
                var objItemCode_1 = JSON.parse(barcodeData.text);
                _this.storage.get("UsuarioLogado").then(function (uData) {
                    _this.storage.get("cateirasDigitais").then(function (carteiras) {
                        if (carteiras != null) {
                            //console.log(carteiras);
                            carteiras.filter(function (item) {
                                if (item.idAreaAcesso == objItemCode_1.idAreaAcesso) {
                                    // console.log("Achei a carteira!");
                                    // console.log(item);
                                    if (item.token != objItemCode_1.token) {
                                        _this.suporte.showAviso("QRCode inválido.");
                                        return;
                                    }
                                    _this.listDeCarteiras = item.listaCarteiras;
                                    var controle_1 = false;
                                    _this.listDeCarteiras.filter(function (c) {
                                        console.log(c);
                                        if (c.idUsuario == uData.userID) {
                                            controle_1 = true;
                                            // console.log("achei o usuário");
                                            //Verifica se tem internet no momento do CheckIn;
                                            _this.storage.get("connectionStatus").then(function (conn) {
                                                var objCheckIn = {
                                                    userID: c.idUsuario,
                                                    areaID: item.idAreaAcesso,
                                                    carteiraID: c.idCarteira,
                                                    nomeDaArea: objItemCode_1.nomeAreaAcesso,
                                                    nomeCondominio: objItemCode_1.nomeCondominio,
                                                    carteirasList: _this.listDeCarteiras,
                                                    minhaCarteira: c,
                                                    dtEntrada: _this.suporte.formatDateTime()
                                                };
                                                var objCheckInOff = {
                                                    userID: c.idUsuario,
                                                    areaID: item.idAreaAcesso,
                                                    carteiraID: c.idCarteira,
                                                    nomeDaArea: objItemCode_1.nomeAreaAcesso,
                                                    nomeCondominio: objItemCode_1.nomeCondominio,
                                                    dtEntrada: _this.suporte.formatDateTime()
                                                };
                                                //console.log(objCheckIn);
                                                if (conn == "On") {
                                                    var check_1;
                                                    _this.suporte.showLoader("Efetuando Check-In...");
                                                    _this.servCkecIn.getEfetuaCheckIn(c.idUsuario, objItemCode_1.idAreaAcesso, c.idCarteira, _this.suporte.formatDateTime())
                                                        .subscribe(function (result) { return check_1 = result; }, function (error) { return console.log("Error de CheckIn"); }, function () {
                                                        _this.suporte.dismissLoader();
                                                        if (check_1.status == 0) {
                                                            _this.suporte.showAviso(check_1.mensagem);
                                                            setTimeout(function () {
                                                                var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_6__carteiraVirtual_qr_carteiraVirtual__["a" /* qr_carteiraVirtual */], objCheckIn);
                                                                m.present();
                                                            }, 1500);
                                                        }
                                                        if (check_1.status == -1) {
                                                            _this.suporte.showAviso(_this.errorMensage);
                                                        }
                                                    });
                                                }
                                                else if (conn == "Off") {
                                                    _this.storage.get("checkInOff").then(function (checkins) {
                                                        //console.log(checkins);
                                                        checkins.push(objCheckInOff);
                                                        _this.storage.set("checkInOff", checkins);
                                                        setTimeout(function () {
                                                            var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_6__carteiraVirtual_qr_carteiraVirtual__["a" /* qr_carteiraVirtual */], objCheckIn);
                                                            m.present();
                                                        }, 1500);
                                                    });
                                                }
                                            });
                                        }
                                    });
                                    if (!controle_1) {
                                        _this.suporte.showAlert("Carteirinha Virtual", "Não ha uma carteirinha disponível para você, notifique a administração.");
                                        _this.navCtrl.pop();
                                    }
                                }
                            });
                        }
                    });
                });
            }
        }, function (err) {
            // An error occurred
            console.log('Error: ' + err);
        });
    };
    return tabAcesso;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
tabAcesso = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabAcesso-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/tabAcesso.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Acesso</ion-title>\n    </ion-navbar>\n</ion-header>\n    \n<ion-content class="bgApp">\n    \n    <ion-row class="rowImageAvatar">\n        <ion-col>\n            <button ion-button color="light" clear block class="text-on-bottom" (click)="lerCodigoDeBarra()">\n                <img src="assets/img/icones/iconeCarteirinha.png" class="imgCarteirinha">\n                <p>Escanear código de acesso</p>\n            </button>   \n        </ion-col>\n    </ion-row>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/tabAcesso.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */]])
], tabAcesso);

//# sourceMappingURL=tabAcesso.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return qr_carteiraVirtual; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceCarteiraDigital__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var qr_carteiraVirtual = (function (_super) {
    __extends(qr_carteiraVirtual, _super);
    function qr_carteiraVirtual(navCtrl, _storage, params, viewCtrl, suporte, servCkecIn, network) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        _this.params = params;
        _this.viewCtrl = viewCtrl;
        _this.suporte = suporte;
        _this.servCkecIn = servCkecIn;
        _this.network = network;
        _this.listDeCarteiras = [];
        _this.pathImageView = _this.urlImageServ + "usuario/";
        _this.itemCodeCondominio = _this.params.data;
        //console.log(this.itemCodeCondominio);
        _this.strNomeDaAreaDeAcesso = _this.itemCodeCondominio.nomeDaArea;
        _this.strNomeCondominio = _this.itemCodeCondominio.nomeCondominio;
        _this.listDeCarteiras = _this.itemCodeCondominio.carteirasList;
        _this.storage.get("connectionStatus").then(function (conn) {
            if (conn == "Off") {
                _this.listDeCarteiras.forEach(function (element) {
                    element.foto = null;
                });
            }
        });
        var objCarteira = _this.itemCodeCondominio.minhaCarteira;
        //this.strDisplayMsg = "Área de acesso " + this.itemCodeCondominio.nomeAreaAcesso + " do condomínio " + this.itemCodeCondominio.nomeCondominio;
        _this.loadCateiraPrincipal(objCarteira);
        return _this;
    }
    qr_carteiraVirtual.prototype.ngOnInit = function () {
        // this.listDeCarteiras = this.listDeCarteirasServer.filter((item) =>{
        //     return item.idArea == this.itemCodeCondominio.idAreaDeAcesso
        // })
        // this.storage.get('UsuarioLogado').then((dataUser) => {
        //     let minhaCarteira = this.listDeCarteiras.filter((i) =>{
        //         return i.idUser == dataUser.userID && i.idArea == this.itemCodeCondominio.idAreaDeAcesso;
        //     });
        //     this.loadCateiraPrincipal(minhaCarteira[0]);     
        // });
    };
    qr_carteiraVirtual.prototype.changeCateira = function (carteirinha) {
        //console.log(carteirinha);
        this.loadCateiraPrincipal(carteirinha);
    };
    qr_carteiraVirtual.prototype.verificaValidade = function (strData) {
        var arrData = strData.split('/');
        var dataDaValidade = new Date(arrData[2] + '-' + arrData[1] + '-' + arrData[0]);
        var hoje = new Date();
        if (dataDaValidade < hoje)
            return false;
        else
            return true;
    };
    qr_carteiraVirtual.prototype.loadCateiraPrincipal = function (carteira) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            //console.log(carteira);
            _this.storage.get("connectionStatus").then(function (conn) {
                var arrItens = dataUser.strApartamento.split("|");
                if (conn == "On")
                    _this.strFotoCarteira = carteira.foto;
                else
                    _this.strFotoCarteira = null;
                _this.strNome = carteira.nome;
                _this.strBloco = arrItens[2];
                _this.strUnidade = arrItens[1];
                _this.strAndar = arrItens[3];
                _this.strValidade = carteira.dataDeValidade;
                _this.isValidade = _this.verificaValidade(carteira.dataDeValidade);
                if (carteira.validado == true && _this.isValidade == true)
                    _this.validado = true;
                else
                    _this.validado = false;
            });
        });
    };
    qr_carteiraVirtual.prototype.ionViewDidLoad = function () {
        // this.storage.get("UsuarioLogado").then((uData) =>{
        //     this.storage.get("cateirasDigitais").then((carteiras) =>{
        //         if (carteiras != null){
        //             //console.log(carteiras);
        //             carteiras.filter((item) => {
        //                 if (item.idAreaAcesso == this.itemCodeCondominio.idAreaAcesso){
        //                     // console.log("Achei a carteira!");
        //                     // console.log(item);
        //                     this.listDeCarteiras = item.listaCarteiras;
        //                     let controle : boolean = false;
        //                     this.listDeCarteiras.filter((c) =>{
        //                         console.log(c);
        //                         if(c.idUsuario == uData.userID){
        //                             controle = true;
        //                            // console.log("achei o usuário");
        //                            //Verifica se tem internet no momento do CheckIn;
        //                            let check : any;
        //                            this.servCkecIn.getEfetuaCheckIn(c.idUsuario,item.idAreaAcesso,c.idCarteira)
        //                                           .subscribe(result => check = result,
        //                                                     error => console.log("Error de CheckIn"),
        //                                                 () => {
        //                                                     if (check.status == 0){
        //                                                         this.loadCateiraPrincipal(c);
        //                                                         this.suporte.showAviso(check.mensagem);
        //                                                     }
        //                                                     if(check.status == -1){
        //                                                         this.suporte.showAviso(this.errorMensage);
        //                                                     }
        //                                                 })
        //                         }
        //                     })  
        //                     if(!controle){
        //                         this.suporte.showAlert("Carteirinha Virtual","Não ha uma carteirinha disponível para você, notifique a administração.");
        //                         this.navCtrl.pop();
        //                     }
        //                 } 
        //             })                  
        //         }
        //     })
        // })
    };
    qr_carteiraVirtual.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return qr_carteiraVirtual;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
qr_carteiraVirtual = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'qr_carteiraVirtual-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/qr_carteiraVirtual.html"*/'<ion-header>\n    <ion-toolbar color="subtopCarteiraVirtual" *ngIf="validado">\n      <ion-title>\n          <ion-icon name="barcode"></ion-icon> Carteirinha Virtual\n      </ion-title>\n      <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n          <span ion-text color="light" showWhen="ios">Voltar</span>\n          <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n      </ion-buttons>\n    </ion-toolbar>\n\n    <ion-toolbar color="fundoCarteiraVirtual" *ngIf="!validado">\n        <ion-title>\n            <ion-icon name="barcode"></ion-icon> Carteira Virtual\n        </ion-title>\n        <ion-buttons start>\n          <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n          </button>\n        </ion-buttons>\n      </ion-toolbar>\n\n  </ion-header>\n\n<ion-content class="bgApp" *ngIf="validado">\n    <ion-row>\n        <ion-col col-12 style="text-align:center;color:#fff">\n            <span>{{strNomeCondominio}}</span><br /><br />\n            <span class="sizeFont">{{strNomeDaAreaDeAcesso}}</span><br/>\n            <span class="sizeFont">Acesso liberado</span>\n        </ion-col>\n    </ion-row>  \n    \n    <ion-row class="rowImageAvatar">\n        <ion-col>\n            <button ion-button color="light" clear block class="text-on-bottom">\n                <img src="{{pathImageView}}{{strFotoCarteira}}" class="imgCarteirinha" *ngIf="strFotoCarteira">\n                <img src="assets/img/no_image.png" class="imgCarteirinha" *ngIf="!strFotoCarteira"><br />\n                <label>\n                    Nome: {{strNome}} <br />\n                    Unidade: {{strUnidade}} <br />\n                    Bloco: {{strBloco}}<br />\n                    Validade : {{strValidade}}\n                </label>\n            </button>   \n        </ion-col>\n    </ion-row>\n</ion-content>\n\n<ion-content class="bgApp2" *ngIf="!validado">\n        <ion-row>\n            <ion-col col-12 style="text-align:center;color:#fff">\n                <span>{{strNomeCondominio}}</span><br /><br />\n                <span class="sizeFont">{{strNomeDaAreaDeAcesso}}</span><br/>\n                <span class="sizeFont">Acesso Negado</span>\n            </ion-col>\n        </ion-row>  \n        \n        <ion-row class="rowImageAvatar">\n            <ion-col>\n                <button ion-button color="light" clear block class="text-on-bottom">\n                    <img src="{{pathImageView}}{{strFotoCarteira}}" class="imgCarteirinha" *ngIf="strFotoCarteira">\n                    <img src="assets/img/no_image.png" class="imgCarteirinha" *ngIf="!strFotoCarteira"><br />\n                    <label>\n                        Nome: {{strNome}} <br />\n                        Unidade: {{strUnidade}} <br />\n                        Bloco: {{strBloco}}<br />\n                        Validade : {{strValidade}}\n                    </label>\n                </button>   \n            </ion-col>\n        </ion-row>\n        <!-- <p *ngIf="!isValidade" class="txtFeedBack"><br />Sua carteira esta VENCIDA procure a administração de seu condomínio.</p> -->\n        <p *ngIf="!validado" class="txtFeedBack"><br />Sua carteira não está VÁLIDA procure a administração de seu condomínio.</p>\n    </ion-content>\n\n<ion-footer >\n    <ion-toolbar color="subtopCarteiraVirtual" *ngIf="validado">\n        <ion-slides [slidesPerView]="3" [loop]="false" [spaceBetween]="3">      \n            <ion-slide *ngFor="let carteira of listDeCarteiras">            \n                <button ion-button color="light" clear class="text-on-bottom_rodape" (click)="changeCateira(carteira)" style="margin-right: 20px">\n                    <img src="{{pathImageView}}{{carteira.foto}}" class="miniaturaImg" *ngIf="carteira.foto">\n                    <img src="assets/img/no_image.png" class="miniaturaImg" *ngIf="!carteira.foto">\n                    <div class="txtRodapeRotulo">{{carteira.nome}}</div>\n                </button>       \n            </ion-slide>\n        </ion-slides> \n    </ion-toolbar>\n\n    <ion-toolbar color="fundoCarteiraVirtual" *ngIf="!validado">\n            <ion-slides [slidesPerView]="3" [loop]="false" [spaceBetween]="3">      \n                <ion-slide *ngFor="let carteira of listDeCarteiras">            \n                    <button ion-button color="light" clear class="text-on-bottom_rodape" (click)="changeCateira(carteira)" style="margin-right: 20px">\n                            <img src="{{pathImageView}}{{carteira.foto}}" class="miniaturaImg" *ngIf="carteira.foto">\n                            <img src="assets/img/no_image.png" class="miniaturaImg" *ngIf="!carteira.foto">\n                        <div class="txtRodapeRotulo">{{carteira.nome}}</div>\n                    </button>       \n                </ion-slide>\n            </ion-slides> \n        </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/qr_carteiraVirtual.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */]])
], qr_carteiraVirtual);

//# sourceMappingURL=qr_carteiraVirtual.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceChat; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceChat = (function () {
    function ServiceChat(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceVeiculo');
    }
    ServiceChat.prototype.listarConversas = function (usuarioID) {
        var params = 'uID=' + usuarioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Usuario/getConversas?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.findConversa = function (conversaID, usuarioID) {
        var params = 'idConversa=' + conversaID + '&uID=' + usuarioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Conversa/getConversaID?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.getConvidadoConversa = function (usuarioID, idConversa) {
        var params = 'conversaID=' + idConversa + '&uID=' + usuarioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Conversa/getConvidadoConversa?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.listarMensagem = function (conversaId) {
        var params = 'conversaID=' + conversaId + '&qtd=' + 10;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Conversa/ListarMensagens?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.enviarMensagem = function (oMensagem) {
        //console.log(oMensagem);
        var params = JSON.stringify(oMensagem);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'mensagem/enviarMensagem', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.pushClientesOff = function (conversaId, userRequestId, strUrlImage, strMensagem) {
        var params = 'id_conversa=' + conversaId + '&UserRequestID=' + userRequestId + '&urlImagePath=' + strUrlImage + '&cnvMensagem=' + strMensagem;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "conversa/getClientesOffPush?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.listarMaisMensagens = function (conversaId, qtdSkip) {
        var params = 'conversaID=' + conversaId + '&qtd=' + qtdSkip;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Conversa/ListarMensagensMaisApp?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.removerConversa = function (idConversa) {
        var params = 'conversaId=' + idConversa;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "conversa/excluir?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.bloquearConversa = function (idConversa, idUsuario) {
        var params = 'conversaId=' + idConversa + '&usuarioBlock=' + idUsuario;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "conversa/getBloqueiaConversa?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.iniciarConversa = function (userLogadoID, convidadoId) {
        var params = 'uID=' + userLogadoID + '&uID2=' + convidadoId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "usuario/iniciarConversa?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.listarClientesDoCondominio = function (idCondominio, clienteID) {
        var params = 'condominioID=' + idCondominio + '&uClienteID=' + clienteID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "cliente/getClientesdoCondominio?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceChat.prototype.listarClientesDaUnidade = function (unidadeID, userId) {
        var params = 'aptID=' + unidadeID + '&clienteId=' + userId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/ListaClientesDoAPT?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceChat;
}());
ServiceChat = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceChat);

//# sourceMappingURL=ServiceChat.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabAreasDeAcesso; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__listCarteiraVirtual__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var tabAreasDeAcesso = (function (_super) {
    __extends(tabAreasDeAcesso, _super);
    function tabAreasDeAcesso(navCtrl, _storage, params, modalCrtl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        _this.params = params;
        _this.modalCrtl = modalCrtl;
        _this.objAreasDeAcessoCarteiras = [];
        _this.storage.get("cateirasDigitais").then(function (dataAcesso) {
            if (dataAcesso != null) {
                _this.objAreasDeAcessoCarteiras = dataAcesso;
            }
        });
        return _this;
    }
    tabAreasDeAcesso.prototype.ngOnInit = function () {
    };
    tabAreasDeAcesso.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    tabAreasDeAcesso.prototype.getCarteirasDaArea = function (objArea) {
        var m = this.modalCrtl.create(__WEBPACK_IMPORTED_MODULE_3__listCarteiraVirtual__["a" /* listCarteiraVirtual */], objArea);
        m.present();
    };
    return tabAreasDeAcesso;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabAreasDeAcesso = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabAreasDeAcesso-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/tabAreasDeAcesso.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>Áreas de acesso</ion-title>\n    </ion-navbar>\n</ion-header>\n    \n<ion-content class="bgApp">    \n    \n    <ion-card *ngFor="let areaAcesso of objAreasDeAcessoCarteiras">\n        <ion-card-content>\n          <ion-item (click)="getCarteirasDaArea(areaAcesso)"> \n              <span>{{areaAcesso.nomeAreaAcesso}}</span><br />\n              <span class="fontSize">Área de acesso</span><br />\n              <span class="fontSize" *ngIf="areaAcesso.permitirConvidado">Permite convidados<br /></span>\n              <span class="fontSize" *ngIf="areaAcesso.permitirConvidado">Max: {{areaAcesso.qtdMaxConvidado}}<br /></span>\n              <span class="fontSize" *ngIf="!areaAcesso.permitirConvidado"><strong>Não</strong> permite convidados<br /></span>\n              <ion-icon iten-end name="home" item-end item-right large></ion-icon>\n          </ion-item> \n        </ion-card-content>\n      </ion-card>\n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/tabAreasDeAcesso.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */]])
], tabAreasDeAcesso);

//# sourceMappingURL=tabAreasDeAcesso.js.map

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return listCarteiraVirtual; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_ServiceCarteiraDigital__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalAddConvidado__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalEditConvidado__ = __webpack_require__(433);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var listCarteiraVirtual = (function (_super) {
    __extends(listCarteiraVirtual, _super);
    function listCarteiraVirtual(navCtrl, _storage, params, viewCtrl, modalCtrl, servCarteira, suporte) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this._storage = _storage;
        _this.params = params;
        _this.viewCtrl = viewCtrl;
        _this.modalCtrl = modalCtrl;
        _this.servCarteira = servCarteira;
        _this.suporte = suporte;
        _this.urlImageServ += "usuario/";
        _this.listDeCarteiras = [];
        _this.listDeConvidados = [];
        var objParams = _this.params.data;
        _this.areaAcessoID = objParams.idAreaAcesso;
        _this.nomeAreaDeAcesso = objParams.nomeAreaAcesso;
        _this.listDeCarteiras = objParams.listaCarteiras;
        _this.listDeConvidados = objParams.convidados;
        _this.permitirConvidado = objParams.permitirConvidado;
        _this.qtdMaxConvidado = objParams.qtdMaxConvidado;
        _this.qtdConvidadosCadastrados = _this.listDeConvidados.length;
        return _this;
    }
    listCarteiraVirtual.prototype.ngOnInit = function () {
    };
    listCarteiraVirtual.prototype.getCarteirasDaAreaDeAcesso = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var carteirasDaUnidade;
            _this.servCarteira.getCarteirasDaUnidade(dataUser.aptId)
                .subscribe(function (result) { return carteirasDaUnidade = result; }, function (error) { return console.log("Error ao obter carteiras"); }, function () {
                var area = carteirasDaUnidade.filter(function (i) {
                    return i.idAreaAcesso == _this.areaAcessoID;
                });
                _this.areaAcessoID = area[0].idAreaAcesso;
                _this.nomeAreaDeAcesso = area[0].nomeAreaAcesso;
                _this.permitirConvidado = area[0].permitirConvidado;
                _this.listDeCarteiras = area[0].listaCarteiras;
                _this.listDeConvidados = area[0].convidados;
                _this.storage.set("cateirasDigitais", carteirasDaUnidade);
            });
        });
    };
    listCarteiraVirtual.prototype.ionViewDidLoad = function () {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    };
    listCarteiraVirtual.prototype.openNovoConvidado = function () {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_6__modalAddConvidado__["a" /* modalAddConvidado */], { "idAreaAcesso": this.areaAcessoID,
            "nomeAreaDeAcesso": this.nomeAreaDeAcesso,
            "qtdConvidado": this.qtdMaxConvidado,
            "qtdConvidadosCadastrados": this.qtdConvidadosCadastrados });
        m.present();
    };
    listCarteiraVirtual.prototype.openEditConvidado = function (convidadoObj) {
        var m = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modalEditConvidado__["a" /* modalEditConvidado */], convidadoObj);
        m.present();
    };
    listCarteiraVirtual.prototype.getAtivaDesativa = function (ct, idConvidado) {
        var _this = this;
        //console.log(event.checked);
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var retornoItem;
            _this.servCarteira.getAtivaDesativaConvidado(idConvidado, _this.areaAcessoID, ct)
                .subscribe(function (result) { return retornoItem = result; }, function (error) { return console.log("Error ao setar ativo para convidado"); }, function () {
                if (retornoItem.status == 0) {
                    _this.suporte.showAviso(retornoItem.mensagem);
                }
                if (retornoItem.status == 1) {
                    _this.suporte.showAviso(retornoItem.mensagem);
                }
                if (retornoItem.status == -1) {
                    _this.suporte.showAviso(_this.errorMensage);
                }
            });
        });
    };
    listCarteiraVirtual.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    listCarteiraVirtual.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getCarteirasDaAreaDeAcesso();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return listCarteiraVirtual;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
listCarteiraVirtual = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'listCarteiraVirtual-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/listCarteiraVirtual.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <ion-title><ion-icon name="card"></ion-icon> Carteiras de {{nomeAreaDeAcesso}}</ion-title>\n        <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n    \n<ion-content class="bgApp">    \n    <ion-refresher (ionRefresh)="doRefresh($event)">\n        <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>\n    <ion-card *ngFor="let carteira of listDeCarteiras">\n        <ion-card-content>\n          <ion-item> \n              <ion-thumbnail iten-start item-left large>\n                <img src="{{urlImageServ}}{{carteira.foto}}" *ngIf="carteira.foto" />\n                <ion-icon iten-end name="contact" item-end item-right large *ngIf="!carteira.foto"></ion-icon>\n              </ion-thumbnail>\n              <span class="fontSizeNome">{{carteira.nome}}</span><br />\n              <span class="fontSize">Validade: {{carteira.dataDeValidade}}</span><br />\n              <span *ngIf="carteira.validado" class="ativado"><ion-icon name="ios-checkmark-circle-outline"></ion-icon></span>\n              <span *ngIf="!carteira.validado" class="inativo">Suspensa</span>\n          </ion-item> \n        </ion-card-content>\n         <p class="divRodapeAtivo" *ngIf="carteira.statusCarteira == \'REGULAR\' && carteira.validado">{{carteira.statusCarteira}}</p>\n         <p class="divRodapeInativo" *ngIf="carteira.statusCarteira == \'EXPIRADO\' && carteira.validado">{{carteira.statusCarteira}}</p>\n      </ion-card>\n\n      <ion-item-divider color="light">\n          <ion-icon name="ios-people-outline"></ion-icon> Meus convidados para {{nomeAreaDeAcesso}}\n      </ion-item-divider>\n\n      <ion-card *ngFor="let convidado of listDeConvidados">\n        <ion-card-content>\n          <ion-item> \n              <ion-thumbnail iten-start item-left large>\n                <img src="{{urlImageServ}}{{convidado.foto}}" *ngIf="convidado.foto" />\n                <img src="assets/img/no_image.png" *ngIf="!convidado.foto" />\n              </ion-thumbnail>\n              <span class="fontSizeNome">{{convidado.nome}} {{convidado.sobrenome}}</span><br />\n              <span class="fontSize">Validade: {{convidado.dataDeValidade}}</span><br />\n              <span class="fontSize" *ngIf="convidado.statusCarteira == \'REGULAR\'">Status: <span class="ativado">{{convidado.statusCarteira}}</span><br /></span>\n              <span class="fontSize" *ngIf="convidado.statusCarteira == \'EXPIRADO\' && convidado.aprovado">Status: <span class="inativo">{{convidado.statusCarteira}}</span><br /></span>\n              \n              <span *ngIf="convidado.aprovado" class="ativado"><ion-icon name="ios-checkmark-circle-outline"></ion-icon></span>\n              <span *ngIf="!convidado.aprovado" class="inativo"><ion-icon name="ios-close-circle-outline"></ion-icon></span>\n              <ion-item>\n                <ion-toggle (ionChange)="getAtivaDesativa($event.checked,convidado.id)" iten-end [checked]="convidado.isAtivado"></ion-toggle>\n              </ion-item>\n          </ion-item> \n\n        </ion-card-content>\n         <ion-item (click)="openEditConvidado(convidado)" class="divRodapeAtivoConvidado" *ngIf="convidado.isAtivado && convidado.aprovado">ATIVO\n                <ion-icon class="marginIcon" iten-end name="ios-create-outline" item-end item-right large></ion-icon>\n                <!-- <ion-icon class="marginIcon" iten-end name="ios-lock-outline" item-end item-right large></ion-icon> -->\n         </ion-item>\n         <ion-item (click)="openEditConvidado(convidado)" class="divRodapeInativoConvidado" *ngIf="!convidado.isAtivado && convidado.aprovado">INATIVO\n                <ion-icon class="marginIcon" iten-end name="ios-create-outline" item-end item-right large></ion-icon>\n                <!-- <ion-icon class="marginIcon" iten-end name="ios-lock-outline" item-end item-right large></ion-icon> -->\n         </ion-item>\n      </ion-card>\n\n      <ion-fab right bottom *ngIf="permitirConvidado">\n        <button ion-fab color="secondary" (click)="openNovoConvidado()"><ion-icon name="add"></ion-icon></button>\n        <!-- <ion-fab-list side="left">\n          <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n          <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n          <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n          <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n        </ion-fab-list> -->\n      </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/listCarteiraVirtual.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_suporte__["a" /* Suporte */]])
], listCarteiraVirtual);

//# sourceMappingURL=listCarteiraVirtual.js.map

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalAddConvidado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var modalAddConvidado = (function (_super) {
    __extends(modalAddConvidado, _super);
    function modalAddConvidado(navCtrl, navParams, sp, _storage, fb, camera, fileUp, servUsuario, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.servUsuario = servUsuario;
        _this.viewCtrl = viewCtrl;
        _this.areaAcessoId = _this.navParams.get("idAreaAcesso");
        _this.nomeAreaDeAcesso = _this.navParams.get("nomeAreaDeAcesso");
        _this.qtdMax = _this.navParams.get("qtdConvidado");
        _this.qtdCadastrados = _this.navParams.get("qtdConvidadosCadastrados");
        //console.log("Area de Acesso: " + this.areaAcessoId);
        _this.form = _this.fb.group({
            nome: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            sobrenome: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            email: [""],
            foto: [""],
            idAreaAcesso: [_this.areaAcessoId]
        });
        return _this;
    }
    modalAddConvidado.prototype.ngOnInit = function () {
    };
    modalAddConvidado.prototype.addConvidado = function () {
        var _this = this;
        if (this.qtdMax <= this.qtdCadastrados) {
            this.sp.showAviso("Limite de convidados por área de acesso atingido!");
            return;
        }
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.form.addControl("idUsuarioResponsavel", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.userID));
                _this.form.addControl("idApartamento", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](dataUser.aptId));
                var userSave = _this.form.value;
                //console.log(userSave);
                _this.sp.showLoader("Cadastrando convidado...");
                var result_1;
                _this.servUsuario.novoConvidadoCarteira(userSave)
                    .subscribe(function (r) { return result_1 = r; }, function (error) { return console.log("Error ao salvar perfil"); }, function () {
                    if (result_1.status == 0) {
                        _this.sp.showAviso(result_1.mensagem);
                        _this.form.reset();
                        _this.navCtrl.pop();
                    }
                    if (result_1.status == 1) {
                        _this.sp.showAviso(result_1.mensagem);
                    }
                    if (result_1.status == -1) {
                        _this.sp.showAviso(_this.errorMensage);
                    }
                    _this.sp.dismissLoader();
                });
            }
        });
    };
    modalAddConvidado.prototype.takePic = function (tpFonte) {
        var _this = this;
        if (tpFonte == 'galeria') {
            var options = {
                quality: 50,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                correctOrientation: true,
                saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then(function (imageURI) {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:           
                _this.pathPreviewImage = imageURI;
                var guid = new Date().getTime();
                var nomeImage = imageURI.substr(imageURI.lastIndexOf('?') + 1);
                nomeImage = guid + nomeImage + ".jpg";
                _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
                _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            }, function (err) {
                // Handle error
                console.log('takePic: ' + err);
            });
        }
        else {
            var options = {
                quality: 50,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.CAMERA,
                allowEdit: true,
                correctOrientation: true,
                encodingType: this.camera.EncodingType.JPEG,
                saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then(function (imageURI) {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:           
                _this.pathPreviewImage = imageURI;
                var guid = new Date().getTime();
                var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                nomeImage = guid + nomeImage;
                // this.form.addControl("foto",new FormControl(nomeImage));
                _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
                _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            }, function (err) {
                // Handle error
                console.log('takePic: ' + err);
            });
        }
    };
    modalAddConvidado.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalAddConvidado;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalAddConvidado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modalAddConvidado-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/modalAddConvidado.html"*/'<ion-header>\n    <ion-toolbar color="secondary">\n        <ion-title>\n        <ion-icon name="person"></ion-icon>  Novo convidado para {{nomeAreaDeAcesso}}\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="form" (submit)="addConvidado()">                \n        <ion-list no-lines>                   \n            <!-- <ion-item-divider color="light">Período</ion-item-divider> -->\n           <ion-item *ngIf="pathPreviewImage">\n                <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n           </ion-item>\n          \n           <ion-item>\n            <button ion-button round padding type="button" color="secondary" (click)="takePic(\'camera\')">Câmera</button>\n            <button ion-button round padding type="button" color="secondary"(click)="takePic(\'galeria\')">Galeria</button>\n          </ion-item>\n          \n            <ion-item>\n                <ion-label>Nome</ion-label>\n                <ion-input type="text" formControlName="nome"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-label>Sobrenome</ion-label>\n                <ion-input type="text" formControlName="sobrenome"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label>E-mail</ion-label>\n                <ion-input type="text" formControlName="email"></ion-input>\n            </ion-item>    \n\n            <ion-item>\n                <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Salvar Convidado</button>\n            </ion-item>  \n        </ion-list>         \n    </form> \n\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/modalAddConvidado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__["a" /* Filetransferencia */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__["a" /* ServiceUsuario */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], modalAddConvidado);

//# sourceMappingURL=modalAddConvidado.js.map

/***/ }),

/***/ 433:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return modalEditConvidado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var modalEditConvidado = (function (_super) {
    __extends(modalEditConvidado, _super);
    function modalEditConvidado(navCtrl, navParams, sp, _storage, fb, camera, fileUp, servUsuario, viewCtrl) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.servUsuario = servUsuario;
        _this.viewCtrl = viewCtrl;
        _this.pathPreviewImage = _this.urlImageServ + "usuario/";
        _this.objConvidado = _this.navParams.data;
        _this.fotoConvidado = _this.objConvidado.foto;
        //console.log(this.objConvidado);
        _this.form = _this.fb.group({
            id: [_this.objConvidado.id],
            nome: [_this.objConvidado.nome, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            sobrenome: [_this.objConvidado.sobrenome, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            email: [_this.objConvidado.email],
            foto: [_this.objConvidado.foto]
        });
        return _this;
    }
    modalEditConvidado.prototype.ngOnInit = function () {
    };
    modalEditConvidado.prototype.EditConvidado = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                // this.form.addControl("idUsuarioResponsavel",new FormControl(dataUser.userID));
                // this.form.addControl("idApartamento",new FormControl(dataUser.aptId));
                var userSave = _this.form.value;
                //console.log(userSave);
                _this.sp.showLoader("Editando convidado...");
                var result_1;
                _this.servUsuario.editConvidadoCarteira(userSave)
                    .subscribe(function (r) { return result_1 = r; }, function (error) { return console.log("Error ao editar convidado"); }, function () {
                    if (result_1.status == 0) {
                        _this.sp.showAviso(result_1.mensagem);
                        _this.form.reset();
                        _this.navCtrl.pop();
                    }
                    if (result_1.status == 1) {
                        _this.sp.showAviso(result_1.mensagem);
                    }
                    if (result_1.status == -1) {
                        _this.sp.showAviso(_this.errorMensage);
                    }
                    _this.sp.dismissLoader();
                });
            }
        });
    };
    modalEditConvidado.prototype.takePic = function (tpFonte) {
        var _this = this;
        if (tpFonte == 'galeria') {
            var options = {
                quality: 50,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                correctOrientation: true,
                saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then(function (imageURI) {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:           
                _this.pathPreviewImage = imageURI;
                var guid = new Date().getTime();
                var nomeImage = imageURI.substr(imageURI.lastIndexOf('?') + 1);
                nomeImage = guid + nomeImage + ".jpg";
                _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
                _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            }, function (err) {
                // Handle error
                console.log('takePic: ' + err);
            });
        }
        else {
            var options = {
                quality: 50,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.CAMERA,
                allowEdit: true,
                correctOrientation: true,
                encodingType: this.camera.EncodingType.JPEG,
                saveToPhotoAlbum: false
            };
            this.camera.getPicture(options).then(function (imageURI) {
                // imageData is either a base64 encoded string or a file URI
                // If it's base64:           
                _this.pathPreviewImage = imageURI;
                var guid = new Date().getTime();
                var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
                nomeImage = guid + nomeImage;
                // this.form.addControl("foto",new FormControl(nomeImage));
                _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
                _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            }, function (err) {
                // Handle error
                console.log('takePic: ' + err);
            });
        }
    };
    modalEditConvidado.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return modalEditConvidado;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
modalEditConvidado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "modalEditConvidado-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/modalEditConvidado.html"*/'<ion-header>\n        <ion-toolbar color="secondary">\n            <ion-title>\n            <ion-icon name="person"></ion-icon>  Editar convidado\n            </ion-title>\n            <ion-buttons start>\n            <button ion-button (click)="dismiss()">\n                <span ion-text color="light" showWhen="ios">Voltar</span>\n                <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n            </button>\n            </ion-buttons>\n        </ion-toolbar>\n    </ion-header>\n    \n    <ion-content>\n    \n        <form [formGroup]="form" (submit)="EditConvidado()">                \n            <ion-list no-lines>                   \n                <!-- <ion-item-divider color="light">Período</ion-item-divider> -->\n               <ion-item *ngIf="fotoConvidado">\n                    <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n               </ion-item>\n              \n               <ion-item>\n                <button ion-button round padding type="button" color="secondary" (click)="takePic(\'camera\')">Câmera</button>\n                <button ion-button round padding type="button" color="secondary"(click)="takePic(\'galeria\')">Galeria</button>\n              </ion-item>\n              \n                <ion-item>\n                    <ion-label>Nome</ion-label>\n                    <ion-input type="text" formControlName="nome"></ion-input>\n                </ion-item>\n                <ion-item>\n                    <ion-label>Sobrenome</ion-label>\n                    <ion-input type="text" formControlName="sobrenome"></ion-input>\n                </ion-item>\n    \n                <ion-item>\n                    <ion-label>E-mail</ion-label>\n                    <ion-input type="text" formControlName="email"></ion-input>\n                </ion-item>    \n    \n                <ion-item>\n                    <button ion-button block padding type="submit" color="topPortaria" [disabled]="!form.valid">Salvar alterações</button>\n                </ion-item>  \n            </ion-list>         \n        </form> \n    \n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/carteiraVirtual/modalEditConvidado.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__["a" /* Filetransferencia */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__["a" /* ServiceUsuario */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], modalEditConvidado);

//# sourceMappingURL=modalEditConvidado.js.map

/***/ }),

/***/ 434:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return administradora; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceBoleto__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__boleto__ = __webpack_require__(436);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var administradora = (function (_super) {
    __extends(administradora, _super);
    function administradora(storage, navCtrl, suporte, modal, alertCtrl, servUsuario, servBoleto) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servUsuario = servUsuario;
        _this.servBoleto = servBoleto;
        _this.storage.get("objCondominio").then(function (oCondominioConfig) {
            if (oCondominioConfig.urlWebServer != null && oCondominioConfig.boletoFolder != null) {
                _this.boletoApp = true;
            }
            if (oCondominioConfig.refId != null && oCondominioConfig.refId > 0) {
                _this.balanceteApp = true;
            }
        });
        return _this;
    }
    administradora.prototype.ionViewDidLoad = function () { };
    administradora.prototype.ngOnInit = function () {
    };
    administradora.prototype.abrirBoleto = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (userLogado) {
            _this.oUserLogado = userLogado;
            if (userLogado.cpf == null) {
                _this.openModalCpf();
            }
            else {
                _this.getBoletoServer(userLogado.cpf);
            }
        });
    };
    administradora.prototype.getBoletoServer = function (strCPF) {
        var _this = this;
        this.storage.get("objCondominio").then(function (oCondominio) {
            var folder = oCondominio.boletoFolder;
            var wsboleto = oCondominio.urlWebServer;
            var objBoletoList;
            _this.suporte.showLoader("Buscando boleto...");
            _this.servBoleto.getBoletoService(strCPF, folder, wsboleto)
                .subscribe(function (result) { return objBoletoList = result; }, function (error) { return _this.suporte.showAviso(_this.errorMensage); }, function () {
                _this.suporte.dismissLoader();
                if (objBoletoList[0].id > 0) {
                    _this.suporte.showAviso(objBoletoList[0].mensagem);
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__boleto__["a" /* boleto */], objBoletoList);
                }
            });
        });
    };
    administradora.prototype.openModalCpf = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Entre com seu CPF',
            message: "Para acesso ao boleto, informe seu CPF.",
            inputs: [
                {
                    name: 'cpf',
                    placeholder: 'cpf'
                },
            ],
            buttons: [
                {
                    text: 'Enviar CPF',
                    handler: function (data) {
                        if (data.cpf != "") {
                            _this.suporte.showLoader("Atualizando CPF...");
                            var result_1;
                            _this.servUsuario.setCpfUser(_this.oUserLogado.userID, data.cpf)
                                .subscribe(function (ret) { return result_1 = ret; }, function (error) { return console.log("Error verificar código."); }, function () {
                                if (result_1.status == 0) {
                                    _this.suporte.dismissLoader();
                                    _this.getBoletoServer(data.cpf);
                                }
                                if (result_1.status == 1) {
                                    _this.suporte.dismissLoader();
                                    _this.suporte.showAviso(result_1.mensagem);
                                }
                                if (result_1.status == -1) {
                                    _this.suporte.dismissLoader();
                                    _this.suporte.showAviso(_this.errorMensage);
                                }
                            });
                        }
                        else {
                            _this.suporte.showAviso("Digite seu CPF");
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    administradora.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return administradora;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
administradora = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'administradora-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/administradora/administradora.html"*/'<ion-header>     \n    <ion-navbar color="danger">\n      <ion-title>Administradora</ion-title>\n    </ion-navbar>\n </ion-header>\n   \n <ion-content class="bgApp">          \n     <ion-refresher (ionRefresh)="doRefresh($event)">\n         <ion-refresher-content></ion-refresher-content>\n     </ion-refresher>\n     \n     <ion-card *ngIf="boletoApp" (click)="abrirBoleto()">\n        <ion-card-content>\n          <ion-item> \n              <ion-icon iten-end name="barcode" item-end item-right large color="topCorrespondencia"></ion-icon>\n              <span class="fontSize">Retire seu boleto</span>\n          </ion-item>\n        </ion-card-content>\n      </ion-card>\n     \n </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/administradora/administradora.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceUsuario__["a" /* ServiceUsuario */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceBoleto__["a" /* ServiceBoleto */]])
], administradora);

//# sourceMappingURL=administradora.js.map

/***/ }),

/***/ 435:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceBoleto; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceBoleto = (function () {
    function ServiceBoleto(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_BOLETO;
    }
    ServiceBoleto.prototype.getBoletoService = function (strCpf, strFolder, strUrlWs) {
        var params = "cpfstr=" + strCpf + "&folderBoleto=" + strFolder + "&linkerp=" + strUrlWs;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "boleto/getBoleto?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceBoleto;
}());
ServiceBoleto = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceBoleto);

//# sourceMappingURL=ServiceBoleto.js.map

/***/ }),

/***/ 436:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return boleto; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var boleto = (function (_super) {
    __extends(boleto, _super);
    function boleto(storage, navCtrl, navParams, suporte, modal, alertCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        //this.strCpfQueryString = this.navParams.get("cpfStr");
        _this.listObjBoleto = _this.navParams.data;
        return _this;
        // this.beneficiario = this.objBoleto.beneficiario;
        // this.beneficiarioCnpj = this.objBoleto.beneficiarioCnpj;
        // this.vencimento = this.objBoleto.vencimento;
        // this.pagador = this.objBoleto.pagador;
        // this.dataDoc = this.objBoleto.dataDoc;
        // this.valorDoc = this.objBoleto.valorDoc;
        // this.urlBoleto = this.objBoleto.urlBoleto;
    }
    boleto.prototype.ionViewDidLoad = function () { };
    boleto.prototype.ngOnInit = function () {
    };
    boleto.prototype.exibirBoleto = function (url) {
        this.suporte.webOptions.location = "yes";
        this.suporte.openUrlSite(url);
    };
    boleto.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return boleto;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
boleto = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'boleto-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/administradora/boleto.html"*/'<ion-header>     \n    <ion-navbar color="danger">\n      <ion-title>Meu boleto</ion-title>\n    </ion-navbar>\n </ion-header>\n   \n <ion-content class="bgApp">          \n     <ion-refresher (ionRefresh)="doRefresh($event)">\n         <ion-refresher-content></ion-refresher-content>\n     </ion-refresher>\n     \n     <ion-card *ngFor="let oBoleto of listObjBoleto">\n        <ion-card-content>\n          <p><strong>Beneficiário:</strong> {{oBoleto.beneficiario}}</p>\n          <p><strong>CNPJ Beneficiário:</strong> {{oBoleto.beneficiarioCnpj}}</p>\n          <p><strong>Vencimento:</strong> {{oBoleto.vencimento}}</p>\n          <p><strong>Pagador:</strong> {{oBoleto.pagador}}</p>\n          <p><strong>Data do documento:</strong> {{oBoleto.dataDoc}}</p>\n          <p><strong>Valor:</strong> {{oBoleto.valorDoc}}</p>\n          <p>\n              <button ion-button round padding type="button" (click)="exibirBoleto(oBoleto.urlBoleto)"  color="danger" style="min-width: 220px"> Visualizar boleto</button>\n          </p>\n        </ion-card-content>\n      </ion-card>\n     \n </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/administradora/boleto.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
], boleto);

//# sourceMappingURL=boleto.js.map

/***/ }),

/***/ 437:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Login; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_ServiceUnidade__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__home_home__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__perfil_novoUsuario__ = __webpack_require__(438);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/*
  Generated class for the login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var Login = (function () {
    function Login(navCtrl, navParams, alertCtrl, fb, sp, storage, device, oneSignal, servUnidade, _servUsuario) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.fb = fb;
        this.sp = sp;
        this.storage = storage;
        this.device = device;
        this.oneSignal = oneSignal;
        this.servUnidade = servUnidade;
        this._servUsuario = _servUsuario;
        this.form = fb.group({
            email: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            senha: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
    }
    Login.prototype.autenticar = function () {
        var _this = this;
        var obj = this.form.value;
        this.sp.showLoader("Bem vindo ao CondomínioApp.com...");
        this._servUsuario.autenticarUsuario(obj.email, obj.senha)
            .subscribe(function (usuario) { return _this.UsuarioLogado = usuario; }, function (error) { return console.log('Error reportado: ' + error); }, function () {
            _this.sp.dismissLoader();
            if (_this.UsuarioLogado.status == 0) {
                _this.storage.set('UsuarioLogado', _this.UsuarioLogado);
                _this.storage.set("connectionStatus", "On");
                _this.registraDevice(_this.UsuarioLogado.userID);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__home_home__["a" /* Home */]);
            }
            else {
                _this.sp.showAlert('Login de Usuário', _this.UsuarioLogado.mensagem);
            }
        });
    };
    Login.prototype.registraDevice = function (userId) {
        var _this = this;
        this.oneSignal.getIds().then(function (objDeviceKey) {
            var dispositivo = {
                id: 0,
                modelo: _this.device.model,
                mobileID: _this.device.uuid,
                usuarioID: userId,
                plataforma: _this.device.platform,
                versao: _this.device.version,
                deviceKey: objDeviceKey.userId
                //deviceKey: 1
            };
            var retorno;
            _this._servUsuario.gravaDispositivo(dispositivo)
                .subscribe(function (r) { return retorno = r; }, function (error) { return console.log('Error ao gravar dispositivo'); }, function () {
                if (retorno.status > 0) {
                    dispositivo.id = retorno.status;
                    _this.storage.set("dispositivo", dispositivo);
                }
                console.log(retorno.mensagem);
            });
        });
    };
    Login.prototype.openModalCodigo = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Código de unidade',
            message: "Entre com o código de sua unidade. Caso seu condomínio não esteja utilizando o Aplicativo CondominioApp.com clique em quero testar.",
            inputs: [
                {
                    name: 'codigo',
                    placeholder: 'Código'
                },
            ],
            buttons: [
                {
                    text: 'Quero testar o app',
                    handler: function (data) {
                        _this.iniciarTeste();
                    }
                },
                {
                    text: 'Validar código',
                    handler: function (data) {
                        //console.log(data.codigo);
                        if (data.codigo != "") {
                            _this.sp.showLoader("Validando código...");
                            var result_1;
                            _this.servUnidade.validarCodigoUnidade(data.codigo)
                                .subscribe(function (ret) { return result_1 = ret; }, function (error) { return console.log("Error verificar código."); }, function () {
                                if (result_1.status == 0) {
                                    _this.sp.dismissLoader();
                                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__perfil_novoUsuario__["a" /* novoUsuario */], data.codigo);
                                }
                                if (result_1.status == 1) {
                                    _this.sp.dismissLoader();
                                    _this.sp.showAviso(result_1.mensagem);
                                }
                                if (result_1.status == -1) {
                                    _this.sp.dismissLoader();
                                    _this.sp.showAviso(__WEBPACK_IMPORTED_MODULE_7__app_globalVariables__["a" /* global */].ERROR_MSG);
                                }
                            });
                        }
                        else {
                            _this.sp.showAviso("Digite o código de sua unidade");
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    Login.prototype.iniciarTeste = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__perfil_novoUsuario__["a" /* novoUsuario */], "172d");
    };
    Login.prototype.openRecuperarSenha = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: 'Recuperar senha',
            message: "Entre com o e-mail cadastrado.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'E-mail'
                },
            ],
            buttons: [
                {
                    text: 'Cancelar',
                    handler: function (data) {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Recuperar senha',
                    handler: function (data) {
                        //console.log(data.email);
                        if (data.email != "" && __WEBPACK_IMPORTED_MODULE_7__app_globalVariables__["a" /* global */].EMAIL_REGEX.test(data.email)) {
                            _this.sp.showLoader("Enviando e-mail de recuperação de senha...");
                            var result_2;
                            _this._servUsuario.enviarEmailGetSenha(data.email.toLowerCase())
                                .subscribe(function (ret) { return result_2 = ret; }, function (error) {
                                _this.sp.dismissLoader();
                                _this.sp.showAviso(__WEBPACK_IMPORTED_MODULE_7__app_globalVariables__["a" /* global */].ERROR_MSG);
                                console.log("Error verificar email.");
                            }, function () {
                                if (result_2.status == 0) {
                                    _this.sp.dismissLoader();
                                    _this.sp.showAviso(result_2.mensagem);
                                }
                                if (result_2.status == 1) {
                                    _this.sp.dismissLoader();
                                    _this.sp.showAviso(result_2.mensagem);
                                }
                                if (result_2.status == -1) {
                                    _this.sp.dismissLoader();
                                    _this.sp.showAviso(__WEBPACK_IMPORTED_MODULE_7__app_globalVariables__["a" /* global */].ERROR_MSG);
                                }
                            });
                        }
                        else {
                            _this.sp.showAviso("Digite um e-mail para validação ");
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    return Login;
}());
Login = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/login/login.html"*/'﻿<!--\n  Generated template for the NewPage page.\n\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>     \n  \n </ion-header>\n<ion-content class="pageLogin">\n  <ion-grid no-padding class="topIpad">\n    <ion-row>\n      <ion-col col-12 text-center>\n        <!-- <img src="assets/img/Icon_CondominioApp.png" class="logoLogin" /><br /> -->\n        <img src="assets/img/simbolo.gif" class="logoLogin" /><br />\n        <img src="assets/img/logotipo2.png" class="nLogoText"/>\n      </ion-col>\n    </ion-row>\n   <!-- <ion-row>\n      <ion-col col-12 text-center>\n        <img src="assets/img/imgCodigo.png" class="imgBtnCodigo" (click)="openModalCodigo()" />\n      </ion-col>\n      <ion-col col-6>\n        <a href="#" ><img src="../../assets/img/imgTeste.png" /></a>\n      </ion-col>\n    </ion-row>-->\n  </ion-grid>\n  \n  <form [formGroup]="form" (submit)="autenticar()" class="topForm">\n    <ion-list no-lines>\n\n      <ion-item>\n        <ion-label>E-mail</ion-label>\n        <ion-input type="text" formControlName="email"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label>Senha</ion-label>\n        <ion-input type="password" formControlName="senha"></ion-input>\n      </ion-item>\n\n      <ion-item text-center>\n        <button ion-button round padding type="submit" color="btnLogin" [disabled]="!form.valid" style="width: 220px">Entrar</button>\n      </ion-item>\n\n      <ion-item text-center>\n        <p class="txt" (click)="openRecuperarSenha()">Esqueci minha senha</p><br />\n        <p  (click)="openModalCodigo()">Não possui uma conta ainda? <span class="txt">Cadastre-se agora!</span></p>\n      </ion-item>\n\n    </ion-list>\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/login/login.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_device__["a" /* Device */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__["a" /* OneSignal */],
        __WEBPACK_IMPORTED_MODULE_9__service_ServiceUnidade__["a" /* ServiceUnidade */],
        __WEBPACK_IMPORTED_MODULE_8__service_ServiceUsuario__["a" /* ServiceUsuario */]])
], Login);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 438:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return novoUsuario; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__home_home__ = __webpack_require__(83);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
var novoUsuario = (function (_super) {
    __extends(novoUsuario, _super);
    function novoUsuario(navCtrl, navParams, sp, _storage, modalCtrl, fb, camera, fileUp, servUsuario) {
        var _this = _super.call(this, navCtrl, _storage) || this;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.sp = sp;
        _this._storage = _storage;
        _this.modalCtrl = modalCtrl;
        _this.fb = fb;
        _this.camera = camera;
        _this.fileUp = fileUp;
        _this.servUsuario = servUsuario;
        _this.codigoUnidade = _this.navParams.data;
        //console.log( this.codigoUnidade);
        _this.form = _this.fb.group({
            foto: [""],
            nome: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            sobrenome: [""],
            cel: [""],
            proprietario: [false],
            email: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required],
            senha: ["", __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    novoUsuario.prototype.ngOnInit = function () {
    };
    novoUsuario.prototype.novoPerfil = function () {
        var _this = this;
        var userSave = this.form.value;
        //console.log(userSave);
        var user = {
            nome: userSave.nome,
            sobrenome: userSave.sobrenome,
            cel: userSave.cel,
            email: userSave.email,
            senha: userSave.senha,
            proprietario: userSave.proprietario,
            foto: userSave.foto,
            codApartamento: this.codigoUnidade
        };
        this.sp.showLoader("Criando perfil...");
        var result;
        this.servUsuario.novoUsuario(user)
            .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao salvar perfil"); }, function () {
            if (result.status == 0) {
                //this.sp.showAviso(result.mensagem);
                _this.form.reset();
                var UsuarioLogado_1;
                _this.servUsuario.autenticarUsuario(user.email, user.senha)
                    .subscribe(function (usuario) { return UsuarioLogado_1 = usuario; }, function (error) { return console.log('Error reportado: ' + error); }, function () {
                    //console.log(this.UsuarioLogado);
                    if (UsuarioLogado_1.status == 0) {
                        _this.storage.set('UsuarioLogado', UsuarioLogado_1);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__home_home__["a" /* Home */]);
                    }
                    else {
                        _this.sp.showAlert('Autenticação', UsuarioLogado_1.mensagem);
                    }
                });
            }
            if (result.status == 1) {
                _this.sp.showAviso(result.mensagem);
            }
            if (result.status == -1) {
                _this.sp.showAviso(__WEBPACK_IMPORTED_MODULE_9__app_globalVariables__["a" /* global */].ERROR_MSG);
            }
            _this.sp.dismissLoader();
        });
    };
    novoUsuario.prototype.takePic = function (tpFonte) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        };
        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
        this.camera.getPicture(options).then(function (imageURI) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;
            var guid = new Date().getTime();
            var nomeImage = imageURI.substr(imageURI.lastIndexOf('/') + 1);
            nomeImage = guid + nomeImage;
            // this.form.addControl("foto",new FormControl(nomeImage));
            _this.form.setControl("foto", new __WEBPACK_IMPORTED_MODULE_3__angular_forms__["b" /* FormControl */](nomeImage));
            _this.fileUp.upload(nomeImage, imageURI, 'Cliente');
            _this.sp.showAviso(_this.fotoAnexada);
        }, function (err) {
            // Handle error
            console.log('takePic: ' + err);
        });
    };
    return novoUsuario;
}(__WEBPACK_IMPORTED_MODULE_8__base_pageBase__["a" /* PageBase */]));
novoUsuario = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "novoUsuario-page",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/perfil/novoUsuario.html"*/'<ion-header>\n        <ion-navbar color="secondary">\n          <ion-title><ion-icon name="person"></ion-icon> Novo perfil</ion-title>\n          <!-- <ion-buttons end>\n            <button ion-button icon-only color="royal" (click)="openModalMinhasReservas()">\n              <ion-icon name="more"></ion-icon>\n            </button>\n          </ion-buttons> -->\n        </ion-navbar>\n      </ion-header>\n      \n      <ion-content>\n        <form [formGroup]="form" (submit)="novoPerfil()">\n            \n             <ion-list no-lines>    \n               <ion-item>\n                 <ion-label stacked>Nome</ion-label>\n                 <ion-input placeholder="Nome de usuário" formControlName="nome" type="text"></ion-input>\n               </ion-item>\n               \n               <ion-item>\n                <ion-label stacked>Sobrenome</ion-label>\n                <ion-input placeholder="Sobrenome" formControlName="sobrenome" type="text"></ion-input>\n              </ion-item>\n    \n              <ion-item>\n                <ion-label stacked>Cel</ion-label>\n                <ion-input placeholder="Celular" formControlName="cel" type="text"></ion-input>\n              </ion-item>\n\n              <ion-item>\n                <ion-label>Sou proprietário</ion-label>\n                <ion-checkbox formControlName="proprietario"></ion-checkbox>\n              </ion-item>\n\n              <ion-item>\n                <ion-label stacked>E-mail</ion-label>\n                <ion-input placeholder="E-mail" formControlName="email" type="text"></ion-input>\n              </ion-item>\n    \n              <ion-item>\n                <ion-label stacked>Senha</ion-label>\n                <ion-input placeholder="Senha" formControlName="senha" type="password"></ion-input>\n              </ion-item>\n          \n              <ion-item>\n                <button ion-button round padding type="button" color="secondary" (click)="takePic(\'camera\')">Câmera</button>\n                <button ion-button round padding type="button" color="secondary"(click)="takePic(\'galeria\')">Galeria</button>\n              </ion-item>\n\n              <!-- <ion-item *ngIf="pathPreviewImage">\n                <img src="{{pathPreviewImage}}" style="max-width:280px;height:auto;margin:4px auto" />\n              </ion-item> -->\n         \n              <ion-item>\n                  <button ion-button block padding type="submit" color="secondary" [disabled]="!form.valid">Salvar perfil</button>\n              </ion-item>\n         \n            </ion-list>\n         \n           </form>\n      </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/perfil/novoUsuario.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_6__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_5__app_app_filetransferencia__["a" /* Filetransferencia */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceUsuario__["a" /* ServiceUsuario */]])
], novoUsuario);

//# sourceMappingURL=novoUsuario.js.map

/***/ }),

/***/ 439:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(440);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);



Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic2_calendar__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_Truncatepipe__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts__ = __webpack_require__(525);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_camera__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_transfer__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_file__ = __webpack_require__(576);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_network__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_device__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_onesignal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_barcode_scanner__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__app_component__ = __webpack_require__(578);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__app_filetransferencia__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__service_ServiceHome__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__service_ServiceComunicado__ = __webpack_require__(67);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__service_ServiceAgenda__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__service_ServiceReserva__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__service_ServiceCorrespondencia__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__service_ServiceTemporada__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__service_ServiceRecado__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__service_ServiceMural__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__service_ServiceClassificado__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__service_ServiceEnquete__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__service_ServiceVaga__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__service_ServiceVeiculo__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__service_ServiceTransferencia__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__service_ServiceProdServ__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__service_ServiceUnidade__ = __webpack_require__(163);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__service_ServiceBoleto__ = __webpack_require__(435);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__service_ServiceCarteiraDigital__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_home_home__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_login_login__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_comunicadoOcorrencia_comunicadoOcorrencia__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_comunicadoOcorrencia_tabComunicados__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__pages_comunicadoOcorrencia_tabOcorrencias__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_comunicadoOcorrencia_tabArquivosComunicados__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__pages_comunicadoOcorrencia_tabArquivosCategoria__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__pages_comunicadoOcorrencia_modalComunicadoContent__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__pages_comunicadoOcorrencia_modalOcorrenciaContent__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_comunicadoOcorrencia_modalNovaOcorrencia__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_agendaReserva_agendaReserva__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__pages_agendaReserva_modalNovaReserva__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__pages_agendaReserva_menuSuspenso__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_agendaReserva_atividade__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_agendaReserva_minhasReservas__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__pages_agendaReserva_calendarioReservas__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_correspondencia_correspondencia__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_portaria_portaria__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_portaria_tabVisitas__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__pages_portaria_tabTemporada__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__pages_portaria_modalNovaTemporada__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__pages_portaria_modalEditTemporada__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__pages_portaria_tabRecados__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__pages_portaria_meusVisitantes__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__pages_portaria_listaDeConvidados__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__pages_portaria_modalNovoVisitante__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__pages_portaria_modalEditVisitante__ = __webpack_require__(400);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__pages_portaria_modalAutorizacaoEntrada__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__pages_portaria_modalAutorizarEntrada__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__pages_portaria_modalAutorizacaoMultEntrada__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__pages_mural_mural__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__pages_mural_modalNovoPost__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__pages_classificado_classificado__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__pages_classificado_tabClassificado__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__pages_classificado_tabMeusAnuncios__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__pages_classificado_modalNovoClassificado__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__pages_classificado_modalEditClassificado__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__pages_enquete_enquete__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__pages_enquete_enqueteDetails__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__pages_enquete_enqueteVotar__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__pages_vaga_vaga__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__pages_vaga_tabClassificadoVagas__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__pages_vaga_tabVagas__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__pages_vaga_tabVeiculos__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__pages_vaga_modalAddVeiculo__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__pages_vaga_modalEditVeiculo__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__pages_vaga_modalChatVaga__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__pages_transferencia_transferencia__ = __webpack_require__(415);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93__pages_transferencia_tabSolicitacoes__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__pages_transferencia_tabTransferidas__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__pages_transferencia_tabRequisicoes__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__pages_transferencia_recebidas__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_97__pages_produtoServicos_produtoServicos__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_98__pages_produtoServicos_tabHome__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_99__pages_produtoServicos_tabCategorias__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_100__pages_produtoServicos_anunciantes__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_101__pages_produtoServicos_loja__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_102__pages_produtoServicos_anuncioDetalhes__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_103__pages_produtoServicos_EnviarContato__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_104__pages_chat_conversas__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_105__pages_chat_mensagens__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_106__pages_chat_contatos__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_107__pages_perfil_perfil__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_108__pages_perfil_novoUsuario__ = __webpack_require__(438);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_109__pages_unidade_unidade__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_110__pages_carteiraVirtual_carteiraVirtual__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_111__pages_carteiraVirtual_tabAcesso__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_112__pages_administradora_administradora__ = __webpack_require__(434);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_113__pages_administradora_boleto__ = __webpack_require__(436);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_114__pages_carteiraVirtual_tabAreasDeAcesso__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_115__pages_carteiraVirtual_qr_carteiraVirtual__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_116__pages_carteiraVirtual_listCarteiraVirtual__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_117__pages_carteiraVirtual_modalAddConvidado__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_118__pages_carteiraVirtual_modalEditConvidado__ = __webpack_require__(433);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
























































































































function createConfig() {
    var c = new __WEBPACK_IMPORTED_MODULE_8_ng2_signalr__["c" /* SignalRConfiguration */]();
    //c.qs = 'CondominioApp',
    c.hubName = 'chatHub';
    c.url = __WEBPACK_IMPORTED_MODULE_22__globalVariables__["a" /* global */].BASE_SIGNALR;
    c.logging = true;
    c.jsonp = true;
    c.withCredentials = false;
    c.transport = [__WEBPACK_IMPORTED_MODULE_8_ng2_signalr__["a" /* ConnectionTransports */].webSockets];
    return c;
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_45__pages_home_home__["a" /* Home */],
            __WEBPACK_IMPORTED_MODULE_46__pages_login_login__["a" /* Login */],
            __WEBPACK_IMPORTED_MODULE_47__pages_comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */],
            __WEBPACK_IMPORTED_MODULE_52__pages_comunicadoOcorrencia_modalComunicadoContent__["a" /* modalComunicadoContent */],
            __WEBPACK_IMPORTED_MODULE_53__pages_comunicadoOcorrencia_modalOcorrenciaContent__["a" /* modalOcorrenciaContent */],
            __WEBPACK_IMPORTED_MODULE_54__pages_comunicadoOcorrencia_modalNovaOcorrencia__["a" /* modalNovaOcorrencia */],
            __WEBPACK_IMPORTED_MODULE_48__pages_comunicadoOcorrencia_tabComunicados__["a" /* tabComunicados */],
            __WEBPACK_IMPORTED_MODULE_49__pages_comunicadoOcorrencia_tabOcorrencias__["a" /* tabOcorrencias */],
            __WEBPACK_IMPORTED_MODULE_50__pages_comunicadoOcorrencia_tabArquivosComunicados__["a" /* tabArquivosComunicados */],
            __WEBPACK_IMPORTED_MODULE_51__pages_comunicadoOcorrencia_tabArquivosCategoria__["a" /* tabArquivosCategoria */],
            __WEBPACK_IMPORTED_MODULE_55__pages_agendaReserva_agendaReserva__["a" /* agendaReserva */],
            __WEBPACK_IMPORTED_MODULE_56__pages_agendaReserva_modalNovaReserva__["a" /* modalNovaReserva */],
            __WEBPACK_IMPORTED_MODULE_57__pages_agendaReserva_menuSuspenso__["a" /* menuSuspenso */],
            __WEBPACK_IMPORTED_MODULE_58__pages_agendaReserva_atividade__["a" /* atividade */],
            __WEBPACK_IMPORTED_MODULE_59__pages_agendaReserva_minhasReservas__["a" /* minhasReservas */],
            __WEBPACK_IMPORTED_MODULE_60__pages_agendaReserva_calendarioReservas__["a" /* calendarioReservas */],
            __WEBPACK_IMPORTED_MODULE_61__pages_correspondencia_correspondencia__["a" /* correspondencia */],
            __WEBPACK_IMPORTED_MODULE_62__pages_portaria_portaria__["a" /* portaria */],
            __WEBPACK_IMPORTED_MODULE_63__pages_portaria_tabVisitas__["a" /* tabVisitas */],
            __WEBPACK_IMPORTED_MODULE_64__pages_portaria_tabTemporada__["a" /* tabTemporada */],
            __WEBPACK_IMPORTED_MODULE_67__pages_portaria_tabRecados__["a" /* tabRecados */],
            __WEBPACK_IMPORTED_MODULE_65__pages_portaria_modalNovaTemporada__["a" /* modalNovaTemporada */],
            __WEBPACK_IMPORTED_MODULE_66__pages_portaria_modalEditTemporada__["a" /* modalEditTemporada */],
            __WEBPACK_IMPORTED_MODULE_68__pages_portaria_meusVisitantes__["a" /* meusVisitantes */],
            __WEBPACK_IMPORTED_MODULE_69__pages_portaria_listaDeConvidados__["a" /* listaDeConvidados */],
            __WEBPACK_IMPORTED_MODULE_70__pages_portaria_modalNovoVisitante__["a" /* modalNovoVisitante */],
            __WEBPACK_IMPORTED_MODULE_71__pages_portaria_modalEditVisitante__["a" /* modalEditVisitante */],
            __WEBPACK_IMPORTED_MODULE_72__pages_portaria_modalAutorizacaoEntrada__["a" /* modalAutorizacaoEntrada */],
            __WEBPACK_IMPORTED_MODULE_73__pages_portaria_modalAutorizarEntrada__["a" /* modalAutorizarEntrada */],
            __WEBPACK_IMPORTED_MODULE_74__pages_portaria_modalAutorizacaoMultEntrada__["a" /* modalAutorizacaoMultEntrada */],
            __WEBPACK_IMPORTED_MODULE_75__pages_mural_mural__["a" /* mural */],
            __WEBPACK_IMPORTED_MODULE_76__pages_mural_modalNovoPost__["a" /* modalNovoPost */],
            __WEBPACK_IMPORTED_MODULE_77__pages_classificado_classificado__["a" /* classificado */],
            __WEBPACK_IMPORTED_MODULE_78__pages_classificado_tabClassificado__["a" /* tabClassificado */],
            __WEBPACK_IMPORTED_MODULE_79__pages_classificado_tabMeusAnuncios__["a" /* tabMeusAnuncios */],
            __WEBPACK_IMPORTED_MODULE_80__pages_classificado_modalNovoClassificado__["a" /* modalNovoClassificado */],
            __WEBPACK_IMPORTED_MODULE_81__pages_classificado_modalEditClassificado__["a" /* modalEditClassificado */],
            __WEBPACK_IMPORTED_MODULE_82__pages_enquete_enquete__["a" /* enquete */],
            __WEBPACK_IMPORTED_MODULE_83__pages_enquete_enqueteDetails__["a" /* enqueteDetails */],
            __WEBPACK_IMPORTED_MODULE_84__pages_enquete_enqueteVotar__["a" /* enqueteVotar */],
            __WEBPACK_IMPORTED_MODULE_85__pages_vaga_vaga__["a" /* vaga */],
            __WEBPACK_IMPORTED_MODULE_86__pages_vaga_tabClassificadoVagas__["a" /* tabClassificadoVagas */],
            __WEBPACK_IMPORTED_MODULE_87__pages_vaga_tabVagas__["a" /* tabVagas */],
            __WEBPACK_IMPORTED_MODULE_88__pages_vaga_tabVeiculos__["a" /* tabVeiculos */],
            __WEBPACK_IMPORTED_MODULE_89__pages_vaga_modalAddVeiculo__["a" /* modalAddVeiculo */],
            __WEBPACK_IMPORTED_MODULE_90__pages_vaga_modalEditVeiculo__["a" /* modalEditVeiculo */],
            __WEBPACK_IMPORTED_MODULE_91__pages_vaga_modalChatVaga__["a" /* modalChatVaga */],
            __WEBPACK_IMPORTED_MODULE_92__pages_transferencia_transferencia__["a" /* transferencia */],
            __WEBPACK_IMPORTED_MODULE_93__pages_transferencia_tabSolicitacoes__["a" /* tabSolicitacoes */],
            __WEBPACK_IMPORTED_MODULE_94__pages_transferencia_tabTransferidas__["a" /* tabTransferidas */],
            __WEBPACK_IMPORTED_MODULE_95__pages_transferencia_tabRequisicoes__["a" /* tabRequisicoes */],
            __WEBPACK_IMPORTED_MODULE_96__pages_transferencia_recebidas__["a" /* recebidas */],
            __WEBPACK_IMPORTED_MODULE_97__pages_produtoServicos_produtoServicos__["a" /* produtoServicos */],
            __WEBPACK_IMPORTED_MODULE_102__pages_produtoServicos_anuncioDetalhes__["a" /* anuncioDetalhes */],
            __WEBPACK_IMPORTED_MODULE_98__pages_produtoServicos_tabHome__["a" /* tabHome */],
            __WEBPACK_IMPORTED_MODULE_99__pages_produtoServicos_tabCategorias__["a" /* tabCategorias */],
            __WEBPACK_IMPORTED_MODULE_100__pages_produtoServicos_anunciantes__["a" /* anunciantes */],
            __WEBPACK_IMPORTED_MODULE_101__pages_produtoServicos_loja__["a" /* loja */],
            __WEBPACK_IMPORTED_MODULE_104__pages_chat_conversas__["a" /* conversas */],
            __WEBPACK_IMPORTED_MODULE_105__pages_chat_mensagens__["a" /* mensagens */],
            __WEBPACK_IMPORTED_MODULE_106__pages_chat_contatos__["a" /* contatos */],
            __WEBPACK_IMPORTED_MODULE_107__pages_perfil_perfil__["a" /* perfil */],
            __WEBPACK_IMPORTED_MODULE_108__pages_perfil_novoUsuario__["a" /* novoUsuario */],
            __WEBPACK_IMPORTED_MODULE_109__pages_unidade_unidade__["a" /* unidade */],
            __WEBPACK_IMPORTED_MODULE_7__app_Truncatepipe__["a" /* TruncatePipe */],
            __WEBPACK_IMPORTED_MODULE_110__pages_carteiraVirtual_carteiraVirtual__["a" /* carteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_111__pages_carteiraVirtual_tabAcesso__["a" /* tabAcesso */],
            __WEBPACK_IMPORTED_MODULE_112__pages_administradora_administradora__["a" /* administradora */],
            __WEBPACK_IMPORTED_MODULE_113__pages_administradora_boleto__["a" /* boleto */],
            __WEBPACK_IMPORTED_MODULE_114__pages_carteiraVirtual_tabAreasDeAcesso__["a" /* tabAreasDeAcesso */],
            __WEBPACK_IMPORTED_MODULE_115__pages_carteiraVirtual_qr_carteiraVirtual__["a" /* qr_carteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_116__pages_carteiraVirtual_listCarteiraVirtual__["a" /* listCarteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_117__pages_carteiraVirtual_modalAddConvidado__["a" /* modalAddConvidado */],
            __WEBPACK_IMPORTED_MODULE_118__pages_carteiraVirtual_modalEditConvidado__["a" /* modalEditConvidado */],
            __WEBPACK_IMPORTED_MODULE_103__pages_produtoServicos_EnviarContato__["a" /* EnviarContato */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_8_ng2_signalr__["d" /* SignalRModule */].forRoot(createConfig),
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_10__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_6_ionic2_calendar__["a" /* NgCalendarModule */],
            __WEBPACK_IMPORTED_MODULE_9_ng2_charts__["ChartsModule"],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */], {
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                monthShortNames: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
                dayNames: ['domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira'],
                dayShortNames: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab'],
                backButtonText: '',
                tabsHideOnSubPages: false
            }, {
                links: []
            }),
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                name: '_condominioAppDB',
                driverOrder: ['indexeddb', 'sqlite', 'websql']
            })
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_19__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_45__pages_home_home__["a" /* Home */],
            __WEBPACK_IMPORTED_MODULE_46__pages_login_login__["a" /* Login */],
            __WEBPACK_IMPORTED_MODULE_47__pages_comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */],
            __WEBPACK_IMPORTED_MODULE_52__pages_comunicadoOcorrencia_modalComunicadoContent__["a" /* modalComunicadoContent */],
            __WEBPACK_IMPORTED_MODULE_53__pages_comunicadoOcorrencia_modalOcorrenciaContent__["a" /* modalOcorrenciaContent */],
            __WEBPACK_IMPORTED_MODULE_54__pages_comunicadoOcorrencia_modalNovaOcorrencia__["a" /* modalNovaOcorrencia */],
            __WEBPACK_IMPORTED_MODULE_48__pages_comunicadoOcorrencia_tabComunicados__["a" /* tabComunicados */],
            __WEBPACK_IMPORTED_MODULE_49__pages_comunicadoOcorrencia_tabOcorrencias__["a" /* tabOcorrencias */],
            __WEBPACK_IMPORTED_MODULE_50__pages_comunicadoOcorrencia_tabArquivosComunicados__["a" /* tabArquivosComunicados */],
            __WEBPACK_IMPORTED_MODULE_51__pages_comunicadoOcorrencia_tabArquivosCategoria__["a" /* tabArquivosCategoria */],
            __WEBPACK_IMPORTED_MODULE_55__pages_agendaReserva_agendaReserva__["a" /* agendaReserva */],
            __WEBPACK_IMPORTED_MODULE_56__pages_agendaReserva_modalNovaReserva__["a" /* modalNovaReserva */],
            __WEBPACK_IMPORTED_MODULE_57__pages_agendaReserva_menuSuspenso__["a" /* menuSuspenso */],
            __WEBPACK_IMPORTED_MODULE_58__pages_agendaReserva_atividade__["a" /* atividade */],
            __WEBPACK_IMPORTED_MODULE_59__pages_agendaReserva_minhasReservas__["a" /* minhasReservas */],
            __WEBPACK_IMPORTED_MODULE_60__pages_agendaReserva_calendarioReservas__["a" /* calendarioReservas */],
            __WEBPACK_IMPORTED_MODULE_61__pages_correspondencia_correspondencia__["a" /* correspondencia */],
            __WEBPACK_IMPORTED_MODULE_62__pages_portaria_portaria__["a" /* portaria */],
            __WEBPACK_IMPORTED_MODULE_63__pages_portaria_tabVisitas__["a" /* tabVisitas */],
            __WEBPACK_IMPORTED_MODULE_64__pages_portaria_tabTemporada__["a" /* tabTemporada */],
            __WEBPACK_IMPORTED_MODULE_67__pages_portaria_tabRecados__["a" /* tabRecados */],
            __WEBPACK_IMPORTED_MODULE_65__pages_portaria_modalNovaTemporada__["a" /* modalNovaTemporada */],
            __WEBPACK_IMPORTED_MODULE_66__pages_portaria_modalEditTemporada__["a" /* modalEditTemporada */],
            __WEBPACK_IMPORTED_MODULE_68__pages_portaria_meusVisitantes__["a" /* meusVisitantes */],
            __WEBPACK_IMPORTED_MODULE_69__pages_portaria_listaDeConvidados__["a" /* listaDeConvidados */],
            __WEBPACK_IMPORTED_MODULE_70__pages_portaria_modalNovoVisitante__["a" /* modalNovoVisitante */],
            __WEBPACK_IMPORTED_MODULE_71__pages_portaria_modalEditVisitante__["a" /* modalEditVisitante */],
            __WEBPACK_IMPORTED_MODULE_72__pages_portaria_modalAutorizacaoEntrada__["a" /* modalAutorizacaoEntrada */],
            __WEBPACK_IMPORTED_MODULE_73__pages_portaria_modalAutorizarEntrada__["a" /* modalAutorizarEntrada */],
            __WEBPACK_IMPORTED_MODULE_74__pages_portaria_modalAutorizacaoMultEntrada__["a" /* modalAutorizacaoMultEntrada */],
            __WEBPACK_IMPORTED_MODULE_75__pages_mural_mural__["a" /* mural */],
            __WEBPACK_IMPORTED_MODULE_76__pages_mural_modalNovoPost__["a" /* modalNovoPost */],
            __WEBPACK_IMPORTED_MODULE_77__pages_classificado_classificado__["a" /* classificado */],
            __WEBPACK_IMPORTED_MODULE_78__pages_classificado_tabClassificado__["a" /* tabClassificado */],
            __WEBPACK_IMPORTED_MODULE_79__pages_classificado_tabMeusAnuncios__["a" /* tabMeusAnuncios */],
            __WEBPACK_IMPORTED_MODULE_80__pages_classificado_modalNovoClassificado__["a" /* modalNovoClassificado */],
            __WEBPACK_IMPORTED_MODULE_81__pages_classificado_modalEditClassificado__["a" /* modalEditClassificado */],
            __WEBPACK_IMPORTED_MODULE_82__pages_enquete_enquete__["a" /* enquete */],
            __WEBPACK_IMPORTED_MODULE_83__pages_enquete_enqueteDetails__["a" /* enqueteDetails */],
            __WEBPACK_IMPORTED_MODULE_84__pages_enquete_enqueteVotar__["a" /* enqueteVotar */],
            __WEBPACK_IMPORTED_MODULE_85__pages_vaga_vaga__["a" /* vaga */],
            __WEBPACK_IMPORTED_MODULE_86__pages_vaga_tabClassificadoVagas__["a" /* tabClassificadoVagas */],
            __WEBPACK_IMPORTED_MODULE_87__pages_vaga_tabVagas__["a" /* tabVagas */],
            __WEBPACK_IMPORTED_MODULE_88__pages_vaga_tabVeiculos__["a" /* tabVeiculos */],
            __WEBPACK_IMPORTED_MODULE_89__pages_vaga_modalAddVeiculo__["a" /* modalAddVeiculo */],
            __WEBPACK_IMPORTED_MODULE_90__pages_vaga_modalEditVeiculo__["a" /* modalEditVeiculo */],
            __WEBPACK_IMPORTED_MODULE_91__pages_vaga_modalChatVaga__["a" /* modalChatVaga */],
            __WEBPACK_IMPORTED_MODULE_92__pages_transferencia_transferencia__["a" /* transferencia */],
            __WEBPACK_IMPORTED_MODULE_93__pages_transferencia_tabSolicitacoes__["a" /* tabSolicitacoes */],
            __WEBPACK_IMPORTED_MODULE_94__pages_transferencia_tabTransferidas__["a" /* tabTransferidas */],
            __WEBPACK_IMPORTED_MODULE_95__pages_transferencia_tabRequisicoes__["a" /* tabRequisicoes */],
            __WEBPACK_IMPORTED_MODULE_96__pages_transferencia_recebidas__["a" /* recebidas */],
            __WEBPACK_IMPORTED_MODULE_97__pages_produtoServicos_produtoServicos__["a" /* produtoServicos */],
            __WEBPACK_IMPORTED_MODULE_102__pages_produtoServicos_anuncioDetalhes__["a" /* anuncioDetalhes */],
            __WEBPACK_IMPORTED_MODULE_98__pages_produtoServicos_tabHome__["a" /* tabHome */],
            __WEBPACK_IMPORTED_MODULE_99__pages_produtoServicos_tabCategorias__["a" /* tabCategorias */],
            __WEBPACK_IMPORTED_MODULE_100__pages_produtoServicos_anunciantes__["a" /* anunciantes */],
            __WEBPACK_IMPORTED_MODULE_101__pages_produtoServicos_loja__["a" /* loja */],
            __WEBPACK_IMPORTED_MODULE_104__pages_chat_conversas__["a" /* conversas */],
            __WEBPACK_IMPORTED_MODULE_105__pages_chat_mensagens__["a" /* mensagens */],
            __WEBPACK_IMPORTED_MODULE_106__pages_chat_contatos__["a" /* contatos */],
            __WEBPACK_IMPORTED_MODULE_107__pages_perfil_perfil__["a" /* perfil */],
            __WEBPACK_IMPORTED_MODULE_108__pages_perfil_novoUsuario__["a" /* novoUsuario */],
            __WEBPACK_IMPORTED_MODULE_109__pages_unidade_unidade__["a" /* unidade */],
            __WEBPACK_IMPORTED_MODULE_110__pages_carteiraVirtual_carteiraVirtual__["a" /* carteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_111__pages_carteiraVirtual_tabAcesso__["a" /* tabAcesso */],
            __WEBPACK_IMPORTED_MODULE_112__pages_administradora_administradora__["a" /* administradora */],
            __WEBPACK_IMPORTED_MODULE_113__pages_administradora_boleto__["a" /* boleto */],
            __WEBPACK_IMPORTED_MODULE_114__pages_carteiraVirtual_tabAreasDeAcesso__["a" /* tabAreasDeAcesso */],
            __WEBPACK_IMPORTED_MODULE_115__pages_carteiraVirtual_qr_carteiraVirtual__["a" /* qr_carteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_116__pages_carteiraVirtual_listCarteiraVirtual__["a" /* listCarteiraVirtual */],
            __WEBPACK_IMPORTED_MODULE_117__pages_carteiraVirtual_modalAddConvidado__["a" /* modalAddConvidado */],
            __WEBPACK_IMPORTED_MODULE_118__pages_carteiraVirtual_modalEditConvidado__["a" /* modalEditConvidado */],
            __WEBPACK_IMPORTED_MODULE_103__pages_produtoServicos_EnviarContato__["a" /* EnviarContato */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_16__ionic_native_device__["a" /* Device */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_15__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_23__service_ServiceUsuario__["a" /* ServiceUsuario */],
            __WEBPACK_IMPORTED_MODULE_18__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_24__service_ServiceHome__["a" /* ServiceHome */],
            __WEBPACK_IMPORTED_MODULE_25__service_ServiceComunicado__["a" /* ServiceComunicado */],
            __WEBPACK_IMPORTED_MODULE_20__app_suporte__["a" /* Suporte */],
            __WEBPACK_IMPORTED_MODULE_21__app_filetransferencia__["a" /* Filetransferencia */],
            __WEBPACK_IMPORTED_MODULE_26__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
            __WEBPACK_IMPORTED_MODULE_27__service_ServiceAgenda__["a" /* ServiceAgenda */],
            __WEBPACK_IMPORTED_MODULE_28__service_ServiceReserva__["a" /* ServiceReserva */],
            __WEBPACK_IMPORTED_MODULE_29__service_ServiceCorrespondencia__["a" /* ServiceCorrespondencia */],
            __WEBPACK_IMPORTED_MODULE_30__service_ServiceVisita__["a" /* ServiceVisita */],
            __WEBPACK_IMPORTED_MODULE_31__service_ServiceTemporada__["a" /* ServiceTemporada */],
            __WEBPACK_IMPORTED_MODULE_32__service_ServiceRecado__["a" /* ServiceRecado */],
            __WEBPACK_IMPORTED_MODULE_33__service_ServiceMural__["a" /* ServiceMural */],
            __WEBPACK_IMPORTED_MODULE_34__service_ServiceClassificado__["a" /* ServiceClassificado */],
            __WEBPACK_IMPORTED_MODULE_35__service_ServiceEnquete__["a" /* ServiceEnquete */],
            __WEBPACK_IMPORTED_MODULE_36__service_ServiceVaga__["a" /* ServiceVaga */],
            __WEBPACK_IMPORTED_MODULE_37__service_ServiceVeiculo__["a" /* ServiceVeiculo */],
            __WEBPACK_IMPORTED_MODULE_38__service_ServiceTransferencia__["a" /* ServiceTransferencia */],
            __WEBPACK_IMPORTED_MODULE_39__service_ServiceProdServ__["a" /* ServiceProdServ */],
            __WEBPACK_IMPORTED_MODULE_40__service_ServiceChat__["a" /* ServiceChat */],
            __WEBPACK_IMPORTED_MODULE_41__service_ServiceSignalr__["a" /* ServiceSignalr */],
            __WEBPACK_IMPORTED_MODULE_42__service_ServiceUnidade__["a" /* ServiceUnidade */],
            __WEBPACK_IMPORTED_MODULE_43__service_ServiceBoleto__["a" /* ServiceBoleto */],
            __WEBPACK_IMPORTED_MODULE_44__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["LOCALE_ID"], useValue: "pt-BR" }
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Suporte; });
/* unused harmony export EmailValidator */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Suporte = (function () {
    function Suporte(alert, loading, inAppBrowser, toastCtrl) {
        this.alert = alert;
        this.loading = loading;
        this.inAppBrowser = inAppBrowser;
        this.toastCtrl = toastCtrl;
        this.webOptions = {
            location: 'no',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Fechar',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
        this.formatDate = function (dt) {
            var date = new Date(dt);
            var day = date.getDate();
            var monthIndex = date.getMonth() + 1;
            var year = date.getFullYear();
            //return day + '/' + monthIndex + '/' + year;
            return year + '-' + monthIndex + '-' + day;
        };
        this.formatDateTime = function () {
            var date = new Date();
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var day = date.getDate();
            var monthIndex = date.getMonth() + 1;
            var year = date.getFullYear();
            //return day + '/' + monthIndex + '/' + year;
            return year + '-' + monthIndex + '-' + day + " " + hours + ":" + minutes;
        };
    }
    Suporte.prototype.showAviso = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.present();
    };
    Suporte.prototype.showAvisoTop = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    Suporte.prototype.showAlert = function (tituloStr, msgStr) {
        var alert = this.alert.create({
            title: tituloStr,
            subTitle: msgStr,
            buttons: ['OK']
        });
        alert.present();
    };
    Suporte.prototype.showLoading = function (msg, tempo) {
        this.loader = this.loading.create({
            content: msg,
            duration: tempo
            //dismissOnPageChange :true
        });
        this.loader.present();
    };
    Suporte.prototype.showLoader = function (msg) {
        this.loader = this.loading.create({
            content: msg
        });
        this.loader.present();
    };
    Suporte.prototype.dismissLoader = function () {
        this.loader.dismiss();
    };
    Suporte.prototype.openFileDocs = function (url) {
        if (url.indexOf(".jpg") != -1 ||
            url.indexOf(".jpeg") != -1 ||
            url.indexOf(".png") != -1 ||
            url.indexOf(".gif") != -1) {
            this.openUrlSite(url);
            return;
        }
        url = 'http://docs.google.com/viewer?url=' + url;
        this.openUrlSite(url);
    };
    // private openFileImage(url) {
    //     //window.open(url, "_blank", "location=yes,toolbar=yes,hardwareback=yes");
    //     let target = "_blank";
    //     this.inAppBrowser.create(url,target,);
    //     //appBrowser.show();
    // }
    Suporte.prototype.openUrlSite = function (url) {
        var target = "_blank";
        this.inAppBrowser.create(url, target, this.webOptions);
    };
    return Suporte;
}());
Suporte = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */]])
], Suporte);

var EmailValidator = (function () {
    function EmailValidator() {
    }
    EmailValidator.isValidMailFormat = function (control) {
        var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "Por favor digite um e-mail válido!": true };
        }
        return null;
    };
    return EmailValidator;
}());

//# sourceMappingURL=app.suporte.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceSignalr; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceSignalr = (function () {
    function ServiceSignalr(signalr) {
        this.signalr = signalr;
        //console.log("Construir Signalr");
        if (this.connection == null)
            this.connection = this.signalr.createConnection();
    }
    ServiceSignalr.prototype.iniciarConexao = function () {
        if (this.connection != null) {
            this.connection.start();
        }
    };
    ServiceSignalr.prototype.pararConexao = function () {
        if (this.connection != null) {
            this.connection.stop();
        }
    };
    return ServiceSignalr;
}());
ServiceSignalr = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ng2_signalr__["b" /* SignalR */]])
], ServiceSignalr);

//# sourceMappingURL=ServiceSignalr.js.map

/***/ }),

/***/ 512:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TruncatePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var TruncatePipe = (function () {
    function TruncatePipe() {
    }
    TruncatePipe.prototype.transform = function (value, args) {
        // let limit = args.length > 0 ? parseInt(args[0], 10) : 10;
        // let trail = args.length > 1 ? args[1] : '...';
        var limit = args ? parseInt(args, 10) : 10;
        var trail = '...';
        return value.length > limit ? value.substring(0, limit) + trail : value;
    };
    return TruncatePipe;
}());
TruncatePipe = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'limitTo'
    })
], TruncatePipe);

//# sourceMappingURL=app.Truncatepipe.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceCarteiraDigital; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceCarteiraDigital = (function () {
    function ServiceCarteiraDigital(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
    }
    ServiceCarteiraDigital.prototype.getCarteirasDaUnidade = function (unidadeID) {
        var params = 'unidadeId=' + unidadeID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "carteira/getCarteiraDaUnidade?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceCarteiraDigital.prototype.getAtivaDesativaConvidado = function (convidadoId, idArea, contr) {
        var params = 'convidadoCarteirinhaId=' + convidadoId + "&idAreaDeAcesso=" + idArea + "&controle=" + contr;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "ConvidadoCarteira/ativaDesativaConvidado?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceCarteiraDigital.prototype.getEfetuaCheckIn = function (uID, idArea, idCarteira, dateTime) {
        var params = 'usuarioID=' + uID + "&areaDeAcessoID=" + idArea + "&carteiraID=" + idCarteira + "&dtCheckIn=" + dateTime;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Carteira/setCheckin?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceCarteiraDigital;
}());
ServiceCarteiraDigital = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceCarteiraDigital);

//# sourceMappingURL=ServiceCarteiraDigital.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceOcorrencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceOcorrencia provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceOcorrencia = (function () {
    function ServiceOcorrencia(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceOcorrencia Provider');
    }
    ServiceOcorrencia.prototype.getOcorrencias = function (userID, idCondominio) {
        var params = 'uID=' + userID + '&condominioID=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "ocorrencia/GetOcorrencias?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceOcorrencia.prototype.addnovaOcorrencia = function (ocorrenciaObj) {
        console.log(ocorrenciaObj);
        var params = JSON.stringify(ocorrenciaObj);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Ocorrencia/addOcorrencia', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceOcorrencia.prototype.addOcorrenciaEmergencia = function (ocorrenciaEmergencia) {
        var params = "clienteId=" + ocorrenciaEmergencia.clienteId + "&condominioId=" + ocorrenciaEmergencia.condominioId + "&idApartamentoOcorrencia=" + ocorrenciaEmergencia.idApartamentoOcorrencia + "&EmergenciaID=" + ocorrenciaEmergencia.EmergenciaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Ocorrencia/addOcorrenciaEmergencia?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceOcorrencia.prototype.getOcorrenciaID = function (ocorrenciaID) {
        var params = 'idOcorrencia=' + ocorrenciaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'ocorrencia/getOcorrenciaID?' + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceOcorrencia;
}());
ServiceOcorrencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceOcorrencia);

//# sourceMappingURL=ServiceOcorrencia.js.map

/***/ }),

/***/ 558:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 266,
	"./af.js": 266,
	"./ar": 267,
	"./ar-dz": 268,
	"./ar-dz.js": 268,
	"./ar-kw": 269,
	"./ar-kw.js": 269,
	"./ar-ly": 270,
	"./ar-ly.js": 270,
	"./ar-ma": 271,
	"./ar-ma.js": 271,
	"./ar-sa": 272,
	"./ar-sa.js": 272,
	"./ar-tn": 273,
	"./ar-tn.js": 273,
	"./ar.js": 267,
	"./az": 274,
	"./az.js": 274,
	"./be": 275,
	"./be.js": 275,
	"./bg": 276,
	"./bg.js": 276,
	"./bn": 277,
	"./bn.js": 277,
	"./bo": 278,
	"./bo.js": 278,
	"./br": 279,
	"./br.js": 279,
	"./bs": 280,
	"./bs.js": 280,
	"./ca": 281,
	"./ca.js": 281,
	"./cs": 282,
	"./cs.js": 282,
	"./cv": 283,
	"./cv.js": 283,
	"./cy": 284,
	"./cy.js": 284,
	"./da": 285,
	"./da.js": 285,
	"./de": 286,
	"./de-at": 287,
	"./de-at.js": 287,
	"./de-ch": 288,
	"./de-ch.js": 288,
	"./de.js": 286,
	"./dv": 289,
	"./dv.js": 289,
	"./el": 290,
	"./el.js": 290,
	"./en-au": 291,
	"./en-au.js": 291,
	"./en-ca": 292,
	"./en-ca.js": 292,
	"./en-gb": 293,
	"./en-gb.js": 293,
	"./en-ie": 294,
	"./en-ie.js": 294,
	"./en-nz": 295,
	"./en-nz.js": 295,
	"./eo": 296,
	"./eo.js": 296,
	"./es": 297,
	"./es-do": 298,
	"./es-do.js": 298,
	"./es.js": 297,
	"./et": 299,
	"./et.js": 299,
	"./eu": 300,
	"./eu.js": 300,
	"./fa": 301,
	"./fa.js": 301,
	"./fi": 302,
	"./fi.js": 302,
	"./fo": 303,
	"./fo.js": 303,
	"./fr": 304,
	"./fr-ca": 305,
	"./fr-ca.js": 305,
	"./fr-ch": 306,
	"./fr-ch.js": 306,
	"./fr.js": 304,
	"./fy": 307,
	"./fy.js": 307,
	"./gd": 308,
	"./gd.js": 308,
	"./gl": 309,
	"./gl.js": 309,
	"./gom-latn": 310,
	"./gom-latn.js": 310,
	"./he": 311,
	"./he.js": 311,
	"./hi": 312,
	"./hi.js": 312,
	"./hr": 313,
	"./hr.js": 313,
	"./hu": 314,
	"./hu.js": 314,
	"./hy-am": 315,
	"./hy-am.js": 315,
	"./id": 316,
	"./id.js": 316,
	"./is": 317,
	"./is.js": 317,
	"./it": 318,
	"./it.js": 318,
	"./ja": 319,
	"./ja.js": 319,
	"./jv": 320,
	"./jv.js": 320,
	"./ka": 321,
	"./ka.js": 321,
	"./kk": 322,
	"./kk.js": 322,
	"./km": 323,
	"./km.js": 323,
	"./kn": 324,
	"./kn.js": 324,
	"./ko": 325,
	"./ko.js": 325,
	"./ky": 326,
	"./ky.js": 326,
	"./lb": 327,
	"./lb.js": 327,
	"./lo": 328,
	"./lo.js": 328,
	"./lt": 329,
	"./lt.js": 329,
	"./lv": 330,
	"./lv.js": 330,
	"./me": 331,
	"./me.js": 331,
	"./mi": 332,
	"./mi.js": 332,
	"./mk": 333,
	"./mk.js": 333,
	"./ml": 334,
	"./ml.js": 334,
	"./mr": 335,
	"./mr.js": 335,
	"./ms": 336,
	"./ms-my": 337,
	"./ms-my.js": 337,
	"./ms.js": 336,
	"./my": 338,
	"./my.js": 338,
	"./nb": 339,
	"./nb.js": 339,
	"./ne": 340,
	"./ne.js": 340,
	"./nl": 341,
	"./nl-be": 342,
	"./nl-be.js": 342,
	"./nl.js": 341,
	"./nn": 343,
	"./nn.js": 343,
	"./pa-in": 344,
	"./pa-in.js": 344,
	"./pl": 345,
	"./pl.js": 345,
	"./pt": 346,
	"./pt-br": 347,
	"./pt-br.js": 347,
	"./pt.js": 346,
	"./ro": 348,
	"./ro.js": 348,
	"./ru": 349,
	"./ru.js": 349,
	"./sd": 350,
	"./sd.js": 350,
	"./se": 351,
	"./se.js": 351,
	"./si": 352,
	"./si.js": 352,
	"./sk": 353,
	"./sk.js": 353,
	"./sl": 354,
	"./sl.js": 354,
	"./sq": 355,
	"./sq.js": 355,
	"./sr": 356,
	"./sr-cyrl": 357,
	"./sr-cyrl.js": 357,
	"./sr.js": 356,
	"./ss": 358,
	"./ss.js": 358,
	"./sv": 359,
	"./sv.js": 359,
	"./sw": 360,
	"./sw.js": 360,
	"./ta": 361,
	"./ta.js": 361,
	"./te": 362,
	"./te.js": 362,
	"./tet": 363,
	"./tet.js": 363,
	"./th": 364,
	"./th.js": 364,
	"./tl-ph": 365,
	"./tl-ph.js": 365,
	"./tlh": 366,
	"./tlh.js": 366,
	"./tr": 367,
	"./tr.js": 367,
	"./tzl": 368,
	"./tzl.js": 368,
	"./tzm": 369,
	"./tzm-latn": 370,
	"./tzm-latn.js": 370,
	"./tzm.js": 369,
	"./uk": 371,
	"./uk.js": 371,
	"./ur": 372,
	"./ur.js": 372,
	"./uz": 373,
	"./uz-latn": 374,
	"./uz-latn.js": 374,
	"./uz.js": 373,
	"./vi": 375,
	"./vi.js": 375,
	"./x-pseudo": 376,
	"./x-pseudo.js": 376,
	"./yo": 377,
	"./yo.js": 377,
	"./zh-cn": 378,
	"./zh-cn.js": 378,
	"./zh-hk": 379,
	"./zh-hk.js": 379,
	"./zh-tw": 380,
	"./zh-tw.js": 380
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 558;

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return mensagens; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceSignalr__ = __webpack_require__(51);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { SignalR } from "ng2-signalr";




var mensagens = (function (_super) {
    __extends(mensagens, _super);
    function mensagens(storage, navCtrl, params, suporte, modal, alertCtrl, servChat, viewCtrl, fb, signalr) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.params = params;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servChat = servChat;
        _this.viewCtrl = viewCtrl;
        _this.fb = fb;
        _this.signalr = signalr;
        _this.listaDeMensagens = [];
        _this.conversaSelect = _this.params.data;
        _this.connection = _this.signalr.connection;
        //console.log(JSON.stringify(this.conversaSelect));
        _this.form = fb.group({
            descricao: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["g" /* Validators */].required]
        });
        return _this;
    }
    mensagens.prototype.ngOnInit = function () {
        var _this = this;
        this.getMensagens();
        var onMessageSent = this.connection.listenFor("addMensagemChatApp");
        onMessageSent.subscribe(function (oMessage) { return _this.receberMsgChat(oMessage); });
    };
    mensagens.prototype.receberMsgChat = function (oMsg) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            //oMsg.userIDdestinatario == dataUser.userID
            //console.log(oMsg);
            if (_this.conversaSelect.id == oMsg.conversaId) {
                var nDate = new Date();
                var mes = nDate.getMonth() + 1;
                var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();
                oMsg.strDataDeCadastro = ddt;
                _this.listaDeMensagens.push(oMsg);
                _this.scrollToBottom();
            }
        });
    };
    mensagens.prototype.enviarMensagem = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var obj = _this.form.value;
            var novaMensagem = {
                id: 0,
                conversaId: _this.conversaSelect.id,
                descricao: obj.descricao,
                usuarioId: dataUser.userID
            };
            //console.log(JSON.stringify(novaMensagem));
            var retorno;
            var nDate = new Date();
            var mes = nDate.getMonth() + 1;
            var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();
            var ddt2 = nDate.getFullYear() + '-' + mes + '-' + nDate.getDate();
            var mensagemPush = {
                conversaId: novaMensagem.conversaId,
                descricao: novaMensagem.descricao,
                id: novaMensagem.id,
                strDataDeCadastro: ddt,
                usuarioId: novaMensagem.usuarioId
            };
            _this.listaDeMensagens.push(mensagemPush);
            _this.form.reset();
            if (_this.content._scroll)
                setTimeout(function () { return _this.content.scrollToBottom(); }, 100);
            _this.servChat.enviarMensagem(novaMensagem)
                .subscribe(function (result) { return retorno = result; }, function (error) { return console.log("error ao enviar a mensagem"); }, function () {
                if (retorno.status > 0) {
                    novaMensagem.id = retorno.status;
                    var obj = { id: retorno.status,
                        descricao: novaMensagem.descricao,
                        conversaId: mensagemPush.conversaId,
                        usuarioId: dataUser.userID,
                        nomeUsuario: dataUser.nome,
                        foto: dataUser.foto,
                        dataDeCadastro: ddt2,
                        lida: false,
                        userIDdestinatario: _this.conversaSelect.idDoUltimoUsuario,
                        strDataDeCadastro: ddt };
                    // console.log(obj);
                    _this.connection.invoke("SendChatMessageIonic", obj).then(function (result) { return console.log("Mensagem enviada com sucesso."); });
                    var resultPushOff_1;
                    _this.servChat.pushClientesOff(obj.conversaId, obj.usuarioId, _this.urlImageServ + "usuario/" + obj.foto, obj.descricao)
                        .subscribe(function (r) { return resultPushOff_1 = r; }, function (error) { return console.log("Error ao enviar"); }, function () {
                        //console.log(resultPushOff);
                    });
                }
                if (retorno.status == -1) {
                    _this.suporte.showAviso(_this.errorMensage);
                }
            });
        });
    };
    mensagens.prototype.scrollToBottom = function () {
        var _this = this;
        if (this.content._scroll)
            setTimeout(function () { return _this.content.scrollToBottom(); }, 1500);
    };
    mensagens.prototype.ionViewDidLoad = function () {
        this.scrollToBottom();
    };
    mensagens.prototype.getMensagens = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.idUsuarioLogado = dataUser.userID;
                _this.suporte.showLoader("Carregando mensagens...");
                _this.servChat.listarMensagem(_this.conversaSelect.id)
                    .subscribe(function (conv) { return _this.listaDeMensagens = conv; }, function (error) { return console.log("Error ao obter as conversas"); }, function () {
                    if (_this.listaDeMensagens.length > 0)
                        _this.qtdSkipMsg = 10;
                    _this.suporte.dismissLoader();
                });
            }
        });
    };
    mensagens.prototype.getMaisMensagens = function () {
        var _this = this;
        var arrMaisMensagens;
        this.servChat.listarMaisMensagens(this.conversaSelect.id, this.qtdSkipMsg)
            .subscribe(function (arr) { return arrMaisMensagens = arr; }, function (error) { return console.log("error ao obter mais mensagens"); }, function () {
            if (arrMaisMensagens.length > 0) {
                _this.qtdSkipMsg += 10;
                for (var i = 0; arrMaisMensagens.length > i; i++) {
                    _this.listaDeMensagens.unshift(arrMaisMensagens[i]);
                }
            }
        });
    };
    mensagens.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    mensagens.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getMaisMensagens();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return mensagens;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* Content */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* Content */])
], mensagens.prototype, "content", void 0);
mensagens = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'mensagens-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/mensagens.html"*/'<ion-header>\n    <ion-navbar color="primary">\n        <ion-title>     \n            <ion-item color="primary">\n                <ion-avatar item-start>\n                    <img src=\'{{urlImageServ}}usuario/{{conversaSelect.foto}}\' *ngIf="conversaSelect.foto">\n                    <img src="assets/img/Icon_CondominioApp.png" *ngIf="!conversaSelect.foto">\n                </ion-avatar> \n                {{conversaSelect.strNome}} \n            </ion-item>\n        </ion-title>\n        <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n        </ion-buttons>\n    </ion-navbar>\n</ion-header>\n      \n<ion-content class="bg">   \n    <ion-refresher (ionRefresh)="doRefresh($event)">\n       <ion-refresher-content></ion-refresher-content>\n    </ion-refresher>  \n\n    <ion-list no-lines class="transparente">\n        <ion-item text-wrap *ngFor="let msg of listaDeMensagens" class="transparente">\n            <span *ngIf="idUsuarioLogado == msg.usuarioId" class="bubble2"> \n                <span class="textMenor">{{msg.strDataDeCadastro}}</span><br />{{msg.descricao}}\n            </span>\n            <span *ngIf="idUsuarioLogado != msg.usuarioId" class="bubble"> \n                    <span class="textMenor">{{msg.strDataDeCadastro}}</span> <br /> {{msg.descricao}}\n            </span>\n        </ion-item>  \n    </ion-list>\n</ion-content>\n\n<ion-footer class="cxInput">\n    <form [formGroup]="form" method="post" (submit)="enviarMensagem()">\n        <ion-textarea type="text" placeholder="Escreva sua mensagem" class="inputLocation" rows="2" formControlName="descricao"></ion-textarea>\n        <ion-buttons end>\n            <button ion-button icon-only clear [disabled]="!form.valid">\n                <ion-icon name="send"></ion-icon>\n            </button>\n        </ion-buttons>\n    </form>\n</ion-footer>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/chat/mensagens.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceChat__["a" /* ServiceChat */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceSignalr__["a" /* ServiceSignalr */]])
], mensagens);

//# sourceMappingURL=mensagens.js.map

/***/ }),

/***/ 57:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProdServ; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceProdServ = (function () {
    function ServiceProdServ(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        // console.log('Hello ServiceTransferencia');
    }
    ServiceProdServ.prototype.listarCategorias = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Categoria/getCategorias", options)
            .map(function (res) { return res.json(); });
    };
    ServiceProdServ.prototype.listarAnunciantesPorCategoria = function (categoriaID, idCondominio) {
        var params = 'catID=' + categoriaID + "&condominioID=" + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Categoria/getAnunciantesPorCategoria?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceProdServ.prototype.listarAnuncios = function (anuncianteID) {
        var params = 'idLojista=' + anuncianteID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Anuncio/getAnunciosDoLojista?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceProdServ.prototype.enviaRequisicaoContratar = function (novoContratar) {
        // console.log(novoContratar);
        var params = JSON.stringify(novoContratar);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'ProdutoServ/EnviarContatoLojista', params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceProdServ;
}());
ServiceProdServ = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceProdServ);

//# sourceMappingURL=ServiceProdServ.js.map

/***/ }),

/***/ 578:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_ServiceCarteiraDigital__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__service_ServiceHome__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(437);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_comunicadoOcorrencia_comunicadoOcorrencia__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_agendaReserva_agendaReserva__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_correspondencia_correspondencia__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_portaria_portaria__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_mural_mural__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_classificado_classificado__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_enquete_enquete__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_vaga_vaga__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_produtoServicos_produtoServicos__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_chat_conversas__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_perfil_perfil__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_unidade_unidade__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_vaga_tabVeiculos__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_agendaReserva_atividade__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_carteiraVirtual_carteiraVirtual__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__globalVariables__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};































var MyApp = (function () {
    function MyApp(platform, alertCtrl, storage, statusBar, splashScreen, modal, device, suporte, _signalR, oneSignal, servUsuario, servOcorrencia, _servHome, servCkecIn) {
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.modal = modal;
        this.device = device;
        this.suporte = suporte;
        this._signalR = _signalR;
        this.oneSignal = oneSignal;
        this.servUsuario = servUsuario;
        this.servOcorrencia = servOcorrencia;
        this._servHome = _servHome;
        this.servCkecIn = servCkecIn;
        this.nomeUsuario = 'Bem Vindo(a)';
        this.strFotoUsuario = "";
        this.bloco_qd = "Nf";
        this.andar_lt = "Nf";
        this.unidade_num = "Nf";
        this.iterator = 0;
        this.urlImageServ = __WEBPACK_IMPORTED_MODULE_30__globalVariables__["a" /* global */].BASE_API_UPLOAD;
    }
    MyApp.prototype.ngOnInit = function () {
        this.initializeApp();
    };
    MyApp.prototype.postCheckinsOffLine = function (dataCheckins) {
        var _this = this;
        var qtdLoop = dataCheckins.length;
        //alert("qtdLoop: " + qtdLoop);
        var element = dataCheckins[this.iterator];
        var check;
        this.servCkecIn.getEfetuaCheckIn(element.userID, element.areaID, element.carteiraID, element.dtEntrada)
            .subscribe(function (result) { return check = result; }, function (error) { return console.log("Error de CheckIn"); }, function () {
            //alert(this.iterator);
            if (_this.iterator < qtdLoop) {
                _this.iterator++;
                //alert(this.iterator);
                _this.postCheckinsOffLine(dataCheckins);
            }
            _this.storage.set("checkInOff", []);
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.oneSignal.startInit('9d9f84c1-f074-4976-84e6-88562d5343b7', '33754247142');
            _this.oneSignal.inFocusDisplaying(_this.oneSignal.OSInFocusDisplayOption.Notification);
            _this.oneSignal.handleNotificationReceived().subscribe(function () {
                // do something when notification is received
            });
            _this.oneSignal.handleNotificationOpened().subscribe(function (c) {
                // do something when a notification is opened
                //alert("handleNotificationOpened: " + JSON.stringify(c));
            });
            _this.oneSignal.endInit();
            var ret;
            var UsuarioLogado;
            _this.storage.get('UsuarioLogado').then(function (data) {
                if (data != null) {
                    _this._servHome.isConnection()
                        .subscribe(function (result) { return ret = result; }, function (error) {
                        _this.storage.set("connectionStatus", "Off");
                        _this.storage.get("checkInOff").then(function (dataOff) {
                            if (dataOff == null) {
                                _this.storage.set("checkInOff", []);
                            }
                        });
                        _this.statusBar.styleDefault();
                        _this.splashScreen.hide();
                        _this.confirmCarregarCarteiraOff("Função Off-line", "Ops! percebemos que você está sem internet neste momento, deseja carregar a carteirinha virtual em modo off-line?");
                    }, function () {
                        //alert(JSON.stringify(ret)); 
                        _this.storage.set("connectionStatus", "On");
                        _this.storage.get("checkInOff").then(function (dataCheckinsArr) {
                            if (dataCheckinsArr != null && dataCheckinsArr.length > 0) {
                                _this.postCheckinsOffLine(dataCheckinsArr);
                                _this.iterator = 0;
                            }
                        });
                        _this.servUsuario.autenticarUsuario(data.email, data.senha)
                            .subscribe(function (user) { return UsuarioLogado = user; }, function (error) { return console.log("error ao autenticar"); }, function () {
                            if (UsuarioLogado.status == 0) {
                                data.nome = UsuarioLogado.nome;
                                data.foto = UsuarioLogado.foto;
                                _this.storage.set('UsuarioLogado', data);
                                // let arrItens = UsuarioLogado.strApartamento.split("|");
                                var arrItens = data.strApartamento.split("|");
                                _this.nomeUsuario = UsuarioLogado.nome;
                                _this.strFotoUsuario = UsuarioLogado.foto;
                                _this.bloco_qd = arrItens[2];
                                _this.unidade_num = arrItens[1];
                                _this.andar_lt = arrItens[3];
                                _this.nomeCondominio = data.strNomeCondominio;
                                _this.codUnidade = data.strCodUnidade;
                                _this.filtraMenu(data.condominioId);
                                _this.rootPage = __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* Home */];
                            }
                            if (UsuarioLogado.status == 1) {
                                _this.suporte.showAviso(UsuarioLogado.mensagem);
                                _this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* Login */];
                            }
                            _this.statusBar.styleDefault();
                            _this.splashScreen.hide();
                        });
                    });
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* Login */];
                    _this.statusBar.styleDefault();
                    _this.splashScreen.hide();
                }
            });
        });
    };
    MyApp.prototype.confirmCarregarCarteiraOff = function (titulo, msg) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.nav.push(__WEBPACK_IMPORTED_MODULE_29__pages_carteiraVirtual_carteiraVirtual__["a" /* carteiraVirtual */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    MyApp.prototype.filtraMenu = function (condominioId) {
        var _this = this;
        this._servHome.getConfiguracaoCondominio(condominioId)
            .subscribe(function (result) { return _this.objCondominio = result; }, function (error) { return console.log("Error ao obter condominio"); }, function () {
            _this.pages = [
                { title: "Comunicados", component: __WEBPACK_IMPORTED_MODULE_15__pages_comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */], imageUrl: "assets/img/icones/branco/iconComunicadoOcorrenia.png" },
                { title: "Reserva", component: __WEBPACK_IMPORTED_MODULE_16__pages_agendaReserva_agendaReserva__["a" /* agendaReserva */], imageUrl: "assets/img/icones/branco/agendaBranco.png" },
                { title: "Veículos", component: __WEBPACK_IMPORTED_MODULE_27__pages_vaga_tabVeiculos__["a" /* tabVeiculos */], imageUrl: "assets/img/icones/branco/vagasBranco.png" },
                { title: "Mural", component: __WEBPACK_IMPORTED_MODULE_19__pages_mural_mural__["a" /* mural */], imageUrl: "assets/img/icones/branco/muralBranco.png" },
                { title: "Correio", component: __WEBPACK_IMPORTED_MODULE_17__pages_correspondencia_correspondencia__["a" /* correspondencia */], imageUrl: "assets/img/icones/branco/iconCorreio.png" },
                { title: "Enquete", component: __WEBPACK_IMPORTED_MODULE_21__pages_enquete_enquete__["a" /* enquete */], imageUrl: "assets/img/icones/branco/enquetesBranco.png" },
                { title: "Produtos", component: __WEBPACK_IMPORTED_MODULE_23__pages_produtoServicos_produtoServicos__["a" /* produtoServicos */], imageUrl: "assets/img/icones/branco/produtosBranco.png" },
                { title: "Atividades", component: __WEBPACK_IMPORTED_MODULE_28__pages_agendaReserva_atividade__["a" /* atividade */], imageUrl: "assets/img/icones/branco/atividadesBranco.png" }
            ];
            if (_this.objCondominio != null) {
                //console.log(this.objCondominio);
                if (_this.objCondominio.portaria) {
                    var itemPortaria = { title: "Portaria", component: __WEBPACK_IMPORTED_MODULE_18__pages_portaria_portaria__["a" /* portaria */], imageUrl: "assets/img/icones/branco/iconAutVisita.png" };
                    _this.pages.push(itemPortaria);
                }
                if (_this.objCondominio.classificado) {
                    var itemClassificado = { title: "Classificados", component: __WEBPACK_IMPORTED_MODULE_20__pages_classificado_classificado__["a" /* classificado */], imageUrl: "assets/img/icones/branco/classificadosBranco.png" };
                    _this.pages.push(itemClassificado);
                }
                if (_this.objCondominio.chat) {
                    var itemChat = { title: "Chat", component: __WEBPACK_IMPORTED_MODULE_24__pages_chat_conversas__["a" /* conversas */], imageUrl: "assets/img/icones/branco/iconChat.png" };
                    _this.pages.push(itemChat);
                }
                if (_this.objCondominio.vaga) {
                    var itemVaga = { title: "Vagas", component: __WEBPACK_IMPORTED_MODULE_22__pages_vaga_vaga__["a" /* vaga */], imageUrl: "assets/img/icones/branco/vagasBranco.png" };
                    _this.pages.push(itemVaga);
                }
            }
        });
    };
    MyApp.prototype.selectUnidades = function () {
        var page = { title: "Unidades", component: __WEBPACK_IMPORTED_MODULE_26__pages_unidade_unidade__["a" /* unidade */] };
        this.openPage(page);
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.title != 'Home')
            this.nav.push(page.component);
        else
            this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        var retorno;
        this.storage.get("dispositivo").then(function (dev) {
            if (dev != null) {
                _this.servUsuario.destroiDevice(dev.id)
                    .subscribe(function (r) { return retorno = r; }, function (error) { return console.log("Error ao destruir device"); }, function () {
                    _this.storage.clear();
                    _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* Login */]);
                });
            }
            else {
                _this.storage.clear();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* Login */]);
            }
        });
    };
    MyApp.prototype.meuPerfil = function () {
        var _this = this;
        var oUser;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Obtendo usuário...");
            _this.servUsuario.getCliente(dataUser.userID)
                .subscribe(function (user) { return oUser = user; }, function (error) { return console.log("Error ao obter usuario"); }, function () {
                _this.suporte.dismissLoader();
                _this.nav.push(__WEBPACK_IMPORTED_MODULE_25__pages_perfil_perfil__["a" /* perfil */], oUser);
            });
        });
    };
    MyApp.prototype.reportEmergencia = function () {
        this.confirmcriarOcorrenciaEmergencia("ATENÇÃO!", "Deseja informar uma emergência?");
    };
    MyApp.prototype.confirmcriarOcorrenciaEmergencia = function (titulo, msg) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Saúde',
                    handler: function () {
                        _this.enviarEmergencia(1);
                    }
                },
                {
                    text: 'Incêndio',
                    handler: function () {
                        _this.enviarEmergencia(2);
                    }
                },
                {
                    text: 'Outros',
                    handler: function () {
                        _this.enviarEmergencia(3);
                    }
                },
                {
                    text: 'Fechar',
                    handler: function () { }
                }
            ]
        });
        confirm.present();
    };
    MyApp.prototype.enviarEmergencia = function (tpEmergencia) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var objPostOcorrencia = {
                clienteId: dataUser.userID,
                condominioId: dataUser.condominioId,
                idApartamentoOcorrencia: dataUser.aptId,
                EmergenciaID: tpEmergencia
            };
            // console.log(objPostOcorrencia);
            var result;
            _this.servOcorrencia.addOcorrenciaEmergencia(objPostOcorrencia)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao enviar a ocorrencia de emergencia"); }, function () {
                if (result.status == 0) {
                    _this.suporte.showAviso(result.mensagem);
                    _this._signalR.connect().then(function (c) {
                        c.invoke("SendNovaOcorrencia", dataUser.condominioId).then(function (r) { return console.log("Nova Ocorrencia enviada com sucesso"); });
                    });
                }
            });
        });
    };
    MyApp.prototype.doRefresh = function (refresher) {
        var _this = this;
        // console.log('Begin async operation', refresher);
        this.storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                var arrItens = data.strApartamento.split("|");
                _this.nomeUsuario = data.nome;
                _this.strFotoUsuario = data.foto;
                _this.bloco_qd = arrItens[2];
                _this.unidade_num = arrItens[1];
                _this.andar_lt = arrItens[3];
                _this.nomeCondominio = data.strNomeCondominio;
                _this.codUnidade = data.strCodUnidade;
                _this.filtraMenu(data.condominioId);
            }
            else {
                // this.nav.setRoot(Login);
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* Login */];
            }
        });
        setTimeout(function () {
            // console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/app/app.html"*/'﻿<ion-menu [content]="content">\n  <ion-header>\n \n  </ion-header>\n\n  <ion-content no-padding>\n      <ion-refresher (ionRefresh)="doRefresh($event)">\n          <ion-refresher-content></ion-refresher-content>\n       </ion-refresher>\n\n       <div class="perfilTop">\n          <div class="divNomeCondominio" *ngIf="nomeCondominio"> {{nomeCondominio}}</div>\n          <ion-list class="lstMenuDisplay" no-lines>\n              <ion-item text-wrap class="bgItem">\n                  <ion-avatar item-start *ngIf="strFotoUsuario">\n                    <img src=\'{{urlImageServ}}usuario/{{strFotoUsuario}}\' *ngIf="strFotoUsuario" class="imgAvatar">\n                    <img src="assets/img/no_image.png" *ngIf="!strFotoUsuario" class="imgAvatar">\n                  </ion-avatar>\n                  <span *ngIf="nomeCondominio" class="sizeFontDisplayMenu">{{nomeUsuario}}<br />\n                    {{bloco_qd}} / Andar/Lt: {{andar_lt}} <br />\n                    Unidade: {{unidade_num}}<br />\n                    Código: {{codUnidade}}\n                  </span>\n                </ion-item>\n          </ion-list>\n          <ion-row>\n              <ion-col menuClose center no-padding>\n                  <button  ion-button color="light" clear  small  class="text-on-bottom-menu" (click)="meuPerfil()">\n                    <img src="assets/img/icones/branco/perfilBranco.png" class="iconeMenu" />\n                    <label class="labelIconDown">Perfil</label>\n                  </button>       \n              </ion-col>\n              <ion-col menuClose center no-padding> \n                <button ion-button color="light" clear  small class="text-on-bottom-menu" (click)="selectUnidades()">\n                    <img src="assets/img/icones/branco/unidadesBranco.png" class="iconeMenu" />\n                  <label class="labelIconDown">Unidades</label>\n                </button>            \n              </ion-col>\n              <ion-col menuClose center no-padding>\n                  <button ion-button color="light" clear  small class="text-on-bottom-menu" (click)="reportEmergencia()">\n                      <img src="assets/img/icones/branco/emergenciaBranco.png" class="iconeMenu" />\n                    <label class="labelIconDown">Emergência</label>\n                  </button> \n              </ion-col>\n            </ion-row>\n        </div>\n      <div class="divDemaisIconesMenu">\n      <ion-grid>\n        <ion-row>\n            <ion-col col-6 menuClose *ngFor="let p of pages" center text-center>\n                <button  ion-button color="light" clear small  class="text-on-bottom-menu-down"  (click)="openPage(p)">\n                  <img src="{{p.imageUrl}}" class="iconeMenuDown" />\n                  <label class="labelIconDown">{{p.title}}</label>\n                </button>       \n            </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col col-12 menuClose>\n              <!-- <button ion-button block padding  color="topReservas" [disabled]="!form.valid">Criar reserva</button> -->\n              <button ion-button block round padding color="primary" type="button" (click)="logout()">\n                Sair\n                <!-- <ion-icon name="log-out" item-end item-right small></ion-icon> -->\n              </button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      </div>  \n\n      <!-- <button menuClose ion-item (click)="reportEmergencia()">\n          <ion-icon name="arrow-dropright"></ion-icon> Emergência\n      </button>\n\n      <button menuClose ion-item (click)="meuPerfil()">\n        <ion-icon name="arrow-dropright"></ion-icon> Meu perfil\n      </button> -->\n\n      <!-- <button menuClose ion-item (click)="logout()">\n          <ion-icon name="arrow-dropright"></ion-icon> Sair\n      </button> -->\n      \n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/app/app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_device__["a" /* Device */],
        __WEBPACK_IMPORTED_MODULE_12__app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_7_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_onesignal__["a" /* OneSignal */],
        __WEBPACK_IMPORTED_MODULE_9__service_ServiceUsuario__["a" /* ServiceUsuario */],
        __WEBPACK_IMPORTED_MODULE_10__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
        __WEBPACK_IMPORTED_MODULE_11__service_ServiceHome__["a" /* ServiceHome */],
        __WEBPACK_IMPORTED_MODULE_8__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 66:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceEnquete; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceEnquete = (function () {
    function ServiceEnquete(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceMural Provider');
    }
    ServiceEnquete.prototype.listarEnquete = function (idCondominio) {
        var params = 'condominioID=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Enquete/getEnquete?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceEnquete.prototype.listarEnquetesNaoRespondidas = function (condominioId, usuarioID) {
        var params = 'condominioID=' + condominioId + "&uID=" + usuarioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "enquete/getEnqueteAtiva?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceEnquete.prototype.votarEnquete = function (idAlternativa, idUsuario, aptId) {
        var params = 'alteID=' + idAlternativa + "&clienteID=" + idUsuario + "&unidadeId=" + aptId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "enquete/postVotarEnquete?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceEnquete;
}());
ServiceEnquete = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceEnquete);

//# sourceMappingURL=ServiceEnquete.js.map

/***/ }),

/***/ 67:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceComunicado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceComunicado = (function () {
    function ServiceComunicado(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceComunicado Provider');
    }
    ServiceComunicado.prototype.ListarUltimos = function (aptId, idCondominio) {
        var params = 'aptID=' + aptId + '&idCondominio=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/getComnunicadoDoApartamento?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceComunicado.prototype.getSelectComunicado = function (idDoComunicado) {
        var params = 'comunicadoID=' + idDoComunicado;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "comunicado/GetComunicado?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceComunicado.prototype.getListComunicadoDocs = function (aptId) {
        var params = 'aptId=' + aptId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'apartamento/getComunicadoDoApartamentoDocs?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceComunicado.prototype.getListCatComunicado = function (condominioID) {
        var params = 'idCondominio=' + condominioID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'Comunicado/getComunicadosPublicosFolders?' + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceComunicado;
}());
ServiceComunicado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceComunicado);

//# sourceMappingURL=ServiceComunicado.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceAgenda; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceAgenda = (function () {
    function ServiceAgenda(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceComunicado Provider');
    }
    ServiceAgenda.prototype.getAreaComumList = function (idCondominio) {
        var params = 'IdCondominio=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "AreaComum/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceAgenda.prototype.getAgendaCondominio = function (idApartamento) {
        var params = 'apartamentoId=' + idApartamento;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "apartamento/getCriarAgenda?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceAgenda.prototype.getAtividadesDoCondominio = function (idCondominio) {
        var params = 'condominioId=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'Atividade/getAtividade?' + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceAgenda;
}());
ServiceAgenda = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceAgenda);

//# sourceMappingURL=ServiceAgenda.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceReserva; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceReserva = (function () {
    function ServiceReserva(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_3__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceComunicado Provider');
    }
    ServiceReserva.prototype.addNovaReserva = function (oReserva) {
        // console.log(oReserva);
        var params = JSON.stringify(oReserva);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Reserva/addReserva', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceReserva.prototype.getListMinhasReservas = function (unidadeId) {
        var params = 'aptID=' + unidadeId;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'Reserva/getMinhasReservas?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceReserva.prototype.rejeitarReserva = function (idDaReserva) {
        var params = 'reservaID=' + idDaReserva;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'Reserva/excluirReserva?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceReserva.prototype.getListReservasDaArea = function (areaID) {
        var params = 'IdareaComum=' + areaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'reserva/?' + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceReserva;
}());
ServiceReserva = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceReserva);

//# sourceMappingURL=ServiceReserva.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceClassificado; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceClassificado = (function () {
    function ServiceClassificado(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceClassificado Provider');
    }
    ServiceClassificado.prototype.listarClassificados = function (idCondominio) {
        var params = 'Idcondominio=' + idCondominio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "classificado/getClassificados?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceClassificado.prototype.listarMeusAnuncios = function (uID) {
        var params = 'IdUser=' + uID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "classificado/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceClassificado.prototype.addNovoClassificado = function (oClassificado) {
        console.log(oClassificado);
        var params = JSON.stringify(oClassificado);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Classificado/addClassificado', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceClassificado.prototype.removerClassificado = function (idAnuncio) {
        var params = 'anuncioID=' + idAnuncio;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "classificado/excluirClassificado?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceClassificado.prototype.editarClassificado = function (oClassificado) {
        console.log(oClassificado);
        var params = JSON.stringify(oClassificado);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'Classificado/editClassificado', params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceClassificado;
}());
ServiceClassificado = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceClassificado);

//# sourceMappingURL=ServiceClassificado.js.map

/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceTransferencia; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceTransferencia = (function () {
    function ServiceTransferencia(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        // console.log('Hello ServiceTransferencia');
    }
    ServiceTransferencia.prototype.listarSolicitacoesPendentes = function (apartamentoID) {
        var params = 'apId=' + apartamentoID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "Transferencia/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTransferencia.prototype.listarTransferenciasConfirmadas = function (idUnidade) {
        var params = 'apartamentoID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "transferencia/getVagasTranferidas?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTransferencia.prototype.listarTransferenciasCedidasConfirmadas = function (idUnidade) {
        var params = 'apartamentoID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "transferencia/getVagasCedidas?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTransferencia.prototype.transferirVaga = function (idTransferencia, cedenteID) {
        var params = 'transID=' + idTransferencia + '&idCendente=' + cedenteID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "transferencia/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTransferencia.prototype.listarRequisicoesDeTransferencias = function (idUnidade) {
        var params = 'unidadeID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "transferencia/getRequisicoesDeVaga/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTransferencia.prototype.cancelarRequisicoesDeTransferencias = function (transferenciaID) {
        var params = 'idTransferencia=' + transferenciaID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "transferencia/CancelarRequisicaoDeVaga/?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceTransferencia;
}());
ServiceTransferencia = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceTransferencia);

//# sourceMappingURL=ServiceTransferencia.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Home; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceSignalr__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_ServiceHome__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__service_ServiceVisita__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__service_ServiceChat__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__service_ServiceEnquete__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__service_ServiceOcorrencia__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__service_ServiceUsuario__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__service_ServiceCarteiraDigital__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__comunicadoOcorrencia_comunicadoOcorrencia__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__agendaReserva_agendaReserva__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__correspondencia_correspondencia__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__portaria_portaria__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__mural_mural__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__classificado_classificado__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__enquete_enquete__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__vaga_vaga__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__chat_conversas__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__chat_mensagens__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__produtoServicos_produtoServicos__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__portaria_modalAutorizarEntrada__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__perfil_perfil__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__unidade_unidade__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__agendaReserva_atividade__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__enquete_enqueteVotar__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__vaga_tabVeiculos__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__carteiraVirtual_carteiraVirtual__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__administradora_administradora__ = __webpack_require__(434);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


































var Home = (function (_super) {
    __extends(Home, _super);
    function Home(navCtrl, alertCtrl, storage, toastCtrl, network, _servHome, signalr, cnxSignalr, servVisita, servChat, modal, suporte, servEnquete, servOcorrencia, servUsuario, servCarteira) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.navCtrl = navCtrl;
        _this.alertCtrl = alertCtrl;
        _this.storage = storage;
        _this.toastCtrl = toastCtrl;
        _this.network = network;
        _this._servHome = _servHome;
        _this.signalr = signalr;
        _this.cnxSignalr = cnxSignalr;
        _this.servVisita = servVisita;
        _this.servChat = servChat;
        _this.modal = modal;
        _this.suporte = suporte;
        _this.servEnquete = servEnquete;
        _this.servOcorrencia = servOcorrencia;
        _this.servUsuario = servUsuario;
        _this.servCarteira = servCarteira;
        _this.oData = new Date();
        _this.nomeUsuario = 'Bem Vindo(a)';
        _this.strFotoUsuario = "";
        _this.bloco_qd = "Nf";
        _this.andar_lt = "Nf";
        _this.unidade_num = "Nf";
        _this.isCarteirinha = false;
        _this.listaDeEnquetesNaoRespondidas = [];
        _this.interativoImg = 'assets/img/icones/information.png';
        _this.signalr.iniciarConexao();
        return _this;
    }
    Home.prototype.ngOnInit = function () {
        var _this = this;
        this.getMeteorologia();
        this.configuraCondominio();
        this.getIsVisitaPendente();
        this.getPrioridade();
        this.getEnquetesNaoRespondidas();
        this.getCarteirasDaAreaDeAcesso();
        this.setToken();
        var onMessageSent = this.signalr.connection.listenFor("addMensagemChatApp");
        onMessageSent.subscribe(function (oMessage) { return _this.setHomeChat(oMessage); });
        // watch network for a disconnect
        this.network.onDisconnect().subscribe(function () {
            //console.log('network was disconnected :-(');
            _this.suporte.showAviso("Você está off-line no momento!");
        });
    };
    Home.prototype.getCarteirasDaAreaDeAcesso = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var carteirasDaUnidade;
            _this.servCarteira.getCarteirasDaUnidade(dataUser.aptId)
                .subscribe(function (result) { return carteirasDaUnidade = result; }, function (error) { return console.log("Error ao obter carteiras"); }, function () {
                if (carteirasDaUnidade.length > 0)
                    _this.isCarteirinha = true;
                _this.storage.set("cateirasDigitais", carteirasDaUnidade);
            });
        });
    };
    Home.prototype.setHomeChat = function (oMensagem) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            //if (dataUser.userID == oMensagem.userIDdestinatario){
            _this.servChat.findConversa(oMensagem.conversaId, dataUser.userID)
                .subscribe(function (conversa) { return _this.conversaAtiva = conversa; }, function (error) { return console.log(_this.errorMensage); }, function () {
                _this.interativoImg = 'assets/img/icones/branco/iconChat.png';
                _this.displayTitulo = "Chat";
                _this.displayDescricao = oMensagem.descricao;
            });
            // }
        });
    };
    Home.prototype.getEnquetesNaoRespondidas = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.servEnquete.listarEnquetesNaoRespondidas(dataUser.condominioId, dataUser.userID)
                    .subscribe(function (enquetes) { return _this.listaDeEnquetesNaoRespondidas = enquetes; }, function (error) { return console.log("Error ao obter enquetes"); }, function () {
                    if (_this.listaDeEnquetesNaoRespondidas.length > 0) {
                        var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_30__enquete_enqueteVotar__["a" /* enqueteVotar */], _this.listaDeEnquetesNaoRespondidas);
                        m.present();
                    }
                });
            }
        });
    };
    Home.prototype.getCodeDeBarra = function () {
        // let opt = {
        //     nomeAreaAcesso : 'Ônibus',
        //     nomeCondominio : 'Palmeiras',
        //     idAreaDeAcesso : 2,
        //     idCondominio : 1
        // }
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                if (dataUser.foto == null) {
                    _this.confirmAdicionarFotoPerfil("Perfil", "Para a utilização da carteira é necessário uma foto de perfil.");
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_32__carteiraVirtual_carteiraVirtual__["a" /* carteiraVirtual */]);
                }
            }
        });
        // let options = {
        //     resultDisplayDuration : 0
        // };
        // this.barcodeScanner.scan(options).then((barcodeData) => {
        //     // Success! Barcode data is here
        //     console.log(JSON.stringify(barcodeData));
        //     if (!barcodeData.cancelled && barcodeData.format == "QR_CODE"){
        //         let objItemCode = JSON.parse(barcodeData.text);
        //         this.navCtrl.push(carteiraVirtual,objItemCode);
        //     }
        //    }, (err) => {
        //        // An error occurred
        //        console.log('Error: ' + err);
        //    });
    };
    Home.prototype.confirmAdicionarFotoPerfil = function (titulo, msg) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Editar Perfil',
                    handler: function () {
                        _this.meuPerfil();
                    }
                },
                {
                    text: 'Fechar',
                    handler: function () { }
                }
            ]
        });
        confirm.present();
    };
    Home.prototype.criarOcorrenciaEmergencia = function () {
        this.confirmcriarOcorrenciaEmergencia("ATENÇÃO!", "Deseja informar uma emergência?");
    };
    Home.prototype.confirmcriarOcorrenciaEmergencia = function (titulo, msg) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Saúde',
                    handler: function () {
                        _this.enviarEmergencia(1);
                    }
                },
                {
                    text: 'Incêndio',
                    handler: function () {
                        _this.enviarEmergencia(2);
                    }
                },
                {
                    text: 'Outros',
                    handler: function () {
                        _this.enviarEmergencia(3);
                    }
                },
                {
                    text: 'Fechar',
                    handler: function () { }
                }
            ]
        });
        confirm.present();
    };
    Home.prototype.enviarEmergencia = function (tpEmergencia) {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var objPostOcorrencia = {
                clienteId: dataUser.userID,
                condominioId: dataUser.condominioId,
                idApartamentoOcorrencia: dataUser.aptId,
                EmergenciaID: tpEmergencia
            };
            var result;
            _this.servOcorrencia.addOcorrenciaEmergencia(objPostOcorrencia)
                .subscribe(function (r) { return result = r; }, function (error) { return console.log("Error ao enviar a ocorrencia de emergencia"); }, function () {
                if (result.status == 0) {
                    _this.suporte.showAviso(result.mensagem);
                    _this.cnxSignalr.connect().then(function (c) {
                        c.invoke("SendNovaOcorrencia", dataUser.condominioId).then(function (r) { return console.log("Nova ocorrencia enviada com sucesso"); });
                    });
                }
            });
        });
    };
    Home.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.configuraCondominio();
        this.getPrioridade();
        this.getIsVisitaPendente();
        this.getEnquetesNaoRespondidas();
        this.getCarteirasDaAreaDeAcesso();
        setTimeout(function () {
            //console.log('Async operation has ended');
            refresher.complete();
        }, 1000);
    };
    Home.prototype.configuraCondominio = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            var arrItens = dataUser.strApartamento.split("|");
            _this.nomeUsuario = dataUser.nome;
            _this.strFotoUsuario = dataUser.foto;
            _this.bloco_qd = arrItens[2];
            _this.unidade_num = arrItens[1];
            _this.andar_lt = arrItens[3];
            _this.nomeCondominio = dataUser.strNomeCondominio;
            _this.codUnidade = dataUser.strCodUnidade;
            _this._servHome.getConfiguracaoCondominio(dataUser.condominioId)
                .subscribe(function (condo) { return _this.objCondominio = condo; }, function (error) { return console.log('Error reportado: ' + error); }, function () {
                _this.classificadoControle = _this.objCondominio.classificado;
                _this.portariaControle = _this.objCondominio.portaria;
                _this.chatControle = _this.objCondominio.chat;
                _this.vagasControle = _this.objCondominio.vaga;
                _this.correioControle = _this.objCondominio.flCorrespondenciaAtivar;
                if (_this.objCondominio.logoAdministradora)
                    _this.logoMarcaHome = _this.urlImageServ + "usuario/" + _this.objCondominio.logoAdministradora;
                else if (_this.objCondominio.logoMarca)
                    _this.logoMarcaHome = _this.urlImageServ + "usuario/" + _this.objCondominio.logoMarca;
                _this.storage.set('objCondominio', _this.objCondominio);
            });
        });
    };
    Home.prototype.getIsVisitaPendente = function () {
        var _this = this;
        var vst;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.servVisita.isVisitaPendente(dataUser.aptId)
                    .subscribe(function (r) { return vst = r; }, function (error) { return console.log("Error ao obter"); }, function () {
                    if (vst != null) {
                        var m = _this.modal.create(__WEBPACK_IMPORTED_MODULE_26__portaria_modalAutorizarEntrada__["a" /* modalAutorizarEntrada */], vst);
                        m.present();
                    }
                });
            }
        });
    };
    Home.prototype.getPrioridade = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            if (dataUser != null) {
                _this.suporte.showLoader("Obtendo informações de sua unidade...");
                var ret_1;
                _this._servHome.getPrioridadeHome(dataUser.aptId)
                    .subscribe(function (result) { return ret_1 = result; }, function (error) { return console.log("Error ao consultar prioridade"); }, function () {
                    _this.interativoImg = 'assets/img/icones/Information.png';
                    var arrRetorno = ret_1.mensagem.split('|');
                    if (ret_1.status == 0) {
                        _this.displayTitulo = arrRetorno[0];
                        //  this.displayDescricao = arrRetorno[2];
                        _this.showComunicado(arrRetorno[0] + " - " + arrRetorno[2]);
                    }
                    else {
                        _this.interativoImg = _this.interativoImg.replace('Information.png', '');
                        _this.displayTitulo = arrRetorno[0];
                        _this.interativoImg = _this.interativoImg + "branco/" + arrRetorno[1];
                        //  this.displayDescricao = arrRetorno[2];  
                        _this.showComunicado(arrRetorno[0] + " - " + arrRetorno[2]);
                    }
                    _this.suporte.dismissLoader();
                });
            }
        });
    };
    Home.prototype.redirecionaLink = function () {
        if (this.displayTitulo != "") {
            switch (this.displayTitulo) {
                case 'Correspondência':
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__correspondencia_correspondencia__["a" /* correspondencia */]);
                    break;
                case 'Comunicado':
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */]);
                    break;
                case 'Portaria':
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__portaria_portaria__["a" /* portaria */]);
                    break;
                case 'Chat':
                    var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_24__chat_mensagens__["a" /* mensagens */], this.conversaAtiva);
                    m.present();
                    break;
                default:
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */]);
            }
        }
    };
    Home.prototype.ionViewDidLoad = function () {
        if (this.oData.getHours() > 6 && this.oData.getHours() < 18) {
            this.urlBanner = 'assets/img/BannerHome/normal_dia_app.png';
        }
        else {
            this.urlBanner = 'assets/img/BannerHome/normal_noite_app.png';
        }
    };
    Home.prototype.showComunicado = function (strConteudo) {
        var _this = this;
        this.storage.get("strUltimoComunicado").then(function (dataUltimo) {
            if (dataUltimo != strConteudo.substring(0, 4)) {
                var toast = _this.toastCtrl.create({
                    message: strConteudo,
                    duration: 10000,
                    position: 'bottom',
                    showCloseButton: true,
                    closeButtonText: "X",
                    cssClass: "toast-success",
                });
                toast.onDidDismiss(function () {
                    _this.storage.set("strUltimoComunicado", strConteudo.substring(0, 4));
                    console.log('Dismissed toast');
                });
                toast.present();
            }
        });
    };
    Home.prototype.setToken = function () {
        var _this = this;
        this.storage.get('UsuarioLogado').then(function (data) {
            if (data != null) {
                _this.cnxSignalr.connect().then(function (c) {
                    c.invoke("getToken", data.userID).then(function (r) { return console.log("Token conectado no app"); });
                });
            }
        });
    };
    Home.prototype.getMeteorologia = function () {
        var _this = this;
        this._servHome.getMeteorologiaCloud()
            .subscribe(function (info) { return _this.tempoInfo = info; }, function (error) { return console.log('Error reportado: ' + error); }, function () {
            //console.log(JSON.parse(this.tempoInfo));
            _this.tempoInfo = JSON.parse(_this.tempoInfo);
            if (_this.oData.getHours() > 6 && _this.oData.getHours() < 18) {
                if (_this.tempoInfo.results.description.indexOf('nub') > -1) {
                    _this.urlBanner = 'assets/img/BannerHome/nublado_dia_app.png';
                }
                else if (_this.tempoInfo.results.description.indexOf('chuv') > -1) {
                    _this.urlBanner = 'assets/img/BannerHome/chuvoso_dia_app.png';
                }
            }
            else {
                if (_this.tempoInfo.results.description.indexOf('nub') > -1) {
                    _this.urlBanner = 'assets/img/BannerHome/nublado_noite_app.png';
                }
                else if (_this.tempoInfo.results.description.indexOf('chuv') > -1) {
                    _this.urlBanner = 'assets/img/BannerHome/chuvoso_noite_app.png';
                }
            }
            _this.valorTemp = _this.tempoInfo.results.temp + 'º';
            _this.imgTemp = 'assets/img/tempo/' + _this.tempoInfo.results.img_id + '.png';
        });
    };
    Home.prototype.getPageComunicado = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__comunicadoOcorrencia_comunicadoOcorrencia__["a" /* comunicadoOcorrencia */]);
    };
    Home.prototype.getAgendaReserva = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_16__agendaReserva_agendaReserva__["a" /* agendaReserva */]);
    };
    Home.prototype.getCorrespondencias = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__correspondencia_correspondencia__["a" /* correspondencia */]);
    };
    Home.prototype.getPortaria = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_18__portaria_portaria__["a" /* portaria */]);
    };
    Home.prototype.getMural = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__mural_mural__["a" /* mural */]);
    };
    Home.prototype.getClassificado = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_20__classificado_classificado__["a" /* classificado */]);
    };
    Home.prototype.getAvisoClassificadoOff = function () {
        this.suporte.showAviso("Esta função está desabilitada pelo seu condomínio.");
    };
    Home.prototype.getEnquetes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_21__enquete_enquete__["a" /* enquete */]);
    };
    Home.prototype.getVagas = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_22__vaga_vaga__["a" /* vaga */]);
    };
    Home.prototype.getProdutos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_25__produtoServicos_produtoServicos__["a" /* produtoServicos */]);
    };
    Home.prototype.getChat = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_23__chat_conversas__["a" /* conversas */]);
    };
    Home.prototype.getRedirectAdministradora = function () {
        if (this.objCondominio.urlWebServer != null && this.objCondominio.boletoFolder != null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_33__administradora_administradora__["a" /* administradora */]);
        }
        else if (this.objCondominio.linkGeraBoleto) {
            this.suporte.openUrlSite(this.objCondominio.linkGeraBoleto);
        }
        else {
            this.suporte.showAviso("A função administradora não foi configurada em seu condomínio.");
        }
    };
    Home.prototype.getVeiculos = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_31__vaga_tabVeiculos__["a" /* tabVeiculos */]);
    };
    Home.prototype.getUnidades = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_28__unidade_unidade__["a" /* unidade */]);
    };
    Home.prototype.getAtividade = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_29__agendaReserva_atividade__["a" /* atividade */]);
        m.present();
    };
    Home.prototype.meuPerfil = function () {
        var _this = this;
        var oUser;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Obtendo usuário...");
            _this.servUsuario.getCliente(dataUser.userID)
                .subscribe(function (user) { return oUser = user; }, function (error) { return console.log("Error ao obter usuario"); }, function () {
                _this.suporte.dismissLoader();
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_27__perfil_perfil__["a" /* perfil */], oUser);
            });
        });
    };
    return Home;
}(__WEBPACK_IMPORTED_MODULE_14__base_pageBase__["a" /* PageBase */]));
Home = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'home-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/home/home.html"*/'﻿<ion-header>     \n \n</ion-header>\n\n<ion-content no-padding>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n      <ion-refresher-content\n          pullingText="Solte para atualizar"\n          refreshingText="Atualizando...">\n      </ion-refresher-content>\n  </ion-refresher>\n\n  <ion-icon name="menu" menuToggle class="menuHomePincipal"></ion-icon>\n\n  <div class="divIconRefresh">\n    <ion-icon name="ios-arrow-down-outline"></ion-icon>\n  </div>\n\n  <img src="{{logoMarcaHome}}" class="iconAdministradora" id="imgLogoAdministradora" *ngIf="logoMarcaHome" />\n  <div class="iconCateirinha" *ngIf="isCarteirinha">\n      <img src="assets/img/icones/branco/carteirinhaBranco.png" (click)="getCodeDeBarra()" />\n      <p class="txtHomeRotulo alignBottons">Carteirinha Virtual</p>\n  </div>\n  \n  <div class="lcdPosDisplay">\n    <div class="lcdDisplay"> \n        <span id="valorTemp">{{valorTemp}}</span><br />\n        <img src="{{imgTemp}}" id="imgTemp" class="tamImgTempo" />\n    </div>\n  </div>\n\n   <ion-grid no-padding>\n    <ion-row>\n      <ion-col no-padding>  \n        <img src="{{urlBanner}}" class="posicaoBanner"/>\n      </ion-col>\n    </ion-row>\n         \n    <ion-row class="margin-topNegativo">\n      <ion-col class="blockComOcorrencia alignBottons">\n        <img src="assets/img/icones/iconComunicadoOcorrenia.png" class="iconDestaqueHome" (click)="getPageComunicado()" />\n        <div class="txtHomeDestaque"> Comunicados e Ocorrências</div>\n      </ion-col>\n      <ion-col class="blockComOcorrencia alignBottons">\n          <span id="iconePrincipal">\n              <img src=\'{{interativoImg}}\' class="iconeinterativo" (click)="redirecionaLink()" />\n           </span>\n        </ion-col>\n      <ion-col  class="blockAgReserva alignBottons">\n        <img src="assets/img/icones/iconAgReserva.png" class="iconDestaqueHome" (click)="getAgendaReserva()" />\n        <div class="txtHomeDestaque"> Agenda e Reservas</div>\n      </ion-col>\n    </ion-row>\n\n    <div class="conteudoDivHome">\n        <ion-row>\n            <ion-col class="alignBottons" *ngIf="portariaControle"> \n              <img src="assets/img/icones/iconAutVisita.png" class="iconHome" (click)="getPortaria()"/> \n              <div class="txtHomeRotulo">Portaria</div>\n            </ion-col>\n            <ion-col class="alignBottons" *ngIf="!portariaControle"> \n              <img src="assets/img/icones/iconPerfil.png" class="iconHome" (click)="meuPerfil()"/> \n              <div class="txtHomeRotulo">Meu perfil</div>\n            </ion-col>\n            <ion-col class="alignBottons" *ngIf="classificadoControle">\n              <img src="assets/img/icones/iconClassificado.png" class="iconHome" (click)="getClassificado()" /> \n              <div  class="txtHomeRotulo">Classificados</div>\n            </ion-col>\n            <ion-col class="alignBottons" *ngIf="!classificadoControle">\n                <img src="assets/img/icones/iconEmergencia.png" class="iconHome" (click)="criarOcorrenciaEmergencia()" />  \n                <div class="txtHomeRotulo">Emergência</div>\n            </ion-col>\n            <ion-col class="alignBottons">\n              <img src="assets/img/icones/iconMural.png" class="iconHome" (click)="getMural()"/> \n              <div  class="txtHomeRotulo">Mural</div>\n            </ion-col>\n          </ion-row>\n      \n          <ion-row>\n            \n            <ion-col class="alignBottons" *ngIf="vagasControle" >\n              <img src="assets/img/icones/iconVagas.png" class="iconHome" (click)="getVagas()" />  \n              <div class="txtHomeRotulo">Vagas</div>\n            </ion-col>\n            \n            <ion-col class="alignBottons" *ngIf="!vagasControle" >\n                <img src="assets/img/icones/iconVagas.png" class="iconHome" (click)="getVeiculos()" />  \n                <div class="txtHomeRotulo">Veículos</div>\n            </ion-col>\n\n            <ion-col class="alignBottons" *ngIf="correioControle"> \n              <img src="assets/img/icones/iconCorreio.png" class="iconHome" (click)="getCorrespondencias()"/> \n              <div class="txtHomeRotulo">Correspondência</div>\n            </ion-col>\n\n            <ion-col class="alignBottons" *ngIf="!correioControle"> \n              <img src="assets/img/icones/iconAtividades.png" class="iconHome" (click)="getAtividade()"/> \n              <div class="txtHomeRotulo">Atividades</div>\n            </ion-col>\n\n            <ion-col class="alignBottons">\n              <img src="assets/img/icones/iconEnquete.png" class="iconHome" (click)="getEnquetes()" /> \n              <div class="txtHomeRotulo">Enquetes</div>\n            </ion-col>\n          </ion-row>\n      \n          <ion-row>\n              <ion-col class="alignBottons" *ngIf="chatControle">\n                <img src="assets/img/icones/iconChat.png" class="iconHome" (click)="getChat()"/>  \n                <div class="txtHomeRotulo">Chat</div>\n              </ion-col>\n              <ion-col class="alignBottons" *ngIf="!chatControle">\n                <img src="assets/img/icones/iconUnidades.png" class="iconHome" (click)="getUnidades()"/>  \n                <div class="txtHomeRotulo">Unidades</div>\n              </ion-col>\n              <ion-col class="alignBottons"> \n                <img src="assets/img/icones/iconServicos.png" class="iconHome" (click)="getProdutos()"/> \n                <div class="txtHomeRotulo">Produtos e Serviços</div>\n              </ion-col>\n              <ion-col class="alignBottons">\n                <img src="assets/img/icones/iconAdmin.png" class="iconHome" (click)="getRedirectAdministradora()" /> \n                <div class="txtHomeRotulo">Administradora</div>\n              </ion-col>\n          </ion-row>\n    </div>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/home/home.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_6__service_ServiceHome__["a" /* ServiceHome */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceSignalr__["a" /* ServiceSignalr */],
        __WEBPACK_IMPORTED_MODULE_4_ng2_signalr__["b" /* SignalR */],
        __WEBPACK_IMPORTED_MODULE_7__service_ServiceVisita__["a" /* ServiceVisita */],
        __WEBPACK_IMPORTED_MODULE_8__service_ServiceChat__["a" /* ServiceChat */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_13__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_9__service_ServiceEnquete__["a" /* ServiceEnquete */],
        __WEBPACK_IMPORTED_MODULE_10__service_ServiceOcorrencia__["a" /* ServiceOcorrencia */],
        __WEBPACK_IMPORTED_MODULE_11__service_ServiceUsuario__["a" /* ServiceUsuario */],
        __WEBPACK_IMPORTED_MODULE_12__service_ServiceCarteiraDigital__["a" /* ServiceCarteiraDigital */]])
], Home);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return atividade; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_ServiceAgenda__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__base_pageBase__ = __webpack_require__(4);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var atividade = (function (_super) {
    __extends(atividade, _super);
    function atividade(storage, sp, agendaService, navCtrl, navParams, viewCtrl) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.sp = sp;
        _this.agendaService = agendaService;
        _this.navCtrl = navCtrl;
        _this.navParams = navParams;
        _this.viewCtrl = viewCtrl;
        _this.listAtividades = [];
        _this.carregaAtividades();
        return _this;
    }
    atividade.prototype.carregaAtividades = function () {
        var _this = this;
        this.sp.showLoader("Carregando atividades...");
        this.storage.get("UsuarioLogado").then(function (data) {
            _this.agendaService.getAtividadesDoCondominio(data.condominioId)
                .subscribe(function (list) { return _this.listAtividades = list; }, function (error) { return console.log("Error ao carregar as Atividades"); }, function () { return _this.sp.dismissLoader(); });
        });
    };
    atividade.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    return atividade;
}(__WEBPACK_IMPORTED_MODULE_5__base_pageBase__["a" /* PageBase */]));
atividade = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: "page-atividade",template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/atividade.html"*/'<ion-header>\n  <ion-toolbar color="topAtividade">\n    <ion-title>\n        <ion-icon name="bicycle"></ion-icon> Atividades do Condomínio\n    </ion-title>\n    <ion-buttons start>\n        <button ion-button (click)="dismiss()">\n            <span ion-text color="light" showWhen="ios">Voltar</span>\n            <ion-icon name="md-close" showWhen="android, windows"></ion-icon>\n        </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>     \n    <ion-list>\n        <ion-item text-wrap *ngFor="let ativ of listAtividades">\n            <ion-avatar item-start>\n                <ion-icon name="calendar" class="iconSize"></ion-icon>\n            </ion-avatar>\n\n            <h3>{{ativ.titulo}}</h3>\n            <div>\n                {{ativ.descricao}}<br /><br />\n                <span *ngIf="ativ.responsavel">Responsável: {{ativ.responsavel}}</span>\n                <span *ngIf="ativ.valor"><br />Valor: {{ativ.strValor}}</span>\n                <span *ngIf="ativ.local"><br />Local: {{ativ.local}}</span>\n            </div><br />\n            <div>Dias da semana:</div>\n            <div *ngFor="let dia of ativ.diasDaSemana" class="fontSize">\n                {{dia}} \n                <span *ngIf="ativ.horaInicio">\n                    - {{ativ.horaInicio}} \n                </span> \n                <span *ngIf="ativ.horaFim">\n                   as {{ativ.horaFim}}\n                </span>\n            </div>\n            <!-- <ion-label class="fontSize"> \n                <ion-icon name="arrow-dropright"></ion-icon> {{ativ.titulo}}<br />\n                {{ativ.descricao}}\n                <ion-item text-wrap *ngFor="let dia of ativ.diasDaSemana" class="fontSize">\n                     {{dia}} - {{ativ.horaInicio}}\n                </ion-item>  \n            </ion-label>-->\n        </ion-item>  \n    </ion-list>\n</ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/agendaReserva/atividade.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_4__service_ServiceAgenda__["a" /* ServiceAgenda */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
], atividade);

//# sourceMappingURL=atividade.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceTemporada; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServiceTemporada = (function () {
    function ServiceTemporada(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceUsuario Provider/ Host' + this._baseUrlServer);
    }
    ServiceTemporada.prototype.addNovaTemporada = function (oTemporada) {
        var params = JSON.stringify(oTemporada);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "hospedagem/AddHospedagem", params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTemporada.prototype.listarTemporadas = function (idUnidade) {
        var params = 'unidadeID=' + idUnidade;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'hospedagem/getHospedagens?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTemporada.prototype.removerTemporada = function (id) {
        var params = 'hospID=' + id;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + 'hospedagem/deleteHospedagem?' + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceTemporada.prototype.editarTemporada = function (oTemporada) {
        var params = JSON.stringify(oTemporada);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + "hospedagem/EditHospedagem", params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceTemporada;
}());
ServiceTemporada = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceTemporada);

//# sourceMappingURL=ServiceTemporada.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return tabVeiculos; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__base_pageBase__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_ServiceVeiculo__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__modalAddVeiculo__ = __webpack_require__(412);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modalEditVeiculo__ = __webpack_require__(413);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var tabVeiculos = (function (_super) {
    __extends(tabVeiculos, _super);
    function tabVeiculos(storage, navCtrl, suporte, modal, alertCtrl, servVeiculo) {
        var _this = _super.call(this, navCtrl, storage) || this;
        _this.storage = storage;
        _this.navCtrl = navCtrl;
        _this.suporte = suporte;
        _this.modal = modal;
        _this.alertCtrl = alertCtrl;
        _this.servVeiculo = servVeiculo;
        _this.listaDeVeiculos = [];
        return _this;
    }
    tabVeiculos.prototype.ngOnInit = function () {
        this.getVeiculos();
    };
    tabVeiculos.prototype.getVeiculos = function () {
        var _this = this;
        this.storage.get("UsuarioLogado").then(function (dataUser) {
            _this.suporte.showLoader("Carregando veículos...");
            _this.servVeiculo.listarMeusCarros(dataUser.aptId)
                .subscribe(function (veiculos) { return _this.listaDeVeiculos = veiculos; }, function (error) { return console.log("Error ao obter Veiculos"); }, function () { return _this.suporte.dismissLoader(); });
        });
    };
    tabVeiculos.prototype.openModalNovoVeiculo = function () {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_6__modalAddVeiculo__["a" /* modalAddVeiculo */]);
        m.present();
    };
    tabVeiculos.prototype.openModalEditVeiculo = function (oEdit) {
        var m = this.modal.create(__WEBPACK_IMPORTED_MODULE_7__modalEditVeiculo__["a" /* modalEditVeiculo */], oEdit);
        m.present();
    };
    tabVeiculos.prototype.excluirVeiculo = function (idVeiculo) {
        this.confirmExcluirVeiculo("Excluir Vaga", "Tem certeza que deseja excluir este veículo?", idVeiculo);
    };
    tabVeiculos.prototype.confirmExcluirVeiculo = function (titulo, msg, veiculoID) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
                {
                    text: 'Não',
                    handler: function () {
                        //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: function () {
                        var retorno;
                        _this.servVeiculo.removerVeiculo(veiculoID)
                            .subscribe(function (r) { return retorno = r; }, function (error) { return console.log("Erro ao deletar veiculo"); }, function () {
                            if (retorno.status == 0) {
                                _this.suporte.showAviso(retorno.mensagem);
                                _this.getVeiculos();
                            }
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    tabVeiculos.prototype.doRefresh = function (refresher) {
        // console.log('Begin async operation', refresher);
        this.getVeiculos();
        setTimeout(function () {
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    };
    return tabVeiculos;
}(__WEBPACK_IMPORTED_MODULE_4__base_pageBase__["a" /* PageBase */]));
tabVeiculos = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'tabVeiculos-page',template:/*ion-inline-start:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabVeiculos.html"*/'<ion-header>\n    \n      <ion-navbar color="dark">\n        <ion-title>Veículos</ion-title>\n      </ion-navbar>\n    \n    </ion-header>\n        \n    <ion-content>\n\n        <ion-refresher (ionRefresh)="doRefresh($event)">\n            <ion-refresher-content></ion-refresher-content>\n        </ion-refresher>\n        <ion-toolbar color="subVagas">\n            <ion-row>\n                <ion-col width-100 style="text-align:center;color:#fff">Deslize para a esquerda para mais opções.</ion-col>\n            </ion-row>  \n        </ion-toolbar>\n        <ion-list>\n           <ion-item-sliding *ngFor="let veiculo of listaDeVeiculos">\n              <ion-item text-wrap>\n                  <p><ion-icon name="car"></ion-icon> Modelo: {{veiculo.modelo}} / Cor: {{veiculo.cor}} <br /> Placa: {{veiculo.placa}}</p>\n                  <p class="txtMenor">Veículo cadastrado</p>\n                  <ion-icon name="ios-arrow-back-outline" item-end item-right small></ion-icon>\n              </ion-item>\n              <ion-item-options side="right">\n                  <button ion-button color="primary" (click)="openModalEditVeiculo(veiculo)">\n                      <ion-icon name="create"></ion-icon>\n                      Editar\n                  </button>\n                  <button ion-button color="remover" (click)="excluirVeiculo(veiculo.id)">\n                        <ion-icon name="trash"></ion-icon>\n                        Remover\n                    </button>\n              </ion-item-options>\n            </ion-item-sliding> \n       </ion-list>\n        \n       <ion-fab right bottom>\n            <button ion-fab color="dark" (click)="openModalNovoVeiculo()"><ion-icon name="add"></ion-icon></button>\n            <!--<ion-fab-list side="left">\n                <button ion-fab><ion-icon name="logo-facebook"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-twitter"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-vimeo"></ion-icon></button>\n                <button ion-fab><ion-icon name="logo-googleplus"></ion-icon></button>\n            </ion-fab-list>-->\n        </ion-fab>\n    </ion-content>'/*ion-inline-end:"/Users/rodrigoalcantara/Documents/CondominioApp/AppCondominio/src/pages/vaga/tabVeiculos.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__app_app_suporte__["a" /* Suporte */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* ModalController */],
        __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__service_ServiceVeiculo__["a" /* ServiceVeiculo */]])
], tabVeiculos);

//# sourceMappingURL=tabVeiculos.js.map

/***/ }),

/***/ 87:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceVeiculo; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
var ServiceVeiculo = (function () {
    function ServiceVeiculo(http) {
        this.http = http;
        this._baseUrlServer = __WEBPACK_IMPORTED_MODULE_2__app_globalVariables__["a" /* global */].BASE_API_URL;
        //console.log('Hello ServiceVeiculo');
    }
    ServiceVeiculo.prototype.listarMeusCarros = function (apartamentoID) {
        var params = 'aptId=' + apartamentoID;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "carro/ListaCarros?" + params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVeiculo.prototype.addVeiculo = function (oVeiculo) {
        //console.log(oVeiculo);
        var params = JSON.stringify(oVeiculo);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'carro/AddCarro', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVeiculo.prototype.editVeiculo = function (oVeiculo) {
        //console.log(oVeiculo);
        var params = JSON.stringify(oVeiculo);
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers, method: "post" });
        return this.http.post(this._baseUrlServer + 'carro/EditCarro', params, options)
            .map(function (res) { return res.json(); });
    };
    ServiceVeiculo.prototype.removerVeiculo = function (id) {
        var params = '_idCarro=' + id;
        var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({ 'Content-Type': 'application/json' });
        var options = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
        return this.http.get(this._baseUrlServer + "carro/dellCarro?" + params, options)
            .map(function (res) { return res.json(); });
    };
    return ServiceVeiculo;
}());
ServiceVeiculo = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceVeiculo);

//# sourceMappingURL=ServiceVeiculo.js.map

/***/ })

},[439]);
//# sourceMappingURL=main.js.map