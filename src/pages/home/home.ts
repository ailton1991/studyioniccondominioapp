﻿import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController, ToastController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { SignalR } from "ng2-signalr";
import { ServiceSignalr } from "../../service/ServiceSignalr";
import { ServiceHome } from '../../service/ServiceHome';
import { ServiceVisita } from "../../service/ServiceVisita";
import { ServiceChat } from "../../service/ServiceChat";
import { ServiceEnquete } from "../../service/ServiceEnquete";
import { ServiceOcorrencia } from "../../service/ServiceOcorrencia";
import { ServiceUsuario } from "../../service/ServiceUsuario";
import { ServiceCarteiraDigital } from "../../service/ServiceCarteiraDigital";

import { Suporte } from "../../app/app.suporte";

import { PageBase } from "../base/pageBase";
import { comunicadoOcorrencia } from '../comunicadoOcorrencia/comunicadoOcorrencia';
import { agendaReserva } from '../agendaReserva/agendaReserva';
import { correspondencia } from '../correspondencia/correspondencia';
import { portaria } from '../portaria/portaria';
import { mural } from '../mural/mural';
import { classificado } from '../classificado/classificado';
import { enquete } from "../enquete/enquete";
import { vaga } from "../vaga/vaga";
import { conversas } from "../chat/conversas";
import { mensagens } from "../chat/mensagens";
import { produtoServicos } from "../produtoServicos/produtoServicos";
import { modalAutorizarEntrada } from "../portaria/modalAutorizarEntrada";
import { perfil } from "../perfil/perfil";
import { unidade } from "../unidade/unidade";
import { atividade } from "../agendaReserva/atividade";
import { enqueteVotar } from "../enquete/enqueteVotar";
import { tabVeiculos } from "../vaga/tabVeiculos";
import { carteiraVirtual } from "../carteiraVirtual/carteiraVirtual";
import { administradora } from "../administradora/administradora";

@Component({
    selector: 'home-page',
    templateUrl: 'home.html'
})

export class Home extends PageBase implements OnInit{

    posicao: number;
    urlBanner: string;
    interativoImg: string;
    tempoInfo: any;
    valorTemp: any;
    imgTemp: string;
    logoMarcaHome: string;

    displayTitulo : string;
    displayDescricao : string;

    objCondominio: any;

    oData = new Date();
    cnx : any;


    portariaControle : boolean;
    vagasControle : boolean;
    chatControle : boolean;
    classificadoControle : boolean; 
    correioControle : boolean;


    conversaAtiva : any;
    listaDeEnquetesNaoRespondidas : any;

    nomeUsuario: string = 'Bem Vindo(a)';
    strFotoUsuario : string = "";
    bloco_qd : string = "Nf";
    andar_lt: string = "Nf";
    unidade_num:string = "Nf";
    nomeCondominio : string;
    urlImageServ : string;
    codUnidade : string;

    isCarteirinha : boolean = false;

    pages: Array<{ title: string, component: any ,imageUrl: string}>;

    constructor(public navCtrl: NavController, 
                public alertCtrl : AlertController,
                public storage: Storage, 
                public toastCtrl : ToastController,
                public network: Network,
                public _servHome: ServiceHome, 
                public signalr : ServiceSignalr, 
                public cnxSignalr : SignalR,
                public servVisita : ServiceVisita,
                public servChat: ServiceChat,
                public modal : ModalController,
                public suporte : Suporte,
                public servEnquete : ServiceEnquete,
                public servOcorrencia : ServiceOcorrencia,
                public servUsuario : ServiceUsuario,
                public servCarteira : ServiceCarteiraDigital) 
                {
                    super(navCtrl, storage);
                    this.listaDeEnquetesNaoRespondidas = [];
                                
                    this.interativoImg = 'assets/img/icones/Information.png';
                    this.signalr.iniciarConexao();
                }

    ngOnInit(){

        this.getMeteorologia();        
        this.configuraCondominio();
        this.getIsVisitaPendente();
        this.getPrioridade();
        this.getEnquetesNaoRespondidas();
        this.getCarteirasDaAreaDeAcesso();
        this.setToken();

        let onMessageSent  = this.signalr.connection.listenFor("addMensagemChatApp");
        onMessageSent.subscribe((oMessage : any) => this.setHomeChat(oMessage)); 

         // watch network for a disconnect
         this.network.onDisconnect().subscribe(() => {
            //console.log('network was disconnected :-(');
            this.suporte.showAviso("Você está off-line no momento!");
        });
    }

    getCarteirasDaAreaDeAcesso(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            let carteirasDaUnidade : any;
            this.servCarteira.getCarteirasDaUnidade(dataUser.aptId)
                    .subscribe(
                        result => carteirasDaUnidade = result,
                        error => console.log("Error ao obter carteiras"),
                        () => {
                            if(carteirasDaUnidade.length > 0)
                                this.isCarteirinha = true;
                                
                            this.storage.set("cateirasDigitais",carteirasDaUnidade);
                        }) 
        }); 
    }

    setHomeChat(oMensagem : any){
        
        this.storage.get("UsuarioLogado").then((dataUser) => {
           
            //if (dataUser.userID == oMensagem.userIDdestinatario){

                this.servChat.findConversa(oMensagem.conversaId, dataUser.userID)
                    .subscribe(
                        conversa => this.conversaAtiva = conversa,
                        error => console.log(this.errorMensage),
                        () => {     
                            this.interativoImg = 'assets/img/icones/branco/iconChat.png';
                            this.displayTitulo = "Chat";
                            this.displayDescricao = oMensagem.descricao;
                        }
                    )
           // }
        })
    }

    getEnquetesNaoRespondidas(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
                this.servEnquete.listarEnquetesNaoRespondidas(dataUser.condominioId,dataUser.userID)
                        .subscribe(
                            enquetes => this.listaDeEnquetesNaoRespondidas = enquetes,
                            error => console.log("Error ao obter enquetes"),
                            () => {
                                if (this.listaDeEnquetesNaoRespondidas.length > 0){
                                    let m = this.modal.create(enqueteVotar,this.listaDeEnquetesNaoRespondidas);
                                    m.present();
                                }
                            })
            }
        })
    }

    getCodeDeBarra(){

        // let opt = {
        //     nomeAreaAcesso : 'Ônibus',
        //     nomeCondominio : 'Palmeiras',
        //     idAreaDeAcesso : 2,
        //     idCondominio : 1
        // }

        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
                if (dataUser.foto == null){
                    this.confirmAdicionarFotoPerfil("Perfil","Para a utilização da carteira é necessário uma foto de perfil.");
                }
                else
                {
                    this.navCtrl.push(carteiraVirtual);
                }
            }
        })

        // let options = {
        //     resultDisplayDuration : 0
        // };

        // this.barcodeScanner.scan(options).then((barcodeData) => {
        //     // Success! Barcode data is here
        //     console.log(JSON.stringify(barcodeData));
            
        //     if (!barcodeData.cancelled && barcodeData.format == "QR_CODE"){
        //         let objItemCode = JSON.parse(barcodeData.text);
        //         this.navCtrl.push(carteiraVirtual,objItemCode);
        //     }
            
        //    }, (err) => {
        //        // An error occurred
        //        console.log('Error: ' + err);
        //    });
    }


    confirmAdicionarFotoPerfil(titulo : string, msg : string) {
        let confirm = this.alertCtrl.create({
          title: titulo,
          message: msg,
          buttons: [
            {
              text: 'Editar Perfil',
              handler: () => {
                this.meuPerfil();
              }
            },
            {
              text: 'Fechar',
              handler: () => {}
            }
          ]
        });
        confirm.present();
      }

    criarOcorrenciaEmergencia(){
        this.confirmcriarOcorrenciaEmergencia("ATENÇÃO!","Deseja informar uma emergência?");
    }

    confirmcriarOcorrenciaEmergencia(titulo : string, msg : string) {
        let confirm = this.alertCtrl.create({
          title: titulo,
          message: msg,
          buttons: [
            {
              text: 'Saúde',
              handler: () => {
                this.enviarEmergencia(1);
              }
            },
            {
                text: 'Incêndio',
                handler: () => {
                    this.enviarEmergencia(2);
                }
              },
              {
                text: 'Outros',
                handler: () => {
                    this.enviarEmergencia(3);
                }
              },
            {
              text: 'Fechar',
              handler: () => {}
            }
          ]
        });
        confirm.present();
      }

      private enviarEmergencia(tpEmergencia : number) : void{
          this.storage.get("UsuarioLogado").then((dataUser) => {
            let objPostOcorrencia = {
                clienteId : dataUser.userID,
                condominioId : dataUser.condominioId,
                idApartamentoOcorrencia : dataUser.aptId,
                EmergenciaID: tpEmergencia
            };

            let result : any;
            this.servOcorrencia.addOcorrenciaEmergencia(objPostOcorrencia)
                    .subscribe(
                        r => result = r,
                        error => console.log("Error ao enviar a ocorrencia de emergencia"),
                        () => {
                            if (result.status == 0){
                                this.suporte.showAviso(result.mensagem);
                                this.cnxSignalr.connect().then((c) => {
                                    c.invoke("SendNovaOcorrencia",dataUser.condominioId).then((r) => console.log("Nova ocorrencia enviada com sucesso"));
                                })
                            }
                        }
                    )
          })
      }


    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.configuraCondominio();
        this.getPrioridade();
        this.getIsVisitaPendente();
        this.getEnquetesNaoRespondidas();
        this.getCarteirasDaAreaDeAcesso();
        
         setTimeout(() => {
           //console.log('Async operation has ended');
           refresher.complete();
         }, 1000);
       }

    configuraCondominio(){
        this.storage.get("UsuarioLogado").then((dataUser) => {

            let arrItens = dataUser.strApartamento.split("|");
            
            this.nomeUsuario = dataUser.nome;
            this.strFotoUsuario = dataUser.foto;
            this.bloco_qd = arrItens[2];
            this.unidade_num = arrItens[1];
            this.andar_lt = arrItens[3];
            this.nomeCondominio = dataUser.strNomeCondominio;
            this.codUnidade = dataUser.strCodUnidade;

            this._servHome.getConfiguracaoCondominio(dataUser.condominioId)
                .subscribe(condo => this.objCondominio = condo,
                error => console.log('Error reportado: ' + <any>error),
                () => {
                    this.classificadoControle = this.objCondominio.classificado;
                    this.portariaControle = this.objCondominio.portaria;
                    this.chatControle = this.objCondominio.chat;
                    this.vagasControle = this.objCondominio.vaga;
                    this.correioControle = this.objCondominio.flCorrespondenciaAtivar;

                    if (this.objCondominio.logoAdministradora)
                        this.logoMarcaHome = this.urlImageServ + "usuario/" + this.objCondominio.logoAdministradora;
                    else if(this.objCondominio.logoMarca)
                        this.logoMarcaHome = this.urlImageServ + "usuario/" + this.objCondominio.logoMarca;

                    
                    this.storage.set('objCondominio', this.objCondominio)
                }
            )  
        })
    }
       
    getIsVisitaPendente(){
        let vst : any;

        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
                this.servVisita.isVisitaPendente(dataUser.aptId)
                    .subscribe(
                        r  => vst = r,
                        error => console.log("Error ao obter"),
                        () =>{

                            if (vst != null){
                                let m = this.modal.create(modalAutorizarEntrada,vst);
                                m.present();
                            }
                        }
                    )
                }
        }) 
    }

    getPrioridade(){
        
        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
                this.suporte.showLoader("Obtendo informações de sua unidade...");
                let ret : any;
                this._servHome.getPrioridadeHome(dataUser.aptId)
                        .subscribe(
                            result => ret = result,
                            error => console.log("Error ao consultar prioridade"),
                            () => {

                                this.interativoImg = 'assets/img/icones/Information.png';
                                let arrRetorno = ret.mensagem.split('|');

                                if (ret.status == 0){
                                    this.displayTitulo = arrRetorno[0];
                                  //  this.displayDescricao = arrRetorno[2];
                                   this.showComunicado(arrRetorno[0] + " - " + arrRetorno[2]);                                    
                                }
                                else
                                {    
                                    this.interativoImg = this.interativoImg.replace('Information.png','');
                                    this.displayTitulo = arrRetorno[0];
                                    this.interativoImg = this.interativoImg  + "branco/" + arrRetorno[1];
                                  //  this.displayDescricao = arrRetorno[2];  
                                  this.showComunicado(arrRetorno[0] + " - " + arrRetorno[2]); 
                                }

                                this.suporte.dismissLoader();
                            }
                        )
            }
        }) 
    }
    
    redirecionaLink(){
        if (this.displayTitulo != ""){
            switch(this.displayTitulo){
                case 'Correspondência' :
                    this.navCtrl.push(correspondencia);
                    break;

                case 'Comunicado' :
                    this.navCtrl.push(comunicadoOcorrencia);
                    break;

                case 'Portaria' :
                    this.navCtrl.push(portaria);
                    break;

                case 'Chat' :
                    let m = this.modal.create(mensagens,this.conversaAtiva);
                    m.present();
                    break;

                default:
                    this.navCtrl.push(comunicadoOcorrencia);
            }
        }
    }

    ionViewDidLoad() {
        if (this.oData.getHours() > 6 && this.oData.getHours() < 18) {
            this.urlBanner = 'assets/img/BannerHome/normal_dia_app.png';
        }
        else {
            this.urlBanner = 'assets/img/BannerHome/normal_noite_app.png';
        }
    }

   

    showComunicado(strConteudo : string) {

        this.storage.get("strUltimoComunicado").then((dataUltimo) =>{

            if (dataUltimo != strConteudo.substring(0,4)){
                const toast = this.toastCtrl.create({
                    message: strConteudo,
                    duration: 10000,
                    position: 'bottom',
                    showCloseButton: true,
                    closeButtonText: "X",
                    cssClass: "toast-success",
                });
                
                toast.onDidDismiss(() => {
        
                    this.storage.set("strUltimoComunicado",strConteudo.substring(0,4));
                    console.log('Dismissed toast');
                });
                
                toast.present();
            }
        })
    }

    setToken(){
        this.storage.get('UsuarioLogado').then((data) => {
            if (data != null) {
                this.cnxSignalr.connect().then((c) =>{
                    c.invoke("getToken",data.userID).then((r) => console.log("Token conectado no app"));
                })
            }
        });
    }

    getMeteorologia() : void {
        this._servHome.getMeteorologiaCloud()
            .subscribe(
            info => this.tempoInfo = info,
            error => console.log('Error reportado: ' + <any>error),
            () => {
                //console.log(JSON.parse(this.tempoInfo));

                this.tempoInfo = JSON.parse(this.tempoInfo);

                if (this.oData.getHours() > 6 && this.oData.getHours() < 18) {
                    if (this.tempoInfo.results.description.indexOf('nub') > -1) {
                        this.urlBanner = 'assets/img/BannerHome/nublado_dia_app.png';
                    } else if (this.tempoInfo.results.description.indexOf('chuv') > -1) {
                        this.urlBanner = 'assets/img/BannerHome/chuvoso_dia_app.png';
                    }
                }
                else {
                    if (this.tempoInfo.results.description.indexOf('nub') > -1) {
                        this.urlBanner = 'assets/img/BannerHome/nublado_noite_app.png';
                    } else if (this.tempoInfo.results.description.indexOf('chuv') > -1) {
                        this.urlBanner = 'assets/img/BannerHome/chuvoso_noite_app.png';
                    }
                }

                this.valorTemp = this.tempoInfo.results.temp + 'º';
                this.imgTemp = 'assets/img/tempo/' + this.tempoInfo.results.img_id + '.png';
            });
    }

    getPageComunicado(): void {
        this.navCtrl.push(comunicadoOcorrencia);
    }

    getAgendaReserva() : void{
        this.navCtrl.push(agendaReserva);
    }

    getCorrespondencias() : void{
        this.navCtrl.push(correspondencia);
    }

    getPortaria() : void{
        this.navCtrl.push(portaria);
    }

    getMural() : void{
        this.navCtrl.push(mural);
    }

    getClassificado() :void{
        this.navCtrl.push(classificado);
    }

    getAvisoClassificadoOff() : void{
        this.suporte.showAviso("Esta função está desabilitada pelo seu condomínio.");
    }

    getEnquetes() : void{
        this.navCtrl.push(enquete);
    }

    getVagas() : void{
        this.navCtrl.push(vaga);
    }

    getProdutos() : void{
        this.navCtrl.push(produtoServicos);
    }

    getChat() : void{
        this.navCtrl.push(conversas);
    }

    getRedirectAdministradora(){
        if (this.objCondominio.urlWebServer != null && this.objCondominio.boletoFolder != null){
             this.navCtrl.push(administradora);
        }
        else if (this.objCondominio.linkGeraBoleto){ 
            this.suporte.openUrlSite(this.objCondominio.linkGeraBoleto);
        }
        else{
            this.suporte.showAviso("A função administradora não foi configurada em seu condomínio.");
        }
    }

    getVeiculos() : void{
        this.navCtrl.push(tabVeiculos);
    }

    getUnidades() : void{
        this.navCtrl.push(unidade);
    }

    getAtividade() : void{
        let m = this.modal.create(atividade);
        m.present();
    }
    meuPerfil(){
        let oUser : any;
        this.storage.get("UsuarioLogado").then((dataUser) => {
            
        this.suporte.showLoader("Obtendo usuário...");

        this.servUsuario.getCliente(dataUser.userID)
            .subscribe(
                user => oUser = user,
                error => console.log("Error ao obter usuario"),
                () => {
                    this.suporte.dismissLoader()
                    this.navCtrl.push(perfil,oUser);
                }
            )
        })
        
    }
}