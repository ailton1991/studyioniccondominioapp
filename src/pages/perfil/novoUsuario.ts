import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { Suporte } from '../../app/app.suporte';
import { ServiceUsuario } from '../../service/ServiceUsuario';

import { PageBase } from "../base/pageBase";
import { global } from "../../app/globalVariables";

import { Home } from "../home/home";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: "novoUsuario-page",
    templateUrl: "novoUsuario.html"
})

export class novoUsuario extends PageBase implements OnInit {

    form: FormGroup; 
    pathPreviewImage : string;
    usuarioLogado : any;
    codigoUnidade : string;
        
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl : ModalController,
                public fb : FormBuilder,
                public camera : Camera,
                public fileUp : Filetransferencia,
                public servUsuario : ServiceUsuario)
    {
        super(navCtrl, _storage);
        this.codigoUnidade = this.navParams.data;

        //console.log( this.codigoUnidade);

        this.form = this.fb.group({
            foto : [""],
            nome : ["", Validators.required],
            sobrenome : [""],
            cel : [""],
            proprietario : [false],
            email: ["", Validators.required],
            senha : ["",Validators.required]
        });
    }
    
    ngOnInit(){  
        
    }

    novoPerfil(){
        let userSave = this.form.value;
        //console.log(userSave);

        let user = {
            nome : userSave.nome,
            sobrenome : userSave.sobrenome,
            cel : userSave.cel,
            email : userSave.email,
            senha : userSave.senha,
            proprietario : userSave.proprietario,
            foto : userSave.foto,
            codApartamento : this.codigoUnidade
        }
        
        this.sp.showLoader("Criando perfil...");
        let result : any;
        this.servUsuario.novoUsuario(user)
                .subscribe(
                    r => result = r,
                    error => console.log("Error ao salvar perfil"),
                    () => {
                        if (result.status == 0){
                           //this.sp.showAviso(result.mensagem);
                            this.form.reset();
                            
                            let UsuarioLogado : any;
                            this.servUsuario.autenticarUsuario(user.email, user.senha)
                            .subscribe(
                                usuario => UsuarioLogado = usuario,
                                error => console.log('Error reportado: ' + <any>error),
                                () => {
                                    //console.log(this.UsuarioLogado);
                                    if (UsuarioLogado.status == 0) {
                                        this.storage.set('UsuarioLogado', UsuarioLogado);
                                        this.navCtrl.setRoot(Home);
                                    }
                                    else {
                                        this.sp.showAlert('Autenticação', UsuarioLogado.mensagem);
                                    }
                                });
                        }

                        if (result.status == 1){
                            this.sp.showAviso(result.mensagem);
                        }

                        if (result.status == -1){
                            this.sp.showAviso(global.ERROR_MSG);
                        }

                        this.sp.dismissLoader();
                    }
                )
    }

    takePic(tpFonte) {
        
                const options: CameraOptions = {
                    quality: 50,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: this.camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    correctOrientation: true,
                    encodingType: this.camera.EncodingType.JPEG,
                    saveToPhotoAlbum: false
                }
        
                if (tpFonte == 'galeria')
                    options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;    
                
        
                this.camera.getPicture(options).then((imageURI) => {
                    // imageData is either a base64 encoded string or a file URI
                    // If it's base64:           
                   // this.pathPreviewImage = imageURI;
        
                    let guid = new Date().getTime();
                    let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        
                    nomeImage = guid + nomeImage;
                    
                   // this.form.addControl("foto",new FormControl(nomeImage));
                   this.form.setControl("foto",new FormControl(nomeImage) )
                   this.fileUp.upload(nomeImage,imageURI,'Cliente');
                   this.sp.showAviso(this.fotoAnexada);

                }, (err) => {
                    // Handle error
                    console.log('takePic: ' + err);
                });
            }
}