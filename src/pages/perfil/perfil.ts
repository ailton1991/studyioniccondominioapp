import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { Suporte } from '../../app/app.suporte';
import { ServiceUsuario } from '../../service/ServiceUsuario';

import { PageBase } from "../base/pageBase";
import { global } from "../../app/globalVariables";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: "perfil-page",
    templateUrl: "perfil.html"
})

export class perfil extends PageBase implements OnInit {

    form: FormGroup; 
    pathPreviewImage : string;
    usuarioLogado : any;
        
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl : ModalController,
                public fb : FormBuilder,
                public camera : Camera,
                public fileUp : Filetransferencia,
                public servUsuario : ServiceUsuario)
    {
        super(navCtrl, _storage);

        this.pathPreviewImage = this.urlImageServ + "usuario/";
        this.usuarioLogado = this.navParams.data;

        //console.log(this.usuarioLogado);

        this.form = this.fb.group({
            id : [this.usuarioLogado.id],
            foto : [this.usuarioLogado.foto],
            nome : [this.usuarioLogado.nome, Validators.required],
            sobrenome : [this.usuarioLogado.sobrenome, Validators.required],
            rg : [this.usuarioLogado.rg],
            cpf : [this.usuarioLogado.cpf],
            cel : [this.usuarioLogado.cel],
            email: [this.usuarioLogado.email, Validators.required],
            senha : [this.usuarioLogado.senha,Validators.required]
        });
    }
    
    ngOnInit(){  
        
    }

    salvarPerfil(){
        let userSave = this.form.value;
        //console.log(userSave);
        this.sp.showLoader("Atualizando perfil...");
        let result : any;
        this.servUsuario.editUsuario(userSave)
                .subscribe(
                    r => result = r,
                    error => console.log("Error ao salvar perfil"),
                    () => {
                        if (result.status == 0){

                            this.storage.get("UsuarioLogado").then((user) =>{
                                user.nome = userSave.nome + " " + userSave.sobrenome;
                                user.foto = userSave.foto;
                                user.email = userSave.email;
                                user.senha = userSave.senha;
                                user.cpf = userSave.cpf;

                                this.storage.set("UsuarioLogado",user);
                            });
                            
                            this.sp.showAviso(result.mensagem);
                            this.form.reset();
                            this.navCtrl.pop();
                        }

                        if (result.status == 1){
                            this.sp.showAviso(result.mensagem);
                        }

                        if (result.status == -1){
                            this.sp.showAviso(global.ERROR_MSG);
                        }

                        this.sp.dismissLoader();
                    }
                )
    }

    takePic(tpFonte) {
        
                const options: CameraOptions = {
                    quality: 50,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: this.camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    correctOrientation: true,
                    encodingType: this.camera.EncodingType.JPEG,
                    saveToPhotoAlbum: false
                }
        
                if (tpFonte == 'galeria')
                    options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;    
                
        
                this.camera.getPicture(options).then((imageURI) => {
                    // imageData is either a base64 encoded string or a file URI
                    // If it's base64:           
                   // this.pathPreviewImage = imageURI;
        
                    let guid = new Date().getTime();
                    let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        
                    nomeImage = guid + nomeImage;
                    
                   // this.form.addControl("foto",new FormControl(nomeImage));
                   this.form.setControl("foto",new FormControl(nomeImage) )
                   this.fileUp.upload(nomeImage,imageURI,'Cliente');
                   this.sp.showAviso(this.fotoAnexada);
        
                }, (err) => {
                    // Handle error
                    console.log('takePic: ' + err);
                });
            }
}