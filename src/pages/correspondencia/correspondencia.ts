import { Component, OnInit } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PageBase } from "../base/pageBase";
import { Suporte } from "../../app/app.suporte";

import { ServiceCorrespondencia } from '../../service/ServiceCorrespondencia';

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-correspondencia',
    templateUrl: 'correspondencia.html'
})
export class correspondencia extends PageBase implements OnInit {

    correspondenciaList : any;

    constructor(public navCtrl: NavController, 
                public navParams: NavParams, 
                public _storage : Storage,
                public serviceCorreio : ServiceCorrespondencia,
                public sp : Suporte) 
    {
        super(navCtrl, _storage);
        this.correspondenciaList = [];
       
    }

    ionViewDidLoad() {
       
    }

    ngOnInit(){ 
        this.sp.showLoader("Carregando correspondências...");
        this._storage.get("UsuarioLogado").then((dataUser) =>{
            this.serviceCorreio.ListarHistorico(dataUser.aptId)
                .subscribe(
                    correspondencias => this.correspondenciaList = correspondencias,
                    error => console.log("falha em obter correspondencias"),
                    () =>{
                        this.sp.dismissLoader();
                        setTimeout(() => this.setVistoDeCorrespondencia(),3500);
                    });
        });
    }
    
    openModel(){
        this.sp.showAlert("Correspondência","Existe uma nova correspondência para sua unidade, não se esqueça de retirar.");
    }

    setVistoDeCorrespondencia(){
        let ret : any;

        this._storage.get("UsuarioLogado").then((dataUser) =>{
            this.serviceCorreio.setVisto(dataUser.aptId)
                .subscribe(
                    r => ret = r,
                    error => console.log("falha em set visto"),
                    () =>{});
        });
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.setVistoDeCorrespondencia();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}