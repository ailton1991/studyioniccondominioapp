import { Component} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceAgenda } from '../../service/ServiceAgenda';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "page-atividade",
    templateUrl: "atividade.html"
})

export class atividade extends PageBase{

    listAtividades : any;

    constructor(
        public storage : Storage,
        public sp : Suporte,
        public agendaService : ServiceAgenda,
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController
    ){
        super(navCtrl, storage);

        this.listAtividades = [];

        this.carregaAtividades();
    }


    carregaAtividades(){
        this.sp.showLoader("Carregando atividades...");
        this.storage.get("UsuarioLogado").then((data) => {
            this.agendaService.getAtividadesDoCondominio(data.condominioId)
                .subscribe(list => this.listAtividades = list,
                            error => console.log("Error ao carregar as Atividades"),
                        () =>this.sp.dismissLoader());
        });
    }

     dismiss() {
        this.viewCtrl.dismiss();
    }
}