import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { ServiceSignalr } from '../../service/ServiceSignalr';
import { ServiceReserva } from '../../service/ServiceReserva';
import { ServiceAgenda } from '../../service/ServiceAgenda';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "modal-novaReserva",
    templateUrl: 'modalNovaReserva.html'
})

export class modalNovaReserva extends PageBase {

    //form: FormGroup;
    pathPreviewImage : string;
    areaComumList : any;
    initmoth : string;
    currentDate : string;
    _horaInicio : string;
    _horaFim : string;
   
    labelPeriodos : string;
    dataLimite : string;
    horariosEspecificos : boolean;

    form: FormGroup;
    arrItemPeriodos : any;
    
    areaComumSelecionada : any;
    
    private periodoSelecionadoID: number;
    private precoPeriodo : any;

    reservaControler : boolean;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public reservaService: ServiceReserva,
        public agendaService: ServiceAgenda,
        public sp: Suporte,
        public fb: FormBuilder,
        public signalr : ServiceSignalr
    ) {

        super(navCtrl, storage);

        this.areaComumSelecionada = this.params.get('oAreaComum');
        let dataSelected = this.params.get('dataPRE');
        
        if (this.validaDataDeReserva(dataSelected))
            this.currentDate = dataSelected.toISOString();
        else
            this.currentDate = new Date().toISOString();

        //var now = new Date();
        //var mesAtual = now.getMonth() + 1;

       // this.initmoth = now.getFullYear() + '-' + mesAtual + '-' + now.getDate();

       let dataFutura = new Date();
       dataFutura = this.addYears(dataFutura,1);

       this.dataLimite = dataFutura.toISOString();

        this.areaComumList = [];
      //  this.listaAreaComum();
        //this.currentDate = (new Date()).toISOString();

        let arrDataMin =  this.currentDate.split('T');
        this.initmoth = arrDataMin[0];

        this.arrItemPeriodos = [];

        
        
        this.form = fb.group({
            areaComumId: [this.areaComumSelecionada.idAreaComum, Validators.required],
            dataDeRealizacao: ["", Validators.required],
            observacao : [""],
            horaInicio : [""],
            horaFim : [""]
        });

        this.areaSelect();
    }

    private validaDataDeReserva(data: Date) : boolean{
        let d = new Date();
        if (data < d)
            return false;
        else
            return true;
    }

    private addYears(date: Date, year: number): Date {
           // date.setDate(date.getDate() + days);
           date.setFullYear(date.getFullYear() + year);
            return date;
    }

    // listaAreaComum(){
    //     this.storage.get("UsuarioLogado").then((data) => {

    //         this.agendaService.getAreaComumList(data.condominioId)
    //             .subscribe(
    //                 areaList => this.areaComumList = areaList,
    //                 error => console.log('error ao carregar as listas de area comum'),
    //                 () => {}
    //             );
    //     });
    // }

    areaSelect(){
        let ev = this.areaComumSelecionada;      

        this.horariosEspecificos = ev.flHorariosEspecifico;

        if (ev.flHorariosEspecifico){
            this.labelPeriodos = "Período";
            this.arrItemPeriodos = ev.lstperiodos;

            this.form.setControl("horaInicio",new FormControl(""));
            this.form.setControl("horaFim",new FormControl(""));

            this.selectPeriodoID(this.arrItemPeriodos[0].id,this.arrItemPeriodos[0].valor,this.arrItemPeriodos[0].horaInicio,this.arrItemPeriodos[0].horaFim);
        }
        else
        { 
            this.labelPeriodos = "Horário de reservas: de " + ev.lstperiodos[0].horaInicio + " até " + ev.lstperiodos[0].horaFim  + " / Valor: R$ " + ev.lstperiodos[0].valor  
            this.arrItemPeriodos = []; 
            this.selectPeriodoID(ev.lstperiodos[0].id,ev.lstperiodos[0].valor,ev.lstperiodos[0].horaInicio,ev.lstperiodos[0].horaFim);

            this.form.setControl("horaInicio",new FormControl("",Validators.required));
            this.form.setControl("horaFim",new FormControl("",Validators.required));
        }
    }

    novaReserva(){
        
        let retorno : any;

        this.storage.get("UsuarioLogado").then((data) => {
            this.form.addControl("apartamentoId",new FormControl(data.aptId));
            this.form.addControl("solicitanteId",new FormControl(data.userID));
            this.form.addControl("periodoID",new FormControl(this.periodoSelecionadoID));
            this.form.addControl("precoDaReserva",new FormControl(this.precoPeriodo));

           // console.log(this.form.value);
            var reservaNova = this.form.value;

            console.log(reservaNova);

            this.reservaService.addNovaReserva(reservaNova)
                    .subscribe(result => retorno = result,
                    error => console.log("Error reportado:"),
                    () => {
                        
                        if (retorno.status == 0){
                            this.sp.showAviso(retorno.mensagem);
                            this.form.reset();
                            this.dismiss();
                            this.signalr.connection.invoke("SendNovaReserva",data.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        }

                        if(retorno.status == 1)
                            this.sp.showAviso(retorno.mensagem);

                        if(retorno.status == -1)
                            this.sp.showAviso("Serviço indisponível no momento, por favor tente mais tarde!");
                    });
        });
    }

    selectPeriodoID(id : number, preco : number, strHoraInicio : string, strHoraFim : string)
    {
        //console.log("Periodo Selecionado: " + id + " preço: " + preco + "Hora inicio " + strHoraInicio + "Hora Fim: " + strHoraFim);

        this.periodoSelecionadoID = id;
        this.precoPeriodo = preco;
        this._horaInicio = strHoraInicio;
        this._horaFim = strHoraFim;

        this.form.setControl("horaInicio",new FormControl(strHoraInicio));
        this.form.setControl("horaFim",new FormControl(strHoraFim));
    }

    checkFirst(indice : number) : boolean{
        if (indice > 0)
            return false;
        else
            return true;
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}