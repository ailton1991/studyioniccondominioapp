import { Component, OnInit } from '@angular/core';
import { NavController,NavParams, ViewController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ServiceAgenda } from '../../service/ServiceAgenda';
import { ServiceReserva } from '../../service/ServiceReserva';


import { Suporte } from '../../app/app.suporte';
import { calendarioReservas } from './calendarioReservas';


@Component({
    selector : "popover-page",
    templateUrl: "menuSuspenso.html"
})

export class menuSuspenso implements OnInit{

   contentEle: any;
   textEle: any;
   
   arrAreaComum : any;

   constructor( public navCtrl : NavController,
                public navParams: NavParams,
                public _serviceAgenda: ServiceAgenda,
                public _serviceReserva : ServiceReserva,
                public sp: Suporte,
                public _storage: Storage,
                public viewCtrl: ViewController) {
                
                 this.arrAreaComum = this.navParams.data;
    }

    

   ngOnInit() 
   {    
      //this.getAreaComum();
   }

   fechar() {
        this.viewCtrl.dismiss();
    }

    changePageReserva(objArea : any){
        //console.log(JSON.stringify(objArea));
        this.navCtrl.push(calendarioReservas,objArea);
        this.fechar();
    }
}