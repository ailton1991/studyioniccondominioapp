import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceReserva } from '../../service/ServiceReserva';

import { PageBase } from "../base/pageBase";
import { modalNovaReserva } from "./modalNovaReserva";


/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: "page-modalCalendarioReserva",
    templateUrl: 'calendarioReservas.html'
})

export class calendarioReservas extends PageBase implements OnInit {

    @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
    @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;

    eventSource;
    viewTitle;
    arrServMethod : any;
    arrEventosDoDia : any;

    isToday:boolean;
    reservaControler : boolean;
      
    selectAreaComum : any;

    dataPreDefinida: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public _serviceReserva : ServiceReserva,
                public sp: Suporte,
                public _storage: Storage,
                public popoverCtrl : PopoverController,
                public modalCtrl : ModalController)
    {
        super(navCtrl, _storage);
        this.arrEventosDoDia = [];
        this.arrServMethod = [];

       this.selectAreaComum = this.navParams.data;
       this.urlArquivosUpload = this.urlArquivosUpload + 'AreaComum/' + this.selectAreaComum.condominioId + "/" + this.selectAreaComum.nome + "/";
       
       //console.log(this.selectAreaComum);
    }
    
    ngOnInit(){  
        this.arrServMethod = this.loadReservasDaArea(); 
        this.storage.get("objCondominio").then((condominio)=> { 
            this.reservaControler = condominio.flReserva;
        })
    }
      
    calendar = {
        mode: 'month',
        currentDate: new Date(),
        dateFormatter: {
            formatMonthViewDay: function(date:Date) {
                return date.getDate().toString();
            }
        }
    };

    abrirArquivo(docStr : string){
        this.sp.openFileDocs(this.urlArquivosUpload + docStr);
    }

    openModalReserva(){
        let modal = this.modalCtrl.create(modalNovaReserva,{oAreaComum: this.selectAreaComum,dataPRE: this.dataPreDefinida});
        modal.present();
    }
           
    ionViewDidLoad() {
        //console.log('ionViewDidLoad tabAgenda'); 
        this.sp.showLoading("Carregando reservas...",3000);
        setTimeout(() => {
           this.eventSource = this.arrServMethod;
        }, 2500);   
    } 
    
    loadEvents() {
        //  console.log("Carregar...");
        //  this.eventSource = this.createRandomEvents();
        // this.eventSource = this.loadAgendaCondominio();
        this.eventSource = this.arrServMethod;
    }

    onViewTitleChanged(title) {
        this.viewTitle = title;
    }

    // onEventSelected(event) {
    //     //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    //    // this.sp.showAlert("Agenda",event.title);
    // }

    changeMode(mode) {
        this.calendar.mode = mode;
    }

    // today() {
    //     this.calendar.currentDate = new Date();
    // }

    onTimeSelected(ev) {
        this.dataPreDefinida = new Date(ev.selectedTime);

        //console.log(this.dataPreDefinida);

        // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
        //     (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
            if (ev.events !== undefined && ev.events.length !== 0){
                this.arrEventosDoDia = ev.events;
            }
            else
            {
                this.arrEventosDoDia = [];
            }
    }

    onCurrentDateChanged(event:Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    loadReservasDaArea(){

        var eventsReservas = [];
        var events = [];
        
        this._serviceReserva.getListReservasDaArea(this.selectAreaComum.idAreaComum)
            .subscribe(
                eventos => eventsReservas = eventos,
                error => console.log("Falha ao carregar a agenda"),
                () => {
                    //console.log(eventsReservas);
                    for(var i = 0; i < eventsReservas.length; i++)
                        {
                            let arrDt = eventsReservas[i].dataDeRealizacao.split("T");
                            let arrDt2 = arrDt[0].split("-");
                            
                            let strdata = arrDt2[0]+ "/" + arrDt2[1] + "/" + arrDt2[2];
                            let t = new Date(strdata);                     

                            events.push({
                                title: "Reservado para o horário:" + eventsReservas[i].horaInicio + " as " + eventsReservas[i].horaFim,
                                startTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                                endTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                                allDay: false
                            });
                        }
                   // console.log(JSON.stringify(events));
                })
        
        return events;
    }

    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }

    markDisabled = (date:Date) => {
        var current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };
}