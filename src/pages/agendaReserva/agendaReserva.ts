import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { NavController, NavParams, PopoverController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceAgenda } from '../../service/ServiceAgenda';

import { PageBase } from "../base/pageBase";
import { atividade } from "./atividade";

import { menuSuspenso } from "./menuSuspenso";
import { minhasReservas } from "./minhasReservas";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: "page-agendaReserva",
    templateUrl: 'agendaReserva.html'
})

export class agendaReserva extends PageBase implements OnInit {

    @ViewChild('popoverContent', { read: ElementRef }) content: ElementRef;
    @ViewChild('popoverText', { read: ElementRef }) text: ElementRef;

    eventSource;
    viewTitle;
    arrServMethod : any;
    arrEventosDoDia : any;
    arrAreasComuns : any;

    isToday:boolean;
        
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public _serviceAgenda: ServiceAgenda,
                public sp: Suporte,
                public _storage: Storage,
                public popoverCtrl : PopoverController,
                public modalCtrl : ModalController)
    {
        super(navCtrl, _storage);
        this.arrEventosDoDia = [];
        this.arrServMethod = [];
        this.arrAreasComuns = [];
    }
    
    ngOnInit(){  
        this.arrServMethod = this.loadAgendaCondominio(); 
        this.getAreaComum();
    }

    getAreaComum(){
        this._storage.get("UsuarioLogado").then((data) => {
            this._serviceAgenda.getAreaComumList(data.condominioId)
                    .subscribe(
                        areas => this.arrAreasComuns = areas,
                        error => console.log("error no get áreas comuns"),
                        () => {});
        });
    }

    calendar = {
        mode: 'month',
        currentDate: new Date(),
        dateFormatter: {
            formatMonthViewDay: function(date:Date) {
                return date.getDate().toString();
            }
        }
    };
   
    openModalMinhasReservas(){
        let modal = this.modalCtrl.create(minhasReservas);
        modal.present();
    }
    openModalAtividade(){
        let modal = this.modalCtrl.create(atividade);
         modal.present();
    }
    
   
    openMenuSuspenso(ev) {
       
        let popover = this.popoverCtrl.create(menuSuspenso,this.arrAreasComuns);
        popover.present({
            ev: ev
        });
    }
           
    ionViewDidLoad() {
        this.sp.showLoading("Carregando eventos...",3000);
        setTimeout(() => {
           this.eventSource = this.arrServMethod;
        }, 2500);   
    } 
    
       loadEvents() {
         //  console.log("Carregar...");
         //  this.eventSource = this.createRandomEvents();
         // this.eventSource = this.loadAgendaCondominio();
          this.eventSource = this.arrServMethod;
        }

    onViewTitleChanged(title) {
        this.viewTitle = title;
    }

    // onEventSelected(event) {
    //     //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    //    // this.sp.showAlert("Agenda",event.title);
    // }

    changeMode(mode) {
        this.calendar.mode = mode;
    }

    // today() {
    //     this.calendar.currentDate = new Date();
    // }

    onTimeSelected(ev) {
       // console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
       //     (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
            if (ev.events !== undefined && ev.events.length !== 0){
                this.arrEventosDoDia = ev.events;
            }
            else
            {
                this.arrEventosDoDia = [];
            }
    }

    onCurrentDateChanged(event:Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    // createRandomEvents() {
    //     var events = [];
    //     for (var i = 0; i < 50; i += 1) {
    //         var date = new Date();
    //         var eventType = Math.floor(Math.random() * 2);
    //         var startDay = Math.floor(Math.random() * 90) - 45;
    //         var endDay = Math.floor(Math.random() * 2) + startDay;
    //         var startTime;
    //         var endTime;
    //         if (eventType === 0) {
    //             startTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + startDay));
    //             if (endDay === startDay) {
    //                 endDay += 1;
    //             }
    //             endTime = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate() + endDay));
    //             events.push({
    //                 title: 'All Day - ' + i,
    //                 startTime: startTime,
    //                 endTime: endTime,
    //                 allDay: true
    //             });
    //         } else {
    //             var startMinute = Math.floor(Math.random() * 24 * 60);
    //             var endMinute = Math.floor(Math.random() * 180) + startMinute;
    //             startTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + startDay, 0, date.getMinutes() + startMinute);
    //             endTime = new Date(date.getFullYear(), date.getMonth(), date.getDate() + endDay, 0, date.getMinutes() + endMinute);
    //             events.push({
    //                 title: 'Event - ' + i,
    //                 startTime: startTime,
    //                 endTime: endTime,
    //                 allDay: false
    //             });
    //         }
    //     }
    //     return events;
    // }

    loadAgendaCondominio(){

        var eventsAgenda = [];
        var events = [];

        this._storage.get("UsuarioLogado").then((data) => {
            this._serviceAgenda.getAgendaCondominio(data.aptId)
                .subscribe(
                    eventos => eventsAgenda = eventos,
                    error => console.log("Falha ao carregar a agenda"),
                    () => {
                        for(var i = 0; i < eventsAgenda.length; i++)
                            {
                                let arrDt = eventsAgenda[i].dataDeRealizacao.split("T");
                                let arrDt2 = arrDt[0].split("-");
                                let t = new Date(arrDt2[0]+ "/" + arrDt2[1] + "/" + arrDt2[2]);

                                events.push({
                                    title: arrDt2[2]+ "/" + arrDt2[1] + "/" + arrDt2[0] + "  -  " + eventsAgenda[i].descricao,
                                    startTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                                    endTime: new Date(Date.UTC(t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate() + 1)),
                                    allDay: false
                                });
                            }
                        //console.log(JSON.stringify(events));
                    }
                )
        });

        return events;
    }


    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }

    markDisabled = (date:Date) => {
        var current = new Date();
        current.setHours(0, 0, 0);
        return date < current;
    };
}