import { Component} from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceReserva } from '../../service/ServiceReserva';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "page-minhasReservas",
    templateUrl: "minhasReservas.html"
})

export class minhasReservas extends PageBase{

    listMinhaReservas : any;
    viewListMinhasReservas : any;

     constructor(
        public storage : Storage,
        public sp : Suporte,
        public reservaService : ServiceReserva,
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public alertCtrl : AlertController
    ){
        super(navCtrl, storage);

        this.listMinhaReservas = [];
        this.viewListMinhasReservas = [];

        this.carregaMinhasReservas();
    }

    carregaMinhasReservas(){
        this.sp.showLoader("Carregando reservas...");
        this.storage.get("UsuarioLogado").then((data) => {
            this.reservaService.getListMinhasReservas(data.aptId)
                .subscribe(list => this.listMinhaReservas = list,
                           Error => console.log("Error ao obter as minha Reservas"),
                           () =>{
                                this.viewListMinhasReservas = [];
                                for(var i = 0; i < this.listMinhaReservas.length; i++){
                                    var oReserva = this.listMinhaReservas[i];
                                    var arrDetailsArea = oReserva.strAreaComun.split('|');
                                    var objViewReserva = {
                                        id : oReserva.id,
                                        areaComumId : oReserva.areaComumId,
                                        apartamentoId : oReserva.apartamentoId,
                                        horaInicio : oReserva.horaInicio,
                                        horaFim : oReserva.horaFim,
                                        editavel : oReserva.editavel,
                                        solicitanteId : oReserva.solicitanteId,
                                        preco : oReserva.precoDaReserva,
                                        isFila : oReserva.isFila,
                                        dtCadastro : oReserva.strDataDeCadastro,
                                        dtRealizacao : oReserva.strdataDeRealizacao,
                                        strAreaComum : arrDetailsArea[0],
                                        descricao : arrDetailsArea[1],
                                        classeStatus : "",    
                                        reservaStatus : "",
                                        justificativaReserva : oReserva.strJustificativa
                                    }

                                    if (oReserva.isAprovado){
                                        objViewReserva.reservaStatus = "Aprovada";
                                        objViewReserva.classeStatus = "aprovado";
                                    }
                                    
                                    if (oReserva.isPendente){
                                         objViewReserva.reservaStatus = "Aguardando aprovação";
                                         objViewReserva.classeStatus = "pendente";
                                    }

                                    if (oReserva.isRejeitado){
                                        objViewReserva.reservaStatus = "Não aprovado";
                                        objViewReserva.classeStatus = "rejeitado";
                                    }

                                    if(oReserva.isExpirado){
                                        objViewReserva.reservaStatus = "Expirada";
                                        objViewReserva.classeStatus = "rejeitado";
                                    }
                                    
                                    //console.log(objViewReserva);
                                    this.viewListMinhasReservas.push(objViewReserva);
                                }
                                this.sp.dismissLoader();
                           });
        });
    }

    excluirReserva(id : number) {
        let confirm = this.alertCtrl.create({
        title: "Apagar visitante",
        message: "Tem certeza que deseja excluir este visitante",
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        let retorno : any;
                        this.reservaService.rejeitarReserva(id)
                            .subscribe(
                                result => retorno = result,
                                Error => console.log("Error ao remover Reserva"),
                                () => {
                                   this.carregaMinhasReservas();
                                   this.sp.showAviso(retorno.mensagem);
                
                                  
                                }
                            )
                    }
                }
            ]
        });
        confirm.present();
    }


    dismiss() {
        this.viewCtrl.dismiss();
    }
}