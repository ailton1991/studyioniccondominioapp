import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceUsuario } from "../../service/ServiceUsuario";
import { ServiceBoleto } from "../../service/ServiceBoleto";
import { boleto } from './boleto';




@Component({
    selector: 'administradora-page',
    templateUrl: 'administradora.html'
})

export class administradora extends PageBase implements OnInit{
    
    boletoApp : boolean;
    balanceteApp : boolean;
    oUserLogado : any;
    
    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController,
                public servUsuario : ServiceUsuario,
                public servBoleto: ServiceBoleto){
        super(navCtrl, storage);

        this.storage.get("objCondominio").then(oCondominioConfig => {
                       
            if (oCondominioConfig.urlWebServer != null && oCondominioConfig.boletoFolder != null){
                this.boletoApp = true;
            }

            if (oCondominioConfig.refId != null && oCondominioConfig.refId > 0){
                this.balanceteApp = true;
            }
        });
    }

    ionViewDidLoad() { }

    ngOnInit(){
       
    }
    
    abrirBoleto(){
        this.storage.get("UsuarioLogado").then(userLogado => {
            this.oUserLogado = userLogado;
            if (userLogado.cpf == null){
                this.openModalCpf();
            }
            else{
                this.getBoletoServer(userLogado.cpf);
            }
        })
    }

    getBoletoServer(strCPF : string){
        this.storage.get("objCondominio").then(oCondominio => {
            
             let folder = oCondominio.boletoFolder;
             let wsboleto = oCondominio.urlWebServer;
             let objBoletoList : any;

             this.suporte.showLoader("Buscando boleto...");
             this.servBoleto.getBoletoService(strCPF,folder,wsboleto)
                        .subscribe(result => objBoletoList = result,
                        error => this.suporte.showAviso(this.errorMensage),
                        () => {
                        this.suporte.dismissLoader();
                            if (objBoletoList[0].id > 0){
                                this.suporte.showAviso(objBoletoList[0].mensagem);
                            }
                            else{
                                this.navCtrl.push(boleto,objBoletoList);
                            }
                        });

         });
    }

    openModalCpf() {
        let prompt = this.alertCtrl.create({
            title: 'Entre com seu CPF',
            message: "Para acesso ao boleto, informe seu CPF.",
            inputs: [
              {
                name: 'cpf',
                placeholder: 'cpf'
              },
            ],
            buttons: [
              {
                text: 'Enviar CPF',
                handler: data => {
                  if (data.cpf != "")
                  {
                    this.suporte.showLoader("Atualizando CPF...");

                    let result : any;
                    this.servUsuario.setCpfUser(this.oUserLogado.userID, data.cpf)
                            .subscribe(
                                ret => result = ret,
                                error => console.log("Error verificar código."),
                                () => {
                                    if (result.status == 0){
                                        this.suporte.dismissLoader();
                                        this.getBoletoServer(data.cpf);
                                    }

                                    if (result.status == 1){
                                        this.suporte.dismissLoader();
                                        this.suporte.showAviso(result.mensagem);
                                    }

                                    if (result.status == -1){
                                        this.suporte.dismissLoader();
                                        this.suporte.showAviso(this.errorMensage);
                                    }
                                })
                  }
                  else
                  {
                    this.suporte.showAviso("Digite seu CPF");
                  }
                }
              }
            ]
          });
          prompt.present();
    }

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
        setTimeout(() => {
          //console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
    }
}