import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController, NavParams } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";


@Component({
    selector: 'boleto-page',
    templateUrl: 'boleto.html'
})

export class boleto extends PageBase implements OnInit{

    listObjBoleto : any;
    strCpfQueryString : string;
    folder : any;
    wsboleto:any;

    id: number;
    beneficiario: string;
    beneficiarioCnpj: string;
    vencimento: string;
    pagador: string;
    dataDoc: string;
    valorDoc:string;
    urlBoleto: string;
    mensagem: string;

    constructor(public storage: Storage,
                public navCtrl : NavController,
                public navParams: NavParams,
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController){
        super(navCtrl, storage);

        //this.strCpfQueryString = this.navParams.get("cpfStr");
        this.listObjBoleto = this.navParams.data;

        // this.beneficiario = this.objBoleto.beneficiario;
        // this.beneficiarioCnpj = this.objBoleto.beneficiarioCnpj;
        // this.vencimento = this.objBoleto.vencimento;
        // this.pagador = this.objBoleto.pagador;
        // this.dataDoc = this.objBoleto.dataDoc;
        // this.valorDoc = this.objBoleto.valorDoc;
        // this.urlBoleto = this.objBoleto.urlBoleto;
    }

    ionViewDidLoad() { }

    ngOnInit(){

    }

    exibirBoleto(url:string){
        this.suporte.webOptions.location = "yes";
        this.suporte.openUrlSite(url);
    }

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
        setTimeout(() => {
          //console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
    }
}