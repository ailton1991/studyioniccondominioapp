import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { Suporte } from '../../app/app.suporte';
import { ServiceUsuario } from '../../service/ServiceUsuario';

import { PageBase } from "../base/pageBase";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: "modalEditConvidado-page",
    templateUrl: "modalEditConvidado.html"
})

export class modalEditConvidado extends PageBase implements OnInit {

    form: FormGroup; 
    pathPreviewImage : string;
    nomeAreaDeAcesso : any;
    objConvidado : any;
    fotoConvidado : any;
            
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public fb : FormBuilder,
                public camera : Camera,
                public fileUp : Filetransferencia,
                public servUsuario : ServiceUsuario,
                public viewCtrl : ViewController)
    {
        super(navCtrl, _storage);
        this.pathPreviewImage = this.urlImageServ + "usuario/";
        this.objConvidado = this.navParams.data;
        this.fotoConvidado = this.objConvidado.foto;
        //console.log(this.objConvidado);

        this.form = this.fb.group({
            id : [this.objConvidado.id],
            nome : [this.objConvidado.nome,Validators.required],
            sobrenome : [this.objConvidado.sobrenome, Validators.required],
            email: [this.objConvidado.email],
            foto:[this.objConvidado.foto]
        });
    }
    
    ngOnInit(){  
        
    }

    EditConvidado(){
       this.storage.get("UsuarioLogado").then((dataUser) =>{
           if (dataUser != null){
                // this.form.addControl("idUsuarioResponsavel",new FormControl(dataUser.userID));
                // this.form.addControl("idApartamento",new FormControl(dataUser.aptId));
                
                let userSave = this.form.value;
                //console.log(userSave);
                this.sp.showLoader("Editando convidado...");
                let result : any;
                this.servUsuario.editConvidadoCarteira(userSave)
                        .subscribe(
                            r => result = r,
                            error => console.log("Error ao editar convidado"),
                            () => {
                                if (result.status == 0){
                                    this.sp.showAviso(result.mensagem);
                                    this.form.reset();
                                    this.navCtrl.pop();
                                }
        
                                if (result.status == 1){
                                    this.sp.showAviso(result.mensagem);
                                }
        
                                if (result.status == -1){
                                    this.sp.showAviso(this.errorMensage);
                                }
        
                                this.sp.dismissLoader();
                            }
                        )
           }
       })
    }

    takePic(tpFonte) {
        
        if (tpFonte == 'galeria'){

                const options: CameraOptions = {
                    quality: 50,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    correctOrientation: true,
                    saveToPhotoAlbum: false
                }

                this.camera.getPicture(options).then((imageURI) => {
                    // imageData is either a base64 encoded string or a file URI
                    // If it's base64:           
                    this.pathPreviewImage = imageURI;
                    
                    let guid = new Date().getTime();
                    let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('?') + 1);
                   
                    nomeImage = guid + nomeImage + ".jpg";
                    
                   this.form.setControl("foto",new FormControl(nomeImage))
                   this.fileUp.upload(nomeImage,imageURI,'Cliente');
        
                }, (err) => {
                    // Handle error
                    console.log('takePic: ' + err);
                });
            }
            else
            {
                const options: CameraOptions = {
                    quality: 50,
                    destinationType: this.camera.DestinationType.FILE_URI,
                    sourceType: this.camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    correctOrientation: true,
                    encodingType: this.camera.EncodingType.JPEG,
                    saveToPhotoAlbum: false
                }

                this.camera.getPicture(options).then((imageURI) => {
                    // imageData is either a base64 encoded string or a file URI
                    // If it's base64:           
                    this.pathPreviewImage = imageURI;
                    
                    let guid = new Date().getTime();
                    let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        
                    nomeImage = guid + nomeImage;
                    
                   // this.form.addControl("foto",new FormControl(nomeImage));
                   this.form.setControl("foto",new FormControl(nomeImage) )
                   this.fileUp.upload(nomeImage,imageURI,'Cliente');
        
                }, (err) => {
                    // Handle error
                    console.log('takePic: ' + err);
                });
            }           
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}