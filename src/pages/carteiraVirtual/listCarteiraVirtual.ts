import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController, ModalController} from 'ionic-angular';

import { ServiceCarteiraDigital } from "../../service/ServiceCarteiraDigital";

import { PageBase } from "../base/pageBase";
import { Suporte } from "../../app/app.suporte";
import { modalAddConvidado } from "./modalAddConvidado";
import { modalEditConvidado } from "./modalEditConvidado";

@Component({
    selector: 'listCarteiraVirtual-page',
    templateUrl: 'listCarteiraVirtual.html'
})

export class listCarteiraVirtual extends PageBase implements OnInit{
    
    nomeAreaDeAcesso : any;
    listDeCarteiras : any;
    listDeConvidados: any;
    permitirConvidado : any;
    areaAcessoID : any;
    qtdMaxConvidado : any;
    qtdConvidadosCadastrados : any;

    constructor(public navCtrl: NavController, 
                public _storage : Storage, 
                public params : NavParams, 
                public viewCtrl: ViewController, 
                public modalCtrl: ModalController,
                public servCarteira : ServiceCarteiraDigital,
                public suporte : Suporte) {
        super(navCtrl, _storage);
         
        this.urlImageServ += "usuario/";

        this.listDeCarteiras = [];
        this.listDeConvidados = [];
        
        let objParams = this.params.data;
        this.areaAcessoID = objParams.idAreaAcesso;
        this.nomeAreaDeAcesso = objParams.nomeAreaAcesso;
        this.listDeCarteiras = objParams.listaCarteiras;
        this.listDeConvidados = objParams.convidados;
        this.permitirConvidado = objParams.permitirConvidado;
        this.qtdMaxConvidado = objParams.qtdMaxConvidado;

        this.qtdConvidadosCadastrados = this.listDeConvidados.length;
    }

    ngOnInit(){
       
    }

    getCarteirasDaAreaDeAcesso(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            let carteirasDaUnidade : any;
            this.servCarteira.getCarteirasDaUnidade(dataUser.aptId)
                    .subscribe(
                        result => carteirasDaUnidade = result,
                        error => console.log("Error ao obter carteiras"),
                        () => {
                            let area = carteirasDaUnidade.filter((i) =>{
                                return i.idAreaAcesso == this.areaAcessoID
                            });

                            this.areaAcessoID = area[0].idAreaAcesso;
                            this.nomeAreaDeAcesso = area[0].nomeAreaAcesso;
                            this.permitirConvidado = area[0].permitirConvidado;
                            this.listDeCarteiras = area[0].listaCarteiras;
                            this.listDeConvidados = area[0].convidados;

                            this.storage.set("cateirasDigitais",carteirasDaUnidade);
                        }) 
        }); 
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

    openNovoConvidado(){
        let m = this.modalCtrl.create(modalAddConvidado,{"idAreaAcesso": this.areaAcessoID,
                                                         "nomeAreaDeAcesso" : this.nomeAreaDeAcesso,
                                                         "qtdConvidado" : this.qtdMaxConvidado,
                                                         "qtdConvidadosCadastrados" : this.qtdConvidadosCadastrados});
        m.present();
    }

    openEditConvidado(convidadoObj : any){
        let m = this.modalCtrl.create(modalEditConvidado,convidadoObj);
        m.present();
    }

    getAtivaDesativa(ct : boolean, idConvidado : number){
        //console.log(event.checked);
        this.storage.get("UsuarioLogado").then((dataUser) => {
            let retornoItem : any;
            this.servCarteira.getAtivaDesativaConvidado(idConvidado,this.areaAcessoID,ct)
                    .subscribe(
                        result => retornoItem = result,
                        error => console.log("Error ao setar ativo para convidado"),
                        () => {
                            if (retornoItem.status == 0){
                                this.suporte.showAviso(retornoItem.mensagem);
                            }
                            if (retornoItem.status == 1){
                                this.suporte.showAviso(retornoItem.mensagem);
                            }
                            if (retornoItem.status == -1){
                                this.suporte.showAviso(this.errorMensage);
                            }
                        }) 
        }); 
    }
    dismiss(){
        this.viewCtrl.dismiss();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getCarteirasDaAreaDeAcesso();
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}