import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ModalController  } from 'ionic-angular';

import{ listCarteiraVirtual } from "./listCarteiraVirtual";

import { PageBase } from "../base/pageBase";

@Component({
    selector: 'tabAreasDeAcesso-page',
    templateUrl: 'tabAreasDeAcesso.html'
})

export class tabAreasDeAcesso extends PageBase implements OnInit{
    
    objAreasDeAcessoCarteiras : any;

    constructor(public navCtrl: NavController, public _storage : Storage, public params : NavParams, public modalCrtl: ModalController) {
        super(navCtrl, _storage);
         
        this.objAreasDeAcessoCarteiras = [];

        this.storage.get("cateirasDigitais").then((dataAcesso) =>{
            if (dataAcesso != null){
                this.objAreasDeAcessoCarteiras = dataAcesso;
            }
        })
    }

    ngOnInit(){
       
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

    getCarteirasDaArea(objArea : any){
        let m = this.modalCrtl.create(listCarteiraVirtual,objArea);
        m.present();
    }
}