import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController  } from 'ionic-angular';

import { PageBase } from "../base/pageBase";

import { tabAcesso } from "./tabAcesso";
import { tabAreasDeAcesso } from "./tabAreasDeAcesso";

@Component({
    templateUrl: 'carteiraVirtual.html'
})

export class carteiraVirtual extends PageBase{
    
    acessoTab: any;
    carteirasTab: any;
    
    constructor(public navCtrl: NavController, public _storage : Storage) {
        super(navCtrl, _storage);
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
       // this.listarRecentes();
        this.acessoTab = tabAcesso;
        this.carteirasTab = tabAreasDeAcesso;
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }
}