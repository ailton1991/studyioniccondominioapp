import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ViewController  } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { ServiceCarteiraDigital } from "../../service/ServiceCarteiraDigital";

import { PageBase } from "../base/pageBase";
import { Suporte } from "../../app/app.suporte";

@Component({
    selector: 'qr_carteiraVirtual-page',
    templateUrl: 'qr_carteiraVirtual.html'
})

export class qr_carteiraVirtual extends PageBase implements OnInit{
    
    itemCodeCondominio : any;
    listDeCarteiras = [];

    strNomeDaAreaDeAcesso: string;
    strNomeCondominio: string

    strFotoCarteira : string;
    strNome: string;
    strUnidade: string;
    strBloco: string;
    strAndar : string;
    strValidade : string;

    validado: boolean;
    isValidade: boolean;

    objAreaDeAcesso : any;
    pathImageView : any;

    constructor(public navCtrl: NavController, 
                public _storage : Storage, 
                public params : NavParams, 
                public viewCtrl: ViewController, 
                public suporte: Suporte,
                public servCkecIn: ServiceCarteiraDigital,
                public network : Network) {
        super(navCtrl, _storage);
         
        this.pathImageView = this.urlImageServ + "usuario/";
        this.itemCodeCondominio = this.params.data;
        //console.log(this.itemCodeCondominio);

        this.strNomeDaAreaDeAcesso = this.itemCodeCondominio.nomeDaArea;
        this.strNomeCondominio = this.itemCodeCondominio.nomeCondominio;
        this.listDeCarteiras = this.itemCodeCondominio.carteirasList;

        this.storage.get("connectionStatus").then((conn) =>{
            if (conn == "Off"){
                this.listDeCarteiras.forEach(element => {
                    element.foto = null
                });
            }
        })
        
        let objCarteira = this.itemCodeCondominio.minhaCarteira;
        //this.strDisplayMsg = "Área de acesso " + this.itemCodeCondominio.nomeAreaAcesso + " do condomínio " + this.itemCodeCondominio.nomeCondominio;

        this.loadCateiraPrincipal(objCarteira);
    }

    ngOnInit(){
       
        // this.listDeCarteiras = this.listDeCarteirasServer.filter((item) =>{
        //     return item.idArea == this.itemCodeCondominio.idAreaDeAcesso
        // })

        // this.storage.get('UsuarioLogado').then((dataUser) => {
        //     let minhaCarteira = this.listDeCarteiras.filter((i) =>{
        //         return i.idUser == dataUser.userID && i.idArea == this.itemCodeCondominio.idAreaDeAcesso;
        //     });

        //     this.loadCateiraPrincipal(minhaCarteira[0]);     
        // });
    }

 
    changeCateira(carteirinha: any){
        //console.log(carteirinha);
        this.loadCateiraPrincipal(carteirinha);
    }

   private verificaValidade(strData: string) : boolean{
        let arrData = strData.split('/');
        let dataDaValidade = new Date(arrData[2] + '-' + arrData[1] + '-' + arrData[0]);
        let hoje = new Date();
        if (dataDaValidade < hoje)
            return false;
        else
            return true;
    }

    private loadCateiraPrincipal(carteira: any){

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            //console.log(carteira);
            this.storage.get("connectionStatus").then((conn) =>{

                    
                    let arrItens = dataUser.strApartamento.split("|");
                    if (conn == "On")
                        this.strFotoCarteira = carteira.foto;
                    else
                        this.strFotoCarteira = null;

                    this.strNome = carteira.nome;
                    this.strBloco = arrItens[2];
                    this.strUnidade = arrItens[1];
                    this.strAndar = arrItens[3];
                
                    this.strValidade = carteira.dataDeValidade;

                    this.isValidade = this.verificaValidade(carteira.dataDeValidade);
            
                    if (carteira.validado == true && this.isValidade == true)
                        this.validado = true;
                    else
                        this.validado = false;
            })
        })
    }

    ionViewDidLoad() {
        // this.storage.get("UsuarioLogado").then((uData) =>{
        //     this.storage.get("cateirasDigitais").then((carteiras) =>{
        //         if (carteiras != null){
        //             //console.log(carteiras);
        //             carteiras.filter((item) => {
        //                 if (item.idAreaAcesso == this.itemCodeCondominio.idAreaAcesso){
        //                     // console.log("Achei a carteira!");
        //                     // console.log(item);
        //                     this.listDeCarteiras = item.listaCarteiras;
                            
        //                     let controle : boolean = false;
        //                     this.listDeCarteiras.filter((c) =>{
        //                         console.log(c);
        //                         if(c.idUsuario == uData.userID){
        //                             controle = true;
        //                            // console.log("achei o usuário");
                                   
        //                            //Verifica se tem internet no momento do CheckIn;
        //                            let check : any;
        //                            this.servCkecIn.getEfetuaCheckIn(c.idUsuario,item.idAreaAcesso,c.idCarteira)
        //                                           .subscribe(result => check = result,
        //                                                     error => console.log("Error de CheckIn"),
        //                                                 () => {
        //                                                     if (check.status == 0){
        //                                                         this.loadCateiraPrincipal(c);
        //                                                         this.suporte.showAviso(check.mensagem);
        //                                                     }

        //                                                     if(check.status == -1){
        //                                                         this.suporte.showAviso(this.errorMensage);
        //                                                     }
        //                                                 })

                                   
        //                         }
        //                     })  

        //                     if(!controle){
        //                         this.suporte.showAlert("Carteirinha Virtual","Não ha uma carteirinha disponível para você, notifique a administração.");
        //                         this.navCtrl.pop();
        //                     }
        //                 } 
        //             })                  
        //         }
        //     })
        // })
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}