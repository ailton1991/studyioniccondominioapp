import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { Suporte } from "../../app/app.suporte";

import { PageBase } from "../base/pageBase";
import { qr_carteiraVirtual } from "../carteiraVirtual/qr_carteiraVirtual";
import { ServiceCarteiraDigital } from "../../service/ServiceCarteiraDigital";

@Component({
    selector: 'tabAcesso-page',
    templateUrl: 'tabAcesso.html'
})

export class tabAcesso extends PageBase implements OnInit{
     
    listDeCarteiras : any;

    constructor(public navCtrl: NavController, 
                public _storage : Storage, 
                public params : NavParams,
                public modal: ModalController,
                public barcodeScanner : BarcodeScanner,
                public servCkecIn: ServiceCarteiraDigital,
                public suporte : Suporte) {
        super(navCtrl, _storage);
        
        this.listDeCarteiras = [];
    }

    ngOnInit(){
       
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

    lerCodigoDeBarra(){
        //  let opt = {
        //     nomeAreaAcesso : 'Ônibus',
        //     nomeCondominio : 'Palmeiras',
        //     idAreaDeAcesso : 2,
        //     idCondominio : 1
        // }

        //this.navCtrl.push(qr_carteiraVirtual,opt);
        // let m = this.modal.create(qr_carteiraVirtual, opt);
        // m.present();
        
        let options = {
            resultDisplayDuration : 0
        };

        this.barcodeScanner.scan(options).then((barcodeData) => {
            // Success! Barcode data is here
           // alert(JSON.stringify(barcodeData));
            
            if (!barcodeData.cancelled && barcodeData.format == "QR_CODE"){
                let objItemCode = JSON.parse(barcodeData.text);
                
                this.storage.get("UsuarioLogado").then((uData) =>{
                    this.storage.get("cateirasDigitais").then((carteiras) =>{
                        if (carteiras != null){
                            //console.log(carteiras);
                            carteiras.filter((item) => {
                                if (item.idAreaAcesso == objItemCode.idAreaAcesso){
                                    // console.log("Achei a carteira!");
                                    // console.log(item);
                                    if (item.token != objItemCode.token){
                                        this.suporte.showAviso("QRCode inválido.");
                                        return;
                                    }

                                    this.listDeCarteiras = item.listaCarteiras;
                                    
                                    let controle : boolean = false;
                                    this.listDeCarteiras.filter((c) =>{
                                        console.log(c);
                                        if(c.idUsuario == uData.userID){
                                            controle = true;
                                           // console.log("achei o usuário");
                                           
                                           //Verifica se tem internet no momento do CheckIn;
                                           this.storage.get("connectionStatus").then((conn) =>{
                                                let objCheckIn = {
                                                    userID : c.idUsuario,
                                                    areaID : item.idAreaAcesso,
                                                    carteiraID : c.idCarteira,
                                                    nomeDaArea : objItemCode.nomeAreaAcesso,
                                                    nomeCondominio: objItemCode.nomeCondominio,
                                                    carteirasList: this.listDeCarteiras,
                                                    minhaCarteira : c,
                                                    dtEntrada : this.suporte.formatDateTime()
                                                }

                                                let objCheckInOff = {
                                                    userID : c.idUsuario,
                                                    areaID : item.idAreaAcesso,
                                                    carteiraID : c.idCarteira,
                                                    nomeDaArea : objItemCode.nomeAreaAcesso,
                                                    nomeCondominio: objItemCode.nomeCondominio,
                                                    dtEntrada : this.suporte.formatDateTime()
                                                }
                                                
                                                //console.log(objCheckIn);

                                                if(conn == "On"){
                                                    let check : any;
                                           
                                                    this.suporte.showLoader("Efetuando Check-In...");
                                                    this.servCkecIn.getEfetuaCheckIn(c.idUsuario,objItemCode.idAreaAcesso,c.idCarteira,this.suporte.formatDateTime())
                                                                   .subscribe(result => check = result,
                                                                             error => console.log("Error de CheckIn"),
                                                                         () => {
                                                                             this.suporte.dismissLoader();
                                                                             if (check.status == 0){
                                                                                this.suporte.showAviso(check.mensagem);
         
                                                                                setTimeout(() =>{
                                                                                    let m = this.modal.create(qr_carteiraVirtual, objCheckIn);
                                                                                    m.present();
                                                                                },1500);
                                                                             }
                 
                                                                             if(check.status == -1){
                                                                                 this.suporte.showAviso(this.errorMensage);
                                                                             }
                                                                         })
                                                }
                                                else if (conn == "Off"){
                                                    this.storage.get("checkInOff").then((checkins) =>{
                                                        //console.log(checkins);
                                                        
                                                        checkins.push(objCheckInOff);  
                                                        this.storage.set("checkInOff",checkins);

                                                        setTimeout(() =>{
                                                            let m = this.modal.create(qr_carteiraVirtual, objCheckIn);
                                                            m.present();
                                                        },1500);
                                                    })
                                                }
                                           }) 
                                        }
                                    })  
        
                                    if(!controle){
                                        this.suporte.showAlert("Carteirinha Virtual","Não ha uma carteirinha disponível para você, notifique a administração.");
                                        this.navCtrl.pop();
                                    }
                                } 
                            })                  
                        }
                    })
                })
            }
            
            }, (err) => {
                // An error occurred
                console.log('Error: ' + err);
            });
    }
}