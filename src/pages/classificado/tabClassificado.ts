import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { ServiceClassificado } from '../../service/ServiceClassificado';
import { ServiceChat } from "../../service/ServiceChat";
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { mensagens } from "../chat/mensagens";
import { modalNovoClassificado } from "./modalNovoClassificado";

@Component({
    selector: 'tabClassificado-page',
    templateUrl: 'tabClassificado.html'
})

export class tabClassificado extends PageBase implements OnInit{
    
    listaDeClassificados : any;
    idUsuarioLogado : number;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servClassificado : ServiceClassificado,
                public servChat : ServiceChat,
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController){
        super(navCtrl, storage);

        this.listaDeClassificados = [];

        // this.storage.get("objCondominio").then((dataCondominio) => {
        //     this.permitePost = dataCondominio.mural;
        // })

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.idUsuarioLogado = dataUser.userID;
        })
    }

    iniciarConversa(convidadoId : number){
        let retorno : any;
        this.storage.get("UsuarioLogado").then((dataUser) => {
              if (dataUser != null){
                  this.suporte.showLoader("Carregando chat...");
                  this.servChat.iniciarConversa(dataUser.userID,convidadoId)
                          .subscribe(
                              result => retorno = result,
                              error => console.log("Error ao iniciar a conversa"),
                              () => {
                                  let conversa : any;
                                  this.servChat.findConversa(retorno.status,dataUser.userID)
                                          .subscribe(
                                              c => conversa = c,
                                              error => console.log("Error ao obter conversa"),
                                              () => {

                                                 // console.log(conversa);
                                                  let m = this.modal.create(mensagens,conversa);
                                                  m.present();
                                              }
                                          )

                                    this.suporte.dismissLoader();
                              }
                          )
              }
          })
    }

    openModalNovoClassificado(){
        let modal = this.modal.create(modalNovoClassificado);
        modal.present();
    }
    // permitirDeletar(userIDpost : number) : boolean{
    //     if(this.userID == userIDpost)
    //         return true;
      
    //     return false;
    // }

    // excluirPost(idPost : number){
    //    this.confirmRemoverPost("Excluir postagem","Tem certeza que deseja excluir este post?",idPost);  
    // }
  
    // confirmRemoverPost(titulo : string, msg : string,postID : number) {
    //       let confirm = this.alertCtrl.create({
    //         title: titulo,
    //         message: msg,
    //         buttons: [
    //           {
    //             text: 'Não',
    //             handler: () => {
    //               //console.log("Negativo");
    //             }
    //           },
    //           {
    //             text: 'Sim',
    //             handler: () => {
    //               let retorno : any;
    //               this.servMural.removerPost(postID)
    //                   .subscribe(result => retorno = result,
    //                   error => console.log("Error ao remover Post!"),
    //               () =>{
    //                   console.log(retorno);
    //                   if(retorno.status == 0){
    //                       this.suporte.showAviso(retorno.mensagem);
    //                       this.listarPosts();
    //                   }
    //               })
    //             }
    //           }
    //         ]
    //       });
    //       confirm.present();
    // }

    ionViewDidLoad() {
       // this.content.scrollToBottom(300);
     }

    ngOnInit(){
        this.getClassificados();
    }

    getClassificados(){
        this.suporte.showLoader("Carregando classificados...");

        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servClassificado.listarClassificados(dataUser.condominioId)
                .subscribe(
                    classifi => this.listaDeClassificados = classifi,
                    error => console.log("Error ao obter a lista de recados"),
                    () => this.suporte.dismissLoader()
                )
           });
    }

    // openModalNovoPost(){
    //     let m = this.modal.create(modalNovoPost);
    //     m.present();
    // }

   

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
       this.getClassificados();
    
        setTimeout(() => {
         // console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }

      doInfinite(infiniteScroll) {
       // console.log('Begin async operation');

        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.servMural.getMaisPosts(dataUser.condominioId,this.ultimoID)
        //             .subscribe(maisPosts => this.listMorePost = maisPosts,
        //             error => console.log("Error ao puxar mais Posts"),
        //             () =>{
        //                 for (let i = 0; i < this.listMorePost.length -1; i++) {
        //                     //console.log(this.listMorePost[i]);
        //                   this.listaDePosts.push(this.listMorePost[i]);
        //                   this.ultimoID = this.listMorePost[i].id;
        //                 }
        //             });
        // })
        setTimeout(() => {
          infiniteScroll.complete();
        }, 2500);
    }
}