import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { ServiceClassificado } from '../../service/ServiceClassificado';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { modalNovoClassificado } from "./modalNovoClassificado";
import { modalEditClassificado } from "./modalEditClassificado";

@Component({
    selector: 'tabMeusAnuncios-page',
    templateUrl: 'tabMeusAnuncios.html'
})

export class tabMeusAnuncios extends PageBase implements OnInit{
    
    listaDeAnuncios : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servClassificado : ServiceClassificado,
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController){
        super(navCtrl, storage);

        this.listaDeAnuncios = [];

        // this.storage.get("objCondominio").then((dataCondominio) => {
        //     this.permitePost = dataCondominio.mural;
        // })

        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.userID = dataUser.userID;
        // })
    }

    openModalNovoClassificado(){
        let modal = this.modal.create(modalNovoClassificado);
        modal.present();
    }

    editClassificado(obj : any){
       let modalEdit = this.modal.create(modalEditClassificado,obj);
       modalEdit.present();
    }


    excluirAnuncio(idAnuncio : number){
       this.confirmRemoverAnuncio("Excluir Anúncio","Tem certeza que deseja excluir este anúncio?",idAnuncio);  
    }
  
    confirmRemoverAnuncio(titulo : string, msg : string,anuncioID : number) {
          let confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
              {
                text: 'Não',
                handler: () => {
                  //console.log("Negativo");
                }
              },
              {
                text: 'Sim',
                 handler: () => {
                  let retorno : any;
                  this.servClassificado.removerClassificado(anuncioID)
                      .subscribe(result => retorno = result,
                      error => console.log("Error ao remover Classificado!"),
                      () =>{
                          //console.log(retorno);
                          if(retorno.status == 0){
                              this.suporte.showAviso(retorno.mensagem);
                              this.getMeusAnuncios();
                          }
                      })
                  }
              }
            ]
          });
          confirm.present();
    }

    ionViewDidLoad() {
       // this.content.scrollToBottom(300);
     }

    ngOnInit(){
       this.getMeusAnuncios();
    }

    getMeusAnuncios(){
        this.suporte.showLoader("Carregando Anúncios...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servClassificado.listarMeusAnuncios(dataUser.userID)
                .subscribe(
                    classifi => this.listaDeAnuncios = classifi,
                    error => console.log("Error ao obter a lista de recados"),
                    () => this.suporte.dismissLoader()
                )
           });
    }



    // openModalNovoPost(){
    //     let m = this.modal.create(modalNovoPost);
    //     m.present();
    // }

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
       this.getMeusAnuncios();
    
        setTimeout(() => {
          //console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }

      doInfinite(infiniteScroll) {
       // console.log('Begin async operation');

        // this.storage.get("UsuarioLogado").then((dataUser) =>{
        //     this.servMural.getMaisPosts(dataUser.condominioId,this.ultimoID)
        //             .subscribe(maisPosts => this.listMorePost = maisPosts,
        //             error => console.log("Error ao puxar mais Posts"),
        //             () =>{
        //                 for (let i = 0; i < this.listMorePost.length -1; i++) {
        //                     //console.log(this.listMorePost[i]);
        //                   this.listaDePosts.push(this.listMorePost[i]);
        //                   this.ultimoID = this.listMorePost[i].id;
        //                 }
        //             });
        // })

        setTimeout(() => {
          infiniteScroll.complete();
        }, 2500);
    }
}