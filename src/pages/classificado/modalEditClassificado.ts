import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { ServiceClassificado } from '../../service/ServiceClassificado';
import { Suporte } from '../../app/app.suporte';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'modalEditClassificado-page',
    templateUrl: 'modalEditClassificado.html'
})

export class modalEditClassificado extends PageBase {

    form: FormGroup;
    pathPreviewImage : string;
    oClassificado : any;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public servClassificado: ServiceClassificado,
        public sp: Suporte,
        public fb: FormBuilder,
        public camera : Camera,
        public fileUp : Filetransferencia
    ) {

        super(navCtrl, storage);
        this.oClassificado = this.params.data;

        this.form = fb.group({
            id:[this.oClassificado.id],
            str2Preco: [this.oClassificado.str2Preco,Validators.pattern("^(([0-9]*)|(([0-9]*)\,([0-9]*)))$")],
            chamada: [this.oClassificado.chamada],
            descricao: [this.oClassificado.descricao,Validators.required],
            foto :[this.oClassificado.foto]
        });
    }

    takePic(tpFonte) {

        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        }

        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;    
        

        this.camera.getPicture(options).then((imageURI) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;

            let guid = new Date().getTime();
            let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);

            nomeImage = guid + nomeImage;
            this.form.setControl("foto",new FormControl(nomeImage))
            this.fileUp.upload(nomeImage,imageURI,'Classificado');
            this.sp.showAviso(this.fotoAnexada);

        }, (err) => {
            // Handle error
            console.log('takePic: ' + err);
        });
    }

    editAnuncio() {

        let result : any;

        this.storage.get("UsuarioLogado").then((data) =>{
            this.form.addControl("usuarioId",new FormControl(data.userID));
            this.form.addControl("condominioId",new FormControl(data.condominioId));
                    
            if (!this.form.contains("foto"))
                this.form.addControl("foto",new FormControl());

            var oAnuncio = this.form.value;
            //console.log(oAnuncio);

            this.servClassificado.editarClassificado(oAnuncio)
                                 .subscribe(
                                     r => result = r,
                                     error => console.log("Error ao Editar"),
                                     () =>{
                                         if (result.status == 0){
                                             this.sp.showAviso(result.mensagem);
                                             this.dismiss();
                                         }
                                     }
                                 )

        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}