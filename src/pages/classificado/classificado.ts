import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController  } from 'ionic-angular';

import { PageBase } from "../base/pageBase";

import { tabClassificado } from "./tabClassificado";
import { tabMeusAnuncios } from "./tabMeusAnuncios";

@Component({
    selector: 'classificado-page',
    templateUrl: 'classificado.html'
})

export class classificado extends PageBase{
    
    classificadoTab: any;
    meusAnunciosTab: any;
    
    constructor(public navCtrl: NavController, public _storage : Storage) {
        super(navCtrl, _storage);
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
       // this.listarRecentes();
        this.classificadoTab = tabClassificado;
        this.meusAnunciosTab = tabMeusAnuncios;
       
    }
    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }
}