﻿import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceComunicado } from '../../service/ServiceComunicado';

import { PageBase } from "../base/pageBase";
import { modalComunicadoContent } from '../comunicadoOcorrencia/modalComunicadoContent';
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'tabComunicados.html'
})
export class tabComunicados extends PageBase implements OnInit{
    UsuarioLogado: any;
    arrComunicados: any;
    items : any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public _serviceComunicado: ServiceComunicado,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController)
    {
        super(navCtrl, _storage);
        
    }

    ngOnInit(){
        this.listarRecentes();
    }

    openModal(comunicadoId: number) {
        let modal = this.modalCtrl.create(modalComunicadoContent, { idDoComunicado: comunicadoId});
        modal.present();
    }
        
    ionViewDidLoad() {
        //console.log('ionViewDidLoad tabComunicados');
    }

    listarRecentes() {
        this.arrComunicados = [];
        this.sp.showLoader("Carregando últimos comunicados...");
        this._storage.get('UsuarioLogado').then((data) => {
            if (data != null) {
                this._serviceComunicado.ListarUltimos(data.aptId, data.condominioId)
                    .subscribe(
                    comunicados => this.arrComunicados = comunicados,
                    error => console.log("erro interno"),
                    () => {
                        this.initializeItems();
                        this.sp.dismissLoader()
                    });
            }
        });
    }

    getItems(ev: any) {
        // Reset items back to all of the items
       
         this.initializeItems();
     
         // set val to the value of the searchbar
         let val = ev.target.value;
        // console.log(val);
         // if the value is an empty string don't filter the items
         if (val && val.trim() != '') {
           this.items = this.arrComunicados.filter((item) => {
             return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
           })
         }
       }

     initializeItems(){

        this._storage.get('UsuarioLogado').then((data) => {
            this.UsuarioLogado = data;

            this.items = [];

           // this.items = this.arrComunicados;

            for (var i = 0; i < this.arrComunicados.length; i++){
                if (this.arrComunicados[i].isProprietario){
                    if (this.UsuarioLogado.userID == this.UsuarioLogado.proprietarioId){
                        this.items.push(this.arrComunicados[i]);
                    }
                }
                else
                {
                  this.items.push(this.arrComunicados[i]);
                }
                
            }
        })
        //console.log(this.items);
     }
    
}