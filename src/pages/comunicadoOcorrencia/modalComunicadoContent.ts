﻿import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ServiceComunicado } from '../../service/ServiceComunicado';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl:'modalComunicadoContent.html' 
})

export class modalComunicadoContent extends PageBase{
    selectComunicadoID: number;
    comunicadoSelectObj: any;

    titulo: string;
    tp: string;
    descricao: string;
    strDataDeRealizacao: string;
    nomeUsuario: string;
    publico : boolean;
    arrArquivos:any;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage : Storage,
        public comunicadoService: ServiceComunicado,
        public sp : Suporte
    ) {

        super(navCtrl, storage);
        this.arrArquivos = [];

        this.selectComunicadoID = this.params.get('idDoComunicado');

        this.comunicadoService.getSelectComunicado(this.selectComunicadoID)
            .subscribe(
                comunicado => this.comunicadoSelectObj = comunicado,
                error => console.log('Erro no get Comunicado: '),
                () => {
                    this.titulo = this.comunicadoSelectObj.titulo;
                    this.tp = this.comunicadoSelectObj.tp;
                    this.descricao = this.comunicadoSelectObj.descricao;
                    this.strDataDeRealizacao = this.comunicadoSelectObj.strDataDeRealizacao;
                    this.nomeUsuario = this.comunicadoSelectObj.nomeUsuario;
                    this.publico = this.comunicadoSelectObj.publico;

                    for (var i = 0; i < this.comunicadoSelectObj.listaDeArquivo.length; i++) {
                        this.arrArquivos.push(this.urlArquivosUpload + 'comunicado/' + this.comunicadoSelectObj.listaDeArquivo[i].condominioId + "/" + this.comunicadoSelectObj.tp + "/" + this.comunicadoSelectObj.listaDeArquivo[i].nome);
                    }
                    //console.log(JSON.stringify(this.arrArquivos));
                }
            );
    }

    viewArquivo(strPath) {
        //alert(strPath);
        this.sp.openFileDocs(strPath);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}