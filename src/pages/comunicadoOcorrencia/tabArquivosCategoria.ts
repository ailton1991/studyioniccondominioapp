import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceComunicado } from '../../service/ServiceComunicado';

import { PageBase } from "../base/pageBase";
import { tabArquivosComunicados } from "./tabArquivosComunicados";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'tabArquivosCategoria.html'
})
export class tabArquivosCategoria extends PageBase implements OnInit {
    UsuarioLogado: any;
    arrFolders: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public _serviceComunicado: ServiceComunicado,
        public sp: Suporte,
        public _storage: Storage,
        public modalCtrl: ModalController) {
        super(navCtrl, _storage);
        this.arrFolders = [];
    }

    // openModal(comunicadoId: number) {
    //     let modal = this.modalCtrl.create(modalComunicadoContent, { idDoComunicado: comunicadoId });
    //     modal.present();
    // }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad tabComunicados');
    }

    ngOnInit() {
        this.listarDocuments();
    }

    listarDocuments() {
        this.sp.showLoader("Carregando pastas...");
        this._storage.get('UsuarioLogado').then((data) => {
            if (data != null) {
                this._serviceComunicado.getListCatComunicado(data.condominioId)
                    .subscribe(
                        comunicados => this.arrFolders = comunicados,
                        error => console.log("erro interno"),
                        () => this.sp.dismissLoader());      
            }
        });
    }

    getArquivos(cat){
        let m = this.modalCtrl.create(tabArquivosComunicados, {strCategoria : cat});
        m.present();
    }
}