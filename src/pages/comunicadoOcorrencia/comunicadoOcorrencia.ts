﻿import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


import { PageBase } from "../base/pageBase";
import { tabComunicados } from "./tabComunicados";
import { tabOcorrencias } from "./tabOcorrencias";
import { tabArquivosCategoria } from "./tabArquivosCategoria";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'comunicadoOcorrencia.html'
})
export class comunicadoOcorrencia extends PageBase {

    comunicadosTab: any;
    ocorrenciasTab: any;
    arquivosTab : any;


    constructor(public navCtrl: NavController, public navParams: NavParams, public _storage : Storage) {
        super(navCtrl, _storage);
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
       // this.listarRecentes();
        this.comunicadosTab = tabComunicados;
        this.ocorrenciasTab = tabOcorrencias;
        this.arquivosTab = tabArquivosCategoria;
    }
    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

}