﻿import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceComunicado } from '../../service/ServiceComunicado';

import { PageBase } from "../base/pageBase";
import { modalComunicadoContent } from '../comunicadoOcorrencia/modalComunicadoContent';
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'tabArquivosComunicados.html'
})
export class tabArquivosComunicados extends PageBase {
    UsuarioLogado: any;
    arrComunicados: any;
    strCategoria : string;
    items : any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl : ViewController,
        public _serviceComunicado: ServiceComunicado,
        public sp: Suporte,
        public _storage: Storage,
        public modalCtrl: ModalController) {
        super(navCtrl, _storage);

        this.items = [];
        this.strCategoria = this.navParams.get("strCategoria");
        //console.log(this.strCategoria);
    }

    openModal(comunicadoId: number) {
        let modal = this.modalCtrl.create(modalComunicadoContent, { idDoComunicado: comunicadoId });
        modal.present();
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad tabComunicados');
    }

    ngOnInit() {
        this.arrComunicados = [];
        this.listarDocuments();
    }

    listarDocuments() {
        this.sp.showLoader("Carregando últimos documentos...");
        this._storage.get('UsuarioLogado').then((data) => {
            if (data != null) 
            {
                let arrAux : any;
                this._serviceComunicado.getListComunicadoDocs(data.aptId)
                    .subscribe(
                    comunicados => arrAux = comunicados,
                    error => console.log("erro interno"),
                    () => { 
                        
                        if (this.strCategoria != "todas"){
                            this.arrComunicados = arrAux.filter(com => com.tp === this.strCategoria);
                        }
                        else
                        {
                            this.arrComunicados = arrAux;
                        }

                        this.initializeItems();
                        this.sp.dismissLoader();
                    });
            }
        });
    }

    getItems(ev: any) {
        // Reset items back to all of the items
       
         this.initializeItems();
     
         // set val to the value of the searchbar
         let val = ev.target.value;
         //console.log(val);
         // if the value is an empty string don't filter the items
         if (val && val.trim() != '') {
           this.items = this.arrComunicados.filter((item) => {
             return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
           })
         }
       }

     initializeItems(){
        this.items = [];
         //for (var i = 0; i < this.listaDeContatos.length -1; i++){
         //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
         //}
         this.items = this.arrComunicados;
 
        //console.log(this.items);
     }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}