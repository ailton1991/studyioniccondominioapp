﻿import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { ServiceOcorrencia } from '../../service/ServiceOcorrencia';
import { ServiceSignalr } from "../../service/ServiceSignalr";
import { Suporte } from '../../app/app.suporte';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'modalNovaOcorrencia.html'
})

export class modalNovaOcorrencia extends PageBase {

    form: FormGroup;
    pathPreviewImage : string;
    ocorrenciaPublica : boolean;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public ocorrenciaService: ServiceOcorrencia,
        public signalr : ServiceSignalr,
        public sp: Suporte,
        public fb: FormBuilder,
        public camera : Camera,
        public fileUp : Filetransferencia
    ) {

        super(navCtrl, storage);
                
        this.form = fb.group({
            descricao: ['', Validators.required],
            publica: [false]
        });

        this.storage.get("objCondominio").then((config) => {
            this.ocorrenciaPublica = config.flOcorrenciaAtivar;
        })
    }

    takePic(tpFonte) {

        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        }

        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;    
        

        this.camera.getPicture(options).then((imageURI) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
           // this.pathPreviewImage = imageURI;

            let guid = new Date().getTime();
            let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);

            nomeImage = guid + nomeImage;
            
            this.form.addControl("foto",new FormControl(nomeImage));
            this.fileUp.upload(nomeImage,imageURI,'ocorrencia');
            this.sp.showAviso(this.fotoAnexada);

        }, (err) => {
            // Handle error
            console.log('takePic: ' + err);
        });
    }

    novaOcorrencia() {

        let result : any;

        this.storage.get("UsuarioLogado").then((data) =>{
            this.form.addControl("clienteId",new FormControl(data.userID));
            this.form.addControl("condominioId",new FormControl(data.condominioId));
            this.form.addControl("idApartamentoOcorrencia",new FormControl(data.aptId));

            var ocorrencia = this.form.value;
            
            if (ocorrencia.publica)
                ocorrencia.publica = false;
            else
                ocorrencia.publica = true;
        
            if (!this.form.contains("foto"))
                this.form.addControl("foto",new FormControl());

            //console.log(ocorrencia);

            this.ocorrenciaService.addnovaOcorrencia(ocorrencia).subscribe(
                ocorr => result = ocorr,
                error => console.log('Error reportado: ' + <any>error),
                () => {
                    //console.log(result);  

                    if(result.status == 0){
                        this.sp.showAviso(result.mensagem);
                        this.ocorrenciaService.getOcorrencias(data.userID,data.condominioId);
                        this.form.reset();
                        this.signalr.connection.invoke("SendNovaOcorrencia",data.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        this.dismiss();
                    }
                    else
                    {
                        this.sp.showAviso(result.mensagem);
                    }
                    
                });
        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}