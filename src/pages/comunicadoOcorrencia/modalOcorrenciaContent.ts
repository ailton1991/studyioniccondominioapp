import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ServiceOcorrencia } from '../../service/ServiceOcorrencia';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl:'modalOcorrenciaContent.html' 
})

export class modalOcorrenciaContent extends PageBase implements OnInit{
    //selectOcorrenciaID: number;
    ocorrenciaSelectObj: any;

    titulo: string;
    descricao: string;
    foto : string;
    nomeResponsavel : string;
    publica : boolean;
    resolvida : boolean;
    parecer : string;
    strDataDeCadastro: string;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage : Storage,
        public ocorrenciaService: ServiceOcorrencia,
        public sp : Suporte
    ) {

        super(navCtrl, storage);
       
        //this.selectOcorrenciaID = this.params.get('idDaOcorrencia');
        this.ocorrenciaSelectObj = this.params.data;

        //console.log(this.ocorrenciaSelectObj);

        this.titulo = this.ocorrenciaSelectObj.titulo;
        this.descricao = this.ocorrenciaSelectObj.descricao;
        this.foto = this.urlArquivosUpload + "ocorrencias/" + this.ocorrenciaSelectObj.foto;
        this.nomeResponsavel = this.ocorrenciaSelectObj.nomeResp;
        this.publica = this.ocorrenciaSelectObj.publica;
        this.resolvida = this.ocorrenciaSelectObj.resolvida;
        this.parecer = this.ocorrenciaSelectObj.parecer;
        this.strDataDeCadastro = this.ocorrenciaSelectObj.strDataDeCadastro;
    }

    ngOnInit(){
        //this.getOcorrenciaID();
    }

    // getOcorrenciaID(){
    //     this.ocorrenciaService.getOcorrenciaID(this.selectOcorrenciaID)
    //     .subscribe(
    //         ocorrencia => this.ocorrenciaSelectObj = ocorrencia,
    //         error => console.log('Erro no get Ocorrencia: '),
    //         () => {
    //             this.titulo = this.ocorrenciaSelectObj.titulo;
    //             this.descricao = this.ocorrenciaSelectObj.descricao;
    //             this.foto = this.urlArquivosUpload + "ocorrencias/" + this.ocorrenciaSelectObj.foto;
    //             this.nomeResponsavel = this.ocorrenciaSelectObj.nomeResponsavel;
    //             this.publica = this.ocorrenciaSelectObj.publica;
    //             this.resolvida = this.ocorrenciaSelectObj.resolvida;
    //             this.parecer = this.ocorrenciaSelectObj.parecer;
    //             this.strDataDeCadastro = this.ocorrenciaSelectObj.strDataDeCadastro;
                
    //             //console.log(JSON.stringify(this.arrArquivos));
    //         }
    //     );
    // }


    dismiss() {
        this.viewCtrl.dismiss();
    }
}