﻿import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ServiceOcorrencia } from '../../service/ServiceOcorrencia';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { modalNovaOcorrencia } from './modalNovaOcorrencia';
import { modalOcorrenciaContent } from './modalOcorrenciaContent';
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-comunicadoOcorrencia',
    templateUrl: 'tabOcorrencias.html'
})
export class tabOcorrencias extends PageBase implements OnInit {
    UsuarioLogado: any;
    arrOcorrencias: any;
    ocorrenciaControle : boolean;
    items : any
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public _serviceOcorrencia: ServiceOcorrencia,
                public _storage: Storage,
                public sp: Suporte,
                public modalCtrl: ModalController) {

        super(navCtrl, _storage);
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
          
        this.arrOcorrencias = [];
    }

    ngOnInit(){
        this.listarOcorrencias();
        this.storage.get("objCondominio").then((condominio)=> { 
            this.ocorrenciaControle = condominio.flOcorrenciaPost;
           // console.log(condominio);
        })
    }

    initializeItems(){
        this.items = [];
         //for (var i = 0; i < this.listaDeContatos.length -1; i++){
         //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
         //}
         this.items = this.arrOcorrencias;
 
        //console.log(this.items);
     }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad tabOcorrencias');
    }

    listarOcorrencias() {
      
        this.sp.showLoader("Carregando últimas ocorrências...");
        this._storage.get('UsuarioLogado').then((data) => {
            if (data != null) {
                this._serviceOcorrencia.getOcorrencias(data.userID, data.condominioId)
                    .subscribe(
                    ocorrencias => this.arrOcorrencias = ocorrencias,
                    error => console.log("erro interno"),
                    () => {
                        this.initializeItems();
                        this.sp.dismissLoader()
                    });
            }
        });
    }

    getItems(ev: any) {
        // Reset items back to all of the items
       
         this.initializeItems();
     
         // set val to the value of the searchbar
         let val = ev.target.value;
       //  console.log(val);
         // if the value is an empty string don't filter the items
         if (val && val.trim() != '') {
           this.items = this.arrOcorrencias.filter((item) => {
             return (item.descricao.toLowerCase().indexOf(val.toLowerCase()) > -1);
           })
         }
    }

    openModal(objOcorrencia: any) {
        let modal = this.modalCtrl.create(modalOcorrenciaContent, objOcorrencia);
        modal.present();
    }

    openModalNovaOcorrencia() {
        let modal = this.modalCtrl.create(modalNovaOcorrencia);
        modal.present();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.listarOcorrencias();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}
