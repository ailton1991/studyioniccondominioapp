import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceChat } from "../../service/ServiceChat";
import { mensagens } from "./mensagens";
import { contatos } from "./contatos";


@Component({
    selector: 'conversas-page',
    templateUrl: 'conversas.html'
})

export class conversas extends PageBase implements OnInit{

    listaDeConversas : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servChat : ServiceChat){
        super(navCtrl, storage);

        this.listaDeConversas = [];
    }

    ngOnInit(){
       this.getConversas();
    }

    addNovaConversa(){
        let m = this.modal.create(contatos);
        m.present();
    }

    getConversas(){
        
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            if (dataUser != null){
                this.suporte.showLoader("Carregando conversas...");

                this.servChat.listarConversas(dataUser.userID)
                .subscribe(
                    conv => this.listaDeConversas = conv,
                    error => console.log("Error ao obter as conversas"),
                    () => this.suporte.dismissLoader()
                )
            }
        })
    }

    openConversa(objConversa : any){

        //console.log(JSON.stringify(objConversa));
        
        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (objConversa.block && objConversa.removeUsuarioId != dataUser.userID){
                this.suporte.showAviso("Não é possível iniciar essa conversa.");
            }
            else
            {
                let m = this.modal.create(mensagens, objConversa);
                m.present();
            }
        })
    }

    

    bloquearConversa(conversaID : number){
        this.confirmBloquearConversa("Bloquear usuário","Tem certeza que deseja bloquear este usuário?",conversaID);  
    }

    desbloquearConversa(conversaID : number){
        this.confirmBloquearConversa("Desbloquear usuário","Tem certeza que deseja desbloquear este usuário?",conversaID);  
    }

    confirmBloquearConversa(titulo : string, msg : string, idConversa : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.storage.get("UsuarioLogado").then((dataUser) => {

                        this.servChat.bloquearConversa(idConversa,dataUser.userID)
                        .subscribe(r => retorno = r,
                                error => console.log("Erro ao deletar veiculo"),
                                ()=>{
                                    if (retorno.status != -1){
                                        this.suporte.showAviso(retorno.mensagem);
                                        this.getConversas();
                                    }
                                    else{
                                        this.suporte.showAviso(this.errorMensage);
                                    }
                                })
                      })
                    }
                }
            ]
        });
        confirm.present();
    }

    removerConversa(id : number){
        this.confirmRemoverConversa("Remover conversa","Tem certeza que deseja remover a conversa?",id);
    }

    confirmRemoverConversa(titulo : string, msg : string, idConversa : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.servChat.removerConversa(idConversa)
                            .subscribe(r => retorno = r,
                                    error => console.log("Erro ao deletar veiculo"),
                                    ()=>{
                                        if (retorno.status == 0){
                                            this.suporte.showAviso(retorno.mensagem);
                                            this.getConversas();
                                        }
                                    })
                    }
                }
            ]
        });
        confirm.present();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getConversas();
    
        setTimeout(() => {
        //console.log('Async operation has ended');
        refresher.complete();
        }, 1500);
    }
}