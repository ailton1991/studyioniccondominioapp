import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Content , NavController, ModalController, AlertController, NavParams, ViewController  } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//import { SignalR } from "ng2-signalr";

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceChat } from "../../service/ServiceChat";
import { ServiceSignalr } from "../../service/ServiceSignalr";

@Component({
    selector: 'mensagens-page',
    templateUrl: 'mensagens.html'
})

export class mensagens extends PageBase implements OnInit{

    @ViewChild(Content) content: Content;

    conversaSelect : any;
    idUsuarioLogado : any;
    qtdSkipMsg : number;
    listaDeMensagens : any;
    form: FormGroup;

    connection : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public params : NavParams,
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servChat : ServiceChat,
        public viewCtrl : ViewController,
        public fb : FormBuilder,
        public signalr : ServiceSignalr){
        super(navCtrl, storage);

        this.listaDeMensagens = [];
        this.conversaSelect = this.params.data;

        this.connection = this.signalr.connection;

        //console.log(JSON.stringify(this.conversaSelect));
      
        
       this.form = fb.group({
            descricao: ['', Validators.required]
        });
    }

    ngOnInit(){
       this.getMensagens();

       let onMessageSent  = this.connection.listenFor("addMensagemChatApp");
       onMessageSent.subscribe((oMessage : any) => this.receberMsgChat(oMessage));       
    }

    receberMsgChat(oMsg : any){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            //oMsg.userIDdestinatario == dataUser.userID
            //console.log(oMsg);
            
            if (this.conversaSelect.id == oMsg.conversaId){
                var nDate = new Date();
                var mes = nDate.getMonth() + 1;
                var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();
                oMsg.strDataDeCadastro = ddt

                this.listaDeMensagens.push(oMsg);
                this.scrollToBottom();
            }
        });
    }

    enviarMensagem(){
        this.storage.get("UsuarioLogado").then((dataUser) => {

            let obj = this.form.value;

            let novaMensagem = {
                id : 0,
                conversaId : this.conversaSelect.id,
                descricao : obj.descricao,
                usuarioId : dataUser.userID
            }

            //console.log(JSON.stringify(novaMensagem));

            let retorno : any;
            
            var nDate = new Date();
            var mes = nDate.getMonth() + 1;
            var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();
            var ddt2 = nDate.getFullYear() + '-' + mes + '-' + nDate.getDate();

            let mensagemPush = {
                conversaId : novaMensagem.conversaId,
                descricao : novaMensagem.descricao,
                id : novaMensagem.id,
                strDataDeCadastro : ddt,
                usuarioId : novaMensagem.usuarioId
            }

            this.listaDeMensagens.push(mensagemPush);
            this.form.reset();

            if(this.content._scroll)
                setTimeout(() =>  this.content.scrollToBottom(),100);

            this.servChat.enviarMensagem(novaMensagem)
                    .subscribe(
                        result => retorno = result,
                        error => console.log("error ao enviar a mensagem"),
                        () => {
                            if (retorno.status > 0){
                                novaMensagem.id = retorno.status;
          
                                var obj = { id : retorno.status,
                                            descricao: novaMensagem.descricao,
                                            conversaId: mensagemPush.conversaId,
                                            usuarioId: dataUser.userID,
                                            nomeUsuario: dataUser.nome, 
                                            foto: dataUser.foto, 
                                            dataDeCadastro : ddt2,
                                            lida : false,
                                            userIDdestinatario : this.conversaSelect.idDoUltimoUsuario,
                                            strDataDeCadastro: ddt};

                                   // console.log(obj);

                                this.connection.invoke("SendChatMessageIonic", obj).then((result) => console.log("Mensagem enviada com sucesso.")); 

                                let resultPushOff : any;
                                this.servChat.pushClientesOff(obj.conversaId,obj.usuarioId,this.urlImageServ + "usuario/" + obj.foto,obj.descricao)
                                        .subscribe(
                                            r => resultPushOff = r,
                                            error => console.log("Error ao enviar"),
                                            () =>{
                                                //console.log(resultPushOff);
                                            });                               
                            }

                            if (retorno.status == -1){
                                this.suporte.showAviso(this.errorMensage);
                            }
                        }
                    )
        })
    }


    scrollToBottom() {
        if(this.content._scroll)
            setTimeout(() =>  this.content.scrollToBottom(),1500);
    }

    ionViewDidLoad() {
        this.scrollToBottom();
    }

    getMensagens(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
                this.idUsuarioLogado = dataUser.userID;

                this.suporte.showLoader("Carregando mensagens...");

                this.servChat.listarMensagem(this.conversaSelect.id)
                    .subscribe(
                        conv => this.listaDeMensagens = conv,
                        error => console.log("Error ao obter as conversas"),
                        () => {
                            if (this.listaDeMensagens.length > 0)
                                this.qtdSkipMsg = 10;

                            this.suporte.dismissLoader();
                        }
                    )   
                }
        })
    }

    getMaisMensagens(){
        let arrMaisMensagens : any;

        this.servChat.listarMaisMensagens(this.conversaSelect.id,this.qtdSkipMsg)
                .subscribe(
                    arr => arrMaisMensagens = arr,
                    error => console.log("error ao obter mais mensagens"),
                    () =>{
                       if (arrMaisMensagens.length > 0){
                           this.qtdSkipMsg += 10;

                           for(var i = 0; arrMaisMensagens.length > i; i++){
                               this.listaDeMensagens.unshift(arrMaisMensagens[i]);
                           }
                       }  
                    }
                )
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getMaisMensagens();
    
        setTimeout(() => {
        //console.log('Async operation has ended');
        refresher.complete();
        }, 2000);
    }
}