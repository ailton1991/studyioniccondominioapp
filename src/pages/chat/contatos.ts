import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController, ViewController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceChat } from "../../service/ServiceChat";
import { mensagens } from "./mensagens";


@Component({
    selector: 'contatos-page',
    templateUrl: 'contatos.html'
})

export class contatos extends PageBase implements OnInit{

    listaDeContatos : any;
    items : any;
    
    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servChat : ServiceChat,
        public viewCtrl: ViewController){
        super(navCtrl, storage);

        this.listaDeContatos = [];
        this.items = [];
    }

    ngOnInit(){
       this.getContatos();
    }

    getContatos(){
        //let uClienteId : number = 0;
        //let arrContatos = [];

        this.suporte.showLoader("Carregando contatos...");

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            if (dataUser != null){
                this.listaDeContatos = [];
                this.servChat.listarClientesDoCondominio(dataUser.condominioId)
                            .subscribe(
                                conts => this.listaDeContatos = conts,
                                error => console.log("Error ao obter contatos do condominio"),
                                () => {   
                                    this.initializeItems();    
                                    this.suporte.dismissLoader();
                                })


                // this.storage.get("lstAutocompletar").then((contatos) =>{

                //     if (contatos != null && contatos != undefined){
                //         if(contatos.length > 0){
                //             uClienteId = contatos[0].id;
                //         }
                //     }
                //     else
                //     {
                //         contatos = [];
                //     }

                    // this.servChat.listarClientesDoCondominio(dataUser.condominioId,uClienteId)
                    //         .subscribe(
                    //             conts => arrContatos = conts,
                    //             error => console.log("Error ao obter contatos do condominio"),
                    //             () => {

                    //                 if (contatos.length > 0){
                    //                     for (var i = 0; i < arrContatos.length -1; i++){
                    //                         contatos.unshift(arrContatos[i]);
                    //                     }
                    //                 }
                    //                 else
                    //                 {
                    //                     for (var j = 0; j < arrContatos.length -1; j++){
                    //                         contatos.push(arrContatos[j]);
                    //                     }
                    //                 }

                    //                 this.storage.set("lstAutocompletar", contatos);
                    //                 this.listaDeContatos = [];
                    //                 this.listaDeContatos = contatos;
                    //                 this.initializeItems();
                                    
                    //                 this.suporte.dismissLoader();
                    //             })
               // })
            }
        })
    }

    initializeItems(){
       this.items = [];
       this.items = this.listaDeContatos;
    }


    getItems(ev: any) {
       // Reset items back to all of the items
        this.initializeItems();
    
        // set val to the value of the searchbar
        let val = ev.target.value;
    
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
          this.items = this.listaDeContatos.filter((item) => {
            return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
          })
        }
      }

      iniciarConversa(convidadoId : number){
          let retorno : any;
          this.storage.get("UsuarioLogado").then((dataUser) => {
                if (dataUser != null){
                    this.suporte.showAviso("Carregando conversa..");
                    this.servChat.iniciarConversa(dataUser.userID,convidadoId)
                            .subscribe(
                                result => retorno = result,
                                error => console.log("Error ao iniciar a conversa"),
                                () => {
                                    let conversa : any;
                                    this.servChat.findConversa(retorno.status,dataUser.userID)
                                            .subscribe(
                                                c => conversa = c,
                                                error => console.log("Error ao obter conversa"),
                                                () => {
                                                    this.suporte.dismissLoader();
                                                   // console.log(conversa);
                                                    let m = this.modal.create(mensagens,conversa);
                                                    m.present();
                                                }
                                            )
                                }
                            )
                }
            })
      }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}