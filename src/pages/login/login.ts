﻿import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Device } from '@ionic-native/device';
import { OneSignal } from '@ionic-native/onesignal';

import { Suporte } from '../../app/app.suporte';
import { global } from "../../app/globalVariables";
import { ServiceUsuario } from '../../service/ServiceUsuario';
import { ServiceUnidade } from "../../service/ServiceUnidade";

import { Home } from '../home/home';
import { novoUsuario } from "../perfil/novoUsuario";

/*
  Generated class for the login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: 'page-login',
    templateUrl: 'login.html'
})

 export class Login {
    
    form: FormGroup;
    UsuarioLogado: any;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl : AlertController,
                public fb: FormBuilder,
                public sp: Suporte,
                public storage: Storage,
                public device : Device,
                public oneSignal : OneSignal,
                public servUnidade : ServiceUnidade,
                public _servUsuario: ServiceUsuario)
    {
        this.form = fb.group({
            email: ['', Validators.required],
            senha : ['',Validators.required]
        });
    }
    
    autenticar() {
        var obj = this.form.value;

        this.sp.showLoader("Bem vindo ao CondomínioApp.com...");

        this._servUsuario.autenticarUsuario(obj.email, obj.senha)
            .subscribe(
                usuario => this.UsuarioLogado = usuario,
                error => console.log('Error reportado: ' + <any>error),
                () => {
                    this.sp.dismissLoader();
                    if (this.UsuarioLogado.status == 0) {
                        this.storage.set('UsuarioLogado', this.UsuarioLogado);
                        this.storage.set("connectionStatus","On");
                         this.registraDevice(this.UsuarioLogado.userID);
                        this.navCtrl.setRoot(Home);
                    }
                    else {
                        this.sp.showAlert('Login de Usuário', this.UsuarioLogado.mensagem);
                    }
                });
    }
    
    private registraDevice(userId : number){

        this.oneSignal.getIds().then((objDeviceKey) =>{

            var dispositivo = {
              id : 0,
              modelo: this.device.model,
              mobileID: this.device.uuid,
              usuarioID: userId,
              plataforma: this.device.platform,
              versao: this.device.version,
              deviceKey: objDeviceKey.userId
              //deviceKey: 1
          };
          

          let retorno : any;
          this._servUsuario.gravaDispositivo(dispositivo)
                .subscribe(
                  r => retorno = r,
                  error => console.log('Error ao gravar dispositivo'),
                  () => {
                    if (retorno.status > 0){
                        dispositivo.id = retorno.status;
                        this.storage.set("dispositivo", dispositivo);
                    }
                    console.log(retorno.mensagem);
                  })
      })       
   }


    openModalCodigo() {
        let prompt = this.alertCtrl.create({
            title: 'Código de unidade',
            message: "Entre com o código de sua unidade. Caso seu condomínio não esteja utilizando o Aplicativo CondominioApp.com clique em quero testar.",
            inputs: [
              {
                name: 'codigo',
                placeholder: 'Código'
              },
            ],
            buttons: [
              {
                text: 'Quero testar o app',
                handler: data => {
                  this.iniciarTeste();
                }
              },
              {
                text: 'Validar código',
                handler: data => {
                  //console.log(data.codigo);
                  if (data.codigo != "")
                  {
                    this.sp.showLoader("Validando código...");

                    let result : any;
                    this.servUnidade.validarCodigoUnidade(data.codigo)
                            .subscribe(
                                ret => result = ret,
                                error => console.log("Error verificar código."),
                                () => {
                                    if (result.status == 0){
                                        this.sp.dismissLoader();
                                        this.navCtrl.push(novoUsuario,data.codigo);
                                    }

                                    if (result.status == 1){
                                        this.sp.dismissLoader();
                                        this.sp.showAviso(result.mensagem);
                                    }

                                    if (result.status == -1){
                                        this.sp.dismissLoader();
                                        this.sp.showAviso(global.ERROR_MSG);
                                    }
                                })
                        
                  }
                  else
                  {
                    this.sp.showAviso("Digite o código de sua unidade");
                  }
                }
              }
            ]
          });
          prompt.present();
    }

    iniciarTeste(){
      this.navCtrl.push(novoUsuario,"172d");
    }

    

    openRecuperarSenha(){
        let prompt = this.alertCtrl.create({
            title: 'Recuperar senha',
            message: "Entre com o e-mail cadastrado.",
            inputs: [
              {
                name: 'email',
                placeholder: 'E-mail'
              },
            ],
            buttons: [
              {
                text: 'Cancelar',
                handler: data => {
                  console.log('Cancel clicked');
                }
              },
              {
                text: 'Recuperar senha',
                handler: data => {
                  //console.log(data.email);
                  if (data.email != "" && global.EMAIL_REGEX.test(data.email))
                  {
                    this.sp.showLoader("Enviando e-mail de recuperação de senha...");

                    let result : any;
                    this._servUsuario.enviarEmailGetSenha(data.email.toLowerCase())
                            .subscribe(
                                ret => result = ret,
                                error => {
                                  this.sp.dismissLoader();
                                  this.sp.showAviso(global.ERROR_MSG);
                                  console.log("Error verificar email.");
                                },() => {
                                    if (result.status == 0){
                                        this.sp.dismissLoader();
                                        this.sp.showAviso(result.mensagem);
                                    }

                                    if (result.status == 1){
                                        this.sp.dismissLoader();
                                        this.sp.showAviso(result.mensagem);
                                    }

                                    if (result.status == -1){
                                        this.sp.dismissLoader();
                                        this.sp.showAviso(global.ERROR_MSG);
                                    }
                                })
                  }
                  else
                  {
                    this.sp.showAviso("Digite um e-mail para validação ");
                  }
                }
              }
            ]
          });
          prompt.present();
    }
}