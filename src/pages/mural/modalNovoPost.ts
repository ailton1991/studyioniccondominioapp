import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Camera, CameraOptions } from '@ionic-native/camera';

import { ServiceMural } from '../../service/ServiceMural';
import { Suporte } from '../../app/app.suporte';
import { Filetransferencia } from '../../app/app.filetransferencia';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'modalNovoPost-page',
    templateUrl: 'modalNovoPost.html'
})

export class modalNovoPost extends PageBase {

    form: FormGroup;
    pathPreviewImage : string;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public servMural: ServiceMural,
        public sp: Suporte,
        public fb: FormBuilder,
        public camera : Camera,
        public fileUp : Filetransferencia
    ) {

        super(navCtrl, storage);
                
        this.form = fb.group({
            titulo: [''],
            conteudo: ['', Validators.required]
        });
    }

    takePic(tpFonte) {

        const options: CameraOptions = {
            quality: 50,
            destinationType: this.camera.DestinationType.FILE_URI,
            sourceType: this.camera.PictureSourceType.CAMERA,
            allowEdit: true,
            correctOrientation: true,
            encodingType: this.camera.EncodingType.JPEG,
            saveToPhotoAlbum: false
        }

        if (tpFonte == 'galeria')
            options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;    
        

        this.camera.getPicture(options).then((imageURI) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:           
            // this.pathPreviewImage = imageURI;

            let guid = new Date().getTime();
            let nomeImage : string = imageURI.substr(imageURI.lastIndexOf('/') + 1);

            nomeImage = guid + nomeImage;
            
            this.form.addControl("img",new FormControl(nomeImage));
            this.fileUp.upload(nomeImage,imageURI,'Mural');

            this.sp.showAviso(this.fotoAnexada);

        }, (err) => {
            // Handle error
            console.log('takePic: ' + err);
        });
    }

    novoPost() {

        let result : any;

        this.storage.get("UsuarioLogado").then((data) =>{
            this.form.addControl("usuarioID",new FormControl(data.userID));
            this.form.addControl("condominioId",new FormControl(data.condominioId));
                    
            if (!this.form.contains("img"))
                this.form.addControl("img",new FormControl());

            var oPost = this.form.value;
            console.log(oPost);

            this.servMural.AddNovoPost(oPost)
                    .subscribe(post => result = post,
                    error => console.log("Error ao postar no mural"),
                () => {

                    if (result.id > 0){
                        this.dismiss();
                    }


                //   var dt = new Date();
                //   var mes = dt.getMonth() + 1;
                //   var strDataDeCadastro = dt.getDate() + '/' + mes + '/' + dt.getFullYear();
                //    this.storage.get("UsuarioLogado").then((dataUser) =>{
                //             let novoItem =  {
                //                 "id": result.id,
                //                 "condominioId": dataUser.condominioId,
                //                 "usuarioId": dataUser.userID,
                //                 "userFoto": dataUser.foto,
                //                 "userNome": dataUser.nome,
                //                 "titulo": oPost.titulo,
                //                 "conteudo": oPost.conteudo,
                //                 "img": "{{urlImageServ}}mural/{{oPost.img}}",
                //                 "strDataDeCadastro": strDataDeCadastro
                //             }           
                //   });

                })

        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}