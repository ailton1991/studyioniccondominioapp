import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { ServiceMural } from '../../service/ServiceMural';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { modalNovoPost } from "./modalNovoPost";

@Component({
    selector: 'mural-page',
    templateUrl: 'mural.html'
})

export class mural extends PageBase implements OnInit{
    
    listaDePosts : any;
    listMorePost : any;
    permitePost : boolean;
    ultimoID : number;
    userID : number;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servMural : ServiceMural,
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController){
        super(navCtrl, storage);

        this.listaDePosts = [];

        this.storage.get("objCondominio").then((dataCondominio) => {
            this.permitePost = dataCondominio.mural;
        })

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.userID = dataUser.userID;
        })
    }

    permitirDeletar(userIDpost : number) : boolean{
        if(this.userID == userIDpost)
            return true;
      
        return false;
    }

    excluirPost(idPost : number){
       this.confirmRemoverPost("Excluir postagem","Tem certeza que deseja excluir este post?",idPost);  
    }
  
    confirmRemoverPost(titulo : string, msg : string,postID : number) {
          let confirm = this.alertCtrl.create({
            title: titulo,
            message: msg,
            buttons: [
              {
                text: 'Não',
                handler: () => {
                  //console.log("Negativo");
                }
              },
              {
                text: 'Sim',
                handler: () => {
                  let retorno : any;
                  this.servMural.removerPost(postID)
                      .subscribe(result => retorno = result,
                      error => console.log("Error ao remover Post!"),
                  () =>{
                      console.log(retorno);
                      if(retorno.status == 0){
                          this.suporte.showAviso(retorno.mensagem);
                          this.listarPosts();
                      }
                  })
                }
              }
            ]
          });
          confirm.present();
    }

    ionViewDidLoad() {
       // this.content.scrollToBottom(300);
     }

    ngOnInit(){
        this.suporte.showLoader("Carregando mural...");
        this.listarPosts();
    }

    listarPosts(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servMural.listarMural(dataUser.condominioId)
                .subscribe(
                    posts => this.listaDePosts = posts,
                    error => console.log("Error ao obter a lista de recados"),
                    () => {
                        this.suporte.dismissLoader();
                       this.ultimoID = this.listaDePosts[this.listaDePosts.length -1].id;
                    }
                )
           });
    }

    openModalNovoPost(){
        let m = this.modal.create(modalNovoPost);
        m.present();
    }

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
       this.listarPosts();
    
        setTimeout(() => {
          console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
      }

      doInfinite(infiniteScroll) {
       // console.log('Begin async operation');

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.servMural.getMaisPosts(dataUser.condominioId,this.ultimoID)
                    .subscribe(maisPosts => this.listMorePost = maisPosts,
                    error => console.log("Error ao puxar mais Posts"),
                    () =>{
                        for (let i = 0; i < this.listMorePost.length -1; i++) {
                            //console.log(this.listMorePost[i]);
                          this.listaDePosts.push(this.listMorePost[i]);
                          this.ultimoID = this.listMorePost[i].id;
                        }
                    });
        })

    
        setTimeout(() => {
          infiniteScroll.complete();
        }, 2500);
    }
}