import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';

import { SignalR } from 'ng2-signalr';

import { Suporte } from '../../app/app.suporte';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'modalAutorizacaoMultEntrada-page',
    templateUrl: 'modalAutorizacaoMultEntrada.html'
})

export class modalAutorizacaoMultEntrada extends PageBase implements OnInit{

    form : any;
    ctVisitaTemp : boolean = false;

    listaDeVisitantes : any;

    currentDateCadastro = (new Date()).toISOString();
    currentDateEntrada = (new Date()).toISOString();
    currentDateSaida = (new Date()).toISOString();

    initmoth : string;
    currentDate : string;

    constructor(public storage: Storage, 
                public navCtrl : NavController,
                public param : NavParams, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public signalr : SignalR,
                public viewCtrl : ViewController,
                public modalCtrl : ModalController,
                public fb: FormBuilder)
                {
                    super(navCtrl, storage);
                    this.listaDeVisitantes = this.param.data;

                    this.currentDate = (new Date()).toISOString();
                    
                    let arrDataMin =  this.currentDate.split('T');
                    this.initmoth = arrDataMin[0];

                  // console.log(JSON.stringify(this.visitante));
                    
                    //console.log(this.visitante);
                    
                    // this.form = fb.group({
                    //     aprovado: [true],
                    //     ctVisitaTemporaria : [false],
                    //     dataDeCadastro : [""],
                    //     dataInicioPermanente : [""],
                    //     dataFimPermanente : [""],
                    //     visitaPermanente : [false],
                    //     visitaCarro : [false],
                    //     descricao : [""],
                    // });

                    this.form = fb.group({
                        dataDeCadastro : [""],
                        descricao : [""]
                    });
                }

    ngOnInit(){

    }

    adicionarMultVisitas(){

        this.sp.showLoader("Criando visitas !");

        this.storage.get("UsuarioLogado").then((dataUser) => {
            
            let novaVisita : any;
            novaVisita = this.form.value;
            let strIdsVisitantes : string = "";
           // console.log(novaVisita);

            for(var i = 0; i < this.listaDeVisitantes.length; i++){
                strIdsVisitantes += this.listaDeVisitantes[i].visitanteID + "|";
            }

             let novaVisitaModal = {
                 strIdVisitantes : strIdsVisitantes,
                 aprovado : novaVisita.aprovado,
                 dataDeCadastro : novaVisita.dataDeCadastro,
                 dataInicioPermanente : "",
                 dataFimPermanente : "",
                 visitaPermanente : novaVisita.visitaPermanente,
                 descricao : novaVisita.descricao,
                 visitaCarro : novaVisita.visitaCarro,
                 apartamentoID : dataUser.aptId
             }
     
             if (novaVisita.ctVisitaTemporaria){
                 novaVisitaModal.dataDeCadastro = novaVisita.dataInicioPermanente;
                 novaVisitaModal.dataInicioPermanente = novaVisita.dataInicioPermanente;
                 novaVisitaModal.dataFimPermanente = novaVisita.dataFimPermanente;
             }
     
             //console.log(novaVisitaModal);
     
            var result : any;
     
             this.servVisita.criarVisitaNVisitantes(novaVisitaModal)
                     .subscribe(s => result = s,
                             error => console.log("Error ao postar nova visita"),
                             () => {
                                
                                 if (result.status == 0){
                                     this.dismiss();
                                     this.form.reset();
                                     this.sp.showAviso(result.mensagem);
                                    this.signalr.connect().then((cnx) =>{
                                        cnx.invoke("SendAutorizacaoVisitaNVisitantes",dataUser.condominioId).then((res) => console.log("Pre autorização de visita multi!"));
                                    })
                                 }

                                 if (result.status == 1){
                                     this.sp.showAviso(result.mensagem);
                                 }

                                 if (result.status == -1){
                                    this.sp.showAviso(this.errorMensage);
                                 }

                                 this.sp.dismissLoader();

                             })
        })      
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}