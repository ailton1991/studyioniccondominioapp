import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, ModalController, NavParams } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';

import { Suporte } from '../../app/app.suporte';

import { ServiceSignalr } from '../../service/ServiceSignalr';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'modalAutorizarEntrada-page',
    templateUrl: 'modalAutorizarEntrada.html'
})

export class modalAutorizarEntrada extends PageBase implements OnInit{

    visitante : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController,
                public param : NavParams, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public signalr : ServiceSignalr,
                public viewCtrl : ViewController,
                public modalCtrl : ModalController,
                public fb: FormBuilder)
                {
                    super(navCtrl, storage);
                    this.visitante = this.param.data;

                   //console.log(JSON.stringify(this.visitante));
                    
                  //console.log(this.visitante);
                }

    ngOnInit(){

    }

    provarVisita(idDaVisita : number){
        let result : any;

        this.servVisita.aprovarVisita(idDaVisita).subscribe(
            retorno => result = retorno,
            error => console.log(result.mensagem),
            ()=>{
                if (result.status == 0){
                    this.sp.showAviso(result.mensagem);
                    this.dismiss();
                    this.storage.get("UsuarioLogado").then((dataUser) =>{
                        //this.signalr.connect().then((c) => {
                            this.signalr.connection.invoke("SendAutorizacaoVisitaApp",idDaVisita,dataUser.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        //});
                    })
                }
            }
        )   
    }

    rejeitarVisita(idDaVisita : number){
        let result : any;

        this.servVisita.reaprovarVisita(idDaVisita).subscribe(
            retorno => result = retorno,
            error => console.log(result.mensagem),
            ()=>{
                if (result.status == 0){
                    this.sp.showAviso(result.mensagem);
                    this.dismiss();
                    this.storage.get("UsuarioLogado").then((dataUser) =>{
                        //this.signalr.connect().then((c) => {
                            this.signalr.connection.invoke("SendAutorizacaoVisitaApp",idDaVisita,dataUser.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        //});
                    })
                }
            }
        )   
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}