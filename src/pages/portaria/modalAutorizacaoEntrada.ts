import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, ModalController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormControl } from '@angular/forms';

import { SignalR } from 'ng2-signalr'

import { Suporte } from '../../app/app.suporte';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'modalAutorizacaoEntrada-page',
    templateUrl: 'modalAutorizacaoEntrada.html'
})

export class modalAutorizacaoEntrada extends PageBase implements OnInit{

    form : any;
    visitante : any;

    ctVisitaTemp : boolean = false;

    currentDateCadastro = (new Date()).toISOString();
    currentDateEntrada = (new Date()).toISOString();
    currentDateSaida = (new Date()).toISOString();

    currentDate : string;
    initmoth : string;
    dataLimite : string;

    constructor(public storage: Storage, 
                public navCtrl : NavController,
                public param : NavParams, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public signalr : SignalR,
                public viewCtrl : ViewController,
                public modalCtrl : ModalController,
                public fb: FormBuilder,
                public alertCtrl : AlertController)
                {
                    super(navCtrl, storage);
                    this.visitante = this.param.data;

                    //console.log(JSON.stringify(this.visitante)); 
                    //console.log(this.visitante);

                    this.currentDate = (new Date()).toISOString();
                    
                    let dataFutura = new Date();
                    dataFutura = this.addDays(dataFutura,30);

                    this.dataLimite = dataFutura.toISOString();

                    let arrDataMin =  this.currentDate.split('T');
                    this.initmoth = arrDataMin[0];

                    this.form = fb.group({
                        visitanteId: [this.visitante.visitanteID],
                        aprovado: [true],
                        ctVisitaTemporaria : [false],
                        dataDeCadastro : [""],
                        dataInicioPermanente : [""],
                        dataFimPermanente : [""],
                        visitaPermanente : [false],
                        visitaCarro : [false],
                        descricao : [""],
                    });
                }

     private addDays(date: Date, days: number): Date {
            date.setDate(date.getDate() + days);
            return date;
    }

    ngOnInit(){

    }

    fixoChange(obj : any){
       // console.log(obj);
        let novaVisita : any;
        novaVisita = this.form.value;
        //console.log(novaVisita.visitaPermanente);
            if(obj.value && (this.visitante.email == null || this.visitante.email == "")){
                
                let confirm = this.alertCtrl.create({
                    title: "E-mail de visitante Fixo",
                    message: "Informe um e-mail de seu visitante fixo para check-in na portaria.",
                    inputs: [
                        {
                          name: 'emailVisitanteFixo',
                          placeholder: 'E-mail visitante'
                        },
                      ],
                    buttons: [
                      {
                        text: 'Não sei',
                        handler: () => {
                          //console.log("Negativo");
                        }
                      },
                      {
                        text: 'Ok',
                        handler: (data) => {
                            if (data.emailVisitanteFixo != "")
                            {
                               // console.log(data.emailVisitanteFixo);
                               this.visitante.email = data.emailVisitanteFixo;
                               let retorno : any;
                               this.servVisita.setEmailDeVisitante(this.visitante.visitanteID,this.visitante.email)
                                        .subscribe(ret => retorno = ret,
                                        error => console.log(this.errorMensage),
                                    () => {
                                        if (retorno.status == 0){
                                            this.sp.showAviso(retorno.mensagem);
                                        }

                                        if (retorno.status == -1){
                                            this.sp.showAviso(this.errorMensage);
                                        }
                                    })
                            }                                  
                        }
                      }
                    ]
                  });
                  confirm.present();
             
            }
    }


    adicionarVisita(){

        this.sp.showLoader("Criando nova visita");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.form.addControl("apartamentoID", new FormControl(dataUser.aptId));
            this.form.addControl("usuarioLogadoId", new FormControl(dataUser.userID));

            let novaVisita : any;
            novaVisita = this.form.value;
            
             let novaVisitaModal = {
                 visitanteId : novaVisita.visitanteId,
                 aprovado : novaVisita.aprovado,
                 dataDeCadastro : novaVisita.dataDeCadastro,
                 dataInicioPermanente : "",
                 dataFimPermanente : "",
                 visitaPermanente : novaVisita.visitaPermanente,
                 descricao : novaVisita.descricao,
                 visitaCarro : novaVisita.visitaCarro,
                 apartamentoID : novaVisita.apartamentoID,
                 usuarioLogadoId : novaVisita.usuarioLogadoId
             }
     
             if (novaVisita.ctVisitaTemporaria){
                 novaVisitaModal.dataDeCadastro = novaVisita.dataInicioPermanente;
                 novaVisitaModal.dataInicioPermanente = novaVisita.dataInicioPermanente;
                 novaVisitaModal.dataFimPermanente = novaVisita.dataFimPermanente;
             }
     
             //console.log(novaVisitaModal);
     
            var result : any;
     
             this.servVisita.novaVisita(novaVisitaModal)
                     .subscribe(s => result = s,
                             error => console.log("Error ao postar nova visita"),
                             () => {
                                
                                 if (result.status == 0){
                                     this.form.reset();
                                     this.sp.showAviso(result.mensagem);
                                     this.signalr.connect().then((cnx) =>{
                                          cnx.invoke("SendAutorizacaoVisita",novaVisitaModal.visitanteId,dataUser.condominioId).then((res) => console.log("Pre autorização de visita enviada!"));
                                          console.log(novaVisitaModal);
                                          console.log(this.visitante);
                                          if(novaVisitaModal.visitaPermanente && this.visitante.email != null && this.visitante.email != "")
                                          {
                                            let retornoEmail : any;
                                            this.servVisita.enviaEmailVisitanteFixo(this.visitante.visitanteID,dataUser.aptId,this.visitante.email)
                                                    .subscribe(retEmail => retornoEmail = retEmail,
                                                    error => console.log(this.errorMensage),
                                                () => {
                                                    if (retornoEmail.status == 0){
                                                        this.sp.showAviso("E-mail de visitante fixo enviado com sucesso!")
                                                    }                                           
                                                })
                                           }
                                     });
                                    
                                     this.dismiss();
                                 }

                                 if (result.status == 1){
                                     this.sp.showAviso(result.mensagem);
                                 }

                                 if (result.status == -1){
                                    this.sp.showAviso(this.errorMensage);
                                 }

                                this.sp.dismissLoader();
                             })
        })      
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}