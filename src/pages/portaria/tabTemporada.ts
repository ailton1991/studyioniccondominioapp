import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { SignalR } from "ng2-signalr";

import { ServiceTemporada } from "../../service/ServiceTemporada";
import{ Suporte } from "../../app/app.suporte";
import { PageBase } from "../base/pageBase";

import { modalNovaTemporada } from "./modalNovaTemporada";
import { modalEditTemporada } from "./modalEditTemporada";


@Component({
    selector: 'tabTemporada-page',
    templateUrl: 'tabTemporada.html'
})

export class tabTemporada extends PageBase{

    temporadaList : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servTemporada : ServiceTemporada,
                public suporte : Suporte,
                public modalCtrl : ModalController,
                public alertCtrl : AlertController,
                public signalr : SignalR){
        super(navCtrl, storage);

        this.temporadaList = [];
        this.getTemporadas();
    }

    getTemporadas(){
        this.suporte.showLoader("Carregando temporadas...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servTemporada.listarTemporadas(dataUser.aptId).subscribe(
                itens => this.temporadaList = itens,
                error => console.log("Error ao listar Temporadas"),
                () => this.suporte.dismissLoader()
            )
        });
    }

    openModalNovaTemporada(){
        let modal = this.modalCtrl.create(modalNovaTemporada);
        modal.present();
    }

    excluirTemporada(temporadaID : number){
      this.confirmRemoverTemporada("Excluir Temporada","Tem certeza que deseja excluir a temporada",temporadaID);  
    }

    confirmRemoverTemporada(titulo : string, msg : string, temporadaID : number) {
        let confirm = this.alertCtrl.create({
          title: titulo,
          message: msg,
          buttons: [
            {
              text: 'Não',
              handler: () => {
                //console.log("Negativo");
              }
            },
            {
              text: 'Sim',
              handler: () => {
                let retorno : any;
                this.servTemporada.removerTemporada(temporadaID)
                    .subscribe(result => retorno = result,
                    error => console.log("Error ao remover Temporada!"),
                () =>{
                    console.log(retorno);
                    if(retorno.status == 0){
                        this.suporte.showAviso(retorno.mensagem);
                        this.getTemporadas();
                    }
                })
              }
            }
          ]
        });
        confirm.present();
      }

    atualizarTemporada(oTemporada : any){
      let modal =  this.modalCtrl.create(modalEditTemporada,oTemporada);
      modal.present();
    }

    doRefresh(refresher) {
        this.getTemporadas();
     
         setTimeout(() => {
           refresher.complete();
         }, 2000);
       }
}