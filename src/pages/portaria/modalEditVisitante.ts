import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators } from '@angular/forms';
import { SignalR } from 'ng2-signalr';

import { ServiceVisita } from '../../service/ServiceVisita';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "modalEditVisitante-page",
    templateUrl: 'modalEditVisitante.html'
})

export class modalEditVisitante extends PageBase {
   
    visitante : any;
    form : any;
    tiposDeVisitas : any;
    ctTipoDeVisita : number; 

    // currentDateEntrada = (new Date()).toISOString();
    // currentDateSaida = (new Date()).toISOString();

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public suporte: Suporte,
        public fb: FormBuilder,
        public signalr : SignalR,
        public servVisita : ServiceVisita
    ) {
        super(navCtrl, storage);

        this.visitante = this.params.data;

        this.tiposDeVisitas = [{id:1,descricao:'Serviço'},
                               {id:2,descricao:'Pessoal'}]

        this.form = fb.group({
            visitanteID: [this.visitante.visitanteID],
            tpVisita: [this.visitante.tpVisita, Validators.required],
            nomeEmpresa: [this.visitante.nomeEmpresa],
            nome : [this.visitante.nome],
            email : [this.visitante.email],
            rg : [this.visitante.rg]
        });
    }

    salvarAlteracaoVisitante(){
        var oVisitante = this.form.value;

        //console.log(oVisitante);
        let result : any;

        this.servVisita.editarVisitante(oVisitante)
                .subscribe(
                    r => result = r,
                    error => console.log("Error ao editar Visitante"),
                    () => {
                        //console.log(result);
                       // if (result.status == 0){
                            this.dismiss();
                            this.suporte.showAviso("Alterações salvas!");
                       // }
                    }          
                )
    }
    // novaTemporada(){

    //     this.suporte.showLoading("Criando nova temporada...",2000);
        
    //     let objTemporada :any;
    //     let retorno : any;

    //     this.storage.get("UsuarioLogado").then((dataUser) => {
    //         this.form.addControl("clienteId",new FormControl(dataUser.userID));
    //         this.form.addControl("apartamentoId",new FormControl(dataUser.aptId));
    //         this.form.addControl("condominioId",new FormControl(dataUser.condominioId));

    //         objTemporada = this.form.value;
           
    //        objTemporada.dataDeEntrada = this.suporte.formatDate(objTemporada.dataDeEntrada);
    //        objTemporada.dataDeSaida = this.suporte.formatDate(objTemporada.dataDeSaida);

    //        // console.log(JSON.stringify(objTemporada));

    //         this.servTemporada.addNovaTemporada(objTemporada).subscribe(
    //             result => retorno = result,
    //             error => console.log("Error ao adicionar nova hospedagem!"),
    //             () =>{

    //                 this.suporte.showAviso("Temporada criada com sucesso!");
    //                 this.dismiss();
    //                // console.log("Retorno: " + JSON.stringify(retorno));

    //                 // if (retorno.status == 0){
    //                 //     var arr = retorno.mensagem.split("|");
    //                 //     console.log(arr);
    //                 //     this.suporte.showAviso(arr[1]);
    //                 // }
    
    //                 // if (retorno.status == 1)
    //                 // {
    //                 //     this.suporte.showAviso(retorno.mensagem);
    //                 // }
    //             }
    //         );
    //     })
    // }

    // paisSelect(oPais : any){
    //     console.log("Selecionado " + JSON.stringify(oPais));
    // }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}