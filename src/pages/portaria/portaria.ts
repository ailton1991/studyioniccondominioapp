import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';

import { PageBase } from "../base/pageBase";


import { tabVisitas } from "./tabVisitas";
import { tabTemporada } from "./tabTemporada";
import { tabRecados } from "./tabRecados";

@Component({
    selector: 'portaria-page',
    templateUrl: 'portaria.html'
})

export class portaria extends PageBase{

    visitasTab : any;
    temporadasTab : any;
    recadosTab : any;

    constructor(public storage: Storage, public navCtrl : NavController){
        super(navCtrl, storage);

        this.visitasTab = tabVisitas;
        this.temporadasTab = tabTemporada;
        this.recadosTab = tabRecados;
    }
}