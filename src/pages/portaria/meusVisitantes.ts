import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, ModalController, AlertController} from 'ionic-angular';

import { SignalR } from 'ng2-signalr'

import { Suporte } from '../../app/app.suporte';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";
import { listaDeConvidados } from "./listaDeConvidados";
import { modalNovoVisitante } from "./modalNovoVisitante";
import { modalEditVisitante } from "./modalEditVisitante";
import { modalAutorizacaoEntrada } from "./modalAutorizacaoEntrada";


@Component({
    selector: 'meusVisitantes-page',
    templateUrl: 'meusVisitantes.html'
})

export class meusVisitantes extends PageBase implements OnInit{

    meusVisitantesList : any;
    items : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public signalr : SignalR,
                public viewCtrl : ViewController,
                public modalCtrl : ModalController,
                public alertCtrl : AlertController)
                {
                    super(navCtrl, storage);
                    this.meusVisitantesList = [];
                }

    ngOnInit(){
        this.getMeusVisitantes();
    }

    getMeusVisitantes(){
        this.sp.showLoader("Carregando meus visitantes...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servVisita.listarMeusVisitantes(dataUser.aptId).subscribe(
                itens => this.meusVisitantesList = itens,
                error => console.log(this.errorMensage),
                () =>{
                    this.initializeItems();
                    this.sp.dismissLoader();
                })
        })        
    }
    
    

    provarVisita(idDaVisita : number){
        let result : any;

        this.servVisita.aprovarVisita(idDaVisita).subscribe(
            retorno => result = retorno,
            error => console.log(result.mensagem),
            ()=>{
                if (result.status == 0){
                    this.sp.showAviso(result.mensagem);
                    
                    this.storage.get("UsuarioLogado").then((dataUser) =>{
                        this.signalr.connect().then((c) => {
                            c.invoke("SendAutorizacaoVisitaApp",idDaVisita,dataUser.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        });
                    })
                }
            }
        )   
    }

    confirmDeleteVisitante(objVisitante : any) {
        let confirm = this.alertCtrl.create({
        title: "Apagar visitante",
        message: "Tem certeza que deseja excluir este visitante",
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        let json : any;
                        this.servVisita.excluirVisitante(objVisitante.visitanteID)
                                .subscribe(
                                    ret => json = ret,
                                    error => console.log(this.errorMensage),
                                    () =>{
                                        if (json.status == 0){
                                            this.sp.showAviso(json.mensagem);
                                            this.getMeusVisitantes();
                                        }
                                        
                                        if (json.status == 1)
                                            this.sp.showAviso(json.mensagem);
                                        
                                        if (json.status == -1)
                                            this.sp.showAviso(this.errorMensage);
                                    }
                                )
                    }
                }
            ]
        });
        confirm.present();
    }

    openNovoVisitante(){
        let m = this.modalCtrl.create(modalNovoVisitante);
        m.present();
    }

    openListaDeConvidados(){
        let m = this.modalCtrl.create(listaDeConvidados);
        m.present();
    }

    openEdit(visitanteObj : any){
        let m = this.modalCtrl.create(modalEditVisitante,visitanteObj);
        m.present();
    }

    openModalAutorizacao(visitanteObj : any){
        let s = this.modalCtrl.create(modalAutorizacaoEntrada,visitanteObj);
        s.present();
    }

    getItems(ev: any) {
        // Reset items back to all of the items
       
         this.initializeItems();
     
         // set val to the value of the searchbar
         let val = ev.target.value;
         console.log(val);
         // if the value is an empty string don't filter the items
         if (val && val.trim() != '') {
           this.items = this.meusVisitantesList.filter((item) => {
             return (item.nome.toLowerCase().indexOf(val.toLowerCase()) > -1);
           })
         }
       }

     initializeItems(){
        this.items = [];
         //for (var i = 0; i < this.listaDeContatos.length -1; i++){
         //     this.items.push(this.listaDeContatos[i].nome + " " + this.listaDeContatos[i].sobrenome);
         //}
         this.items = this.meusVisitantesList;
 
        //console.log(this.items);
     }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getMeusVisitantes();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}