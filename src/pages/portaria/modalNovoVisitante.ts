import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController, AlertController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { SignalR } from 'ng2-signalr';

import { ServiceVisita } from '../../service/ServiceVisita';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";
import { modalAutorizacaoEntrada } from "./modalAutorizacaoEntrada";

@Component({
    selector: "modalNovoVisitante-page",
    templateUrl: 'modalNovoVisitante.html'
})

export class modalNovoVisitante extends PageBase {
   
    form : any;
    tiposDeVisitas : any;
    ctTipoDeVisita : number;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public suporte: Suporte,
        public fb: FormBuilder,
        public signalr : SignalR,
        public servVisita : ServiceVisita,
        public alertCtrl : AlertController,
        public modal : ModalController) {
        super(navCtrl, storage);

        this.tiposDeVisitas = [{id:1,descricao:'Serviço'},
                               {id:2,descricao:'Pessoal'}]

        this.form = fb.group({
            tpVisita: ["", Validators.required],
            nomeEmpresa: [""],
            nome : ["", Validators.required],
            email : [""],
            rg : [""]
        });
    }

    addNovoVisitante(){

        this.storage.get("UsuarioLogado").then((dataUser) => {

            this.form.addControl("apartamentoID", new FormControl(dataUser.aptId));

            let oVisitante = this.form.value;
            //console.log(oVisitante);
            let result : any;
    
            //console.log(JSON.stringify(oVisitante));
    
            this.servVisita.novoVisitante(oVisitante)
                    .subscribe(
                         r => result = r,
                         error => console.log("Error ao cadastrar novo Visitante"),
                         () => {
                             if (result.status > 0){
                                
                                this.form.reset();
                                this.dismiss();

                                var visitanteModel = {
                                    visitanteID :  result.status,
                                    apartamentoID : oVisitante.apartamentoID,
                                    corCarro : oVisitante.corCarro,
                                    modeloCarro : oVisitante.modeloCarro,
                                    nome : oVisitante.nome,
                                    email : oVisitante.email,
                                    nomeEmpresa : oVisitante.nomeEmpresa,
                                    placaCarro : oVisitante.placaCarro,
                                    rg : oVisitante.rg,
                                    tpVisita : oVisitante.tpVisita
                                }

                                let confirm = this.alertCtrl.create({
                                    title: "Informar nova visita",
                                    message: "Deseja informar uma nova visita para este visitante?",
                                    buttons: [
                                      {
                                        text: 'Não',
                                        handler: () => {
                                          //console.log("Negativo");
                                        }
                                      },
                                      {
                                        text: 'Sim',
                                        handler: () => {
                                          console.log(JSON.stringify(visitanteModel));
                                          
                                          let modal = this.modal.create(modalAutorizacaoEntrada,visitanteModel);
                                          modal.present();                                          
                                        }
                                      }
                                    ]
                                  });
                                  confirm.present();
                             }
    
                             if (result.status == 0){
                                this.suporte.showAviso(result.mensagem);
                             }
    
                             if (result.status == -1){
                                 this.suporte.showAviso("Serviço indisponível, tente mais tarde.");
                             }
                         }   
                    )
        })
    }

    // confirmRemoverTemporada(titulo : string, msg : string, temporadaID : number) {
       
    //   }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}