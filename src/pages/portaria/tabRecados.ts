import { Component, OnInit, ViewChild  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController, Content  } from 'ionic-angular';
//import { SignalR } from "ng2-signalr";

import { ServiceSignalr } from '../../service/ServiceSignalr';
import { ServiceRecado } from '../../service/ServiceRecado';

import { Suporte } from "../../app/app.suporte";

import { PageBase } from "../base/pageBase";


@Component({
    selector: 'tabRecados-page',
    templateUrl: 'tabRecados.html'
})

export class tabRecados extends PageBase implements OnInit{
    @ViewChild(Content) content: Content;

    listaDeRecados : any;
    form : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servRecado : ServiceRecado, 
                public fb : FormBuilder,
                public signalr : ServiceSignalr,
                public sp : Suporte){
        super(navCtrl, storage);

        this.listaDeRecados = [];

        this.form = fb.group({
            descricao : ["",Validators.maxLength(144)] 
        });
    }

    scrollToBottom() {
        this.content.scrollToBottom();
      }

      ionViewDidLoad() {
        //console.log('ionViewDidLoad mensagens');
        setTimeout(() => this.scrollToBottom(),1000);
      }

    addRecado(){
        let oRecado : any;
        let retorno : any;
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.form.addControl("autor",new FormControl(dataUser.nome));
            this.form.addControl("fotoAutor",new FormControl(dataUser.foto));
            this.form.addControl("apartamentoId", new FormControl(dataUser.aptId));

            oRecado = this.form.value;

           // console.log(JSON.stringify(oRecado));

            this.servRecado.AddNovoRecado(oRecado)
                .subscribe(result => retorno = result,
                error => console.log(retorno.mensagem),
            () =>{
                if (retorno.status == 0){
                    this.ngOnInit();
                    
                    this.form.setControl("descricao", new FormControl(""));
                   // this.scrollToBottom();
                    setTimeout(() => this.scrollToBottom(),1000);

                    this.storage.get("UsuarioLogado").then((dataUser) => {

                        var nDate = new Date();
                        var mes = nDate.getMonth() + 1;
                        var ddt = nDate.getDate() + '/' + mes + '/' + nDate.getFullYear();

                        var arr = dataUser.strApartamento.split('|');
                        //this.signalr.connect().then((cx) =>{
                            this.signalr.connection.invoke("SendRecadoPortaria",{
                                lixeira: false,
                                visto: false,
                                apartamentoId: dataUser.aptId,
                                strApartamento: arr[1],
                                condominioID: dataUser.condominioId,
                                autor: dataUser.nome,
                                fotoAutor: dataUser.foto,
                                strDataDeCadastro: ddt,
                                descricao: oRecado.descricao
                            }).then((resultServ : string) => console.log("Enviado com sucesso"));
                        //})

                    });
                }
            })
        });
    }

    ngOnInit(){
        this.sp.showLoader("Carregando recados...")
       this.storage.get("UsuarioLogado").then((dataUser) => {
        this.servRecado.listarRecados(dataUser.aptId)
            .subscribe(
                recados => this.listaDeRecados = recados,
                error => console.log("Error ao obter a lista de recados"),
                () => this.sp.dismissLoader()
            )
       });
    }
}