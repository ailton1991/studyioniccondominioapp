import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, AlertController, ModalController } from 'ionic-angular';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

import { SignalR } from 'ng2-signalr'

import { Suporte } from '../../app/app.suporte';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";
import { modalAutorizacaoMultEntrada } from "./modalAutorizacaoMultEntrada";


@Component({
    selector: 'listaDeConvidados-page',
    templateUrl: 'listaDeConvidados.html'
})

export class listaDeConvidados extends PageBase implements OnInit{
    form : any;
    tiposDeVisitas : any;
    listaDeVisitantes : any;
    novosVisitantes : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public fb : FormBuilder,
                public signalr : SignalR,
                public modal : ModalController,
                public viewCtrl : ViewController,
                public alertCtrl : AlertController)
                {
                    super(navCtrl, storage);

                    this.listaDeVisitantes = [];
                    this.novosVisitantes = [];

                    this.form = fb.group({
                        tpVisita: ["", Validators.required],
                        nomeVisitante : ["",Validators.required]
                    });
                }

    ngOnInit(){
        this.tiposDeVisitas = [{id:1,descricao:'Serviço'},
                              {id:2,descricao:'Pessoal'}]
    }

    addVisitante(){
        let novo = this.form.value;
        this.listaDeVisitantes.push(novo.nomeVisitante)
        this.form.setControl("nomeVisitante", new FormControl());
        //console.log(novo.nomeVisitante);
    }

    remove(strObj : any){
       let indice = this.listaDeVisitantes.indexOf(strObj);
       this.listaDeVisitantes.splice(indice, 1);
    }

    criarMultiplosVisitantes(){
        if (this.listaDeVisitantes.length > 0){

            let strNomesVisitantes : string = "";
            let f = this.form.value;

            for(var i = 0; i < this.listaDeVisitantes.length; i++){
                strNomesVisitantes += this.listaDeVisitantes[i] + "|";
            }

            this.storage.get("UsuarioLogado").then((dataUser) =>{
                this.servVisita.novosMultiplosVisitantes(strNomesVisitantes,dataUser.aptId,f.tpVisita)
                        .subscribe(
                           r => this.novosVisitantes = r,
                           error => console.log("Error ao criar multiplos visitantes."),
                           () => {
                            let confirm = this.alertCtrl.create({
                                title: "Novas visitas",
                                message: "Deseja agendar uma visita para seus novos visitantes ?",
                                buttons: [
                                  {
                                    text: 'Não',
                                    handler: () => {
                                      //console.log("Negativo");
                                    }
                                  },
                                  {
                                    text: 'Sim',
                                    handler: () => {
                                        let m = this.modal.create(modalAutorizacaoMultEntrada,this.novosVisitantes);
                                        m.present();
                                    }
                                  }
                                ]
                              });
                              confirm.present();
                           } 
                        )
            })
            
        }
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

    
}