import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { SignalR } from 'ng2-signalr';

import { ServiceTemporada } from '../../service/ServiceTemporada';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "modal-novaTemporada",
    templateUrl: 'modalNovaTemporada.html'
})

export class modalNovaTemporada extends PageBase {
    paisList : any;
    form : any;

    currentDateEntrada = (new Date()).toISOString();
    currentDateSaida = (new Date()).toISOString();

    dataMinima : string;
    dataLimite : string;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public suporte: Suporte,
        public fb: FormBuilder,
        public signalr : SignalR,
        public servTemporada : ServiceTemporada
    ) {
        super(navCtrl, storage);

        let data = new Date();
        this.dataMinima = data.toISOString();
        
        this.dataLimite = this.addYears(data,1).toISOString();

        this.paisList = [{id:1,nome:'Brasil'},
        {id:2,nome:'Afeganistão'},
        {id:3,nome:'África do Sul'},
        {id:4,nome:'Akrotiri'},
        {id:5,nome:'Albânia'},
        {id:6,nome:'Alemanha'},
        {id:7,nome:'Andorra'},
        {id:8,nome:'Angola'},
        {id:9,nome:'Anguila'},
        {id:10,nome:'Antárctida'},
        {id:11,nome:'Antígua e Barbuda'},
        {id:12,nome:'Antilhas Neerlandesas'},
        {id:13,nome:'Arábia Saudita'},
        {id:14,nome:'Arctic Ocean'},
        {id:15,nome:'Argélia'},
        {id:16,nome:'Argentina'},
        {id:17,nome:'Arménia'},
        {id:18,nome:'Aruba'},
        {id:19,nome:'Ashmore and Cartier Islands'},
        {id:20,nome:'Atlantic Ocean'},
        {id:21,nome:'Austrália'},
        {id:22,nome:'Áustria'},
        {id:23,nome:'Azerbaijão'},
        {id:24,nome:'Baamas'},
        {id:25,nome:'Bangladeche'},
        {id:26,nome:'Barbados'},
        {id:27,nome:'Barém'},
        {id:28,nome:'Bélgica'},
        {id:29,nome:'Belize'},
        {id:30,nome:'Benim'},
        {id:31,nome:'Bermudas'},
        {id:32,nome:'Bielorrússia'},
        {id:33,nome:'Birmânia'},
        {id:34,nome:'Bolívia'},
        {id:35,nome:'Bósnia e Herzegovina'},
        {id:36,nome:'Botsuana'},
        {id:37,nome:'Brunei'},
        {id:38,nome:'Bulgária'},
        {id:39,nome:'Burquina Faso'},
        {id:40,nome:'Burúndi'},
        {id:41,nome:'Butão'},
        {id:42,nome:'Cabo Verde'},
        {id:43,nome:'Camarões'},
        {id:44,nome:'Camboja'},
        {id:45,nome:'Canadá'},
        {id:46,nome:'Catar'},
        {id:47,nome:'Cazaquistão'},
        {id:48,nome:'Chade'},
        {id:49,nome:'Chile'},
        {id:50,nome:'China'},
        {id:51,nome:'Chipre'},
        {id:52,nome:'Clipperton Island'},
        {id:53,nome:'Colômbia'},
        {id:54,nome:'Comores'},
        {id:55,nome:'Congo Brazzaville'},
        {id:56,nome:'Congo Kinshasa'},
        {id:57,nome:'Brazzaville'},
        {id:58,nome:'Kinshasa'},
        {id:59,nome:'Coral Sea Islands'},
        {id:60,nome:'Coreia do Norte'},
        {id:61,nome:'Coreia do Sul'},
        {id:62,nome:'Costa do Marfim'},
        {id:63,nome:'Costa Rica'},
        {id:64,nome:'Croácia'},
        {id:65,nome:'Cuba'},
        {id:66,nome:'Dhekelia'},
        {id:67,nome:'Dinamarca'},
        {id:68,nome:'Domínica'},
        {id:69,nome:'Egipto'},
        {id:70,nome:'Emiratos Árabes Unidos'},
        {id:71,nome:'Equador'},
        {id:72,nome:'Eritreia'},
        {id:73,nome:'Eslováquia'},
        {id:74,nome:'Eslovénia'},
        {id:75,nome:'Espanha'},
        {id:76,nome:'Estados Unidos'},
        {id:77,nome:'Estónia'},
        {id:78,nome:'Etiópia'},
        {id:79,nome:'Faroé'},
        {id:80,nome:'Fiji'},
        {id:81,nome:'Filipinas'},
        {id:82,nome:'Finlândia'},
        {id:83,nome:'França'},
        {id:84,nome:'Gabão'},
        {id:85,nome:'Gâmbia'},
        {id:86,nome:'Gana'},
        {id:87,nome:'Gaza Strip'},
        {id:88,nome:'Geórgia'},
        {id:89,nome:'Geórgia do Sul e Sandwich do Sul'},
        {id:90,nome:'Gibraltar'},
        {id:91,nome:'Granada'},
        {id:92,nome:'Grécia'},
        {id:93,nome:'Gronelândia'},
        {id:94,nome:'Guame'},
        {id:95,nome:'Guatemala'},
        {id:96,nome:'Guernsey'},
        {id:97,nome:'Guiana'},
        {id:98,nome:'Guiné'},
        {id:99,nome:'Guiné Equatorial'},
        {id:100,nome:'Guiné'},
        {id:101,nome:'Bissau'},
        {id:102,nome:'Haiti'},
        {id:103,nome:'Honduras'},
        {id:104,nome:'Hong Kong'},
        {id:105,nome:'Hungria'},
        {id:106,nome:'Iémen'},
        {id:107,nome:'Ilha Bouvet'},
        {id:108,nome:'Ilha do Natal'},
        {id:109,nome:'Ilha Norfolk'},
        {id:110,nome:'Ilhas Caimão'},
        {id:111,nome:'Ilhas Cook'},
        {id:112,nome:'Ilhas dos Cocos'},
        {id:113,nome:'Ilhas Falkland'},
        {id:114,nome:'Ilhas Heard e McDonald'},
        {id:115,nome:'Ilhas Marshall'},
        {id:116,nome:'Ilhas Salomão'},
        {id:117,nome:'Ilhas Turcas e Caicos'},
        {id:118,nome:'Ilhas Virgens Americanas'},
        {id:119,nome:'Ilhas Virgens Britânicas'},
        {id:120,nome:'Índia'},
        {id:121,nome:'Indian Ocean'},
        {id:122,nome:'Indonésia'},
        {id:123,nome:'Irão'},
        {id:124,nome:'Iraque'},
        {id:125,nome:'Irlanda'},
        {id:126,nome:'Islândia'},
        {id:127,nome:'Israel'},
        {id:128,nome:'Itália'},
        {id:129,nome:'Jamaica'},
        {id:130,nome:'Jan Mayen'},
        {id:131,nome:'Japão'},
        {id:132,nome:'Jersey'},
        {id:133,nome:'Jibuti'},
        {id:134,nome:'Jordânia'},
        {id:135,nome:'Kuwait'},
        {id:136,nome:'Laos'},
        {id:137,nome:'Lesoto'},
        {id:138,nome:'Letónia'},
        {id:139,nome:'Líbano'},
        {id:140,nome:'Libéria'},
        {id:141,nome:'Líbia'},
        {id:142,nome:'Listenstaine'},
        {id:143,nome:'Lituânia'},
        {id:144,nome:'Luxemburgo'},
        {id:145,nome:'Macau'},
        {id:146,nome:'Macedónia'},
        {id:147,nome:'Madagáscar'},
        {id:148,nome:'Malásia'},
        {id:149,nome:'Malávi'},
        {id:150,nome:'Maldivas'},
        {id:151,nome:'Mali'},
        {id:152,nome:'Malta'},
        {id:153,nome:'Man, Isle of'},
        {id:154,nome:'Marianas do Norte'},
        {id:155,nome:'Marrocos'},
        {id:156,nome:'Maurícia'},
        {id:157,nome:'Mauritânia'},
        {id:158,nome:'Mayotte'},
        {id:159,nome:'México'},
        {id:160,nome:'Micronésia'},
        {id:161,nome:'Moçambique'},
        {id:162,nome:'Moldávia'},
        {id:163,nome:'Mónaco'},
        {id:164,nome:'Mongólia'},
        {id:165,nome:'Monserrate'},
        {id:166,nome:'Montenegro'},
        {id:167,nome:'Mundo'},
        {id:168,nome:'Namíbia'},
        {id:169,nome:'Nauru'},
        {id:170,nome:'Navassa Island'},
        {id:171,nome:'Nepal'},
        {id:172,nome:'Nicarágua'},
        {id:173,nome:'Níger'},
        {id:174,nome:'Nigéria'},
        {id:175,nome:'Niue'},
        {id:176,nome:'Noruega'},
        {id:177,nome:'Nova Caledónia'},
        {id:178,nome:'Nova Zelândia'},
        {id:179,nome:'Omã'},
        {id:180,nome:'Pacific Ocean'},
        {id:181,nome:'Países Baixos'},
        {id:182,nome:'Palau'},
        {id:183,nome:'Panamá'},
        {id:184,nome:'Papua'},
        {id:185,nome:'Nova Guiné'},
        {id:186,nome:'Paquistão'},
        {id:187,nome:'Paracel Islands'},
        {id:188,nome:' Paraguai'},
        {id:189,nome:'Peru'},
        {id:190,nome:'Pitcairn'},
        {id:191,nome:'Polinésia Francesa'},
        {id:192,nome:'Polónia'},
        {id:193,nome:'Porto Rico'},
        {id:194,nome:'Portugal'},
        {id:195,nome:'Quénia'},
        {id:196,nome:'Quirguizistão'},
        {id:197,nome:'Quiribáti'},
        {id:198,nome:'Reino Unido'},
        {id:199,nome:'República Centro'},
        {id:200,nome:'Africana'},
        {id:201,nome:'República Checa'},
        {id:202,nome:'República Dominicana'},
        {id:203,nome:'Roménia'},
        {id:204,nome:'Ruanda'},
        {id:205,nome:'Rússia'},
        {id:206,nome:'Salvador'},
        {id:207,nome:'Samoa'},
        {id:208,nome:'Samoa Americana'},
        {id:209,nome:'Santa Helena'},
        {id:210,nome:'Santa Lúcia'},
        {id:211,nome:'São Cristóvão e Neves'},
        {id:212,nome:'São Marinho'},
        {id:213,nome:'São Pedro e Miquelon'},
        {id:214,nome:'São Tomé e Príncipe'},
        {id:215,nome:'São Vicente e Granadinas'},
        {id:216,nome:'Sara Ocidental'},
        {id:217,nome:'Seicheles'},
        {id:218,nome:'Senegal'},
        {id:219,nome:'Serra Leoa'},
        {id:220,nome:'Sérvia'},
        {id:221,nome:'Singapura'},
        {id:222,nome:'Síria'},
        {id:223,nome:'Somália'},
        {id:224,nome:'Southern Ocean'},
        {id:225,nome:'Spratly Islands'},
        {id:226,nome:'Sri Lanca'},
        {id:227,nome:'Suazilândia'},
        {id:228,nome:'Sudão'},
        {id:229,nome:'Suécia'},
        {id:230,nome:'Suíça'},
        {id:231,nome:'Suriname'},
        {id:232,nome:'Svalbard e Jan Mayen'},
        {id:233,nome:'Tailândia'},
        {id:234,nome:'Taiwan'},
        {id:235,nome:'Tajiquistão'},
        {id:236,nome:'Tanzânia'},
        {id:237,nome:'Território Britânico do Oceano Índico'},
        {id:238,nome:'Territórios Austrais Franceses'},
        {id:239,nome:'Timor Leste'},
        {id:240,nome:'Togo'},
        {id:241,nome:'Tokelau'},
        {id:242,nome:'Tonga'},
        {id:243,nome:'Trindade e Tobago'},
        {id:244,nome:'Tunísia'},
        {id:245,nome:'Turquemenistão'},
        {id:246,nome:'Turquia'},
        {id:247,nome:'Tuvalu'},
        {id:248,nome:'Ucrânia'},
        {id:249,nome:'Uganda'},
        {id:250,nome:'União Europeia'},
        {id:251,nome:'Uruguai'},
        {id:252,nome:'Usbequistão'},
        {id:253,nome:'Vanuatu'},
        {id:254,nome:'Vaticano'},
        {id:255,nome:'Venezuela'},
        {id:256,nome:'Vietname'},
        {id:257,nome:'Wake Island'},
        {id:258,nome:'Wallis e Futuna'},
        {id:259,nome:'West Bank'},
        {id:260,nome:'Zâmbia'},
        {id:261,nome:'Zimbabué'}];

        this.form = fb.group({
            nomeResponsavel: ["", Validators.required],
            rgResponsavel: [""],
            passaporteResponsavel : [""],
            origemResponsavel : [""],
            horaChegada : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$")],
            nAdults : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            nCriancas : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            placa : [""],
            cor : [""],
            modelo : [""],
            dataDeEntrada : ["", Validators.required],
            dataDeSaida : ["", Validators.required],
            descricao : ["",Validators.maxLength(144)]
        });
    }

    novaTemporada(){

        this.suporte.showLoading("Criando nova temporada...",2000);
        
        let objTemporada :any;
        let retorno : any;

        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.form.addControl("clienteId",new FormControl(dataUser.userID));
            this.form.addControl("apartamentoId",new FormControl(dataUser.aptId));
            this.form.addControl("condominioId",new FormControl(dataUser.condominioId));

            objTemporada = this.form.value;
           
           objTemporada.dataDeEntrada = this.suporte.formatDate(objTemporada.dataDeEntrada);
           objTemporada.dataDeSaida = this.suporte.formatDate(objTemporada.dataDeSaida);

           // console.log(JSON.stringify(objTemporada));

            this.servTemporada.addNovaTemporada(objTemporada).subscribe(
                result => retorno = result,
                error => console.log("Error ao adicionar nova hospedagem!"),
                () =>{

                    if (retorno.status == 0){
                        var arr = retorno.mensagem.split("|");
                       // console.log(arr);
                        this.suporte.showAviso(arr[1]);

                        this.signalr.connect().then((c) => {
                            c.invoke("SendNovaHospedagem",arr[0],dataUser.condominioId).then((s) => console.log("Nova hospedagem enviada com sucesso!"));
                        })
                        
                        this.dismiss();
                    }
    
                    if (retorno.status == 1)
                    {
                        this.suporte.showAviso(retorno.mensagem);
                    }

                    if (retorno.status == -1){
                        this.suporte.showAviso("Serviço indisponível no momento, por favor tente mais tarde!");
                    }
                }
            );
        })
    }

    paisSelect(oPais : any){
        console.log("Selecionado " + JSON.stringify(oPais));
    }

    private addYears(date: Date, year: number): Date {
        date.setFullYear(date.getFullYear() + year);
        return date;
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}