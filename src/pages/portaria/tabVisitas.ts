import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController } from 'ionic-angular';

import { SignalR } from 'ng2-signalr'

import { Suporte } from '../../app/app.suporte';
import { ServiceVisita } from '../../service/ServiceVisita';

import { PageBase } from "../base/pageBase";
import { meusVisitantes } from "./meusVisitantes";


@Component({
    selector: 'tabVisitas-page',
    templateUrl: 'tabVisitas.html'
})

export class tabVisitas extends PageBase implements OnInit{

    visitasAtivasList : any;

    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servVisita : ServiceVisita, 
                public sp : Suporte, 
                public signalr : SignalR,
                public alertCtrl : AlertController,
                public modal : ModalController)
                {
                    super(navCtrl, storage);
                    this.visitasAtivasList = [];
                }

    ngOnInit(){
        this.getVisitasAtivas();
    }

    getVisitasAtivas(){
        this.sp.showLoader("Carregando visitas...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servVisita.listarVisitasAtivas(dataUser.aptId).subscribe(
                itens => this.visitasAtivasList = itens,
                error => console.log('falha ao obter as visitas'),
                () =>this.sp.dismissLoader())
        })
    }

    provarVisita(idDaVisita : number){
        let result : any;

        this.servVisita.aprovarVisita(idDaVisita).subscribe(
            retorno => result = retorno,
            error => console.log(result.mensagem),
            ()=>{
                if (result.status == 0){
                    this.sp.showAviso(result.mensagem);
                    
                    this.storage.get("UsuarioLogado").then((dataUser) =>{
                        this.signalr.connect().then((c) => {
                            c.invoke("SendAutorizacaoVisitaApp",idDaVisita,dataUser.condominioId).then((resutServer : string) => console.log("Enviado com sucesso!"));
                        });
                    })
                }
            }
        )   
    }
   
     confirmRemoverPost(titulo : string, msg : string,visitaID : number) {
           let confirm = this.alertCtrl.create({
             title: titulo,
             message: msg,
             buttons: [
               {
                 text: 'Não',
                 handler: () => {
                   //console.log("Negativo");
                 }
               },
               {
                 text: 'Sim',
                 handler: () => {
                   let retorno : any;
                   this.servVisita.excluirVisita(visitaID)
                       .subscribe(result => retorno = result,
                       error => console.log("Error ao remover Visita!"),
                   () =>{
                      // console.log(retorno);
                       if(retorno.status == 0){
                           this.sp.showAviso(retorno.mensagem);
                           this.getVisitasAtivas();
                       }
                   })
                 }
               }
             ]
           });
           confirm.present();
     }


    removerVisita(visitaID : number){
        this.confirmRemoverPost("Visita permanente","Tem certeza que deseja excluir esta visita?", visitaID); 
    }

    openModalMeusVisitantes(){
        let m = this.modal.create(meusVisitantes);
        m.present();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getVisitasAtivas();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}