import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { SignalR } from 'ng2-signalr';

import { ServiceTemporada } from '../../service/ServiceTemporada';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

@Component({
    selector: "modal-editTemporada",
    templateUrl: 'modalEditTemporada.html'
})

export class modalEditTemporada extends PageBase{

    form : any;
    temporadaObj : any;

   // currentDateEntrada = (new Date()).toISOString();
   // currentDateSaida = (new Date()).toISOString();

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public suporte: Suporte,
        public fb: FormBuilder,
        public signalr : SignalR,
        public servTemporada : ServiceTemporada
    ) {
        super(navCtrl, storage);

        this.temporadaObj = this.params.data;
       

        this.form = fb.group({
            id: [this.temporadaObj.hospedagemId],
            // rgResponsavel: [""],
            // passaporteResponsavel : [""],
            // origemResponsavel : [""],
            //horaChegada : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$")],
            //nAdults : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            //nCriancas : ["",Validators.pattern("^([0-1]?[0-9]|2[0-4])?$")],
            //placa : [""],
            //cor : [""],
            //modelo : [""],
            dataDeEntrada : [this.temporadaObj.hospDataDeEntrada, Validators.required],
            dataDeSaida : [this.temporadaObj.hospDataDeSaida, Validators.required],
            descricao : [this.temporadaObj.hospDescricao,Validators.maxLength(144)]
        });
    }

    editTemporada(){
        let temporadaPost : any;
       // let retornoServ : any;

        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.form.addControl("clienteId",new FormControl(dataUser.userID));
            this.form.addControl("apartamentoId",new FormControl(dataUser.aptId));
            this.form.addControl("condominioId",new FormControl(dataUser.condominioId));

            temporadaPost = this.form.value;

            temporadaPost.dataDeEntrada = this.suporte.formatDate(temporadaPost.dataDeEntrada);
            temporadaPost.dataDeSaida = this.suporte.formatDate(temporadaPost.dataDeSaida);

            console.log(temporadaPost);

            // this.servTemporada.editarTemporada(temporadaPost)
            //     .subscribe(result => retornoServ = result,
            //         error => console.log("Error ao atualizada a temporada!"),
            //         () => {
            //             console.log(JSON.stringify(retornoServ));

            //             if(retornoServ.status == 0){
            //                 this.suporte.showAviso(retornoServ.mensagem);
            //                 this.dismiss();
            //             }

            //             if(retornoServ.status == 1){
            //                 this.suporte.showAviso(retornoServ.mensagem);
            //             }
            //         })
        })       
    }
    
    dismiss(){
        this.viewCtrl.dismiss();
    }

}