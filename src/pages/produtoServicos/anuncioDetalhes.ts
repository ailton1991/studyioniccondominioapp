import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { EnviarContato } from "./EnviarContato";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'anuncioDetalhes-page',
    templateUrl: 'anuncioDetalhes.html'
})

export class anuncioDetalhes extends PageBase implements OnInit {
    
    objAnuncio : any;
    objAnunciante : any;
    arrFotosproduto : any;

    strNomeFantasia : any;

    isCotacao : boolean = false;

    chamada : any;
    descricao : any;
    preco : any;
    siteUrl : any;
    cotacaoUrl : any;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController,
                public viewCtrl : ViewController)
    {
        super(navCtrl, _storage);

        this.arrFotosproduto = [];
        
        this.objAnuncio = this.navParams.get("anuncioObj");
        this.objAnunciante = this.navParams.get("anuncianteObj");
        
        if (this.objAnuncio.descricao.indexOf('{{residencial}}') > 0){
            this.isCotacao = true;
            this.descricao = this.objAnuncio.descricao.replace('{{residencial}}','');
            this.cotacaoUrl = "https://wwws.portoseguro.com.br/vendaonline/residencia/home.ns?cod=fa5ed517499248709a85e99e978a68d4&amp;utm_source=X1029J&amp;utm_medium=geradorLinks&amp;utm_campaign=GeradordeLinks_IC30YJ&amp;utm_content=ESTASA_CORRETORA_DE_SEGUROS";
        }else if (this.objAnuncio.descricao.indexOf('{{SeguroDeVida}}') > 0){
            this.isCotacao = true;
            this.descricao = this.objAnuncio.descricao.replace('{{SeguroDeVida}}','');
            this.cotacaoUrl = "https://wwws.portoseguro.com.br/vendaonline/vidamaissimples/home.ns?cod=0acac145832542b28f96a0840cb4ca34&utm_source=X1029J&utm_medium=geradorLinks&utm_campaign=GeradordeLinks_IC30YJ&utm_content=ESTASA_CORRETORA_DE_SEGUROS";
        }
        else{
            this.descricao = this.objAnuncio.descricao;
        }

        this.strNomeFantasia = this.objAnunciante.nomeFantasia;
        this.preco = this.objAnuncio.precoView;
        this.chamada = this.objAnuncio.chamada;
        
        this.siteUrl = this.objAnunciante.siteUrl;

        //console.log(this.objAnuncio);
        //console.log(this.objAnunciante);
        //console.log(this.objAnuncio.ftSlide);
        if (this.objAnuncio.ftSlide){
            let arr = this.objAnuncio.ftSlide.split("|");
            arr.forEach(element => {
                if (element != "")
                    this.arrFotosproduto.push(element); 
            });
        }
        
        //console.log(this.arrFotosproduto);

    }

    ngOnInit(){
        
    }
    
    openCotacaoResidencial(){
        this.sp.openUrlSite(this.cotacaoUrl);
    }
     
    openSaibaMais(){
        if (this.siteUrl != null && this.siteUrl != ""){
            this.sp.openUrlSite(this.siteUrl);
        }
        else
        {
            this.sp.showAviso("Não há um endereço externo configurado.");
        }
    }

    openEnviarContato(){
        let m = this.modalCtrl.create(EnviarContato,{"anuncianteObj" : this.objAnunciante,"anuncioObj" : this.objAnuncio});
        m.present();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}