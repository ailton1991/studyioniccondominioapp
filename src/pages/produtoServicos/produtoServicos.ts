import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { PageBase } from "../base/pageBase";
import { tabHome } from "./tabHome";
import { tabCategorias } from "./tabCategorias";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-produtoServicos',
    templateUrl: 'produtoServicos.html'
})

export class produtoServicos extends PageBase {

    homeTab: any;
    categoriaTab: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public _storage : Storage) {
        super(navCtrl, _storage);
        this.homeTab = tabHome;
        this.categoriaTab = tabCategorias; 
    }

    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }
}