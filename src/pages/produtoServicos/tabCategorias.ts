import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceProdServ } from "../../service/ServiceProdServ";
import { anunciantes } from "./anunciantes";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'tabCategorias-page',
    templateUrl: 'tabCategorias.html'
})
export class tabCategorias extends PageBase implements OnInit {
    
    listDeCategorias : any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController,
                public servProdServ : ServiceProdServ)
    {
        super(navCtrl, _storage);
        
    }

    ngOnInit(){
        this.getCategorias();
    }

    getCategorias(){
        this.sp.showLoader("Carregando categorias...");
        this.servProdServ.listarCategorias()
            .subscribe(
                r => this.listDeCategorias = r,
                error => console.log("Error ao Listar categorias"),
                () => this.sp.dismissLoader())
    }

    listarAnunciantes(categoriaObj : any){
        let m = this.modalCtrl.create(anunciantes,categoriaObj);
        m.present();
    }
}