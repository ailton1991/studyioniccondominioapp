import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceProdServ } from "../../service/ServiceProdServ";
import { anuncioDetalhes } from "./anuncioDetalhes";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

declare var google;

@Component({
    selector: 'loja-page',
    templateUrl: 'loja.html'
})

export class loja extends PageBase implements OnInit {
    
    @ViewChild("map") mapElement : ElementRef;

    anunciante : any;
    listDeAnuncios : any;
    listRetornoServ : any;
    ftBannerAnunciantes : any;
    ftLogoAnunciante : any;

    map: any;
    
   // mapElement: HTMLElement;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController,
                public servProdServ : ServiceProdServ,
                public viewCtrl : ViewController)
    {
        super(navCtrl, _storage);
        this.listDeAnuncios = [];
        this.listRetornoServ = [];
        this.anunciante = this.navParams.data;
        this.ftBannerAnunciantes = this.anunciante.ftBanner;
        this.ftLogoAnunciante = this.anunciante.ftLogo;
    }

    ngOnInit(){
        this.getAnuncios(this.anunciante.id);
    }

    ionViewDidLoad() {
       this.loadMap();
    }

    loadMap() {
       
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': this.anunciante.endereco.logradouro + "," + this.anunciante.endereco.complemento + ', Brasil', 'region': 'BR' },
                         (results, status) => {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    
                                    var latitude = results[0].geometry.location.lat();
                                    var longitude = results[0].geometry.location.lng();
                                    
                                    let latLng = new google.maps.LatLng(latitude, longitude);

                                    let mapOptions = {
                                        center: latLng,
                                        zoom: 15,
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                                    }
                                    
                                    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                                    setTimeout(() => this.addMarker(),1000);
                                }
                            }
                        });
      }

      addMarker(){
        
         let marker = new google.maps.Marker({
           map: this.map,
           animation: google.maps.Animation.DROP,
           position: this.map.getCenter()
         });
        
         let content = "";         
        
         this.addInfoWindow(marker, content);
       }

       addInfoWindow(marker, content){
        
         let infoWindow = new google.maps.InfoWindow({
           content: content
         });
        
         google.maps.event.addListener(marker, 'click', () => {
           infoWindow.open(this.map, marker);
         });
       }

    getAnuncios(anuncianteID : number){
        this.sp.showLoader("Carregando anúncios...");
        this.servProdServ.listarAnuncios(anuncianteID)
            .subscribe(
                r => this.listRetornoServ = r,
                error => console.log("Error ao Listar Anuncios"),
                () => {

                    for(let i = 0; i < this.listRetornoServ.length; i++){

                        let obj = {
                            id : this.listRetornoServ[i].id,
                            preco : this.listRetornoServ[i].preco,
                            chamada : this.listRetornoServ[i].chamada,
                            dataDeCadastro : this.listRetornoServ[i].dataDeCadastro,
                            descricao : this.listRetornoServ[i].descricao,
                            online : this.listRetornoServ[i].online,
                            tpAnuncio : this.listRetornoServ[i].tpAnuncio,
                            ftProduto : "",
                            ftSlide : ""
                        };

                        let itemArr = this.listRetornoServ[i].ftProduto.split('|');
                        obj.ftProduto = itemArr[0];
                        
                        for(let j = 1; j <= itemArr.length -1; j++){
                            obj.ftSlide += '|' + itemArr[j];
                        }
                        
                       // console.log(obj.ftSlide);
                        this.listDeAnuncios.push(obj);
                       // this.listDeAnuncios[i].ftProduto = itemArr[0];
                    }
                    //console.log(this.listDeAnuncios);

                    this.sp.dismissLoader();
                })
    }

    detalhesAnuncio(anuncioObj : any){
        let m = this.modalCtrl.create(anuncioDetalhes,{'anuncioObj': anuncioObj,'anuncianteObj': this.anunciante});
        m.present();
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}