import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";
import { ServiceProdServ } from "../../service/ServiceProdServ";
import { loja } from "./loja";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'tabHome-page',
    templateUrl: 'tabHome.html'
})
export class tabHome extends PageBase implements OnInit {
    
    listDeCategorias : any;
    listDeAnunciantes : any;

    strCategoria : string;
    
    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController,
                public servProdServ : ServiceProdServ)
    {
        super(navCtrl, _storage); 

    }

    ngOnInit(){
        this.getCategorias();
        //this.listarAnunciantes(8);
    }

    getCategorias(){
       // this.sp.showLoader("Carregando categorias...");
        this.servProdServ.listarCategorias()
            .subscribe(
                r => this.listDeCategorias = r,
                error => console.log("Error ao Listar categorias"),
                () => {
                    this.listarAnunciantes(8);
                    //this.sp.dismissLoader()
                })
    }

    listarAnunciantes(idCategoria : number){
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.sp.showLoader("Carregando anunciantes...");
            this.servProdServ.listarAnunciantesPorCategoria(idCategoria, dataUser.condominioId)
                .subscribe(
                    r => this.listDeAnunciantes = r,
                    error => console.log("Error ao Listar Anunciantes"),
                    () => this.sp.dismissLoader())
        })
    }

    listaLoja(anuncianteObj : any){
      let m = this.modalCtrl.create(loja,anuncianteObj);
      m.present();
    }
}