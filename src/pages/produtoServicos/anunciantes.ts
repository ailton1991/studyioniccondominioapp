import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceProdServ } from "../../service/ServiceProdServ";
import { loja } from "./loja";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'anunciantes-page',
    templateUrl: 'anunciantes.html'
})
export class anunciantes extends PageBase implements OnInit {
    
    categoria : any;
    listDeAnunciantes : any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl: ModalController,
                public servProdServ : ServiceProdServ,
                public viewCtrl : ViewController)
    {
        super(navCtrl, _storage);
        this.categoria = this.navParams.data;
    }

    ngOnInit(){
        this.getAnunciantes();
    }

    getAnunciantes(){
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.sp.showLoader("Carregando anunciantes...");
            this.servProdServ.listarAnunciantesPorCategoria(this.categoria.id, dataUser.condominioId)
                .subscribe(
                    r => this.listDeAnunciantes = r,
                    error => console.log("Error ao Listar Anunciantes"),
                    () => this.sp.dismissLoader())
        })
    }

    listaLoja(anuncianteObj : any){
        let m = this.modalCtrl.create(loja,anuncianteObj);
        m.present();
      }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}