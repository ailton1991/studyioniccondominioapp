import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceProdServ } from "../../service/ServiceProdServ";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'EnviarContato-page',
    templateUrl: 'EnviarContato.html'
})
export class EnviarContato extends PageBase implements OnInit {
    
    anunciante : any;
    anuncio : any;
    form : FormGroup;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public fb: FormBuilder,
                public modalCtrl: ModalController,
                public servProdServ : ServiceProdServ,
                public viewCtrl : ViewController)
    {
        super(navCtrl, _storage);

        this.anunciante = this.navParams.get("anuncianteObj");
        this.anuncio = this.navParams.get("anuncioObj");

        this.form = fb.group({
           // nome: ['', Validators.required],
           // email : ['',Validators.required],
            telefone : ['',Validators.required],
            mensagem : ['']
        });
    }

    ngOnInit(){
        
    }

    enviarContato(){
        this.storage.get("UsuarioLogado").then((user) =>{
            this.form.addControl("nome",new FormControl(user.nome));
            this.form.addControl("email",new FormControl(user.email));
            this.form.addControl("idAnunciante",new FormControl(this.anunciante.id));
            this.form.addControl("itemClicado",new FormControl(this.anuncio.chamada));
            //console.log(this.form.value);
            let novoContratar = this.form.value;
            let ret : any;
            this.servProdServ.enviaRequisicaoContratar(novoContratar)
                .subscribe(retorno => ret = retorno,
                        error => console.log(this.errorMensage),
                        () => {
                            if (ret.status == 0){
                                this.sp.showAviso(ret.mensagem);
                                this.form.reset();
                                this.dismiss();
                            }

                            if (ret.status == -1){
                                this.sp.showAviso(ret.mensagem);
                            }
                        })
        })
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}