import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController, ViewController  } from 'ionic-angular';
import { SignalR } from "ng2-signalr";

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceTransferencia } from "../../service/ServiceTransferencia";

import { modalChatVaga } from "../vaga/modalChatVaga";

@Component({
    selector: 'tabSolicitacoes-page',
    templateUrl: 'tabSolicitacoes.html'
})

export class tabSolicitacoes extends PageBase implements OnInit{

    solicitacoesList : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public viewCtrl : ViewController,
        public servTransferencia : ServiceTransferencia,
        public signalr : SignalR){
            super(navCtrl, storage);

            this.solicitacoesList = [];
    }

    ngOnInit(){
        this.getSolicitacoes();
    }

    getSolicitacoes(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.suporte.showLoader("Carregando solicitações de vaga...");

            this.servTransferencia.listarSolicitacoesPendentes(dataUser.aptId)
                    .subscribe(
                        list => this.solicitacoesList = list,
                        error => console.log("Error em obter solicitacoes"),
                        () => this.suporte.dismissLoader()
                    )
        })
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

    transferirVaga(transferenciaID : number){
        this.confirmTransferenciaDeVaga("Transferência de Vaga","Tem certeza que deseja transferir esta vaga para a unidade em questão?", transferenciaID);  
      }
  
      confirmTransferenciaDeVaga(titulo : string, msg : string, transferenciaID : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.storage.get("UsuarioLogado").then((dataUser) =>{
                        this.servTransferencia.transferirVaga(transferenciaID, dataUser.userID)
                            .subscribe(r => retorno = r,
                                    error => console.log("Falha ao Transferir sua Vaga"),
                                () => {
                                    if (retorno.status == 0){
                                        this.getSolicitacoes();
                                        this.suporte.showAviso(retorno.mensagem);

                                        this.signalr.connect().then((c) =>{
                                            c.invoke("SendVagaTransferida",dataUser.condominioId,transferenciaID).then((result) => console.log("Mensagem de Transferencia de Vaga enviada."));
                                        })
                                    }

                                    if (retorno.status == 1){
                                        this.suporte.showAviso(retorno.mensagem);
                                    }
                                })
                      })
                    }
                }
            ]
        });
        confirm.present();
    }

    presentActionSheetClientes(idUnidade:number) {
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            if (dataUser != null){
                let m = this.modal.create(modalChatVaga,{idAptId: idUnidade, usuarioID : dataUser.userID});
                m.present();
            }
        })
      }


    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getSolicitacoes();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}