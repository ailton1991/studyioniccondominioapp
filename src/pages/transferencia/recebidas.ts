import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceTransferencia } from "../../service/ServiceTransferencia";


@Component({
    selector: 'recebidas-page',
    templateUrl: 'recebidas.html'
})

export class recebidas extends PageBase implements OnInit{

    transferenciasConfirmadasList : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public viewCtrl : ViewController,
        public servTransferencia : ServiceTransferencia){
            super(navCtrl, storage);

            this.transferenciasConfirmadasList = [];
    }

    ngOnInit(){
        this.getTranferencias();
    }

    getTranferencias(){
        this.storage.get("UsuarioLogado").then((dataUser) =>{
             this.servTransferencia.listarTransferenciasConfirmadas(dataUser.aptId)
                 .subscribe(
                     list => this.transferenciasConfirmadasList = list,
                     error => console.log("Error em obter confirmações"),
                     () => this.suporte.dismissLoader()
                 )
         })
    }
    
    dismiss(){
        this.viewCtrl.dismiss();
    }
   
    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
     
        this.getTranferencias();    
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}