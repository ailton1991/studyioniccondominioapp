import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceTransferencia } from "../../service/ServiceTransferencia";


@Component({
    selector: 'tabRequisicoes-page',
    templateUrl: 'tabRequisicoes.html'
})

export class tabRequisicoes extends PageBase implements OnInit{

    requisicoesDeVagas : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servTransferencia : ServiceTransferencia){
        super(navCtrl, storage);

        this.requisicoesDeVagas = [];
    }

    ngOnInit(){
        this.getRequisicoes();
    }

    getRequisicoes(){
      
       this.storage.get("UsuarioLogado").then((dataUser) => {

            this.suporte.showLoader("Carregando minhas requisições...");

            this.servTransferencia.listarRequisicoesDeTransferencias(dataUser.aptId)
                .subscribe(
                    r => this.requisicoesDeVagas = r,
                    error => console.log("Error ao Listar requisições"),
                    () => this.suporte.dismissLoader()
                )
       })
   }

    excluirRequisicao(trasnID : number){
        this.confirmSolicitacaoDeVaga("Requisição de Vaga","Deseja excluir esta requisição de vaga?",trasnID);  
      }
  
    confirmSolicitacaoDeVaga(titulo : string, msg : string, id : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.suporte.showLoader('Cancelando solicitações...');
                      this.servTransferencia.cancelarRequisicoesDeTransferencias(id)
                            .subscribe(
                                r => retorno = r,
                                error => console.log('error ao carregar rquisições'),
                                () => {

                                    if (retorno.status == 0){
                                        this.suporte.showAviso("Solicitação removida com sucesso.");
                                        this.getRequisicoes();
                                    }

                                    if (retorno.status == 1){

                                    }

                                    this.suporte.dismissLoader();
                                }
                            )
                    }
                }
            ]
        });
        confirm.present();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getRequisicoes();
         setTimeout(() => {
           refresher.complete();
         }, 2000);
       }
}