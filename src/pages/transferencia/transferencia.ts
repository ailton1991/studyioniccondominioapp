import { Component  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ViewController, NavParams  } from 'ionic-angular';

import { PageBase } from "../base/pageBase";

import { tabSolicitacoes } from "./tabSolicitacoes";
import { tabTransferidas} from "./tabTransferidas";
import { tabRequisicoes} from "./tabRequisicoes";

@Component({
    selector: 'transferencia-page',
    templateUrl: 'transferencia.html'
})

export class transferencia extends PageBase{

    solicitacoesTab: any;
    transferidasTab: any;
    requisicoesTab : any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public _storage : Storage,  public viewCtrl : ViewController,) {
        super(navCtrl, _storage);

        this.solicitacoesTab = tabSolicitacoes;
        this.transferidasTab = tabTransferidas;
        this.requisicoesTab = tabRequisicoes;
    }
    
    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }

}