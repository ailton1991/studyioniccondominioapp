import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceTransferencia } from "../../service/ServiceTransferencia";


@Component({
    selector: 'tabTransferidas-page',
    templateUrl: 'tabTransferidas.html'
})

export class tabTransferidas extends PageBase implements OnInit{

    transferenciasConfirmadasList : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servTransferencia : ServiceTransferencia){
        super(navCtrl, storage);

        this.transferenciasConfirmadasList = [];

}

    ngOnInit(){
        this.getTransferenciasCedidas();
    }

   getTransferenciasCedidas(){
      

       this.storage.get("UsuarioLogado").then((dataUser) => {

            this.suporte.showLoader("Carregando transferidas...");

            this.servTransferencia.listarTransferenciasCedidasConfirmadas(dataUser.aptId)
                .subscribe(
                    r => this.transferenciasConfirmadasList = r,
                    error => console.log("Error ao Listar Transferidas"),
                    () => this.suporte.dismissLoader()
                )
       })
   }

    // requisitarVaga(unidadeID : number){
    //     this.confirmSolicitacaoDeVaga("Solicitação de Vaga","Deseja enviar uma solicitação desta vaga para a unidade em questão?",unidadeID);  
    //   }
  
    // confirmSolicitacaoDeVaga(titulo : string, msg : string, unidadeID : number) {
    //     let confirm = this.alertCtrl.create({
    //     title: titulo,
    //     message: msg,
    //     buttons: [
    //             {
    //                 text: 'Não',
    //                 handler: () => {
    //                 //console.log("Negativo");
    //                 }
    //             },
    //             {
    //                 text: 'Sim',
    //                 handler: () => {
    //                   let retorno : any;
    //                   this.storage.get("UsuarioLogado").then((dataUser) =>{

    //                     if (unidadeID != dataUser.aptId)
    //                     {
    //                         this.servVaga.enviarSolicitacao(unidadeID,dataUser.aptId)
    //                             .subscribe(result => retorno = result,
    //                             error => console.log("Error ao remover Temporada!"),
    //                         () =>{
    //                             //console.log(retorno);
    //                             if(retorno.status == 0){
    //                                 this.suporte.showAviso(retorno.mensagem);
    //                             }
    //                             if(retorno.status == 1){
    //                                 this.suporte.showAlert("Aviso",retorno.mensagem);
    //                             }
    //                         })
    //                     }
    //                     else
    //                     {
    //                         this.suporte.showAviso("Ops, Você não pode requisitar sua própria vaga.");
    //                     }
    //                   })
    //                 }
    //             }
    //         ]
    //     });
    //     confirm.present();
    // }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getTransferenciasCedidas();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}