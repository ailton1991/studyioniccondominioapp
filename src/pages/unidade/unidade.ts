import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Suporte } from '../../app/app.suporte';
import { ServiceUnidade } from '../../service/ServiceUnidade';

import { PageBase } from "../base/pageBase";
//import { global } from "../../app/globalVariables";

/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
    selector: "unidade-page",
    templateUrl: "unidade.html"
})

export class unidade extends PageBase implements OnInit {
    
    unidadesLst : any;
    idUnidadeAtiva : number;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public sp: Suporte,
                public _storage: Storage,
                public modalCtrl : ModalController,
                public servUnidade : ServiceUnidade)
    {
        super(navCtrl, _storage);
        this.unidadesLst = [];
        //console.log(this.usuarioLogado);
    }
    
    ngOnInit(){  
        this.getMinhasUnidades();
    }

    getMinhasUnidades(){
        this._storage.get("UsuarioLogado").then((dataUser) => {
            if (dataUser != null){
               // console.log(dataUser);
                this.idUnidadeAtiva = dataUser.aptId;

                this.sp.showLoader("Carregando unidades...");
                this.servUnidade.listarMinhaUnidades(dataUser.userID)
                        .subscribe(
                            unidades => this.unidadesLst = unidades,
                            error => console.log("Error ao obter novas unidades"),
                            () => this.sp.dismissLoader()
                        )
            }
        })
    }

    setUnidadeAtiva(idUnidade : number){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            //console.log(dataUser);

            this.sp.showLoader("Trocando unidade...");

            let result : any;
            this.servUnidade.getUnidadeID(idUnidade)
                    .subscribe(
                        r => result = r,
                        error => console.log("Error ao obter unidade"),
                        () => {
                            dataUser.aptId = result.id;
                            dataUser.condominioId = result.condominioId;
                            dataUser.proprietarioId = result.proprietarioID;
                            dataUser.strApartamento = result.codigo + '|' + result.numero + '|' + result.grupoDescricao;
                            dataUser.strNomeCondominio = result.strNomeCondominio;
                            dataUser.strCodUnidade = result.codigo;

                            //console.log(dataUser);

                            this.storage.set("UsuarioLogado",dataUser);

                            let retorno : any;
                            this.servUnidade.updateNotificacoes(idUnidade,dataUser.userID)
                                    .subscribe(
                                        rtn => retorno = rtn,
                                        error => console.log("Error ao set notificações"),
                                        () => this.getMinhasUnidades()
                                    )

                            this.sp.dismissLoader();
                        }
                    )
        })
    }

    setNotification(id : number, ct : boolean){

        console.log("id da not: " + id + " controle: " + ct);

        let result : any;
        this.servUnidade.checkNotification(id,ct)
                .subscribe(
                    r => result = r,
                    error => console.log("Error ao alterar notificações"),
                    () => {
                        this.getMinhasUnidades();
                        this.sp.showAviso(result.mensagem);
                    }
                )
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getMinhasUnidades();
     
         setTimeout(() => {
           //console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}