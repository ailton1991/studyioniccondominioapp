import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceVeiculo } from "../../service/ServiceVeiculo";


@Component({
    selector: 'modalAddVeiculo-page',
    templateUrl: 'modalAddVeiculo.html'
})

export class modalAddVeiculo extends PageBase {

    form: FormGroup;
    pathPreviewImage : string;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public sp: Suporte,
        public fb: FormBuilder,
        public servVeiculo : ServiceVeiculo
    ) {

        super(navCtrl, storage);
                
        this.form = fb.group({
            modelo: [''],
            cor: [''],
            Placa : ['',Validators.required]
        });
    }

   
    novoVeiculo() {

        let result : any;

        this.storage.get("UsuarioLogado").then((data) =>{
            this.form.addControl("apartamentoId",new FormControl(data.aptId));
            
            var oVeiculo = this.form.value;
            //console.log(oVeiculo);

            this.servVeiculo.addVeiculo(oVeiculo)
                .subscribe(
                    r => result = r,
                    error => console.log("error ao postar novo veiculo"),
                    () =>{
                        if (result.status == 0){
                            this.form.reset()
                            this.dismiss();
                            this.sp.showAviso(result.mensagem);
                        }

                        if(result.status == 1){
                            this.sp.showAviso(result.mensagem);
                        }

                        if(result.status == -1){
                            this.sp.showAviso("Serviço indisponível no momento, favor tente mais tarde!");
                        }
                    }
                )

        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}