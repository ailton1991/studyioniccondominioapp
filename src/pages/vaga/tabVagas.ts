import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceVaga } from "../../service/ServiceVaga";

import { transferencia } from "../transferencia/transferencia";
import { recebidas} from "../transferencia/recebidas";


@Component({
    selector: 'tabVagas-page',
    templateUrl: 'tabVagas.html'
})

export class tabVagas extends PageBase implements OnInit{


    listaDeVagasDisplay : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servVaga : ServiceVaga)
        {
            super(navCtrl, storage);

            this.listaDeVagasDisplay = [];
        }

    ngOnInit(){
        this.getMinhasVagas();
    }

    openModalTransferencia(){
        let m = this.modal.create(transferencia);
        m.present();
    }
    openModalRecebidas(){
        let m = this.modal.create(recebidas);
        m.present();
    }

    getMinhasVagas(){
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.suporte.showLoader("Carregando minhas vagas...");

            this.servVaga.ListarMinhasVagas(dataUser.aptId)
                .subscribe(
                    list => this.listaDeVagasDisplay = list,
                    error => console.log("error em obter minha vagas"),
                    () => this.suporte.dismissLoader()
                )
        })
    }

    disponibilizarVaga(apID : number){
        this.confirmDisponibilizarVaga("Vagas","Deseja disponibilizar esta vaga no classificado?",apID);  
      }
  
    confirmDisponibilizarVaga(titulo : string, msg : string, aptID : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        let result : any;
                        
                        this.servVaga.disponibilizarVaga(aptID)
                            .subscribe(
                                r => result = r,
                                error => console.log("Error ao disponibilizar a Vaga"),
                                () =>{
                                    if(result.status == 0){
                                        this.getMinhasVagas();
                                        this.suporte.showAviso(result.mensagem);
                                    }
                                }
                            )
                    }
                }
            ]
        });
        confirm.present();
    }

    retomarMinhaVaga(vagaId : number){
        this.confirmRetomarVaga("Vagas","Tem certeza que deseja retomar sua vaga, se a mesma estiver transferida para outra Unidade, esta tranferência será cancelada.",vagaId);  
      }
  
    confirmRetomarVaga(titulo : string, msg : string, idVaga : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        let result : any;
                        
                        this.servVaga.retomarVaga(idVaga)
                            .subscribe(
                                r => result = r,
                                error => console.log("Error ao retomar a Vaga"),
                                () =>{
                                    if(result.status == 0){
                                        this.getMinhasVagas();
                                        this.suporte.showAviso(result.mensagem);
                                    }
                                }
                            )
                    }
                }
            ]
        });
        confirm.present();
    }

    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getMinhasVagas();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}