import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController, NavParams, ViewController } from 'ionic-angular';

import { ServiceChat } from '../../service/ServiceChat';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { mensagens } from "../chat/mensagens";

@Component({
    templateUrl: 'modalChatVaga.html'
})

export class modalChatVaga extends PageBase implements OnInit{
    
    listaDeUsuariosChat : any;

    unidadeId : number;
    userId : number;
    
    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public params : NavParams,
                public servChat : ServiceChat,
                public suporte : Suporte,
                public modal : ModalController,
                public alertCtrl : AlertController,
                public viewCtrl : ViewController){
        super(navCtrl, storage);

        this.listaDeUsuariosChat = [];

        this.unidadeId = this.params.get("idAptId");
        this.userId = this.params.get("usuarioID");
    }

    ionViewDidLoad() {

    }

    ngOnInit(){
       this.getUsuariosClientesDaUnidade();
    }

    getUsuariosClientesDaUnidade(){
        this.suporte.showLoader("Carregando usuários...");

        this.servChat.listarClientesDaUnidade(this.unidadeId,this.userId)
            .subscribe(
                arr => this.listaDeUsuariosChat = arr,
                error => console.log("Error ao obter a lista de Usuario Chat"),
                () => {
                    console.log(this.listaDeUsuariosChat);
                    this.suporte.dismissLoader();
                }
            )
    }

    iniciarConversa(convidadoId : number){
        let retorno : any;
        this.suporte.showLoader("Iniciando conversa...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
              if (dataUser != null){
                  this.servChat.iniciarConversa(dataUser.userID,convidadoId)
                          .subscribe(
                              result => retorno = result,
                              error => console.log("Error ao iniciar a conversa"),
                              () => {
                                this.suporte.dismissLoader();
                                  let conversa : any;
                                  this.servChat.findConversa(retorno.status,dataUser.userID)
                                          .subscribe(
                                              c => conversa = c,
                                              error => console.log("Error ao obter conversa"),
                                              () => {
                                                // console.log(conversa);
                                                let m = this.modal.create(mensagens,conversa);
                                                m.present();   
                                              }
                                          )
                              }
                          )
              }
          })
    }

    dismiss(){
        this.viewCtrl.dismiss();
    }
}