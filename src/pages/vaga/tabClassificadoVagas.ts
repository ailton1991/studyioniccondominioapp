import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController} from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceVaga } from "../../service/ServiceVaga";
import { ServiceChat } from "../../service/ServiceChat";

import { modalChatVaga } from "./modalChatVaga";

@Component({
    selector: 'tabClassificadoVagas-page',
    templateUrl: 'tabClassificadoVagas.html'
})

export class tabClassificadoVagas extends PageBase implements OnInit{

    vagaDisponibilizadasList : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servVaga : ServiceVaga,
        public servChat : ServiceChat){
        super(navCtrl, storage);

        this.vagaDisponibilizadasList = [];

}

    ngOnInit(){
        this.getVagasDisponibilizadas();
    }

    getVagasDisponibilizadas(){
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.suporte.showLoader("Carregando classificados de vaga...");
            this.servVaga.ListarVagasDisponibilizadas(dataUser.condominioId)
                    .subscribe(
                        listVagas => this.vagaDisponibilizadasList = listVagas,
                        error => console.log("Error em obter classificado de vagas"),
                        () => {
                            //console.log(this.vagaDisponibilizadasList);
                            this.suporte.dismissLoader();
                        }
                    )
        })
    }

    requisitarVaga(unidadeID : number){
        this.confirmSolicitacaoDeVaga("Solicitação de Vaga","Deseja enviar uma solicitação desta vaga para a unidade em questão?",unidadeID);  
      }
  
    confirmSolicitacaoDeVaga(titulo : string, msg : string, unidadeID : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.storage.get("UsuarioLogado").then((dataUser) =>{

                        if (unidadeID != dataUser.aptId)
                        {
                            this.servVaga.enviarSolicitacao(unidadeID,dataUser.aptId,dataUser.userID)
                                .subscribe(result => retorno = result,
                                error => console.log("Error ao remover Temporada!"),
                            () =>{
                                //console.log(retorno);
                                if(retorno.status == 0){
                                    this.suporte.showAviso(retorno.mensagem);
                                }
                                if(retorno.status == 1){
                                    this.suporte.showAlert("Aviso",retorno.mensagem);
                                }
                            })
                        }
                        else
                        {
                            this.suporte.showAviso("Ops, Você não pode requisitar sua própria vaga.");
                        }
                      })
                    }
                }
            ]
        });
        confirm.present();
    }


    presentActionSheetClientes(idUnidade:number) {
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            if (dataUser != null){
                let m = this.modal.create(modalChatVaga,{idAptId: idUnidade, usuarioID : dataUser.userID});
                m.present();
            }
        })
      }

    doRefresh(refresher) {
        this.getVagasDisponibilizadas();
     
         setTimeout(() => {
           refresher.complete();
         }, 2000);
       }
}