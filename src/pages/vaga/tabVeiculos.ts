import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, AlertController  } from 'ionic-angular';

import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

import { ServiceVeiculo } from "../../service/ServiceVeiculo";
import{ modalAddVeiculo } from "./modalAddVeiculo";
import { modalEditVeiculo } from "./modalEditVeiculo";


@Component({
    selector: 'tabVeiculos-page',
    templateUrl: 'tabVeiculos.html'
})

export class tabVeiculos extends PageBase implements OnInit{

    listaDeVeiculos : any;

    constructor(public storage: Storage, 
        public navCtrl : NavController, 
        public suporte : Suporte,
        public modal : ModalController,
        public alertCtrl : AlertController,
        public servVeiculo : ServiceVeiculo){
        super(navCtrl, storage);

        this.listaDeVeiculos = [];
}

    ngOnInit(){
       this.getVeiculos();
    }


    getVeiculos(){
        this.storage.get("UsuarioLogado").then((dataUser) =>{
            this.suporte.showLoader("Carregando veículos...");
            this.servVeiculo.listarMeusCarros(dataUser.aptId)
                .subscribe(
                    veiculos => this.listaDeVeiculos = veiculos,
                    error => console.log("Error ao obter Veiculos"),
                    () => this.suporte.dismissLoader())
        })
    }

    openModalNovoVeiculo(){
        let m = this.modal.create(modalAddVeiculo);
        m.present();
    }

    openModalEditVeiculo(oEdit){
        let m = this.modal.create(modalEditVeiculo,oEdit);
        m.present();
    }

    excluirVeiculo(idVeiculo : number){
        this.confirmExcluirVeiculo("Excluir Vaga","Tem certeza que deseja excluir este veículo?",idVeiculo);  
    }
  
    confirmExcluirVeiculo(titulo : string, msg : string, veiculoID : number) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                      let retorno : any;
                      this.servVeiculo.removerVeiculo(veiculoID)
                            .subscribe(r => retorno = r,
                                    error => console.log("Erro ao deletar veiculo"),
                                ()=>{
                                    if (retorno.status == 0){
                                        this.suporte.showAviso(retorno.mensagem);
                                        this.getVeiculos();
                                    }
                                })
                    }
                }
            ]
        });
        confirm.present();
    }



    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.getVeiculos();
     
         setTimeout(() => {
           console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}