import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';


import { PageBase } from "../base/pageBase";

import { tabClassificadoVagas } from "../vaga/tabClassificadoVagas";
import { tabVeiculos } from "../vaga/tabVeiculos";
import { tabVagas } from "../vaga/tabVagas";
/*
  Generated class for the comunicadoOcorrencia page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
    selector: 'page-vaga',
    templateUrl: 'vaga.html'
})

export class vaga extends PageBase {

    classificadoTab: any;
    veiculosTab: any;
    ctrlVagasTab : any;


    constructor(public navCtrl: NavController, public navParams: NavParams, public _storage : Storage) {
        super(navCtrl, _storage);
        //this.UsuarioLogado = <>this._storage.get("UsuarioLogado");
       // this.listarRecentes();
        this.classificadoTab = tabClassificadoVagas;
        this.veiculosTab = tabVeiculos;
        this.ctrlVagasTab = tabVagas;
    }
    ionViewDidLoad() {
        //console.log('ionViewDidLoad comunicadoOcorrenciaPage');
    }

}