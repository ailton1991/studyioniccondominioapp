import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Suporte } from '../../app/app.suporte';

import { PageBase } from "../base/pageBase";

import { ServiceVeiculo } from "../../service/ServiceVeiculo";


@Component({
    selector: 'modalEditVeiculo-page',
    templateUrl: 'modalEditVeiculo.html'
})

export class modalEditVeiculo extends PageBase {

    form: FormGroup;
    pathPreviewImage : string;

    oVeiculo : any;

    constructor(
        public platform: Platform,
        public params: NavParams,
        public navCtrl: NavController,
        public viewCtrl: ViewController,
        public storage: Storage,
        public sp: Suporte,
        public fb: FormBuilder,
        public servVeiculo : ServiceVeiculo
    ) {

        super(navCtrl, storage);
                    
        this.oVeiculo = this.params.data;

        this.form = fb.group({
            id : [this.oVeiculo.id],
            apartamentoId : [this.oVeiculo.apartamentoId],
            modelo: [this.oVeiculo.modelo],
            cor: [this.oVeiculo.cor],
            Placa : [this.oVeiculo.placa,Validators.required]
        });
    }

   
    editVeiculo() {

        let result : any;

        this.storage.get("UsuarioLogado").then((data) =>{
                        
            var oVeiculo = this.form.value;
            console.log(oVeiculo);

            this.servVeiculo.editVeiculo(oVeiculo)
                .subscribe(
                    r => result = r,
                    error => console.log("error ao editar veiculo"),
                    () =>{
                        if (result.status == 0){
                            this.form.reset()
                            this.dismiss();
                            this.sp.showAviso(result.mensagem);
                        }

                        if(result.status == -1){
                            this.sp.showAviso("Serviço indisponível no momento, favor tente mais tarde!");
                        }
                    }
                )
        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}