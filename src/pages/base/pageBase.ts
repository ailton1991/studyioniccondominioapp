﻿import { Storage } from '@ionic/storage';
import { NavController } from 'ionic-angular';


import { global } from '../../app/globalVariables';

export abstract class PageBase {

    public urlImageServ: string = global.BASE_API_UPLOAD;
    public urlArquivosUpload: string  = global.BASE_API_UPLOAD;
    public errorMensage : string = global.ERROR_MSG;
    public fotoAnexada : string = global.FOTO_ANEXADA;
    
    constructor(public navCtrl: NavController, public storage : Storage ) {   
    }
}