import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController } from 'ionic-angular';

import { ServiceEnquete } from '../../service/ServiceEnquete';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";
import { enqueteDetails } from "./enqueteDetails";

@Component({
    selector: 'enquete-page',
    templateUrl: 'enquete.html'
})

export class enquete extends PageBase implements OnInit{
    
    listaDeEnquetes : any;
    
    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public servEnquete : ServiceEnquete,
                public suporte : Suporte,
                public modal : ModalController){
        super(navCtrl, storage);

        this.listaDeEnquetes = [];
    }

    ionViewDidLoad() {
       
     }

    ngOnInit(){
        this.listarEnquetes();
    }

    listarEnquetes(){
        this.suporte.showLoader("Carregando enquetes...");
        this.storage.get("UsuarioLogado").then((dataUser) => {
            this.servEnquete.listarEnquete(dataUser.condominioId)
                .subscribe(
                    enquetes => this.listaDeEnquetes = enquetes,
                    error => console.log("Error ao obter a lista de Enquetes"),
                    () =>this.suporte.dismissLoader())
           });
    }

    detalhesEnquetes(oEnquete : any){
        let m = this.modal.create(enqueteDetails,oEnquete);
        m.present();
    }

    doRefresh(refresher) {
       // console.log('Begin async operation', refresher);
       this.listarEnquetes();
    
        setTimeout(() => {
          //console.log('Async operation has ended');
          refresher.complete();
        }, 2000);
    }
}