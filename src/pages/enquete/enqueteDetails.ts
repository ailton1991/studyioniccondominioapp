import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, NavParams, ViewController } from 'ionic-angular';

import { ServiceEnquete } from '../../service/ServiceEnquete';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

@Component({
    selector: 'enqueteDetails-page',
    templateUrl: 'enqueteDetails.html'
})

export class enqueteDetails extends PageBase implements OnInit{
    

    // Doughnut
  public doughnutChartLabels: any;
  public doughnutChartData: any;
  public doughnutChartType:string = 'doughnut';



    oEnquete : any;
    
    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public param : NavParams,
                public servEnquete : ServiceEnquete,
                public suporte : Suporte,
                public viewCtrl : ViewController,
                public modal : ModalController){
        super(navCtrl, storage);

        this.doughnutChartLabels = [];
        this.doughnutChartData = [];
    }

    ionViewDidLoad() {
       //this.content.scrollToBottom(300);
    }

    ngOnInit(){
       this.oEnquete = this.param.data;

       for(var i = 0; i < this.oEnquete.alternativasModel.length; i++){
           this.doughnutChartLabels.push(this.oEnquete.alternativasModel[i].descricao);
           this.doughnutChartData.push(this.oEnquete.alternativasModel[i].qtdVotos);
       }
       //console.log(this.oEnquete);
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}