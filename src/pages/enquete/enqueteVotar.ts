import { Component, OnInit  } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, ModalController, NavParams, ViewController, AlertController } from 'ionic-angular';

import { ServiceEnquete } from '../../service/ServiceEnquete';
import { Suporte } from '../../app/app.suporte';
import { PageBase } from "../base/pageBase";

@Component({
    selector: 'enqueteVotar-page',
    templateUrl: 'enqueteVotar.html'
})

export class enqueteVotar extends PageBase implements OnInit{

    arrEnquetesNaoRespondidas : any;
    oEnquete : any;
    
    constructor(public storage: Storage, 
                public navCtrl : NavController, 
                public alertCtrl : AlertController,
                public param : NavParams,
                public servEnquete : ServiceEnquete,
                public suporte : Suporte,
                public viewCtrl : ViewController,
                public modal : ModalController){
        super(navCtrl, storage);
            
        this.arrEnquetesNaoRespondidas = this.param.data;
        this.oEnquete = this.arrEnquetesNaoRespondidas[0];

        console.log(this.arrEnquetesNaoRespondidas);
        console.log(this.oEnquete);
    }

    ionViewDidLoad() {
       //this.content.scrollToBottom(300);
    }

    ngOnInit(){
       
    }

    votarEnquete(idAlternativa : number, strDescricao: string){
        //console.log("alternativa Id: " + idAlternativa);
        this.confirmVotarEnquete("Enquetes","Tem certeza que deseja votar em " + strDescricao + "?",idAlternativa);
    }

    confirmVotarEnquete(titulo : string, msg : string, alternativaID : number) {
        let confirm = this.alertCtrl.create({
          title: titulo,
          message: msg,
          buttons: [
            {
              text: 'Não',
              handler: () => {
                //console.log("Negativo");
              }
            },
            {
              text: 'Sim',
              handler: () => {
                  this.suporte.showLoader("Enviando seu voto...");
                this.storage.get("UsuarioLogado").then((dataUser) => {
                    if (dataUser != null){

                       
                        //console.log(this.arrEnquetesNaoRespondidas);

                        let retorno : any;
                        this.servEnquete.votarEnquete(alternativaID,dataUser.userID,dataUser.aptId)
                            .subscribe(result => retorno = result,
                            error => console.log("Error ao votar na alternativa"),
                            () =>{
                              //  console.log(retorno); 
                                if(retorno.status == 0){
                                    this.suporte.showAviso(retorno.mensagem);
                                    this.arrEnquetesNaoRespondidas.shift();
                                    if (this.arrEnquetesNaoRespondidas.length > 0){
                                        this.oEnquete = this.arrEnquetesNaoRespondidas[0];
                                        this.suporte.dismissLoader();
                                    }
                                    else
                                    {
                                        this.suporte.dismissLoader();
                                        this.dismiss();
                                    }
                                }

                                if (retorno.status == 1){
                                    this.suporte.showAviso(retorno.mensagem);
                                    this.suporte.dismissLoader();
                                }

                                if (retorno.status == -1){
                                    this.suporte.showAviso(this.errorMensage);
                                    this.suporte.dismissLoader();
                                }
                            })
                    }
                });
              }
            }
          ]
        });
        confirm.present();
      }


    dismiss() {
        this.viewCtrl.dismiss();
    }
}