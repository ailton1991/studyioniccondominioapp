﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceUsuario {
     
    private _baseUrlServer = global.BASE_API_URL;

    constructor(public http: Http) {
        
    }
    
    autenticarUsuario(strEmail: string, strPassWord: string) {
        
        let params = 'email=' + strEmail + '&senha=' + strPassWord;
        
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'usuario/login', params, options)
            .map(res => res.json());                
     }

     getCliente(clienteID : number){
        let params = 'uID=' + clienteID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Usuario/getUserID?" + params, options)
            .map(res => res.json());
     }

     editUsuario(objUser : any){
         
        let params = JSON.stringify(objUser);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Usuario/EditPerfilUser', params, options)
            .map(res => res.json());
     }

     novoUsuario(objUser : any){
        let params = JSON.stringify(objUser);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Apartamento/AddCliente', params, options)
            .map(res => res.json());
     }


     editConvidadoCarteira(objUser : any){
        let params = JSON.stringify(objUser);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'ConvidadoCarteira/EditConvidado', params, options)
            .map(res => res.json());
     }

     novoConvidadoCarteira(objUser : any){
        let params = JSON.stringify(objUser);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'ConvidadoCarteira/AddConvidado', params, options)
            .map(res => res.json());
     }

     ativaDesativaConvidadoCarteira(carteiraConvidadoId: number, areaId: number, ct: boolean){
        let params = "convidadoCarteirinhaId=" + carteiraConvidadoId + "&idAreaDeAcesso=" + areaId + "&controle=" + ct;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "ConvidadoCarteira/ativaDesativaConvidado?" + params, options)
            .map(res => res.json());
     }

     enviarEmailGetSenha(emailStr : string){
        let params = 'strEmail=' + emailStr;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "usuario/getNovaSenha?" + params, options)
            .map(res => res.json());
     }

     gravaDispositivo(objDevice : any){
        let params = JSON.stringify(objDevice);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Usuario/dispositivo', params, options)
            .map(res => res.json());
     }

     destroiDevice(deviceId : number){
        let params = 'deviceID=' + deviceId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "device/DestruirDeviceId?" + params, options)
            .map(res => res.json());
     }

     setCpfUser(userID : number, cpfStr : string){
        
       let params = 'idUser=' + userID + "&cpfStr=" + cpfStr;
       let headers = new Headers({ 'Content-Type': 'application/json' });
       let options = new RequestOptions({ headers: headers });

       return this.http.get(this._baseUrlServer + "usuario/atualizaCpf?" + params, options)
           .map(res => res.json());
    }
}