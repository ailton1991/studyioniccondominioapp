﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceOcorrencia provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceOcorrencia {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceOcorrencia Provider');
    }

    getOcorrencias(userID: number, idCondominio: number) {
        let params = 'uID=' + userID + '&condominioID=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "ocorrencia/GetOcorrencias?" + params, options)
            .map(res => res.json());
    }


    addnovaOcorrencia(ocorrenciaObj : any){

        console.log(ocorrenciaObj);

        let params = JSON.stringify(ocorrenciaObj);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Ocorrencia/addOcorrencia', params, options)
            .map(res => res.json());       
    }

    addOcorrenciaEmergencia(ocorrenciaEmergencia : any){
        
        let params = "clienteId=" + ocorrenciaEmergencia.clienteId + "&condominioId=" + ocorrenciaEmergencia.condominioId + "&idApartamentoOcorrencia=" + ocorrenciaEmergencia.idApartamentoOcorrencia + "&EmergenciaID=" + ocorrenciaEmergencia.EmergenciaID;
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Ocorrencia/addOcorrenciaEmergencia?" + params, options)
            .map(res => res.json());
    }

    getOcorrenciaID(ocorrenciaID : number){
         
        let params = 'idOcorrencia=' + ocorrenciaID;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'ocorrencia/getOcorrenciaID?' + params, options)
            .map(res => res.json());    
    }
}