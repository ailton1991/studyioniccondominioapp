import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceCarteiraDigital{

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
       
    }

    getCarteirasDaUnidade(unidadeID: number) {
        let params = 'unidadeId=' + unidadeID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "carteira/getCarteiraDaUnidade?" + params, options)
            .map(res => res.json());
    }

    getAtivaDesativaConvidado(convidadoId: number, idArea: number, contr : boolean) {
        let params = 'convidadoCarteirinhaId=' + convidadoId + "&idAreaDeAcesso=" + idArea + "&controle=" + contr;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "ConvidadoCarteira/ativaDesativaConvidado?" + params, options)
            .map(res => res.json());
    }

    getEfetuaCheckIn(uID: number, idArea: number, idCarteira : boolean, dateTime: string) {
        let params = 'usuarioID=' + uID + "&areaDeAcessoID=" + idArea + "&carteiraID=" + idCarteira + "&dtCheckIn=" + dateTime;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Carteira/setCheckin?" + params, options)
            .map(res => res.json());
    }

    // addNovoClassificado(oClassificado){
    //     console.log(oClassificado);

    //     let params = JSON.stringify(oClassificado);
        
    //     let headers = new Headers({'Content-Type': 'application/json'});
    //     let options = new RequestOptions({ headers: headers, method: "post"});
        
    //     return this.http.post(this._baseUrlServer + 'Classificado/addClassificado', params, options)
    //         .map(res => res.json());  
    // }
}