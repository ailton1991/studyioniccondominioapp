import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceMural {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
       // console.log('Hello ServiceMural Provider');
    }

    listarMural(idCondominio: number) {
        let params = 'condominioID=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Mural/getPostMural?" + params, options)
            .map(res => res.json());
    }


    getMaisPosts(idCondominio : number, uIDPost : number){
        let params = 'condominioID=' + idCondominio + '&postLastId=' + uIDPost;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Mural/getPostMuralMais?" + params, options)
            .map(res => res.json());
    }

    AddNovoPost(oPost: any){  
       // console.log(oPost);

        let params = JSON.stringify(oPost);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Mural/NovoPost', params, options)
            .map(res => res.json());   
    }

    removerPost(idPost : number){
        let params = 'postId=' + idPost;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Mural/DeletePost?" + params, options)
            .map(res => res.json());
    }
}