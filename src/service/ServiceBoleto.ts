import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceBoleto{

    private _baseUrlServer: string = global.BASE_API_BOLETO;

    constructor(public http: Http) {
       
    }

    getBoletoService(strCpf : string, strFolder: string, strUrlWs:string ) {
        let params = "cpfstr=" + strCpf + "&folderBoleto=" + strFolder + "&linkerp=" + strUrlWs;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "boleto/getBoleto?" + params, options)
            .map(res => res.json());
    }
}