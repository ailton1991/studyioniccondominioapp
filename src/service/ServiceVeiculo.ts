import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceVeiculo {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceVeiculo');
    }

    listarMeusCarros(apartamentoID: number) {
        let params = 'aptId=' + apartamentoID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "carro/ListaCarros?" + params, options)
            .map(res => res.json());
    }

    addVeiculo(oVeiculo : any){
        //console.log(oVeiculo);
        
        let params = JSON.stringify(oVeiculo);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'carro/AddCarro', params, options)
            .map(res => res.json());  
    }

    editVeiculo(oVeiculo : any){
        //console.log(oVeiculo);
        
        let params = JSON.stringify(oVeiculo);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'carro/EditCarro', params, options)
            .map(res => res.json());  
    }

    removerVeiculo(id : number){
        let params = '_idCarro=' + id;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "carro/dellCarro?" + params, options)
            .map(res => res.json());
    }
}