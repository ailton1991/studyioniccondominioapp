import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceVisita {
     
    private _baseUrlServer = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceUsuario Provider/ Host' + this._baseUrlServer);
    }

    listarVisitasAtivas(idUnidade: number) {
        let params = 'apartamentoID=' + idUnidade;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visita/GetVisitasAtivas?' + params, options)
            .map(res => res.json());
    }
    
    listarMeusVisitantes(idUnidade : number){
        let params = 'unidId=' + idUnidade;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visitante/getVisitantesDoApartamento?' + params, options)
            .map(res => res.json());
    }
    
    novoVisitante(oVisitante : any){
        let params = JSON.stringify(oVisitante);

        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "apartamento/addVisitante", params,options)
                            .map(res => res.json());
    }

    novosMultiplosVisitantes(strVisitantes : any, unidadeID : number, visitasTP : number){
        let params = 'listVisitantesNomes=' + strVisitantes + "&apartamentoID=" + unidadeID + "&tpVisitante=" + visitasTP;
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visitante/AdicionarMutiplosVisitantes?' + params, options)
            .map(res => res.json());
    }

    criarVisitaNVisitantes(novasVisitas : any){
        let params = JSON.stringify(novasVisitas);
        
        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "visita/CriarVisitaForNVisitantes", params,options)
                            .map(res => res.json());
    }

    findVisitante(idVisitante : number){
        let params = 'visitanteID=' + idVisitante;
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visitante/getVisitante?' + params, options)
            .map(res => res.json());
    }

    editarVisitante(oVisitante : any){
        let params = JSON.stringify(oVisitante);
        
        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "visitante/Edit", params,options)
                            .map(res => res.json());
    }

    novaVisita(oVisita : any){
        let params = JSON.stringify(oVisita);

        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "apartamento/addNovaVisitaParaVisitante", params,options)
                            .map(res => res.json());
    }
    
    aprovarVisita(visitaID : number){
       let params = 'visitaId=' + visitaID;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visita/GetAprovarVisita?' + params, options)
            .map(res => res.json());
    }

    reaprovarVisita(visitaID : number){
        let params = 'visitaId=' + visitaID;
 
         let headers = new Headers({ 'Content-Type': 'application/json' });
         let options = new RequestOptions({ headers: headers });
 
         return this.http.get(this._baseUrlServer + 'visita/GetReprovarVisita?' + params, options)
             .map(res => res.json());
     }

    isVisitaPendente(unidadeID : number){
        let params = 'aptIdApartamento=' + unidadeID;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'apartamento/getIdVisitantePendente?' + params, options)
            .map(res => res.json());
    }

    excluirVisita(idVisita : number){
        let params = 'idVisita=' + idVisita;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visita/GetExcluirVisita?' + params, options)
            .map(res => res.json());
    }

    excluirVisitante(visitanteID : number){
        let params = 'idVisitante=' + visitanteID;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visitante/ExcluirVisitante?' + params, options)
            .map(res => res.json());
    }

    setEmailDeVisitante(vstId : number, sEmail: string){
        let params = 'id=' + vstId + '&emailStr=' + sEmail;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'visitante/setEmailDeVisitante?' + params, options)
            .map(res => res.json());
    }

    enviaEmailVisitanteFixo(visitanteId:number, unidadeId : number, emailstr : string){
        let params = 'idVisitante=' + visitanteId + '&idApartamento=' + unidadeId + '&email=' + emailstr;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
       
        return this.http.get(this._baseUrlServer + '/Visitante/EnviarEmailQRCodeVisitante?' + params, options)
            .map(res => res.json());
    }
}