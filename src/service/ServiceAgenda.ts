import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

import { global } from '../app/globalVariables'
/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceAgenda {
    private _baseUrlServer = global.BASE_API_URL;
    constructor(public http: Http) {
        //console.log('Hello ServiceComunicado Provider');
    }

    getAreaComumList(idCondominio: number)
    {
        let params = 'IdCondominio=' + idCondominio;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        
        return this.http.get(this._baseUrlServer + "AreaComum/?"+params,options)
            .map(res => res.json());
    }

    getAgendaCondominio(idApartamento: number) {
        let params = 'apartamentoId=' + idApartamento;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/getCriarAgenda?" + params, options)
            .map(res => res.json());
    }

    getAtividadesDoCondominio(idCondominio: number) {
        let params = 'condominioId=' + idCondominio;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'Atividade/getAtividade?' + params, options)
            .map(res => res.json());
    }
}