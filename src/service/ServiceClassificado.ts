import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceClassificado {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceClassificado Provider');
    }

    listarClassificados(idCondominio: number) {
        let params = 'Idcondominio=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "classificado/getClassificados?" + params, options)
            .map(res => res.json());
    }
    
    listarMeusAnuncios(uID: number) {
        let params = 'IdUser=' + uID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "classificado/?" + params, options)
            .map(res => res.json());
    }


    addNovoClassificado(oClassificado){
        console.log(oClassificado);

        let params = JSON.stringify(oClassificado);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Classificado/addClassificado', params, options)
            .map(res => res.json());  
    }

    removerClassificado(idAnuncio : number){
        let params = 'anuncioID=' + idAnuncio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "classificado/excluirClassificado?" + params, options)
            .map(res => res.json());
    }

    editarClassificado(oClassificado : any){
        console.log(oClassificado);
        
        let params = JSON.stringify(oClassificado);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Classificado/editClassificado', params, options)
            .map(res => res.json());  
    }
}