﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

/*
  Generated class for the ServiceHome provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceHome {

    private _baseWebUrl = global.BASE_API_URL;

    constructor(public http: Http) {
       // console.log('Hello ServiceHome Provider');
    }
   

    getConfiguracaoCondominio(idCondominio : number) {
       
        let params = 'condominioId=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "Condominio/getCondominioConfig?" + params, options)
                      .map(res => res.json());
    }

    getMeteorologiaCloud() {

        let params = 'cid=BRXX0201';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "externo/GetJsonTempo?" + params, options)
                      .map(res => res.json());
    }

    getPrioridadeHome(idUnidade : number){
        let params = 'aptID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "apartamento/getPrioridadeAppIonic?" + params, options)
                      .map(res => res.json());
    }

    getNovaNoticia(idUnidade: number, condominioID: number, usuarioID : number) {

        let params = 'aptId=' + idUnidade + '&idCondominio=' + condominioID + "&idUsuario=" + usuarioID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "Condominio/getNovaNoticias?" + params, options)
                      .map(res => res.json());
    }

    getVerificaVeiculo(idUnidade: number) {

        let params = 'aptID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "carro/verificarCarro?" + params, options)
                      .map(res => res.json());
    }

    isConnection(){
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseWebUrl + "values", options)
                      .timeout(2500)
                      .map(res => res.json());
    }
}