import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceTransferencia {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
       // console.log('Hello ServiceTransferencia');
    }

    listarSolicitacoesPendentes(apartamentoID: number) {
        let params = 'apId=' + apartamentoID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Transferencia/?" + params, options)
            .map(res => res.json());
    }

    listarTransferenciasConfirmadas(idUnidade : number){
        let params = 'apartamentoID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "transferencia/getVagasTranferidas?" + params, options)
            .map(res => res.json());
    }

    listarTransferenciasCedidasConfirmadas(idUnidade : number){
        let params = 'apartamentoID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "transferencia/getVagasCedidas?" + params, options)
            .map(res => res.json());
    }

    transferirVaga(idTransferencia : number, cedenteID: number){
        let params = 'transID=' + idTransferencia + '&idCendente=' + cedenteID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "transferencia/?" + params, options)
            .map(res => res.json());
    }

    listarRequisicoesDeTransferencias(idUnidade : number){
        let params = 'unidadeID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "transferencia/getRequisicoesDeVaga/?" + params, options)
            .map(res => res.json());
    }
    
    cancelarRequisicoesDeTransferencias(transferenciaID : number){
        let params = 'idTransferencia=' + transferenciaID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "transferencia/CancelarRequisicaoDeVaga/?" + params, options)
            .map(res => res.json());
    }
}