import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceRecado {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceRecado Provider');
    }

    listarRecados(idUnidade: number) {
        let params = 'aptID=' + idUnidade;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "recado/getRecadosApt?" + params, options)
            .map(res => res.json());
    }

    AddNovoRecado(oRecado: any){  
        console.log(oRecado);

        let params = JSON.stringify(oRecado);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'recado/postRecado', params, options)
            .map(res => res.json());    
       
    }
}