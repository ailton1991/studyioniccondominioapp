import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceProdServ {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
       // console.log('Hello ServiceTransferencia');
    }

    listarCategorias() {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Categoria/getCategorias", options)
            .map(res => res.json());
    }

    listarAnunciantesPorCategoria(categoriaID : number, idCondominio : number){
        let params = 'catID=' + categoriaID + "&condominioID=" + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Categoria/getAnunciantesPorCategoria?" + params, options)
            .map(res => res.json());
    }

    listarAnuncios(anuncianteID : number) {
        let params = 'idLojista=' + anuncianteID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Anuncio/getAnunciosDoLojista?" + params, options)
            .map(res => res.json());
    }

    enviaRequisicaoContratar(novoContratar : any){

       // console.log(novoContratar);

        let params = JSON.stringify(novoContratar);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'ProdutoServ/EnviarContatoLojista', params, options)
            .map(res => res.json());       
    }
}