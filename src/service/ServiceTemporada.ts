import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceTemporada {
     
    private _baseUrlServer = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceUsuario Provider/ Host' + this._baseUrlServer);
    }

    addNovaTemporada(oTemporada : any){
        let params = JSON.stringify(oTemporada);
        
        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "hospedagem/AddHospedagem", params,options)
                            .map(res => res.json());
    }

    listarTemporadas(idUnidade : number){
        let params = 'unidadeID=' + idUnidade;
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'hospedagem/getHospedagens?' + params, options)
            .map(res => res.json());
    }

    removerTemporada(id : number){
        let params = 'hospID=' + id;
        
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'hospedagem/deleteHospedagem?' + params, options)
            .map(res => res.json());
    }

    editarTemporada(oTemporada : any){
        let params = JSON.stringify(oTemporada);
        
        let headers = new Headers({'Content-type' : 'application/json'});
        let options = new RequestOptions({ headers : headers, method: "post"});

        return this.http.post(this._baseUrlServer + "hospedagem/EditHospedagem", params,options)
                            .map(res => res.json());
    }
}