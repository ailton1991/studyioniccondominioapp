import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceChat {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceVeiculo');
    }

    listarConversas(usuarioID: number) {
        let params = 'uID=' + usuarioID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Usuario/getConversas?" + params, options)
            .map(res => res.json());
    }

    findConversa(conversaID: number, usuarioID) {
        let params = 'idConversa=' + conversaID + '&uID=' + usuarioID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Conversa/getConversaID?" + params, options)
            .map(res => res.json());
    }

    getConvidadoConversa(usuarioID : number, idConversa : number){

        let params = 'conversaID=' + idConversa + '&uID=' + usuarioID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Conversa/getConvidadoConversa?" + params, options)
            .map(res => res.json());
    }

    listarMensagem(conversaId : number){
       
        let params = 'conversaID=' + conversaId + '&qtd=' + 10;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Conversa/ListarMensagens?" + params, options)
            .map(res => res.json());
    }

    enviarMensagem(oMensagem : any){
        //console.log(oMensagem);
        
        let params = JSON.stringify(oMensagem);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'mensagem/enviarMensagem', params, options)
            .map(res => res.json());   
    }

    pushClientesOff(conversaId: number, userRequestId: number, strUrlImage : string, strMensagem: string){
        let params = 'id_conversa=' + conversaId + '&UserRequestID=' + userRequestId + '&urlImagePath=' + strUrlImage + '&cnvMensagem=' + strMensagem;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "conversa/getClientesOffPush?" + params, options)
            .map(res => res.json());          
    }

    listarMaisMensagens(conversaId : number, qtdSkip : number){
        let params = 'conversaID=' + conversaId + '&qtd=' + qtdSkip;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Conversa/ListarMensagensMaisApp?" + params, options)
            .map(res => res.json());
    }

    removerConversa(idConversa : number){
        let params = 'conversaId=' + idConversa;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "conversa/excluir?" + params, options)
            .map(res => res.json());
    }

    bloquearConversa(idConversa : number, idUsuario : number){
        let params = 'conversaId=' + idConversa + '&usuarioBlock=' + idUsuario;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "conversa/getBloqueiaConversa?" + params, options)
            .map(res => res.json());
    }

    iniciarConversa(userLogadoID : number, convidadoId : number){
        let params = 'uID=' + userLogadoID + '&uID2=' + convidadoId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "usuario/iniciarConversa?" + params, options)
            .map(res => res.json());
    }

    listarClientesDoCondominio(idCondominio : number){
        let params = 'condominioID=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "cliente/getClientesdoCondominioApp?" + params, options)
            .map(res => res.json());
    }

    listarClientesDaUnidade(unidadeID : number, userId : number){
        let params = 'aptID=' + unidadeID + '&clienteId=' + userId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/ListaClientesDoAPT?" + params, options)
            .map(res => res.json());
    }
}