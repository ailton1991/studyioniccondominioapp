import { Injectable } from '@angular/core';
import { SignalR } from "ng2-signalr";
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()

export class ServiceSignalr {
    
        public connection : any;

        constructor(public signalr : SignalR) {
            //console.log("Construir Signalr");
            if (this.connection == null)
                this.connection = this.signalr.createConnection();
        }

        iniciarConexao(){
            if (this.connection != null){
                this.connection.start();
            }
        }

        pararConexao(){
            if (this.connection != null){
                this.connection.stop();
            }
        }
    }