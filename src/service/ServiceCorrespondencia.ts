import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceCorrespondencia {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceCorrespondencia Provider');
    }

    ListarHistorico(apartamentoID: number) {
        let params = 'aptID=' + apartamentoID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "correspondencia/getHistorico?" + params, options)
            .map(res => res.json());
    }

    setVisto(apartamentoID : number){
        let params = 'aptID=' + apartamentoID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "correspondencia/setVisto?" + params, options)
            .map(res => res.json());
    }
}