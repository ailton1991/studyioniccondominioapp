import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables';

import 'rxjs/add/operator/map';

@Injectable()
export class ServiceUnidade {
     
    private _baseUrlServer = global.BASE_API_URL;

    constructor(public http: Http) {
        
    }
    
    listarMinhaUnidades(clienteId : number){
        let params = 'clienteID=' + clienteId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Cliente/getApartamentosDoCliente?" + params, options)
            .map(res => res.json());
    }

    getUnidadeID(unidadeId : number){
        let params = 'idApartamento=' + unidadeId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Apartamento/getApartamento?" + params, options)
            .map(res => res.json());
    }

    updateNotificacoes(unidadeID : number, idCliente){
        let params = 'aptId=' + unidadeID + '&clienteId=' + idCliente;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Notificacao/updateNotificacoes?" + params, options)
            .map(res => res.json());
    }

    checkNotification(notificationId : number, controle : boolean){
        let params = 'uID=' + notificationId + '&controleAtivo=' + controle;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Notificacao/setConfigNotificacao?" + params, options)
            .map(res => res.json());
    }

    validarCodigoUnidade(strCodigo : string){
        let params = 'cod_apt=' + strCodigo;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/validaCodApartamento?" + params, options)
            .map(res => res.json());
    }
}