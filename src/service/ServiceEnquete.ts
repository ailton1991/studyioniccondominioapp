import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceEnquete {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceMural Provider');
    }

    listarEnquete(idCondominio: number) {
        let params = 'condominioID=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "Enquete/getEnquete?" + params, options)
            .map(res => res.json());
    }

    listarEnquetesNaoRespondidas(condominioId : number, usuarioID : number){
        let params = 'condominioID=' + condominioId + "&uID=" + usuarioID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "enquete/getEnqueteAtiva?" + params, options)
            .map(res => res.json());
    }

    votarEnquete(idAlternativa : number, idUsuario : number, aptId: number){
        let params = 'alteID=' + idAlternativa + "&clienteID=" + idUsuario + "&unidadeId=" + aptId;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "enquete/postVotarEnquete?" + params, options)
            .map(res => res.json());
    }
}