import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { global } from '../app/globalVariables'
import 'rxjs/add/operator/map';

/*
  Generated class for the ServiceRecado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceVaga {

    private _baseUrlServer: string = global.BASE_API_URL;

    constructor(public http: Http) {
        //console.log('Hello ServiceVaga');
    }

    ListarVagasDisponibilizadas(idCondominio: number) {
        let params = 'condominio_ID=' + idCondominio;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "condominio/?" + params, options)
            .map(res => res.json());
    }

    ListarMinhasVagas(apartamentoID: number) {
        let params = 'Id_apt=' + apartamentoID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(res => res.json());
    }

    enviarSolicitacao(aptIdFonte: number, aptIdDest : number, idUsuarioSolicitante : number){
        let params = "aptFonte=" + aptIdFonte + "&aptDest=" + aptIdDest + "&solicitanteID=" + idUsuarioSolicitante;
        let headers = new Headers({ 'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(res => res.json());
    }

    disponibilizarVaga(unidadeID : number){
        let params = 'Id_apartamento=' + unidadeID;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/?" + params, options)
            .map(res => res.json());
    }

    retomarVaga(idDaVaga : number){
        let params = 'vagaID=' + idDaVaga;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "apartamento/retomarVagaDisponibilizada?" + params, options)
            .map(res => res.json());
    }
}