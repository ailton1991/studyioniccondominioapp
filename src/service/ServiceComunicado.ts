﻿import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

import { global } from '../app/globalVariables'
/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceComunicado {
    private _baseUrlServer = global.BASE_API_URL;
    constructor(public http: Http) {
        //console.log('Hello ServiceComunicado Provider');
    }

    ListarUltimos(aptId: number, idCondominio: number)
    {
        let params = 'aptID=' + aptId + '&idCondominio=' + idCondominio;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        
        return this.http.get(this._baseUrlServer + "apartamento/getComnunicadoDoApartamento?"+params,options)
            .map(res => res.json());
    }

    getSelectComunicado(idDoComunicado: number) {
        let params = 'comunicadoID=' + idDoComunicado;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + "comunicado/GetComunicado?" + params, options)
            .map(res => res.json());
    }

    getListComunicadoDocs(aptId: number) {
        let params = 'aptId=' + aptId;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'apartamento/getComunicadoDoApartamentoDocs?' + params, options)
            .map(res => res.json());
    }

    getListCatComunicado(condominioID: number) {
        let params = 'idCondominio=' + condominioID;
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'Comunicado/getComunicadosPublicosFolders?' + params, options)
            .map(res => res.json());
    }
}