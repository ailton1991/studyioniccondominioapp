import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/map';

import { global } from '../app/globalVariables'
/*
  Generated class for the ServiceComunicado provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ServiceReserva {
    private _baseUrlServer = global.BASE_API_URL;
    constructor(public http: Http) {
        //console.log('Hello ServiceComunicado Provider');
    }


    addNovaReserva(oReserva: any){
        // console.log(oReserva);

        let params = JSON.stringify(oReserva);
        
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({ headers: headers, method: "post"});
        
        return this.http.post(this._baseUrlServer + 'Reserva/addReserva', params, options)
            .map(res => res.json());    
    }


    getListMinhasReservas(unidadeId: number) {
        let params = 'aptID=' + unidadeId;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'Reserva/getMinhasReservas?' + params, options)
            .map(res => res.json());
    }

    rejeitarReserva(idDaReserva: number) {
        let params = 'reservaID=' + idDaReserva;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'Reserva/excluirReserva?' + params, options)
            .map(res => res.json());
    }


    getListReservasDaArea(areaID: number) {
        let params = 'IdareaComum=' + areaID;

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.get(this._baseUrlServer + 'reserva/?' + params, options)
            .map(res => res.json());
    }
}