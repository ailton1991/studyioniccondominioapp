﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NgCalendarModule  } from 'ionic2-calendar';
import { TruncatePipe }   from './app.Truncatepipe';

import { SignalRModule } from 'ng2-signalr';
import { SignalRConfiguration , ConnectionTransports } from 'ng2-signalr';

import { ChartsModule } from 'ng2-charts';

import { HttpModule  } from '@angular/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera } from '@ionic-native/camera';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Network } from '@ionic-native/network';
import { Device } from '@ionic-native/device';
import { OneSignal } from '@ionic-native/onesignal';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { MyApp } from './app.component';
import { Suporte } from './app.suporte';
import { Filetransferencia } from './app.filetransferencia';
import { global } from "./globalVariables";

import { ServiceUsuario } from '../service/ServiceUsuario';
import { ServiceHome } from '../service/ServiceHome';
import { ServiceComunicado } from '../service/ServiceComunicado';
import { ServiceOcorrencia} from '../service/ServiceOcorrencia';
import { ServiceAgenda } from '../service/ServiceAgenda';
import { ServiceReserva } from '../service/ServiceReserva';
import { ServiceCorrespondencia } from '../service/ServiceCorrespondencia';
import { ServiceVisita } from '../service/ServiceVisita';
import { ServiceTemporada } from '../service/ServiceTemporada';
import { ServiceRecado } from '../service/ServiceRecado';
import { ServiceMural } from "../service/ServiceMural";
import { ServiceClassificado } from "../service/ServiceClassificado";
import { ServiceEnquete } from "../service/ServiceEnquete";
import { ServiceVaga } from "../service/ServiceVaga";
import { ServiceVeiculo } from "../service/ServiceVeiculo";
import { ServiceTransferencia } from "../service/ServiceTransferencia";
import { ServiceProdServ } from "../service/ServiceProdServ";
import { ServiceChat } from "../service/ServiceChat";
import { ServiceSignalr } from "../service/ServiceSignalr";
import { ServiceUnidade } from "../service/ServiceUnidade";
import { ServiceBoleto } from "../service/ServiceBoleto";
import { ServiceCarteiraDigital } from "../service/ServiceCarteiraDigital";

import { Home } from '../pages/home/home';
import { Login } from '../pages/login/login';
import { comunicadoOcorrencia } from '../pages/comunicadoOcorrencia/comunicadoOcorrencia';
import { tabComunicados } from '../pages/comunicadoOcorrencia/tabComunicados';
import { tabOcorrencias } from '../pages/comunicadoOcorrencia/tabOcorrencias';
import { tabArquivosComunicados } from '../pages/comunicadoOcorrencia/tabArquivosComunicados';
import { tabArquivosCategoria } from '../pages/comunicadoOcorrencia/tabArquivosCategoria';
import { modalComunicadoContent } from '../pages/comunicadoOcorrencia/modalComunicadoContent';
import { modalOcorrenciaContent } from '../pages/comunicadoOcorrencia/modalOcorrenciaContent';
import { modalNovaOcorrencia } from '../pages/comunicadoOcorrencia/modalNovaOcorrencia';
import { agendaReserva } from '../pages/agendaReserva/agendaReserva';
import { modalNovaReserva } from '../pages/agendaReserva/modalNovaReserva';
import { menuSuspenso } from '../pages/agendaReserva/menuSuspenso';
import { atividade } from '../pages/agendaReserva/atividade';
import { minhasReservas } from '../pages/agendaReserva/minhasReservas';
import { calendarioReservas } from '../pages/agendaReserva/calendarioReservas';
import { correspondencia } from '../pages/correspondencia/correspondencia';
import { portaria } from '../pages/portaria/portaria';
import { tabVisitas } from '../pages/portaria/tabVisitas';
import { tabTemporada } from '../pages/portaria/tabTemporada';
import { modalNovaTemporada } from "../pages/portaria/modalNovaTemporada";
import { modalEditTemporada } from "../pages/portaria/modalEditTemporada";
import { tabRecados } from '../pages/portaria/tabRecados';
import { meusVisitantes } from "../pages/portaria/meusVisitantes";
import { listaDeConvidados } from "../pages/portaria/listaDeConvidados";
import { modalNovoVisitante } from "../pages/portaria/modalNovoVisitante";
import { modalEditVisitante } from "../pages/portaria/modalEditVisitante";
import { modalAutorizacaoEntrada } from "../pages/portaria/modalAutorizacaoEntrada";
import { modalAutorizarEntrada } from "../pages/portaria/modalAutorizarEntrada";
import { modalAutorizacaoMultEntrada } from "../pages/portaria/modalAutorizacaoMultEntrada";
import { mural } from '../pages/mural/mural';
import { modalNovoPost } from '../pages/mural/modalNovoPost';
import { classificado } from "../pages/classificado/classificado";
import { tabClassificado } from "../pages/classificado/tabClassificado";
import { tabMeusAnuncios } from "../pages/classificado/tabMeusAnuncios";
import { modalNovoClassificado } from "../pages/classificado/modalNovoClassificado";
import { modalEditClassificado } from "../pages/classificado/modalEditClassificado";
import { enquete } from "../pages/enquete/enquete";
import { enqueteDetails } from "../pages/enquete/enqueteDetails";
import { enqueteVotar } from "../pages/enquete/enqueteVotar";
import { vaga } from "../pages/vaga/vaga";
import { tabClassificadoVagas } from "../pages/vaga/tabClassificadoVagas";
import { tabVagas } from "../pages/vaga/tabVagas";
import { tabVeiculos } from "../pages/vaga/tabVeiculos";
import { modalAddVeiculo } from "../pages/vaga/modalAddVeiculo";
import { modalEditVeiculo } from "../pages/vaga/modalEditVeiculo";
import { modalChatVaga } from "../pages/vaga/modalChatVaga";
import { transferencia } from "../pages/transferencia/transferencia";
import { tabSolicitacoes } from "../pages/transferencia/tabSolicitacoes";
import { tabTransferidas } from "../pages/transferencia/tabTransferidas";
import { tabRequisicoes } from "../pages/transferencia/tabRequisicoes";
import { recebidas } from "../pages/transferencia/recebidas";
import { produtoServicos } from "../pages/produtoServicos/produtoServicos";
import { tabHome } from "../pages/produtoServicos/tabHome";
import { tabCategorias } from "../pages/produtoServicos/tabCategorias";
import { anunciantes } from "../pages/produtoServicos/anunciantes";
import { loja } from "../pages/produtoServicos/loja";
import { anuncioDetalhes } from "../pages/produtoServicos/anuncioDetalhes";
import { EnviarContato } from "../pages/produtoServicos/EnviarContato";
import { conversas } from "../pages/chat/conversas";
import { mensagens } from "../pages/chat/mensagens";
import { contatos } from "../pages/chat/contatos";
import { perfil } from "../pages/perfil/perfil";
import { novoUsuario } from "../pages/perfil/novoUsuario";
import { unidade } from "../pages/unidade/unidade";
import { carteiraVirtual } from "../pages/carteiraVirtual/carteiraVirtual";
import { tabAcesso } from "../pages/carteiraVirtual/tabAcesso";
import { administradora } from "../pages/administradora/administradora";
import { boleto } from "../pages/administradora/boleto";
import { tabAreasDeAcesso } from "../pages/carteiraVirtual/tabAreasDeAcesso";
import { qr_carteiraVirtual } from "../pages/carteiraVirtual/qr_carteiraVirtual";
import { listCarteiraVirtual } from "../pages/carteiraVirtual/listCarteiraVirtual";
import { modalAddConvidado } from "../pages/carteiraVirtual/modalAddConvidado";
import { modalEditConvidado } from "../pages/carteiraVirtual/modalEditConvidado";

export function createConfig(): SignalRConfiguration {
  const c = new SignalRConfiguration();
  //c.qs = 'CondominioApp',
  c.hubName = 'chatHub';
  c.url = global.BASE_SIGNALR;
  c.logging = true;
  c.jsonp = true;
  c.withCredentials = false;
  c.transport = [ConnectionTransports.webSockets];
  return c;
}

@NgModule({
  declarations: [
      MyApp,
      Home,
      Login,
      comunicadoOcorrencia,
      modalComunicadoContent,
      modalOcorrenciaContent,
      modalNovaOcorrencia,
      tabComunicados,
      tabOcorrencias,
      tabArquivosComunicados,
      tabArquivosCategoria,
      agendaReserva,
      modalNovaReserva,
      menuSuspenso,
      atividade,
      minhasReservas,
      calendarioReservas,
      correspondencia,
      portaria,
      tabVisitas,
      tabTemporada,
      tabRecados,
      modalNovaTemporada,
      modalEditTemporada,
      meusVisitantes,
      listaDeConvidados,
      modalNovoVisitante,
      modalEditVisitante,
      modalAutorizacaoEntrada,
      modalAutorizarEntrada,
      modalAutorizacaoMultEntrada,
      mural,
      modalNovoPost,
      classificado,
      tabClassificado,
      tabMeusAnuncios,
      modalNovoClassificado,
      modalEditClassificado,
      enquete,
      enqueteDetails,
      enqueteVotar,
      vaga,
      tabClassificadoVagas,
      tabVagas,
      tabVeiculos,
      modalAddVeiculo,
      modalEditVeiculo,
      modalChatVaga,
      transferencia,
      tabSolicitacoes,
      tabTransferidas,
      tabRequisicoes,
      recebidas,
      produtoServicos,
      anuncioDetalhes,
      tabHome,
      tabCategorias,
      anunciantes,
      loja,
      conversas,
      mensagens,
      contatos,
      perfil,
      novoUsuario,
      unidade,
      TruncatePipe,
      carteiraVirtual, 
      tabAcesso, 
      administradora,
      boleto,
      tabAreasDeAcesso, 
      qr_carteiraVirtual,
      listCarteiraVirtual,
      modalAddConvidado,
      modalEditConvidado,
      EnviarContato 
  ],
  imports: [
      SignalRModule.forRoot(createConfig),
      BrowserModule,
      HttpModule,
      NgCalendarModule,
      ChartsModule,
      IonicModule.forRoot(MyApp,{
        monthNames: ['Janeiro', 'Fevereiro', 'Março','Abril','Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthShortNames: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago','Set','Out', 'Nov', 'Dez'],
        dayNames: ['domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira','sexta-feira'],
        dayShortNames: ['dom', 'seg', 'ter', 'qua','qui','sex','sab'],
        backButtonText: '',
        tabsHideOnSubPages:false
      }),
      IonicStorageModule.forRoot({
          name: '_condominioAppDB',
          driverOrder: ['indexeddb', 'sqlite', 'websql']
      })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      Home,
      Login,
      comunicadoOcorrencia,
      modalComunicadoContent,
      modalOcorrenciaContent,
      modalNovaOcorrencia,
      tabComunicados,
      tabOcorrencias,
      tabArquivosComunicados,
      tabArquivosCategoria,
      agendaReserva,
      modalNovaReserva,
      menuSuspenso,
      atividade,
      minhasReservas,
      calendarioReservas,
      correspondencia,
      portaria,
      tabVisitas,
      tabTemporada,
      tabRecados,
      modalNovaTemporada,
      modalEditTemporada,
      meusVisitantes,
      listaDeConvidados,
      modalNovoVisitante,
      modalEditVisitante,
      modalAutorizacaoEntrada,
      modalAutorizarEntrada,
      modalAutorizacaoMultEntrada,
      mural,
      modalNovoPost,
      classificado,
      tabClassificado,
      tabMeusAnuncios,
      modalNovoClassificado,
      modalEditClassificado,
      enquete,
      enqueteDetails,
      enqueteVotar,
      vaga,
      tabClassificadoVagas,
      tabVagas,
      tabVeiculos,
      modalAddVeiculo,
      modalEditVeiculo,
      modalChatVaga,
      transferencia,
      tabSolicitacoes,
      tabTransferidas,
      tabRequisicoes,
      recebidas,
      produtoServicos,
      anuncioDetalhes,
      tabHome,
      tabCategorias,
      anunciantes,
      loja,
      conversas,
      mensagens,
      contatos,
      perfil,
      novoUsuario,
      unidade,
      carteiraVirtual, 
      tabAcesso, 
      administradora,
      boleto,
      tabAreasDeAcesso, 
      qr_carteiraVirtual,
      listCarteiraVirtual,
      modalAddConvidado,
      modalEditConvidado,
      EnviarContato 
  ],
  providers: [
      StatusBar,
      SplashScreen,
      InAppBrowser,
      Camera,
      Device,
      FileTransfer,
      File,
      Network,
      OneSignal,
      ServiceUsuario,
      BarcodeScanner,
      ServiceHome, 
      ServiceComunicado,
      Suporte, 
      Filetransferencia,
      ServiceOcorrencia, 
      ServiceAgenda,
      ServiceReserva,
      ServiceCorrespondencia,
      ServiceVisita,
      ServiceTemporada,
      ServiceRecado,
      ServiceMural,
      ServiceClassificado,
      ServiceEnquete,
      ServiceVaga,
      ServiceVeiculo,
      ServiceTransferencia,
      ServiceProdServ,
      ServiceChat,
      ServiceSignalr,
      ServiceUnidade,
      ServiceBoleto,
      ServiceCarteiraDigital,
      { provide: ErrorHandler, useClass: IonicErrorHandler},
      { provide: LOCALE_ID, useValue: "pt-BR" }]
})


export class AppModule {}