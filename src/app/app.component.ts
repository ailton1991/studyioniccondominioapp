﻿import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform, AlertController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import { OneSignal } from '@ionic-native/onesignal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignalR } from 'ng2-signalr';

import { ServiceCarteiraDigital } from "../service/ServiceCarteiraDigital";
import { ServiceUsuario } from '../service/ServiceUsuario';
import { ServiceOcorrencia } from "../service/ServiceOcorrencia";
import { ServiceHome } from "../service/ServiceHome";
import { Suporte } from './app.suporte';

import { Home } from '../pages/home/home';
import { Login } from '../pages/login/login';
import { comunicadoOcorrencia } from '../pages/comunicadoOcorrencia/comunicadoOcorrencia';
import { agendaReserva } from '../pages/agendaReserva/agendaReserva';
import { correspondencia } from '../pages/correspondencia/correspondencia';
import { portaria } from '../pages/portaria/portaria';
import { mural } from '../pages/mural/mural';
import { classificado } from '../pages/classificado/classificado';
import { enquete } from "../pages/enquete/enquete";
import { vaga } from "../pages/vaga/vaga";
import { produtoServicos } from "../pages/produtoServicos/produtoServicos";
import { conversas } from "../pages/chat/conversas";
import { perfil } from "../pages/perfil/perfil";
import { unidade } from "../pages/unidade/unidade";
import { tabVeiculos } from "../pages/vaga/tabVeiculos";
import { atividade } from "../pages/agendaReserva/atividade";
import { carteiraVirtual } from "../pages/carteiraVirtual/carteiraVirtual";

import { global } from "./globalVariables";
import { minhasReservas } from '../pages/agendaReserva/minhasReservas';

@Component({
    templateUrl: 'app.html'
})

export class MyApp  implements OnInit  {
    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    nomeUsuario: string = 'Bem Vindo(a)';
    strFotoUsuario : string = "";
    bloco_qd : string = "Nf";
    andar_lt: string = "Nf";
    unidade_num:string = "Nf";
    nomeCondominio : string;
    objCondominio : any;
    urlImageServ : string;
    codUnidade : string;
    pushRedierctPage : any;
    isModal : boolean = false;

    iterator : number = 0;

    pages: Array<{ title: string, component: any ,imageUrl: string}>;

    constructor(public platform: Platform, 
                public alertCtrl : AlertController,
                public storage: Storage, 
                public statusBar: StatusBar, 
                public splashScreen: SplashScreen,
                public modal : ModalController,
                public device : Device,
                public suporte: Suporte,
                public _signalR: SignalR,
                public oneSignal: OneSignal,
                public servUsuario : ServiceUsuario,
                public servOcorrencia : ServiceOcorrencia,
                public _servHome : ServiceHome,
                public servCkecIn : ServiceCarteiraDigital ) 
    {

        this.urlImageServ = global.BASE_API_UPLOAD;
    }

    ngOnInit(){
        this.initializeApp();
    }
    

    postCheckinsOffLine(dataCheckins : any){
                let qtdLoop = dataCheckins.length;
                //alert("qtdLoop: " + qtdLoop);
                let element = dataCheckins[this.iterator]
                let check : any;
                this.servCkecIn.getEfetuaCheckIn(element.userID,element.areaID,element.carteiraID,element.dtEntrada)
                .subscribe(result => check = result,
                          error => console.log("Error de CheckIn"),
                        () => {
                            //alert(this.iterator);
                            if (this.iterator < qtdLoop){
                                this.iterator++;
                                //alert(this.iterator);
                                this.postCheckinsOffLine(dataCheckins);
                            }
                            this.storage.set("checkInOff",[]);
                        })   
    }

    initializeApp() {
        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            
            this.oneSignal.startInit('9d9f84c1-f074-4976-84e6-88562d5343b7', '33754247142');
            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    
            this.oneSignal.handleNotificationReceived().subscribe(() => {
            // do something when notification is received
            });
    
            this.oneSignal.handleNotificationOpened().subscribe((c) => {
                // do something when a notification is opened
                //alert("handleNotificationOpened: " + JSON.stringify(c));
                //console.log(c);
                if (c.notification.payload.title == 'Correspondência'){
                    this.pushRedierctPage = correspondencia;
                }

                if (c.notification.payload.title == 'Novo comunicado.'){
                    this.pushRedierctPage = comunicadoOcorrencia;
                }

                if (c.notification.payload.title == 'Reserva'){
                   this.pushRedierctPage = minhasReservas;
                   this.isModal = true;
                }
            });
            
            this.oneSignal.endInit();
           
            //let ret : any;           
                     
            let UsuarioLogado : any;

            this.storage.get('UsuarioLogado').then((data) => {
                if (data != null) {
                    this.servUsuario.autenticarUsuario(data.email,data.senha)
                    .subscribe(
                        user => UsuarioLogado = user,
                        error => {
                            this.storage.get("cateirasDigitais").then((cateiras) =>{
                                if (cateiras != null){
                                    this.storage.set("connectionStatus","Off");
        
                                    this.storage.get("checkInOff").then((dataOff) =>{
                                        if (dataOff == null){
                                            this.storage.set("checkInOff",[]);
                                        }
                                    })

                                    this.statusBar.styleDefault();
                                    this.splashScreen.hide();
                                    this.confirmCarregarCarteiraOff("Função Off-line","Ops! percebemos que você está sem internet neste momento, deseja carregar a carteirinha virtual em modo off-line?");     
                                }
                                else
                                {
                                    this.suporte.showAviso("Ops! algo saiu errado, verifique sua conexão de internet!");
                                }
                            })
                        },
                        () => {
                            if (UsuarioLogado.status == 0){

                                this.storage.set("connectionStatus","On");

                                this.storage.get("checkInOff").then((dataCheckinsArr) =>{
                                    if (dataCheckinsArr != null && dataCheckinsArr.length > 0){
                                        this.postCheckinsOffLine(dataCheckinsArr);

                                        this.iterator = 0;
                                    }
                                })

                                data.nome = UsuarioLogado.nome;
                                data.foto = UsuarioLogado.foto;

                                this.storage.set('UsuarioLogado', data);
                                // let arrItens = UsuarioLogado.strApartamento.split("|");
                                let arrItens = data.strApartamento.split("|");
                                
                                this.nomeUsuario = UsuarioLogado.nome;
                                this.strFotoUsuario = UsuarioLogado.foto;
                                this.bloco_qd = arrItens[2];
                                this.unidade_num = arrItens[1];
                                this.andar_lt = arrItens[3];
                                this.nomeCondominio = data.strNomeCondominio;
                                this.codUnidade = data.strCodUnidade;

                                this.filtraMenu(data.condominioId);

                                this.storage.get('dispositivo').then((d) =>{
                                    if (d == null){
                                        this.registraDevice(UsuarioLogado.userID);
                                    }
                                })

                                this.rootPage = Home;

                                if (this.pushRedierctPage != null){
                                    setTimeout(() => {
                                        if (this.isModal)
                                        {
                                            let m = this.modal.create(this.pushRedierctPage);
                                            m.present();
                                        }
                                        else
                                        {
                                            this.nav.push(this.pushRedierctPage);   
                                        }
                                    }, 3500);
                                }
                            }

                            if (UsuarioLogado.status == 1){
                                this.suporte.showAviso(UsuarioLogado.mensagem);
                                this.rootPage = Login;
                            }

                            this.statusBar.styleDefault();
                            this.splashScreen.hide();
                        })        
                }
                else
                {
                    this.rootPage = Login;

                    this.statusBar.styleDefault();
                    this.splashScreen.hide();
                }
            }); 
        });
    }

    confirmCarregarCarteiraOff(titulo : string, msg : string) {
        let confirm = this.alertCtrl.create({
        title: titulo,
        message: msg,
        buttons: [
                {
                    text: 'Não',
                    handler: () => {
                    //console.log("Negativo");
                    }
                },
                {
                    text: 'Sim',
                    handler: () => {
                        this.nav.push(carteiraVirtual);
                    }
                }
            ]
        });
        confirm.present();
    }


    filtraMenu(condominioId : number){

        this._servHome.getConfiguracaoCondominio(condominioId)
                .subscribe(
                    result => this.objCondominio = result,
                    error => console.log("Error ao obter condominio"),
                    () => {

                        this.pages = [
                            { title: "Comunicados", component: comunicadoOcorrencia, imageUrl: "assets/img/icones/branco/iconComunicadoOcorrenia.png" },
                            { title: "Reserva", component: agendaReserva, imageUrl: "assets/img/icones/branco/agendaBranco.png" },  
                            { title: "Veículos", component : tabVeiculos, imageUrl: "assets/img/icones/branco/vagasBranco.png"},
                            { title: "Mural", component: mural, imageUrl: "assets/img/icones/branco/muralBranco.png" },
                            { title: "Correio", component : correspondencia, imageUrl: "assets/img/icones/branco/iconCorreio.png"},
                            { title: "Enquete", component : enquete, imageUrl: "assets/img/icones/branco/enquetesBranco.png"},
                            { title: "Produtos", component : produtoServicos, imageUrl: "assets/img/icones/branco/produtosBranco.png"},                            
                            { title: "Atividades", component : atividade, imageUrl: "assets/img/icones/branco/atividadesBranco.png"}
                        ];


                        if (this.objCondominio != null){
                            //console.log(this.objCondominio);
            
                            if (this.objCondominio.portaria){
                                var itemPortaria = { title: "Portaria", component: portaria, imageUrl: "assets/img/icones/branco/iconAutVisita.png" };
                                this.pages.push(itemPortaria);
                            }
            
                            if (this.objCondominio.classificado){
                                var itemClassificado = { title: "Classificados", component: classificado, imageUrl: "assets/img/icones/branco/classificadosBranco.png" };
                                this.pages.push(itemClassificado);
                            }

                            if (this.objCondominio.chat){
                                var itemChat = { title: "Chat", component : conversas, imageUrl: "assets/img/icones/branco/iconChat.png"};
                                this.pages.push(itemChat);
                            }

                            if (this.objCondominio.vaga){
                                var itemVaga = { title: "Vagas", component: vaga, imageUrl: "assets/img/icones/branco/vagasBranco.png"};
                                this.pages.push(itemVaga);
                            }
                        }
                    }
                )
    }

    selectUnidades(){
        var page = { title : "Unidades", component : unidade};
        this.openPage(page);
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        if (page.title != 'Home')
           this.nav.push(page.component);
        else
          this.nav.setRoot(page.component);
    }

    logout(){
        let retorno : any;
       
            this.storage.get("dispositivo").then((dev) => {
                if (dev != null){
                
                    this.servUsuario.destroiDevice(dev.id)
                        .subscribe(
                            r => retorno = r,
                            error => console.log("Error ao destruir device"),
                            () => {
                                this.storage.clear();
                                this.nav.setRoot(Login);
                            })
                        }
                        else
                        {
                            this.storage.clear();
                            this.nav.setRoot(Login);
                        }
            })
    }

    meuPerfil(){
        let oUser : any;
        this.storage.get("UsuarioLogado").then((dataUser) => {
            
        this.suporte.showLoader("Obtendo usuário...");

        this.servUsuario.getCliente(dataUser.userID)
            .subscribe(
                user => oUser = user,
                error => console.log("Error ao obter usuario"),
                () => {
                    this.suporte.dismissLoader()
                    this.nav.push(perfil,oUser);
                }
            )
        })
        
    }

    reportEmergencia(){
        this.confirmcriarOcorrenciaEmergencia("ATENÇÃO!","Deseja informar uma emergência?");
    }
    
    confirmcriarOcorrenciaEmergencia(titulo : string, msg : string) {
        let confirm = this.alertCtrl.create({
          title: titulo,
          message: msg,
          buttons: [
            {
              text: 'Saúde',
              handler: () => {
                this.enviarEmergencia(1);
              }
            },
            {
                text: 'Incêndio',
                handler: () => {
                    this.enviarEmergencia(2);
                }
              },
              {
                text: 'Outros',
                handler: () => {
                    this.enviarEmergencia(3);
                }
              },
            {
              text: 'Fechar',
              handler: () => {}
            }
          ]
        });
        confirm.present();
      }

      private enviarEmergencia(tpEmergencia : number) : void{
          this.storage.get("UsuarioLogado").then((dataUser) => {
            
            let objPostOcorrencia = {
                clienteId : dataUser.userID,
                condominioId : dataUser.condominioId,
                idApartamentoOcorrencia : dataUser.aptId,
                EmergenciaID: tpEmergencia
            };

           // console.log(objPostOcorrencia);

            let result : any;
            this.servOcorrencia.addOcorrenciaEmergencia(objPostOcorrencia)
                    .subscribe(
                        r => result = r,
                        error => console.log("Error ao enviar a ocorrencia de emergencia"),
                        () => {
                            if (result.status == 0){
                                this.suporte.showAviso(result.mensagem);
                                this._signalR.connect().then((c) =>{
                                    c.invoke("SendNovaOcorrencia",dataUser.condominioId).then((r) => console.log("Nova Ocorrencia enviada com sucesso"));
                                })
                            }
                        }
                    )
          })
      }


      private registraDevice(userId : number){

        this.oneSignal.getIds().then((objDeviceKey) =>{

            var dispositivo = {
              id : 0,
              modelo: this.device.model,
              mobileID: this.device.uuid,
              usuarioID: userId,
              plataforma: this.device.platform,
              versao: this.device.version,
              deviceKey: objDeviceKey.userId
              //deviceKey: 1
          };
          

          let retorno : any;
          this.servUsuario.gravaDispositivo(dispositivo)
                .subscribe(
                  r => retorno = r,
                  error => console.log('Error ao gravar dispositivo'),
                  () => {
                    if (retorno.status > 0){
                        dispositivo.id = retorno.status;
                        this.storage.set("dispositivo", dispositivo);
                    }
                    console.log(retorno.mensagem);
                  })
      })       
   }


    doRefresh(refresher) {
        // console.log('Begin async operation', refresher);
        this.storage.get('UsuarioLogado').then((data) => {
            if (data != null) {

                let arrItens = data.strApartamento.split("|");

                this.nomeUsuario = data.nome;
                this.strFotoUsuario = data.foto;
                this.bloco_qd = arrItens[2];
                this.unidade_num = arrItens[1];
                this.andar_lt = arrItens[3];
                this.nomeCondominio = data.strNomeCondominio;
                this.codUnidade = data.strCodUnidade;

                this.filtraMenu(data.condominioId);
            }
            else{
               // this.nav.setRoot(Login);
               this.rootPage = Login;
            }
        });
     
         setTimeout(() => {
          // console.log('Async operation has ended');
           refresher.complete();
         }, 2000);
       }
}