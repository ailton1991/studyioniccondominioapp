﻿export const global = Object.freeze({
    
    BASE_API_URL : "http://www.condominioapp.com/mobile/api/",
    BASE_API_UPLOAD : "http://www.condominioapp.com/uploads/",
    BASE_SIGNALR : "http://www.condominioapp.com/mobile/signalr/js",
    BASE_API_BOLETO: "http://techdog-001-site16.dtempurl.com/api/",
    EMAIL_REGEX : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    PHONE_REGEX : /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/,
    ERROR_MSG : "Serviço indisponível no momento.",
    FOTO_ANEXADA : "Foto anexada com sucesso!"
});