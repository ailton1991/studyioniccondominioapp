﻿import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { LoadingController, AlertController, ToastController } from 'ionic-angular';


@Injectable()
export class Suporte {

    loader : any;

    webOptions : InAppBrowserOptions = {
        location : 'no',//Or 'no' 
        hidden : 'no', //Or  'yes'
        clearcache : 'yes',
        clearsessioncache : 'yes',
        zoom : 'yes',//Android only ,shows browser zoom controls 
        hardwareback : 'yes',
        mediaPlaybackRequiresUserAction : 'no',
        shouldPauseOnSuspend : 'no', //Android only 
        closebuttoncaption : 'Fechar', //iOS only
        disallowoverscroll : 'no', //iOS only 
        toolbar : 'yes', //iOS only 
        enableViewportScale : 'no', //iOS only 
        allowInlineMediaPlayback : 'no',//iOS only 
        presentationstyle : 'pagesheet',//iOS only 
        fullscreen : 'yes',//Windows only    
    };

    constructor(
        public alert: AlertController,
        public loading: LoadingController,
        public inAppBrowser: InAppBrowser,
        public toastCtrl: ToastController
    )
    { }    

    showAviso(msg : string){
        let toast = this.toastCtrl.create({
                        message: msg,
                        duration: 3000
                    });
    
        toast.present();        
    }

    showAvisoTop(msg : string){
        let toast = this.toastCtrl.create({
                        message: msg,
                        duration: 3000,
                        position: 'top'
                    });
    
        toast.present();        
    }

    showAlert(tituloStr: string, msgStr: string) {
        let alert = this.alert.create({
            title: tituloStr,
            subTitle: msgStr,
            buttons: ['OK']
        });
        alert.present();
    }

    showLoading(msg: string, tempo: number) {
        this.loader = this.loading.create({
            content: msg,
            duration: tempo
            //dismissOnPageChange :true
        });
        this.loader.present();
    }

    showLoader(msg: string) {
        this.loader = this.loading.create({
            content: msg  
        });
        this.loader.present();
    }

    dismissLoader(){
        this.loader.dismiss();
    }

    openFileDocs(url) {
         
        if (url.indexOf(".jpg") != -1 ||
            url.indexOf(".jpeg") != -1 ||
            url.indexOf(".png") != -1 ||
            url.indexOf(".gif") != -1) 
            {
                this.openUrlSite(url);
                return;
            }
            
            url = 'http://docs.google.com/viewer?url=' + url;

           this.openUrlSite(url);    
    }

    // private openFileImage(url) {
    //     //window.open(url, "_blank", "location=yes,toolbar=yes,hardwareback=yes");

    //     let target = "_blank";
    //     this.inAppBrowser.create(url,target,);
    //     //appBrowser.show();
    // }

    public openUrlSite(url){
        let target = "_blank";
        this.inAppBrowser.create(url,target,this.webOptions);
    }

    formatDate = function(dt){   
        let date = new Date(dt);
    
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
    
        //return day + '/' + monthIndex + '/' + year;
        return year + '-' + monthIndex + '-' + day;
    }

    formatDateTime = function(){   
        let date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var day = date.getDate();
        var monthIndex = date.getMonth() + 1;
        var year = date.getFullYear();
    
        //return day + '/' + monthIndex + '/' + year;
        return year + '-' + monthIndex + '-' + day + " " + hours + ":" + minutes;
    }
}


export class EmailValidator {

    static isValidMailFormat(control: FormControl) {
        let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

        if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
            return { "Por favor digite um e-mail válido!": true };
        }

        return null;
    }
}