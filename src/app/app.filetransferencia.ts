import { Injectable } from '@angular/core';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

import { global } from './globalVariables';

@Injectable()
export class Filetransferencia{

    private _baseUrlServer = global.BASE_API_URL;

    private fileTransfer: FileTransferObject;
    private options: FileUploadOptions;

    constructor(private transfer: FileTransfer){
        this.fileTransfer = this.transfer.create();

        this.options = {
            fileKey : "file",
            chunkedMode : true            
        };
    }

    upload(strNomeArquivo : string, localPath: string, metodoUp: string) : void{
        
        this.options.fileName = strNomeArquivo;

        this.fileTransfer.upload(localPath, encodeURI(this._baseUrlServer + 'upload/' + metodoUp) , this.options).then((data) => {
             // success
             console.log('Upload com Sucesso');
        }, (err) => {
            console.log(err);
        });
    }
}